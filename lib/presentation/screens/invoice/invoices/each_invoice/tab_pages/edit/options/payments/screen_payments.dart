import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/payments_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/payments/screen_add_payment.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/datetime_to_date_string_converter.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/payments.dart';
import 'package:linq_pe/utilities/colors.dart';

class PaymentsScreen extends StatelessWidget {
  const PaymentsScreen({super.key, required this.totalAmount});
  final double totalAmount;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.18,
        title: Text('Payments',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      body: SafeArea(child: SingleChildScrollView(
        child: BlocBuilder<InvoicesBloc, InvoicesState>(
          builder: (context, invoiceState) {
            InvoiceEditDTO? invoice;
            List<PaymentsDTO> paymentsList = [];
            double totalPaymentAmount = 0.00;
            if (invoiceState is displayInvoice) {
              invoice = invoiceState.invoice;
            }
            if (invoice == null) {
              return const SizedBox();
            }
            if (invoice.paymentsList != null) {
              paymentsList = invoice.paymentsList!;
            }
            if (invoice.paymentsList != null &&
                invoice.paymentsList!.isNotEmpty) {
              for (var payment in invoice.paymentsList!) {
                totalPaymentAmount = totalPaymentAmount + payment.amount;
              }
            }
            double balanceDue = totalAmount - totalPaymentAmount;
               balanceDue= double.parse(balanceDue.toStringAsFixed(2));
            log(invoice.paymentsList.toString());
            return Column(
              children: [
                SizedBox(
                  height: size.height * 0.02,
                ),
                EditContainerWidget(
                  padding: 0,
                  child: Column(
                    children: [
                      PaymentContainer(
                        totalAmount: totalAmount,
                        headerText: 'Total',
                        paymentsList: paymentsList,
                        size: size,
                      ),
                      // SizedBox(
                      //   height: size.height * 0.0,
                      // ),
                      BalanceDueContainer(
                        balanceDue: balanceDue,
                        totalPaymentAmount: totalPaymentAmount,
                        headerText: 'Paid',
                        paymentText: 'Balance Due',
                        size: size,
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      )),
    );
  }
}

class PaymentContainer extends ConsumerWidget {
  const PaymentContainer(
      {super.key,
      required this.size,
      required this.headerText,
      required this.paymentsList,
      required this.totalAmount});

  final Size size;
  final String headerText;

  final List<PaymentsDTO> paymentsList;
  final double totalAmount;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Container(
          width: size.width,
          decoration: BoxDecoration(
              color: LinqPeColors.kBlackColor.withOpacity(0.5),
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(5), topRight: Radius.circular(5))),
          padding: EdgeInsets.symmetric(
              vertical: size.height * 0.01, horizontal: size.width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(headerText,
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kWhiteColor,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
              Text('₹$totalAmount',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kWhiteColor,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
            ],
          ),
        ),
        SizedBox(
          height: size.height * 0.01,
        ),
        Column(
          children: List.generate(
              paymentsList.length,
              (index) => Column(
                    children: [
                      InkWell(
                        onTap: () {
                          addpaymentMethod(
                              paymentsList[index].paymentMethod, ref);
                          addPaymentDTO(paymentsList[index], ref);
                          addpaymentDate(paymentsList[index].date, ref);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AddPaymentScreen(
                                  isToEdit: true,
                                ),
                              ));
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: size.height * 0.01,
                              horizontal: size.width * 0.05),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  dateTimeToStringConverter(
                                      paymentsList[index].date),
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                      letterSpacing: .5,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                              Text("₹${paymentsList[index].amount.toString()}",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                      letterSpacing: .5,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.01,
                      ),
                    ],
                  )),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: size.height * 0.01, horizontal: size.width * 0.05),
          child: Material(
            child: InkWell(
              onTap: () {
                addPaymentDTO(null, ref);
                addpaymentDate(DateTime.now(),ref);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddPaymentScreen(
                        isToEdit: false,
                      ),
                    ));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Add Payment',
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.04,
                          color: LinqPeColors.kBlackColor.withOpacity(0.5),
                          fontWeight: FontWeight.w400,
                        ),
                      )),
                  Text('₹0.00',
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.04,
                          color: LinqPeColors.kBlackColor.withOpacity(0.5),
                          fontWeight: FontWeight.w400,
                        ),
                      )),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class BalanceDueContainer extends StatelessWidget {
  const BalanceDueContainer(
      {super.key,
      required this.size,
      required this.headerText,
      required this.paymentText,
      required this.balanceDue,
      required this.totalPaymentAmount});

  final Size size;
  final String headerText;

  final String paymentText;
  final double balanceDue;
  final double totalPaymentAmount;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: size.width,
          decoration: BoxDecoration(
            color: LinqPeColors.kBlackColor.withOpacity(0.5),
            // borderRadius: const BorderRadius.only(
            //     topLeft: Radius.circular(5),
            //     topRight: Radius.circular(5))
          ),
          padding: EdgeInsets.symmetric(
              vertical: size.height * 0.01, horizontal: size.width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(headerText,
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kWhiteColor,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
              Text('₹$totalPaymentAmount',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kWhiteColor,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: size.height * 0.01, horizontal: size.width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(paymentText,
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
              Text(balanceDue < 0 ? '-₹${-balanceDue}' : '₹$balanceDue',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
            ],
          ),
        ),
        SizedBox(
          height: size.height * 0.01,
        ),
      ],
    );
  }
}
