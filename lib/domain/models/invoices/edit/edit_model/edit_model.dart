import 'package:hive/hive.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/business_details/business_details.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/client/client.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/discount/discount.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/invoice_number/invoice_number.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/item/item.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/notes/notes.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/payment_info/payment_info.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/payments/payments.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/photo/photo.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/signature/signature.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/tax/tax.dart';

part 'edit_model.g.dart';

@HiveType(typeId: 21)
class InvoiceEditModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final InvoiceNumberModel invoiceNumberDetails;
  @HiveField(2)
  final BusinessDetailsModel? businessDetails;
  @HiveField(3)
  final DiscountModel? discountDetails;
  @HiveField(4)
  final List<ItemModel>? itemsList;
  @HiveField(5)
  final NotesModel? notes;
  @HiveField(6)
  final PaymentInfoModel? paymentInfo;
  @HiveField(7)
  final List<PaymentsModel>? paymentsList;
  @HiveField(8)
  final List<PhotoModel>? photoList;
  @HiveField(9)
  final SignatureModel? signature;
  @HiveField(10)
  final TaxModel? tax;
  @HiveField(11)
  final ClientModel? client;
  @HiveField(12)
  final bool? isToReview;
  @HiveField(13)
  final String? reviewLink;
  @HiveField(14)
  final bool isPaid;
  @HiveField(15)
  final String? paidMethod;
  @HiveField(16)
  final double? balanceDue;
  @HiveField(17)
  final bool isInvoice;
  @HiveField(18)
  final bool isEstimate;

  InvoiceEditModel(
      {required this.invoiceId,
      required this.businessDetails,
      required this.discountDetails,
      required this.notes,
      required this.invoiceNumberDetails,
      required this.itemsList,
      required this.paymentInfo,
      required this.paymentsList,
      required this.photoList,
      required this.signature,
      required this.tax,
      required this.balanceDue,
      required this.client,
      required this.isToReview,
      required this.reviewLink,
      required this.isPaid,
      required this.paidMethod,
       required this.isEstimate,
        required this.isInvoice,
      });
}

class InvoiceEditBox {
  static Box<InvoiceEditModel>? _getbox;
  static Box<InvoiceEditModel> getInstance() {
    return _getbox ??= Hive.box('InvoiceEditBox');
  }
}
