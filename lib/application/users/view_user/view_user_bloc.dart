import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linq_pe/application/view_dto/user/user.dart';
import 'package:linq_pe/infrastructure/users/users_implementation.dart';

part 'view_user_event.dart';
part 'view_user_state.dart';
part 'view_user_bloc.freezed.dart';

class ViewUserBloc extends Bloc<ViewUserEvent, ViewUserState> {
  ViewUserBloc() : super(const viewuserinitial()) {
    on<getUserDetail>((event, emit) async {
      emit(const viewUsers(isLoading: true, user: null, isError: false));
      final result = await UsersImplementation.instance.getUserDetails();
      result.fold(
          (l) =>
              emit(const viewUsers(isLoading: false, user: null, isError: true)),
          (r) {
        final user = converstUserModelToDTO(r);

        emit(viewUsers(isLoading: false, user: user, isError: false));
      });
    });
  }
}
