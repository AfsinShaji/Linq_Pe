import 'package:hive/hive.dart';



part 'invoice_number.g.dart';

@HiveType(typeId: 12)
class InvoiceNumberModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final String invoiceNumber;
  @HiveField(2)
  final DateTime invoiceDate;
  @HiveField(3)
  final DateTime? dueDate;
  @HiveField(4)
   final String? poNumber;
  @HiveField(5)
  final String? terms;

  InvoiceNumberModel({
    required this.dueDate,
    required this.invoiceDate,
    required this.invoiceId,
    required this.invoiceNumber,
    required this.poNumber,
   required this.terms,
  });
}

class InvoiceNumberBox {
  static Box<InvoiceNumberModel>? _getbox;
  static Box<InvoiceNumberModel> getInstance() {
    return _getbox ??= Hive.box('InvoiceNumberBox');
  }
}
