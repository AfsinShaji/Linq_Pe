part of 'view_user_bloc.dart';

@freezed
class ViewUserEvent with _$ViewUserEvent {
  const factory ViewUserEvent.getUserDetail() = getUserDetail;
}