part of 'user_permission_bloc.dart';

@freezed
class UserPermissionEvent with _$UserPermissionEvent {
  const factory UserPermissionEvent.isUserAccountPermitted() = isUserAccountPermitted;
}