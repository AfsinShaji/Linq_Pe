import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:linq_pe/application/Razorpay/razorpay_bloc.dart';
import 'package:linq_pe/application/authentication/authentication_bloc.dart';
import 'package:linq_pe/application/contacts/contacts_bloc.dart';
import 'package:linq_pe/application/invoices/all_invoices/all_invoices_bloc.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/invoices/my_items/my_items_bloc.dart';
import 'package:linq_pe/application/ledger/ledger_bloc.dart';
import 'package:linq_pe/application/party/customer/customer_bloc.dart';
import 'package:linq_pe/application/party/expense/expense_bloc.dart';
import 'package:linq_pe/application/rolling/rolling_bloc.dart';
import 'package:linq_pe/application/seondary_party/secondary_party_bloc.dart';
import 'package:linq_pe/application/split_amount/split_amount_bloc.dart';
import 'package:linq_pe/application/splitted/splitted_bloc.dart';
import 'package:linq_pe/application/transactions/transactions_bloc.dart';
import 'package:linq_pe/application/users/user_permission/user_permission_bloc.dart';
import 'package:linq_pe/application/users/users/users_bloc.dart';
import 'package:linq_pe/application/users/view_user/view_user_bloc.dart';

import 'package:linq_pe/domain/models/contacts/contacts.dart';
import 'package:linq_pe/domain/models/invoices/edit/edit_model/edit_model.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/business_details/business_details.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/client/client.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/discount/discount.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/invoice_number/invoice_number.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/item/item.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/my_items/my_items.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/notes/notes.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/payment_info/payment_info.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/payments/payments.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/photo/photo.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/signature/signature.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/tax/tax.dart';
import 'package:linq_pe/domain/models/ledger/ledger.dart';
import 'package:linq_pe/domain/models/ledger/pay_back_ledger.dart';
import 'package:linq_pe/domain/models/party/Expense_account/expense_account.dart';
import 'package:linq_pe/domain/models/party/customer/customer.dart';
import 'package:linq_pe/domain/models/rolling/pay_back_account.dart';
import 'package:linq_pe/domain/models/rolling/rolling.dart';
import 'package:linq_pe/domain/models/splitted/splitted_accounts.dart';
import 'package:linq_pe/domain/models/transactions/secondary_transactions.dart';
import 'package:linq_pe/domain/models/transactions/party_account_model.dart';
import 'package:linq_pe/domain/models/transactions/transaction_model.dart';

import 'package:linq_pe/infrastructure/contacts/contacts_implementation.dart';
import 'package:linq_pe/infrastructure/invoices/invoices_implementation.dart';
import 'package:linq_pe/infrastructure/invoices/options/my_items/my_items_implementation.dart';
import 'package:linq_pe/infrastructure/ledger/ledger_implementation.dart';
import 'package:linq_pe/infrastructure/party/party_implementation.dart';
import 'package:linq_pe/infrastructure/rolling/rolling_infrastructure.dart';
import 'package:linq_pe/infrastructure/splitting/splitting_implementation.dart';
import 'package:linq_pe/infrastructure/transactions/transactions_implementation.dart';

import 'package:linq_pe/presentation/screens/splash_scren/screen_splash.dart';
import 'package:motion/motion.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await Hive.initFlutter();

  Hive.registerAdapter(ContactsModelAdapter());
  await Hive.openBox<ContactsModel>('ContactsBox');
  await ContactsImplementation.instance.openContactsBox();
  Hive.registerAdapter(CustomerModelAdapter());
  await Hive.openBox<CustomerModel>('CustomerBox');
  await PartyImplementation.instance.openCustomerBox();
  Hive.registerAdapter(PartyAccountsModelAdapter());
  await Hive.openBox<PartyAccountsModel>('PartyAcccountsBox');
  Hive.registerAdapter(SecondaryTransactionsModelAdapter());
  Hive.registerAdapter(TransactionTypeAdapter());
  await Hive.openBox<SecondaryTransactionsModel>('SecondaryTransactionsBox');
  await TransactionsImplementation.instance.openTransactionBox();
  Hive.registerAdapter(TransactionModelAdapter());
  await Hive.openBox<TransactionModel>('TransactionModel');
  await TransactionsImplementation.instance.openPartyAccountsBox();
  Hive.registerAdapter(ExpenseAccountModelAdapter());
  await Hive.openBox<ExpenseAccountModel>('ExpenseAccountModel');
  await TransactionsImplementation.instance.openExpenseBox();
  Hive.registerAdapter(SplittedAccountsModelAdapter());
  await Hive.openBox<SplittedAccountsModel>('SplittedAccountsModel');
  await SplittingImplementation.instance.opensplittingBox();
  Hive.registerAdapter(PayBackAccountModelAdapter());
  await Hive.openBox<PayBackAccountModel>('PayBackAccountModel');
  Hive.registerAdapter(RollingAccountsModelAdapter());
  await Hive.openBox<RollingAccountsModel>('RollingAccountsModel');
  await RollingImplementation.instance.openRollingBox();

  Hive.registerAdapter(PayBackLedgerModelAdapter());
  await Hive.openBox<PayBackLedgerModel>('PayBackLedgerBox');
  Hive.registerAdapter(LedgerModelAdapter());
  await Hive.openBox<LedgerModel>('LedgerBox');
  await LedgerImplementation.instance.openLedgerBox();

  Hive.registerAdapter(BusinessDetailsModelAdapter());
  await Hive.openBox<BusinessDetailsModel>('BusinessDetailsBox');
  Hive.registerAdapter(ClientModelAdapter());
  await Hive.openBox<ClientModel>('ClientBox');
  Hive.registerAdapter(DiscountModelAdapter());
  await Hive.openBox<DiscountModel>('DiscountBox');
  Hive.registerAdapter(InvoiceNumberModelAdapter());
  await Hive.openBox<InvoiceNumberModel>('InvoiceNumberBox');
  Hive.registerAdapter(ItemModelAdapter());
  await Hive.openBox<ItemModel>('ItemsBox');
  Hive.registerAdapter(NotesModelAdapter());
  await Hive.openBox<NotesModel>('NotesBox');
  Hive.registerAdapter(PaymentInfoModelAdapter());
  await Hive.openBox<PaymentInfoModel>('PaymentInfBox');
  Hive.registerAdapter(PaymentsModelAdapter());
  await Hive.openBox<PaymentsModel>('PaymentsBox');
  Hive.registerAdapter(PhotoModelAdapter());
  await Hive.openBox<PhotoModel>('PhotoBox');
  Hive.registerAdapter(SignatureModelAdapter());
  await Hive.openBox<SignatureModel>('SignatureBox');
  Hive.registerAdapter(TaxModelAdapter());
  await Hive.openBox<TaxModel>('TaxBox');
  Hive.registerAdapter(InvoiceEditModelAdapter());
  await Hive.openBox<InvoiceEditModel>('InvoicesBox');
  await InvoicesImplementation.instance.openInvoicesBox();

  Hive.registerAdapter(MyItemsModelAdapter());
  await Hive.openBox<MyItemsModel>('MyItemsBox');
  await MyItemsImplementation.instance.openMyItemsBox();

  await Motion.instance.initialize();
  Motion.instance.setUpdateInterval(60.fps);
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ContactsBloc(),
        ),
        BlocProvider(
          create: (context) => CustomerBloc(),
        ),
        BlocProvider(
          create: (context) => TransactionsBloc(),
        ),
        BlocProvider(
          create: (context) => SecondaryPartyBloc(),
        ),
        BlocProvider(
          create: (context) => SplitAmountBloc(),
        ),
        BlocProvider(
          create: (context) => ExpenseBloc(),
        ),
        BlocProvider(
          create: (context) => SplittedBloc(),
        ),
        BlocProvider(
          create: (context) => LedgerBloc(),
        ),
        BlocProvider(
          create: (context) => RollingBloc(),
        ),
        BlocProvider(
          create: (context) => AuthenticationBloc(),
        ),
        BlocProvider(
          create: (context) => UsersBloc(),
        ),
        BlocProvider(
          create: (context) => UserPermissionBloc(),
        ),
        BlocProvider(
          create: (context) => InvoicesBloc(),
        ),
        BlocProvider(
          create: (context) => AllInvoicesBloc(),
        ),
        BlocProvider(
          create: (context) => MyItemsBloc(),
        ),
         BlocProvider(
          create: (context) => RazorpayBloc(),
        ),
         BlocProvider(
          create: (context) => ViewUserBloc(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Linq Pe',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: const SplashScreen(),
      ),
    );
  }
}
