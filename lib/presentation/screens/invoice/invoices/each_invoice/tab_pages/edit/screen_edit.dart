import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/all_invoices/all_invoices_bloc.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/item_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/additional_details/screen_additional_details.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/business_details/screen_business_details.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/clients/screen_clients.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/discount/screen_discount.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/invoice_number/screen_invoice_number.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/items/screen_items.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/payment_info/screen_payment_info.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/payments/screen_payments.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/photo/screen_photo.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/signature/screen_signature.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/tax/screen_tax.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/datetime_to_date_string_converter.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/image_picking_sheet.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/business_details.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/discount.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/invoice_number.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/item.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/signature.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/tax.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

import 'widgets/each_container_widget.dart';

class EditScreen extends StatelessWidget {
  EditScreen({
    super.key,
    required this.isEstimate,
  });
  final bool isEstimate;
  final TextEditingController reviewLinkTextController =
      TextEditingController();
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      // resizeToAvoidBottomInset: true,
      backgroundColor: LinqPeColors.kBlackColor.withOpacity(0.1),
      body: SafeArea(child:
          SingleChildScrollView(child: BlocBuilder<InvoicesBloc, InvoicesState>(
        builder: (context, invoiceState) {
          InvoiceEditDTO? invoice;
          List<Map<String, String>> itemPriceMapList = [];
          List<ItemDTO?>? itemsList = [];
          double subtotal = 0.00;
          double discountAmount = 0.00;
          double tax = 0.00;
          double totalAmount = 0.0;
          double totalPaymentAmount = 0.0;
          double balanceDue = 0.0;

          if (invoiceState is displayInvoice) {
            invoice = invoiceState.invoice;
          }
          if (invoice == null) {
            return const SizedBox();
          }
          if (invoice.reviewLink != null) {
            reviewLinkTextController.text = invoice.reviewLink!;
          }
          if (invoice.itemsList != null) {
            for (var items in invoice.itemsList!) {
              subtotal = subtotal + items.total;
              itemsList.add(items);
              itemsList.add(items);
              itemsList.add(items);
              itemPriceMapList.addAll([
                {items.description: '${items.quantity} X ₹${items.unitCost}'},
                {'': '₹${items.total}'},
              ]);

              items.discountAmount != null
                  ? itemPriceMapList.addAll([
                      {
                        'Discount': items.discount == 'Percentage'
                            ? '-₹${(items.discountAmount! * items.quantity * items.unitCost) / 100}'
                            : '-₹${(items.discountAmount)}'
                      },
                      {'': ''}
                    ])
                  : itemPriceMapList.add({'': ''});
            }
          }
          itemPriceMapList.addAll([
            {'Add Item': '0 X ₹0.00'},
            {'': '₹0.00'}
          ]);
          itemsList.add(null);
          itemsList.add(null);
          log(itemsList.toString());

          if (invoice.discountDetails != null) {
            if (invoice.discountDetails!.discountType == 'Percentage' &&
                invoice.discountDetails!.percentage > 0) {
              discountAmount =
                  subtotal * (invoice.discountDetails!.percentage / 100);
            } else if (invoice.discountDetails!.discountType == 'Flat Amount' &&
                invoice.discountDetails!.percentage > 0) {
              discountAmount = invoice.discountDetails!.percentage;
            }
          }

          if (invoice.tax != null &&
              invoice.itemsList != null &&
              invoice.itemsList!.isNotEmpty) {
            if (invoice.tax!.taxType == 'Deducted') {
              double deductingTotal = 0.0;
              for (var items in invoice.itemsList!) {
                if (items.isTaxable) {
                  deductingTotal = deductingTotal + items.total;
                }
              }
              if (deductingTotal > 0 && invoice.tax!.rate != null) {
                tax = (deductingTotal - discountAmount) *
                    (invoice.tax!.rate! / 100);
                tax = -tax;
                //decimel rounding
                tax = double.parse(tax.toStringAsFixed(2));
              }
            } else if (invoice.tax!.taxType == 'On The Total') {
              double deductingTotal = 0.0;
              for (var items in invoice.itemsList!) {
                if (items.isTaxable) {
                  log(items.total.toString());
                  deductingTotal = deductingTotal + items.total;
                }
              }

              if (deductingTotal > 0 && invoice.tax!.rate != null) {
                tax = (deductingTotal - discountAmount) *
                    (invoice.tax!.rate! / 100);
              }
              if (invoice.tax!.isInclusive) {
                tax = tax / (1 + (invoice.tax!.rate! / 100));
              }
              //decimel rounding
              tax = double.parse(tax.toStringAsFixed(2));
            } else if (invoice.tax!.taxType == 'Per Item') {
              double totalTax = 0.00;
              for (var items in invoice.itemsList!) {
                if (items.isTaxable &&
                    items.taxRate != null &&
                    items.taxRate! > 0) {
                  double eachTotal = (items.total * (items.taxRate! / 100));
                  if (invoice.tax!.isInclusive) {
                    eachTotal = eachTotal / (1 + (items.taxRate! / 100));
                  }
                  totalTax = totalTax + eachTotal;
                }
              }
              tax = totalTax;
              //decimel rounding
              tax = double.parse(tax.toStringAsFixed(2));
            }
          }
          if (invoice.paymentsList != null &&
              invoice.paymentsList!.isNotEmpty) {
            for (var payment in invoice.paymentsList!) {
              totalPaymentAmount = totalPaymentAmount + payment.amount;
            }
          }

          totalAmount = (subtotal - discountAmount);
          if (invoice.tax != null &&
              (invoice.tax!.isInclusive == false ||
                  invoice.tax!.taxType == 'Deducted')) {
            totalAmount = totalAmount + tax;
          }
          totalAmount = double.parse(totalAmount.toStringAsFixed(2));
          balanceDue = totalAmount - totalPaymentAmount;
          balanceDue = double.parse(balanceDue.toStringAsFixed(2));

          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            BlocProvider.of<InvoicesBloc>(context).add(
                InvoicesEvent.addBalanceDues(
                    invoiceId: invoice!.invoiceId, balanceDue: balanceDue));
            BlocProvider.of<AllInvoicesBloc>(context)
                .add(const AllInvoicesEvent.getAllInvoicesList());
          });

          return Column(
            children: [
              InvoiceNumberConsumerWidget(
                  invoice: invoice, size: size, isEstimate: isEstimate),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ClientsScreen(),
                      ));
                },
                child: EditContainerWidget(
                  child: Row(
                    children: [
                      Text('To:',
                          style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                              letterSpacing: .5,
                              fontSize: size.width * 0.035,
                              color: LinqPeColors.kBlackColor,
                              fontWeight: FontWeight.w600,
                            ),
                          )),
                      Text(
                          invoice.client == null ||
                                  invoice.client!.clientName == 'No Client'
                              ? 'Client'
                              : invoice.client!.clientName,
                          style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                              letterSpacing: .5,
                              fontSize: size.width * 0.035,
                              color: invoice.client == null ||
                                      invoice.client!.clientName == 'No Client'
                                  ? LinqPeColors.kBlackColor.withOpacity(0.3)
                                  : LinqPeColors.kBlackColor,
                              fontWeight: FontWeight.w600,
                            ),
                          )),
                    ],
                  ),
                ),
              ),
              ItemPriceContainer(
                numberOfRows: itemPriceMapList.length,
                isFromItems: true,
                itemPriceMapList: itemPriceMapList,
                totalName: 'Subtotal',
                totalPrice: '₹$subtotal',
                itemList: itemsList,
                onTapList: const [
                  // () {

                  // },
                  // () {
                  //   Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //         builder: (context) => ItemsScreen(),
                  //       ));
                  // }
                ],
              ),
              ItemPriceWidget(
                  balanceDue: balanceDue,
                  totalAmount: totalAmount,
                  invoice: invoice,
                  discountAmount: discountAmount,
                  taxAmount: tax),
              Column(
                children: List.generate(
                  invoice.photoList == null ? 0 : invoice.photoList!.length,
                  (index) => InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PhotoScreen(
                                isToEdit: true,
                                imageId: invoice!.photoList![index].photoId,
                                additionalDetails:
                                    invoice.photoList![index].addionalDetails,
                                details: invoice.photoList![index].description,
                                imageFilePath:
                                    invoice.photoList![index].photo.path),
                          ));
                    },
                    child: EditContainerWidget(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Photo ${index + 1}',
                            style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                letterSpacing: .5,
                                fontSize: size.width * 0.035,
                                color:
                                    LinqPeColors.kBlackColor.withOpacity(0.3),
                                fontWeight: FontWeight.w600,
                              ),
                            )),
                        // Icon(
                        //   Icons.attach_file,
                        //   color: LinqPeColors.kBlackColor.withOpacity(0.3),
                        // )
                      ],
                    )),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  openImagesPickingSheet(
                      context, size, false, false, null, null);
                },
                child: EditContainerWidget(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Add Photo',
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                            letterSpacing: .5,
                            fontSize: size.width * 0.035,
                            color: LinqPeColors.kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w600,
                          ),
                        )),
                    Icon(
                      Icons.attach_file,
                      color: LinqPeColors.kBlackColor.withOpacity(0.3),
                    )
                  ],
                )),
              ),
              isEstimate
                  ? const SizedBox()
                  : AdditionalDetails(
                      size: size,
                      detail: 'Payment Info',
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PaymentInfoScreen(),
                            ));
                      }),
              SignatureWidget(size: size, invoice: invoice),
              AdditionalDetails(
                size: size,
                detail: 'Notes',
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AdditionalDetailsScreen(),
                      ));
                },
              ),
              EditContainerWidget(
                  child: ReviewSwitchRow(
                      reviewLinkTextController: reviewLinkTextController,
                      size: size,
                      fieldName: 'Request Review',
                      invoiceEditDTO: invoice)),
              InkWell(
                onTap: () {
                  if (isEstimate) {
                    BlocProvider.of<InvoicesBloc>(context).add(
                        InvoicesEvent.convertingEstimateToInvoice(
                            invoiceId: invoice!.invoiceId));
                    Future.delayed(
                      const Duration(milliseconds: 100),
                      () {
                        BlocProvider.of<AllInvoicesBloc>(context)
                            .add(const AllInvoicesEvent.getAllInvoicesList());

                        Navigator.pop(context);
                      },
                    );
                  } else {
                    if (invoice!.isPaid == false) {
                      openMarkPaidBottomSheet(context, size);
                    } else {
                      BlocProvider.of<InvoicesBloc>(context).add(
                          InvoicesEvent.markAsPaidOrUnPaid(
                              invoiceId: invoice.invoiceId,
                              paymentMethod: null,
                              isPaid: false));
                    }
                  }
                },
                child: EditContainerWidget(
                    alignment: Alignment.center,
                    child: Text(
                      isEstimate
                          ? 'Make Invoice'
                          : invoice.isPaid
                              ? 'Mark Unpaid'
                              : 'Mark Paid',
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            letterSpacing: .5,
                            fontSize: size.width * 0.038,
                            color: LinqPeColors.kBlackColor),
                        fontWeight: FontWeight.w600,
                      ),
                    )),
              ),
              SizedBox(
                height: size.height * 0.2,
              ),
            ],
          );
        },
      ))),
    );
  }
}

class ReviewSwitchRow extends ConsumerWidget {
  const ReviewSwitchRow(
      {super.key,
      required this.size,
      required this.fieldName,
      required this.reviewLinkTextController,
      required this.invoiceEditDTO});
  final TextEditingController reviewLinkTextController;

  final Size size;
  final String fieldName;

  final InvoiceEditDTO invoiceEditDTO;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: size.height * 0.03,
              child: Center(
                child: Text(fieldName,
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        letterSpacing: .5,
                        fontSize: size.width * 0.04,
                        color: LinqPeColors.kBlackColor,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
              ),
            ),
            SizedBox(
              height: size.height * 0.045,
              child: FittedBox(
                fit: BoxFit.fill,
                child: Switch(
                    value: ref.watch(isToReviewProvider),
                    activeColor: LinqPeColors.kBlueColor,
                    onChanged: (value) {
                      addisToReview(value, ref);
                      BlocProvider.of<InvoicesBloc>(context).add(
                          InvoicesEvent.addReviews(
                              invoiceId: invoiceEditDTO.invoiceId,
                              reviewLink: '',
                              isToReview: value));
                    }),
              ),
            )
          ],
        ),
        ref.watch(isToReviewProvider)
            ? ReviewField(
                ref: ref,
                size: size,
                textEditingController: reviewLinkTextController,
                isNumber: false,
                hintText: 'Review Link',
                initialValue: invoiceEditDTO.reviewLink != null
                    ? invoiceEditDTO.reviewLink!
                    : '')
            : const SizedBox(),
      ],
    );
  }
}

class ReviewField extends StatefulWidget {
  const ReviewField(
      {super.key,
      required this.size,
      required this.textEditingController,
      required this.isNumber,
      required this.hintText,
      this.fontSize,
      this.height,
      required this.initialValue,
      required this.ref});

  final Size size;
  final TextEditingController? textEditingController;
  final bool isNumber;
  final String hintText;
  final double? height;
  final double? fontSize;
  final String initialValue;
  final WidgetRef ref;

  @override
  State<ReviewField> createState() => _ReviewFieldState();
}

class _ReviewFieldState extends State<ReviewField> {
  @override
  void initState() {
    if (widget.initialValue.isNotEmpty) {
      // widget.textEditingController!.text = widget.initialValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.size.width,
      height: widget.height ?? widget.size.height * 0.04,
      child: TextField(
        // textAlign: TextAlign.right, // Align text to the right
        // textDirection: TextDirection.ltr,
        onChanged: (value) {
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.addReviews(
              invoiceId: widget.ref.watch(invoiceIdProvider),
              reviewLink: value,
              isToReview: true));
        },
        keyboardType:
            widget.isNumber ? TextInputType.phone : TextInputType.emailAddress,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: widget.hintText,
            hintStyle: GoogleFonts.roboto(
              textStyle: TextStyle(
                letterSpacing: .5,
                fontSize: widget.fontSize ?? widget.size.width * 0.035,
                color: LinqPeColors.kBlackColor.withOpacity(0.45),
                fontWeight: FontWeight.w600,
              ),
            )),
        controller: widget.textEditingController,
        style: GoogleFonts.roboto(
          textStyle: TextStyle(
            letterSpacing: .5,
            fontSize: widget.fontSize ?? widget.size.width * 0.035,
            color: LinqPeColors.kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}

class InvoiceNumberConsumerWidget extends ConsumerWidget {
  const InvoiceNumberConsumerWidget({
    super.key,
    required this.invoice,
    required this.size,
    required this.isEstimate,
  });

  final InvoiceEditDTO invoice;
  final Size size;
  final bool isEstimate;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    String dueText = '';
    if (invoice.isPaid) {
      dueText = 'Paid';
    } else if (invoice.invoiceNumberDetails.terms == 'None') {
      dueText = 'No Due';
    } else if (invoice.invoiceNumberDetails.terms == 'Due On Receipt') {
      dueText = 'Due on receipt';
    } else if (invoice.invoiceNumberDetails.terms == 'Custom') {
      if (invoice.invoiceNumberDetails.dueDate != null) {
        final difference =
            invoice.invoiceNumberDetails.dueDate!.difference(DateTime.now());
        if (difference.inDays < 0) {
          dueText = 'OverDue';
        } else {
          dueText = 'Due in ${difference.inDays} days';
        }
      }
    }
    return EditContainerWidget(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Material(
            color: LinqPeColors.kWhiteColor,
            child: InkWell(
              onTap: () {
                addinvoiceDate(invoice.invoiceNumberDetails.invoiceDate, ref);
                addinvoiceDueDate(
                    invoice.invoiceNumberDetails.dueDate ??
                        invoice.invoiceNumberDetails.invoiceDate,
                    ref);
                addterms(invoice.invoiceNumberDetails.terms ?? 'None', ref);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          InvoiceNumberScreen(isEstimate: isEstimate),
                    ));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(invoice.invoiceNumberDetails.invoiceNumber,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.045,
                          color: LinqPeColors.kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      )),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: dueText.isEmpty
                            ? null
                            : dueText == 'Paid'
                                ? Border.all(color: LinqPeColors.kGreenColor)
                                : Border.all(color: LinqPeColors.kGreyColor),
                        color: dueText == 'Paid'
                            ? LinqPeColors.kGreenColor
                            : LinqPeColors.kWhiteColor),
                    padding: dueText == 'Paid'
                        ? EdgeInsets.symmetric(
                            horizontal: size.width * 0.05,
                            vertical: size.width * 0.01)
                        : EdgeInsets.all(size.width * 0.01),
                    child: Text(dueText,
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                            letterSpacing: .5,
                            fontSize: size.width * 0.035,
                            color: LinqPeColors.kBlackColor,
                            fontWeight: FontWeight.w400,
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: size.height * 0.01,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Material(
                color: const Color.fromRGBO(255, 255, 255, 1),
                child: BusinessDetailsWidget(invoice: invoice, size: size),
              ),
              Material(
                color: LinqPeColors.kWhiteColor,
                child: InkWell(
                  onTap: () {
                    addinvoiceDate(
                        invoice.invoiceNumberDetails.invoiceDate, ref);
                    addinvoiceDueDate(
                        invoice.invoiceNumberDetails.dueDate ??
                            invoice.invoiceNumberDetails.invoiceDate,
                        ref);
                    addterms(invoice.invoiceNumberDetails.terms ?? 'None', ref);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              InvoiceNumberScreen(isEstimate: isEstimate),
                        ));
                  },
                  child: Text(
                      '${invoice.invoiceNumberDetails.invoiceDate.day}-${invoice.invoiceNumberDetails.invoiceDate.month}-${invoice.invoiceNumberDetails.invoiceDate.year}',
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.035,
                          color: LinqPeColors.kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      )),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class SignatureWidget extends ConsumerWidget {
  const SignatureWidget({
    super.key,
    required this.size,
    required this.invoice,
  });

  final Size size;
  final InvoiceEditDTO? invoice;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return AdditionalDetails(
      size: size,
      detail: invoice!.signature != null
          ? 'Signed on ${dateTimeToStringConverter(invoice!.signature!.signDate!)}'
          : 'Signature',
      onTap: () {
        if (invoice!.signature != null) {
          addsignature(invoice!.signature!.signature, ref);
        }
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SignatureScreen(invoice!.signature != null
                  ? invoice!.signature!.signature
                  : null),
            ));
      },
    );
  }
}

class ItemPriceWidget extends ConsumerWidget {
  const ItemPriceWidget(
      {super.key,
      required this.invoice,
      required this.discountAmount,
      required this.taxAmount,
      required this.totalAmount,
      required this.balanceDue});
  final InvoiceEditDTO invoice;
  final double discountAmount;
  final double taxAmount;
  final double totalAmount;
  final double balanceDue;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      addbalanceDue(balanceDue, ref);
    });

    double paymentAmount = 0.00;
    if (invoice.paymentsList != null && invoice.paymentsList!.isNotEmpty) {
      for (var pay in invoice.paymentsList!) {
        paymentAmount = paymentAmount + pay.amount;
      }
    }
    return ItemPriceContainer(
        isFromItems: false,
        onTapList: [
          () {
            addDiscountType(
                invoice.discountDetails == null
                    ? 'No Discount'
                    : invoice.discountDetails!.discountType,
                ref);
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ScreenDiscount(),
                ));
          },
          () {
            addtaxType(
                invoice.tax == null ? 'None' : invoice.tax!.taxType, ref);
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TaxScreen(),
                ));
          },
          () {},
          () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      PaymentsScreen(totalAmount: totalAmount),
                ));
          }
        ],
        numberOfRows: 4,
        itemPriceMapList: [
          {'Discount': discountAmount == 0 ? '₹0.0' : '-₹$discountAmount '},
          {'Tax': taxAmount < 0 ? '-₹${-taxAmount}' : '₹$taxAmount'},
          {'Total': '₹$totalAmount'},
          {
            invoice.paymentsList == null || invoice.paymentsList!.isEmpty
                    ? 'Payments'
                    : 'Paid':
                invoice.paymentsList == null || invoice.paymentsList!.isEmpty
                    ? '₹0.00'
                    : '₹$paymentAmount'
          }
        ],
        totalName: 'Balance Due',
        totalPrice: balanceDue < 0 ? '-₹${-balanceDue}' : '₹$balanceDue');
  }
}

class BusinessDetailsWidget extends ConsumerWidget {
  const BusinessDetailsWidget({
    super.key,
    required this.invoice,
    required this.size,
  });

  final InvoiceEditDTO? invoice;
  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return InkWell(
      onTap: () {
        if (invoice!.businessDetails != null &&
            invoice!.businessDetails!.businessLogo != null) {
          addBusinessDetailLogo(
              invoice!.businessDetails!.businessLogo!.path, ref);
        }

        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => BusinessDetailsScreen(),
            ));
      },
      child: Text('Business Info',
          style: GoogleFonts.roboto(
            textStyle: TextStyle(
              letterSpacing: .5,
              fontSize: size.width * 0.035,
              color: LinqPeColors.kBlackColor.withOpacity(0.3),
              fontWeight: FontWeight.w600,
            ),
          )),
    );
  }
}

class AdditionalDetails extends StatelessWidget {
  const AdditionalDetails(
      {super.key,
      required this.size,
      required this.detail,
      required this.onTap});

  final Size size;
  final String detail;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: EditContainerWidget(
          child: Text(detail,
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.035,
                  color: LinqPeColors.kBlackColor.withOpacity(0.3),
                  fontWeight: FontWeight.w600,
                ),
              ))),
    );
  }
}

class ItemPriceContainer extends ConsumerWidget {
  const ItemPriceContainer({
    super.key,
    required this.numberOfRows,
    required this.itemPriceMapList,
    required this.totalName,
    required this.totalPrice,
    required this.onTapList,
    required this.isFromItems,
    this.itemList,
  });
  final int numberOfRows;
  final List<Map<String, String>> itemPriceMapList;
  final String totalName;
  final String totalPrice;
  final List<Function()> onTapList;
  final bool isFromItems;
  final List<ItemDTO?>? itemList;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    return EditContainerWidget(
      padding: 0,
      child: Column(
        children: [
          Column(
            children: List.generate(
                numberOfRows,
                (index) => itemPriceMapList[index].keys.first.isEmpty &&
                        itemPriceMapList[index].values.first.isEmpty
                    ? Divider(
                        color: LinqPeColors.kBlackColor,
                        height: size.height * 0.001,
                        thickness: size.height * 0.0001,
                      )
                    : Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: size.width * 0.05,
                            vertical: size.width * 0.015),
                        child: Material(
                          color: LinqPeColors.kWhiteColor,
                          child: InkWell(
                            onTap: isFromItems
                                ? () {
                                    if (itemList != null) {
                                      if (itemList![index] != null) {
                                        addisTaxable(
                                            itemList![index]!.isTaxable, ref);
                                      } else {
                                        addisTaxable(false, ref);
                                      }
                                    } else {
                                      addisTaxable(false, ref);
                                    }
                                    if (itemList != null &&
                                        itemList![index] != null) {
                                      addtotal(
                                          itemList![index]!.total.toString(),
                                          ref);
                                    } else {
                                      addtotal('0.00', ref);
                                    }
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => ItemsScreen(
                                            isToEdit: itemList == null ||
                                                    itemList![index] == null
                                                ? false
                                                : true,
                                            item: itemList == null
                                                ? null
                                                : itemList![index],
                                          ),
                                        ));
                                  }
                                : onTapList[index],
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(itemPriceMapList[index].keys.first,
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                        letterSpacing: .5,
                                        fontSize: size.width * 0.035,
                                        color: LinqPeColors.kBlackColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )),
                                Text(itemPriceMapList[index].values.first,
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                        letterSpacing: .5,
                                        fontSize: size.width * 0.035,
                                        color: LinqPeColors.kBlackColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )),
                              ],
                            ),
                          ),
                        ),
                      )),
          ),
          Container(
            decoration: const BoxDecoration(
                color: LinqPeColors.kGreyColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(5),
                    bottomRight: Radius.circular(5))),
            padding: EdgeInsets.symmetric(
                vertical: size.height * 0.01, horizontal: size.width * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(totalName,
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        letterSpacing: .5,
                        fontSize: size.width * 0.035,
                        color: LinqPeColors.kWhiteColor,
                        fontWeight: FontWeight.w600,
                      ),
                    )),
                Text(totalPrice,
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        letterSpacing: .5,
                        fontSize: size.width * 0.035,
                        color: LinqPeColors.kWhiteColor,
                        fontWeight: FontWeight.w600,
                      ),
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }
}

openMarkPaidBottomSheet(
  BuildContext context,
  Size size,
) {
  showModalBottomSheet(
    backgroundColor: LinqPeColors.kWhiteColor,
    context: context,
    builder: (BuildContext context) {
      return Container(
        width: size.width,
        decoration: const BoxDecoration(
            color: LinqPeColors.kWhiteColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50))),
        height: size.height * 0.5,
        child: const Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            PaymentMethodWidget(method: 'Select Payment Method'),
            PaymentMethodDivider(),
            PaymentMethodWidget(method: 'Bank transfer'),
            PaymentMethodDivider(),
            PaymentMethodWidget(method: 'Cash'),
            PaymentMethodDivider(),
            PaymentMethodWidget(method: 'Check'),
            PaymentMethodDivider(),
            PaymentMethodWidget(method: 'Credit card'),
            PaymentMethodDivider(),
            PaymentMethodWidget(method: 'PayPal'),
            PaymentMethodDivider(),
            PaymentMethodWidget(method: 'Peer-to-peer payment'),
            PaymentMethodDivider(),
            PaymentMethodWidget(method: 'Point-of-sale system'),
          ],
        ),
      );
    },
  );
}

class PaymentMethodDivider extends StatelessWidget {
  const PaymentMethodDivider({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Divider(
      color: LinqPeColors.kBlackColor,
      height: size.height * 0.001,
      thickness: size.height * 0.0001,
    );
  }
}

class PaymentMethodWidget extends ConsumerWidget {
  const PaymentMethodWidget({
    super.key,
    required this.method,
  });
  final String method;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        BlocProvider.of<InvoicesBloc>(context).add(
            InvoicesEvent.markAsPaidOrUnPaid(
                invoiceId: ref.watch(invoiceIdProvider),
                paymentMethod: method,
                isPaid: true));
        addmarkPaidType(method, ref);
        Navigator.pop(context);
      },
      child: Text(method,
          style: GoogleFonts.roboto(
            textStyle: TextStyle(
              letterSpacing: .5,
              fontSize: size.width * 0.04,
              color: LinqPeColors.kBlackColor,
              fontWeight: FontWeight.w500,
            ),
          )),
    );
  }
}
