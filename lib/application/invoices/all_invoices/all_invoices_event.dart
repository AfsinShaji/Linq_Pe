part of 'all_invoices_bloc.dart';

@freezed
class AllInvoicesEvent with _$AllInvoicesEvent {
  const factory AllInvoicesEvent.getAllInvoicesList() = getAllInvoicesList;
}