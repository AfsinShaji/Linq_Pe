import 'package:hive/hive.dart';

part 'notes.g.dart';

@HiveType(typeId: 16)
class NotesModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final String notes;


  NotesModel({
    required this.invoiceId,
    required this.notes
  });
}

class NotesBox {
  static Box<NotesModel>? _getbox;
  static Box<NotesModel> getInstance() {
    return _getbox ??= Hive.box('NotesBox');
  }
}
