import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/invoice_number.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/payments.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

Future<void> showDatePickers(
    BuildContext context, Size size, String dateScreen) async {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
          content: SizedBox(
              width: size.width * 0.8,
              height: size.width * 0.8,
              child: DatePickerWidget(
                dateScreen: dateScreen,
              )));
    },
  );
}

class DatePickerWidget extends ConsumerWidget {
  const DatePickerWidget({
    required this.dateScreen,
    super.key,
  });
  final String dateScreen;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SfDateRangePicker(
      onSelectionChanged: (value) {
        if (dateScreen == 'Add Payment') {
          addpaymentDate(value.value, ref);
        } else if (dateScreen == 'InvoiceNumberDate') {
          addinvoiceDate(value.value, ref);
        } else if (dateScreen == 'InvoiceNumberDueDate') {
          addinvoiceDueDate(value.value, ref);
        }
        Navigator.pop(context);
      },
    );
  }
}
