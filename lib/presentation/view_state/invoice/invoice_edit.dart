import 'package:flutter_riverpod/flutter_riverpod.dart';

final invoicePageProvider = StateProvider(
  (ref) {
    return 0;
  },
);
addinvoicePage(int id, WidgetRef ref) {

  ref.read(invoicePageProvider.notifier).state = id;
}

final invoiceIdProvider = StateProvider(
  (ref) {
    return '';
  },
);
addInvoiceId(String id, WidgetRef ref) {

  ref.read(invoiceIdProvider.notifier).state = id;
}

final isToReviewProvider = StateProvider(
  (ref) {
    return false;
  },
);
addisToReview(bool type, WidgetRef ref) {
  ref.read(isToReviewProvider.notifier).state = type;
 
}

final markPaidTypeProvider = StateProvider(
  (ref) {
    return '';
  },
);
addmarkPaidType(String type, WidgetRef ref) {

  ref.read(markPaidTypeProvider.notifier).state = type;
}

final balanceDueProvider = StateProvider(
  (ref) {
    return 0.00;
  },
);
addbalanceDue(double amount, WidgetRef ref) {

  ref.read(balanceDueProvider.notifier).state = amount;
}