import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/users/users/users_bloc.dart';
import 'package:linq_pe/presentation/screens/authentication/registration/widgets/drop_down_button.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/loading_overlay.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/sign_button.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/sign_text_field.dart';
import 'package:linq_pe/presentation/screens/premium_plan/screen_premium_plan.dart';
import 'package:linq_pe/presentation/view_state/registration_riverpod.dart/registration.dart';
import 'package:linq_pe/presentation/widgets/issue_box.dart';
import 'package:linq_pe/utilities/colors.dart';
import 'package:image_picker/image_picker.dart';
import 'package:linq_pe/utilities/list.dart';

class AddBusinessScreen extends ConsumerWidget {
  AddBusinessScreen({super.key});

  final formKey = GlobalKey<FormState>();

  final TextEditingController businessNameTextController =
      TextEditingController();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    final width = size.width;
    String userName = ref.watch(fullNameProvider);

    return Scaffold(
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            LinqPeColors.kPinkColor.withOpacity(0.7),
            LinqPeColors.kPinkColor.withOpacity(0.9)
          ],
          // begin: Alignment.topLeft,
          // end: Alignment.bottomRight,
        )),
        child: SafeArea(
          child: Center(
            child: Form(
              key: formKey,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: BlocBuilder<UsersBloc, UsersState>(
                      builder: (context, userState) {
                    if (userState is updateBusinessResult) {
                      if (userState.isLoading) {
                        Future.delayed(Duration.zero, () {
                          showLoadingOverlay(context);
                        });
                      } else if (userState.isError) {
                        Future.delayed(Duration.zero, () {
                          issueBox(context, size);
                        });
                      } else if (userState.isSuccess) {
                        WidgetsBinding.instance
                            .addPostFrameCallback((timeStamp) {
                          Navigator.pushAndRemoveUntil(
                            context,
                            CupertinoPageRoute(
                              builder: (context) => PremiumPlanScreen(
                                isPermitted: true,
                                userName: userName,
                              ),
                            ),
                            (route) => false,
                          );
                        });
                      }
                    }
                    return Column(
                      children: [
                   //     LogoWidget(),
                        Text(
                          'Logo',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              letterSpacing: .5,
                              fontSize: size.width * 0.045,
                              color: LinqPeColors.kWhiteColor,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        AddLogoStackWidget(size: size),
                        SizedBox(
                          height: size.height * 0.05,
                        ),
                        SignTextField(
                          controller: businessNameTextController,
                          text: "Enter Business Name",
                          icon: Icons.business,
                          isTextPasswordType: false,
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                        ),
                        SizedBox(
                          height: size.height * 0.02,
                        ),
                        const DropdownButtton(
                          isBusiness: true,
                          isPurpose: false,
                          items: LinqPeList.businessTypeList,
                        ),
                        SizedBox(
                          height: size.height * 0.02,
                        ),
                        //  SizedBox(
                        //   height:size.height*0.005 ,
                        // ),
                        const DropdownButtton(
                          isBusiness: false,
                          isPurpose: true,
                          items: LinqPeList.purposeType,
                        ),
          
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        RegisterButtonWidget(
                          formKey: formKey,
                          width: width,
                          businessName: businessNameTextController.text,
                        ),
                      ],
                    );
                  }),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class AddLogoStackWidget extends ConsumerWidget {
  const AddLogoStackWidget({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return InkWell(
      onTap: () {
        openImagePickingSheet(context, size, ref);
      },
      child: Stack(
        children: [
          SizedBox(
            height: size.height * 0.15,
            width: size.height * 0.2,
          ),
          Positioned(
            top: size.height * 0.02,
            left: size.width * 0.07,
            child: Container(
              height: size.height * 0.13,
              width: size.height * 0.13,
              decoration: BoxDecoration(
                  color: LinqPeColors.kWhiteColor.withOpacity(0.9),
                  borderRadius: BorderRadius.circular(15)),
              child: ref.watch(businessLogoProvider) == null
                  ? const SizedBox()
                  : Image.file(
                      File(ref.watch(businessLogoProvider)!.path),
                      fit: BoxFit.contain,
                    ),
            ),
          ),
          Positioned(
              left: size.width * 0.28,
              child: Container(
                height: size.height * 0.05,
                width: size.height * 0.05,
                decoration: BoxDecoration(
                    color: LinqPeColors.kBlackColor.withOpacity(0.9),
                    borderRadius: BorderRadius.circular(100)),
                child: const Icon(
                  Icons.add,
                  color: LinqPeColors.kWhiteColor,
                ),
              ))
        ],
      ),
    );
  }
}

class RegisterButtonWidget extends ConsumerWidget {
  const RegisterButtonWidget({
    super.key,
    required this.formKey,
    required this.width,
    required this.businessName,
  });

  final GlobalKey<FormState> formKey;
  final double width;
  final String businessName;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SignButton(
        text: "Add Business",
        onTap: () {
          if (formKey.currentState!.validate() &&
              ref.watch(businessTypeProvider) != 'Business Type' &&
              ref.watch(purposeTypeProvider) != 'Purpose' &&
              ref.watch(businessTypeProvider).isNotEmpty &&
              ref.watch(purposeTypeProvider).isNotEmpty &&
              ref.watch(businessLogoProvider) != null) {
            log('look${ref.watch(businessLogoProvider)!.path}');
            BlocProvider.of<UsersBloc>(context).add(updateUsersBusiness(
                userPusrpose: ref.watch(purposeTypeProvider),
                userBusinessType: ref.watch(purposeTypeProvider),
                userBusinessName: businessName,
                userBusinessLogo: ref.watch(businessLogoProvider)!.path));
          }
        },
        width: width);
  }
}

openImagePickingSheet(BuildContext context, Size size, WidgetRef ref) {
  showModalBottomSheet(
    backgroundColor: LinqPeColors.kWhiteColor,
    context: context,
    builder: (context) => Container(
      decoration: const BoxDecoration(
          color: LinqPeColors.kWhiteColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50), topRight: Radius.circular(50))),
      height: size.height * 0.2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      openCamera(ref);
                    },
                    child: Container(
                      height: size.height * 0.1,
                      width: size.width * 0.23,
                      decoration: BoxDecoration(
                        color: LinqPeColors.kPinkColor,
                        borderRadius: BorderRadius.circular(
                          15,
                        ),
                      ),
                      child: Icon(
                        Icons.camera,
                        color: LinqPeColors.kWhiteColor,
                        size: size.width * 0.1,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  Text('Camera',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.036,
                          color: LinqPeColors.kBlackColor,
                          fontWeight: FontWeight.w500,
                        ),
                      )),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(),
                child: Column(
                  children: [
                    InkWell(
                      onTap: () {
                        openGallery(ref);
                      },
                      child: Container(
                        height: size.height * 0.1,
                        width: size.width * 0.23,
                        decoration: BoxDecoration(
                          color: LinqPeColors.kPinkColor,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Icon(
                          Icons.image,
                          color: LinqPeColors.kWhiteColor,
                          size: size.width * 0.1,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Text('Gallery',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            letterSpacing: .5,
                            fontSize: size.width * 0.036,
                            color: LinqPeColors.kBlackColor,
                            fontWeight: FontWeight.w500,
                          ),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}

Future<void> openCamera(WidgetRef ref) async {
  final picker = ImagePicker();
  final pickedImage = await picker.pickImage(source: ImageSource.camera);

  if (pickedImage != null) {
    addBusinessLogo(pickedImage, ref);
    // Use the picked file (e.g., upload or display the image)
  }
}

Future<void> openGallery(WidgetRef ref) async {
  final picker = ImagePicker();
  final pickedImage = await picker.pickImage(source: ImageSource.gallery);

  if (pickedImage != null) {
    addBusinessLogo(pickedImage, ref);
    // Use the picked file (e.g., upload or display the image)
  }
}
