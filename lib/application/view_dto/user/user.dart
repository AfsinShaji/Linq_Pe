import 'package:linq_pe/domain/models/users/users.dart';

class UserProfileDTO {
  final String name;
  final String userId;
  final String phoneNumber;
  final String email;
  final String businessName;
  final String purpose;
  final String businessType;
  final String businessLogo;
  final bool isPremium;
  final DateTime? trialStartDate;

  UserProfileDTO({
     required this.trialStartDate,
    required this.isPremium,
    required this.name,
    required this.userId,
    required this.phoneNumber,
    required this.email,
    required this.businessName,
    required this.purpose,
    required this.businessType,
    required this.businessLogo,
  });
}

 UserProfileDTO converstUserModelToDTO(UserProfile user) {
  return UserProfileDTO(
    trialStartDate: user.trialStartDate,
      isPremium: user.isPremium,
      name: user.userName,
      userId: user.userId,
      phoneNumber: user.phoneNumber,
      email: user.userEmail,
      businessName: user.businessName,
      purpose: user.purpose,
      businessType: user.businessType,
      businessLogo: user.businessLogo);
}
