import 'package:dartz/dartz.dart';
import 'package:linq_pe/domain/core/failure/failure.dart';
import 'package:linq_pe/domain/models/users/users.dart';

abstract class UsersRepository {
  Future<void> addUser(
      {required String name,
      required String email,
      required String phoneNumber,
      required String businessType,
      required String purpose});
  Future<bool?> isAccountPermitted();
  Future<Either<MainFailure, UserProfile?>> addTrialDate();
  Future<Either<MainFailure, String>> updateUserBusiness(
      {required String userPusrpose,
      required String userBusinessName,
      required String userBusinessLogo,
      required String userBusinessType});
  Future<Either<MainFailure, UserProfile>> getUserDetails();
}
