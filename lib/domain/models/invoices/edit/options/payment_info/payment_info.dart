import 'package:hive/hive.dart';

part 'payment_info.g.dart';

@HiveType(typeId: 23)
class PaymentInfoModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final String? paypalEmail;
  @HiveField(2)
  final String? businessName;
  @HiveField(3)
  final String? paymentInstructions;
  @HiveField(4)
  final String? additionalPaymentInstructions;

  PaymentInfoModel({
    required this.invoiceId,
    required this.additionalPaymentInstructions,
    required this.businessName,
    required this.paymentInstructions,
    required this.paypalEmail,
  });
}

class PaymentInfoBox {
  static Box<PaymentInfoModel>? _getbox;
  static Box<PaymentInfoModel> getInstance() {
    return _getbox ??= Hive.box('PaymentInfoBox');
  }
}
