import 'package:linq_pe/domain/models/invoices/edit/options/client/client.dart';

class ClientDTO {
  final String invoiceId;

  final String clientName;

  final String? clientEmail;

  final String? mobileNumber;

  final String? phoneNumber;

  final String? faxNumber;

  final String? address1;

  final String? address2;

  final String? address3;

  ClientDTO({
    required this.invoiceId,
    required this.address1,
    required this.address2,
    required this.address3,
    required this.clientEmail,
    required this.clientName,
    required this.faxNumber,
    required this.mobileNumber,
    required this.phoneNumber,
  });
}

ClientDTO? convertClientModelToDTO(ClientModel? client) {
   if (client == null) {
    return null;
  }
  return ClientDTO(
      invoiceId: client.invoiceId,
      address1: client.address1,
      address2: client.address2,
      address3: client.address3,
      clientEmail: client.clientEmail,
      clientName: client.clientName,
      faxNumber: client.faxNumber,
      mobileNumber: client.mobileNumber,
      phoneNumber: client.phoneNumber);
}
