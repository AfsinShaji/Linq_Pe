import 'package:hive/hive.dart';

part 'client.g.dart';

@HiveType(typeId: 22)
class ClientModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final String clientName;
  @HiveField(2)
  final String? clientEmail;
  @HiveField(3)
  final String? mobileNumber;
  @HiveField(4)
  final String? phoneNumber;
  @HiveField(5)
  final String? faxNumber;
  @HiveField(6)
  final String? address1;
  @HiveField(7)
  final String? address2;
  @HiveField(8)
  final String? address3;

  ClientModel({
    required this.invoiceId,
    required this.address1,
    required this.address2,
    required this.address3,
    required this.clientEmail,
    required this.clientName,
    required this.faxNumber,
    required this.mobileNumber,
    required this.phoneNumber,
  });
}

class ClientBox {
  static Box<ClientModel>? _getbox;
  static Box<ClientModel> getInstance() {
    return _getbox ??= Hive.box('ClientBox');
  }
}
