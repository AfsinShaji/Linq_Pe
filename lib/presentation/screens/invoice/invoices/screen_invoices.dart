import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/all_invoices/all_invoices_bloc.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';

import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/screen_each_invoice.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/datetime_to_date_string_converter.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

class InvoicesScreen extends StatefulWidget {
  const InvoicesScreen({
    super.key,
    required this.isInvoice,
    required this.isEstimate,
  });
  final bool isInvoice;
  final bool isEstimate;

  @override
  State<InvoicesScreen> createState() => _InvoicesScreenState();
}

class _InvoicesScreenState extends State<InvoicesScreen> {
  @override
  void initState() {
    BlocProvider.of<AllInvoicesBloc>(context)
        .add(const AllInvoicesEvent.getAllInvoicesList());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<InvoiceEditDTO> invoiceList = [];
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: BlocBuilder<AllInvoicesBloc, AllInvoicesState>(
        builder: (context, invoiceState) {
          List<InvoiceEditDTO> invoicesList = [];
          if (invoiceState is displayInvoiceList) {
            if (widget.isInvoice) {
              invoicesList = invoiceState.invoiceList
                  .where((element) => element.isInvoice == true)
                  .toList();
            } else if (widget.isEstimate) {
              invoicesList = invoiceState.invoiceList
                  .where((element) => element.isEstimate == true)
                  .toList();
            }
          }
          return FloatingAddWidget(
            invoiceList: invoicesList,
            isEstimated: widget.isEstimate,
            isInvoice: widget.isInvoice,
          );
        },
      ),
      appBar: AppBar(
        titleSpacing: size.width * 0.18,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        title: Text(widget.isEstimate ? 'Estimates' : 'Invoices',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.08,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w600,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      body: SafeArea(child: BlocBuilder<AllInvoicesBloc, AllInvoicesState>(
        builder: (context, invoiceState) {
          if (invoiceState is displayInvoiceList) {
            if (widget.isInvoice) {
              invoiceList = invoiceState.invoiceList
                  .where((element) => element.isInvoice == true)
                  .toList();
            } else if (widget.isEstimate) {
              invoiceList = invoiceState.invoiceList
                  .where((element) => element.isEstimate == true)
                  .toList();
            }
            invoiceList = invoiceList.reversed.toList();
          }
          if (invoiceList.isEmpty) {
            return Center(
              child: Image.asset('assets/images/Reading list-amico.png'),
            );
          }
          return ListView.separated(
              itemBuilder: (context, index) {
                return InvoiceListTileWidget(isEstimate: widget.isEstimate,
                    index: index, invoiceList: invoiceList, size: size);
              },
              separatorBuilder: (context, index) => Divider(
                    color: LinqPeColors.kBlackColor,
                    height: size.height * 0.001,
                    thickness: size.height * 0.0001,
                  ),
              itemCount: invoiceList.length);
        },
      )),
    );
  }
}

class InvoiceListTileWidget extends ConsumerWidget {
  const InvoiceListTileWidget(
      {super.key,
      required this.invoiceList,
      required this.size,
      required this.index,
      required this.isEstimate});
  final int index;
  final bool isEstimate;
  final List<InvoiceEditDTO> invoiceList;
  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ListTile(
      onTap: () {
        BlocProvider.of<InvoicesBloc>(context).add(
            InvoicesEvent.getInvoicesDetails(
                invoiceId: invoiceList[index].invoiceId));
        addInvoiceId(invoiceList[index].invoiceId, ref);
        if (invoiceList[index].isToReview != null) {
          addisToReview(invoiceList[index].isToReview!, ref);
        }
        Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (context) => EachInvoiceScreen(isEstimate: isEstimate),
            ));
      },
      title: Text(
        invoiceList[index].client == null
            ? 'No Client'
            : invoiceList[index].client!.clientName,
        style: GoogleFonts.poppins(
          textStyle: TextStyle(
            letterSpacing: .5,
            fontSize: size.width * 0.045,
            color: LinqPeColors.kBlackColor,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      subtitle: Text(
        invoiceList[index].invoiceNumberDetails.invoiceNumber,
        style: GoogleFonts.poppins(
          textStyle: TextStyle(
            letterSpacing: .5,
            fontSize: size.width * 0.03,
            color: LinqPeColors.kBlackColor.withOpacity(0.6),
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      trailing: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            invoiceList[index].balanceDue == null
                ? '₹ 0.00'
                : '₹ ${invoiceList[index].balanceDue.toString()}',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .5,
                fontSize: size.width * 0.045,
                color: LinqPeColors.kBlackColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Text(
            dateTimeToStringConverter(
                invoiceList[index].invoiceNumberDetails.invoiceDate),
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .5,
                fontSize: size.width * 0.03,
                color: LinqPeColors.kBlackColor.withOpacity(0.6),
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FloatingAddWidget extends ConsumerWidget {
  const FloatingAddWidget(
      {super.key,
      required this.invoiceList,
      required this.isEstimated,
      required this.isInvoice});

  final List<InvoiceEditDTO> invoiceList;
  final bool isEstimated;
  final bool isInvoice;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return FloatingActionButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
        backgroundColor: LinqPeColors.kPinkColor,
        onPressed: () {
          if (isInvoice) {
            String invoiceNum = 'INV0000';
            final length = (invoiceList.length + 1).toString();
            String finalInvoiceNum =
                '${invoiceNum.substring(0, invoiceNum.length - length.length)}$length';
            log('$finalInvoiceNum ${invoiceList.length}');
            BlocProvider.of<InvoicesBloc>(context)
                .add(InvoicesEvent.createInvoicesNumber(
              invoiceNumber: finalInvoiceNum,
            ));
            Future.delayed(
              const Duration(milliseconds: 100),
              () => BlocProvider.of<AllInvoicesBloc>(context)
                  .add(const AllInvoicesEvent.getAllInvoicesList()),
            );
          } else if (isEstimated) {
            String estimatedNum = 'EST0000';
            final length = (invoiceList.length + 1).toString();
            String finalestimatedNum =
                '${estimatedNum.substring(0, estimatedNum.length - length.length)}$length';
            log('$finalestimatedNum ${invoiceList.length}');
            BlocProvider.of<InvoicesBloc>(context)
                .add(InvoicesEvent.createEstimatesNumber(
              estimateNumber: finalestimatedNum,
            ));
            Future.delayed(
              const Duration(milliseconds: 100),
              () => BlocProvider.of<AllInvoicesBloc>(context)
                  .add(const AllInvoicesEvent.getAllInvoicesList()),
            );
          }
        },
        child: const CircleAvatar(
          backgroundColor: LinqPeColors.kPinkColor,
          child: Icon(
            Icons.add,
            color: LinqPeColors.kWhiteColor,
          ),
        ));
  }
}
