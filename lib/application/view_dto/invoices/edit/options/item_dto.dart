import 'package:linq_pe/domain/models/invoices/edit/options/item/item.dart';

class ItemDTO {
  final String invoiceId;

  final String description;

  final double unitCost;

  final String? unit;

  final int quantity;

  final String? discount;

  final double? discountAmount;

  final bool isTaxable;

  final double? taxRate;

  final String? addiotionalDetails;
  final double total;
  final String itemId;

  ItemDTO(
      {
         required  this.itemId,
        required this.invoiceId,
      required this.addiotionalDetails,
      required this.discount,
      required this.description,
      required this.discountAmount,
      required this.isTaxable,
      required this.quantity,
      required this.taxRate,
      required this.unit,
      required this.unitCost,
      required  this.total});
}

List<ItemDTO>? convertItemModelToDTO(List<ItemModel>? items) {
  if (items == null) {
    return null;
  }
  List<ItemDTO> itemList = [];
  for (var item in items) {
    itemList.add(ItemDTO(
      itemId:item.itemId,
      total: item.total,
        invoiceId: item.invoiceId,
        addiotionalDetails: item.addiotionalDetails,
        discount: item.discount,
        description: item.description,
        discountAmount: item.discountAmount,
        isTaxable: item.isTaxable,
        quantity: item.quantity,
        taxRate: item.taxRate,
        unit: item.unit,
        unitCost: item.unitCost));
  }
  return itemList;
}
