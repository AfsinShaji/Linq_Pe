part of 'authentication_bloc.dart';

@freezed
class AuthenticationState with _$AuthenticationState {
  const factory AuthenticationState.authenticationinitial() =
      authenticationinitial;
  const factory AuthenticationState.authResults
  ({required bool isLoading,required bool isSuccess,required bool isError}) = authResults;
}
