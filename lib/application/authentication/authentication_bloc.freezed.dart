// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'authentication_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthenticationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String email, String password, BuildContext context)
        logIn,
    required TResult Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)
        registration,
    required TResult Function() loggingOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password, BuildContext context)?
        logIn,
    TResult? Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)?
        registration,
    TResult? Function()? loggingOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password, BuildContext context)?
        logIn,
    TResult Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)?
        registration,
    TResult Function()? loggingOut,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(logIn value) logIn,
    required TResult Function(registration value) registration,
    required TResult Function(loggingOut value) loggingOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(logIn value)? logIn,
    TResult? Function(registration value)? registration,
    TResult? Function(loggingOut value)? loggingOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(logIn value)? logIn,
    TResult Function(registration value)? registration,
    TResult Function(loggingOut value)? loggingOut,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthenticationEventCopyWith<$Res> {
  factory $AuthenticationEventCopyWith(
          AuthenticationEvent value, $Res Function(AuthenticationEvent) then) =
      _$AuthenticationEventCopyWithImpl<$Res, AuthenticationEvent>;
}

/// @nodoc
class _$AuthenticationEventCopyWithImpl<$Res, $Val extends AuthenticationEvent>
    implements $AuthenticationEventCopyWith<$Res> {
  _$AuthenticationEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$logInImplCopyWith<$Res> {
  factory _$$logInImplCopyWith(
          _$logInImpl value, $Res Function(_$logInImpl) then) =
      __$$logInImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String email, String password, BuildContext context});
}

/// @nodoc
class __$$logInImplCopyWithImpl<$Res>
    extends _$AuthenticationEventCopyWithImpl<$Res, _$logInImpl>
    implements _$$logInImplCopyWith<$Res> {
  __$$logInImplCopyWithImpl(
      _$logInImpl _value, $Res Function(_$logInImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = null,
    Object? password = null,
    Object? context = null,
  }) {
    return _then(_$logInImpl(
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      context: null == context
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
    ));
  }
}

/// @nodoc

class _$logInImpl implements logIn {
  const _$logInImpl(
      {required this.email, required this.password, required this.context});

  @override
  final String email;
  @override
  final String password;
  @override
  final BuildContext context;

  @override
  String toString() {
    return 'AuthenticationEvent.logIn(email: $email, password: $password, context: $context)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$logInImpl &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.context, context) || other.context == context));
  }

  @override
  int get hashCode => Object.hash(runtimeType, email, password, context);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$logInImplCopyWith<_$logInImpl> get copyWith =>
      __$$logInImplCopyWithImpl<_$logInImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String email, String password, BuildContext context)
        logIn,
    required TResult Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)
        registration,
    required TResult Function() loggingOut,
  }) {
    return logIn(email, password, context);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password, BuildContext context)?
        logIn,
    TResult? Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)?
        registration,
    TResult? Function()? loggingOut,
  }) {
    return logIn?.call(email, password, context);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password, BuildContext context)?
        logIn,
    TResult Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)?
        registration,
    TResult Function()? loggingOut,
    required TResult orElse(),
  }) {
    if (logIn != null) {
      return logIn(email, password, context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(logIn value) logIn,
    required TResult Function(registration value) registration,
    required TResult Function(loggingOut value) loggingOut,
  }) {
    return logIn(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(logIn value)? logIn,
    TResult? Function(registration value)? registration,
    TResult? Function(loggingOut value)? loggingOut,
  }) {
    return logIn?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(logIn value)? logIn,
    TResult Function(registration value)? registration,
    TResult Function(loggingOut value)? loggingOut,
    required TResult orElse(),
  }) {
    if (logIn != null) {
      return logIn(this);
    }
    return orElse();
  }
}

abstract class logIn implements AuthenticationEvent {
  const factory logIn(
      {required final String email,
      required final String password,
      required final BuildContext context}) = _$logInImpl;

  String get email;
  String get password;
  BuildContext get context;
  @JsonKey(ignore: true)
  _$$logInImplCopyWith<_$logInImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$registrationImplCopyWith<$Res> {
  factory _$$registrationImplCopyWith(
          _$registrationImpl value, $Res Function(_$registrationImpl) then) =
      __$$registrationImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String name,
      String email,
      String phoneNumber,
      BuildContext context,
      String businessType,
      String purpose,
      String password});
}

/// @nodoc
class __$$registrationImplCopyWithImpl<$Res>
    extends _$AuthenticationEventCopyWithImpl<$Res, _$registrationImpl>
    implements _$$registrationImplCopyWith<$Res> {
  __$$registrationImplCopyWithImpl(
      _$registrationImpl _value, $Res Function(_$registrationImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? email = null,
    Object? phoneNumber = null,
    Object? context = null,
    Object? businessType = null,
    Object? purpose = null,
    Object? password = null,
  }) {
    return _then(_$registrationImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      context: null == context
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
      businessType: null == businessType
          ? _value.businessType
          : businessType // ignore: cast_nullable_to_non_nullable
              as String,
      purpose: null == purpose
          ? _value.purpose
          : purpose // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$registrationImpl implements registration {
  const _$registrationImpl(
      {required this.name,
      required this.email,
      required this.phoneNumber,
      required this.context,
      required this.businessType,
      required this.purpose,
      required this.password});

  @override
  final String name;
  @override
  final String email;
  @override
  final String phoneNumber;
  @override
  final BuildContext context;
  @override
  final String businessType;
  @override
  final String purpose;
  @override
  final String password;

  @override
  String toString() {
    return 'AuthenticationEvent.registration(name: $name, email: $email, phoneNumber: $phoneNumber, context: $context, businessType: $businessType, purpose: $purpose, password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$registrationImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.context, context) || other.context == context) &&
            (identical(other.businessType, businessType) ||
                other.businessType == businessType) &&
            (identical(other.purpose, purpose) || other.purpose == purpose) &&
            (identical(other.password, password) ||
                other.password == password));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name, email, phoneNumber,
      context, businessType, purpose, password);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$registrationImplCopyWith<_$registrationImpl> get copyWith =>
      __$$registrationImplCopyWithImpl<_$registrationImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String email, String password, BuildContext context)
        logIn,
    required TResult Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)
        registration,
    required TResult Function() loggingOut,
  }) {
    return registration(
        name, email, phoneNumber, context, businessType, purpose, password);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password, BuildContext context)?
        logIn,
    TResult? Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)?
        registration,
    TResult? Function()? loggingOut,
  }) {
    return registration?.call(
        name, email, phoneNumber, context, businessType, purpose, password);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password, BuildContext context)?
        logIn,
    TResult Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)?
        registration,
    TResult Function()? loggingOut,
    required TResult orElse(),
  }) {
    if (registration != null) {
      return registration(
          name, email, phoneNumber, context, businessType, purpose, password);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(logIn value) logIn,
    required TResult Function(registration value) registration,
    required TResult Function(loggingOut value) loggingOut,
  }) {
    return registration(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(logIn value)? logIn,
    TResult? Function(registration value)? registration,
    TResult? Function(loggingOut value)? loggingOut,
  }) {
    return registration?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(logIn value)? logIn,
    TResult Function(registration value)? registration,
    TResult Function(loggingOut value)? loggingOut,
    required TResult orElse(),
  }) {
    if (registration != null) {
      return registration(this);
    }
    return orElse();
  }
}

abstract class registration implements AuthenticationEvent {
  const factory registration(
      {required final String name,
      required final String email,
      required final String phoneNumber,
      required final BuildContext context,
      required final String businessType,
      required final String purpose,
      required final String password}) = _$registrationImpl;

  String get name;
  String get email;
  String get phoneNumber;
  BuildContext get context;
  String get businessType;
  String get purpose;
  String get password;
  @JsonKey(ignore: true)
  _$$registrationImplCopyWith<_$registrationImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$loggingOutImplCopyWith<$Res> {
  factory _$$loggingOutImplCopyWith(
          _$loggingOutImpl value, $Res Function(_$loggingOutImpl) then) =
      __$$loggingOutImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$loggingOutImplCopyWithImpl<$Res>
    extends _$AuthenticationEventCopyWithImpl<$Res, _$loggingOutImpl>
    implements _$$loggingOutImplCopyWith<$Res> {
  __$$loggingOutImplCopyWithImpl(
      _$loggingOutImpl _value, $Res Function(_$loggingOutImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$loggingOutImpl implements loggingOut {
  const _$loggingOutImpl();

  @override
  String toString() {
    return 'AuthenticationEvent.loggingOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$loggingOutImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String email, String password, BuildContext context)
        logIn,
    required TResult Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)
        registration,
    required TResult Function() loggingOut,
  }) {
    return loggingOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password, BuildContext context)?
        logIn,
    TResult? Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)?
        registration,
    TResult? Function()? loggingOut,
  }) {
    return loggingOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password, BuildContext context)?
        logIn,
    TResult Function(
            String name,
            String email,
            String phoneNumber,
            BuildContext context,
            String businessType,
            String purpose,
            String password)?
        registration,
    TResult Function()? loggingOut,
    required TResult orElse(),
  }) {
    if (loggingOut != null) {
      return loggingOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(logIn value) logIn,
    required TResult Function(registration value) registration,
    required TResult Function(loggingOut value) loggingOut,
  }) {
    return loggingOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(logIn value)? logIn,
    TResult? Function(registration value)? registration,
    TResult? Function(loggingOut value)? loggingOut,
  }) {
    return loggingOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(logIn value)? logIn,
    TResult Function(registration value)? registration,
    TResult Function(loggingOut value)? loggingOut,
    required TResult orElse(),
  }) {
    if (loggingOut != null) {
      return loggingOut(this);
    }
    return orElse();
  }
}

abstract class loggingOut implements AuthenticationEvent {
  const factory loggingOut() = _$loggingOutImpl;
}

/// @nodoc
mixin _$AuthenticationState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() authenticationinitial,
    required TResult Function(bool isLoading, bool isSuccess, bool isError)
        authResults,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? authenticationinitial,
    TResult? Function(bool isLoading, bool isSuccess, bool isError)?
        authResults,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? authenticationinitial,
    TResult Function(bool isLoading, bool isSuccess, bool isError)? authResults,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(authenticationinitial value)
        authenticationinitial,
    required TResult Function(authResults value) authResults,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(authenticationinitial value)? authenticationinitial,
    TResult? Function(authResults value)? authResults,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(authenticationinitial value)? authenticationinitial,
    TResult Function(authResults value)? authResults,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthenticationStateCopyWith<$Res> {
  factory $AuthenticationStateCopyWith(
          AuthenticationState value, $Res Function(AuthenticationState) then) =
      _$AuthenticationStateCopyWithImpl<$Res, AuthenticationState>;
}

/// @nodoc
class _$AuthenticationStateCopyWithImpl<$Res, $Val extends AuthenticationState>
    implements $AuthenticationStateCopyWith<$Res> {
  _$AuthenticationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$authenticationinitialImplCopyWith<$Res> {
  factory _$$authenticationinitialImplCopyWith(
          _$authenticationinitialImpl value,
          $Res Function(_$authenticationinitialImpl) then) =
      __$$authenticationinitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$authenticationinitialImplCopyWithImpl<$Res>
    extends _$AuthenticationStateCopyWithImpl<$Res, _$authenticationinitialImpl>
    implements _$$authenticationinitialImplCopyWith<$Res> {
  __$$authenticationinitialImplCopyWithImpl(_$authenticationinitialImpl _value,
      $Res Function(_$authenticationinitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$authenticationinitialImpl implements authenticationinitial {
  const _$authenticationinitialImpl();

  @override
  String toString() {
    return 'AuthenticationState.authenticationinitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$authenticationinitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() authenticationinitial,
    required TResult Function(bool isLoading, bool isSuccess, bool isError)
        authResults,
  }) {
    return authenticationinitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? authenticationinitial,
    TResult? Function(bool isLoading, bool isSuccess, bool isError)?
        authResults,
  }) {
    return authenticationinitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? authenticationinitial,
    TResult Function(bool isLoading, bool isSuccess, bool isError)? authResults,
    required TResult orElse(),
  }) {
    if (authenticationinitial != null) {
      return authenticationinitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(authenticationinitial value)
        authenticationinitial,
    required TResult Function(authResults value) authResults,
  }) {
    return authenticationinitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(authenticationinitial value)? authenticationinitial,
    TResult? Function(authResults value)? authResults,
  }) {
    return authenticationinitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(authenticationinitial value)? authenticationinitial,
    TResult Function(authResults value)? authResults,
    required TResult orElse(),
  }) {
    if (authenticationinitial != null) {
      return authenticationinitial(this);
    }
    return orElse();
  }
}

abstract class authenticationinitial implements AuthenticationState {
  const factory authenticationinitial() = _$authenticationinitialImpl;
}

/// @nodoc
abstract class _$$authResultsImplCopyWith<$Res> {
  factory _$$authResultsImplCopyWith(
          _$authResultsImpl value, $Res Function(_$authResultsImpl) then) =
      __$$authResultsImplCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isLoading, bool isSuccess, bool isError});
}

/// @nodoc
class __$$authResultsImplCopyWithImpl<$Res>
    extends _$AuthenticationStateCopyWithImpl<$Res, _$authResultsImpl>
    implements _$$authResultsImplCopyWith<$Res> {
  __$$authResultsImplCopyWithImpl(
      _$authResultsImpl _value, $Res Function(_$authResultsImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isSuccess = null,
    Object? isError = null,
  }) {
    return _then(_$authResultsImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isSuccess: null == isSuccess
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$authResultsImpl implements authResults {
  const _$authResultsImpl(
      {required this.isLoading,
      required this.isSuccess,
      required this.isError});

  @override
  final bool isLoading;
  @override
  final bool isSuccess;
  @override
  final bool isError;

  @override
  String toString() {
    return 'AuthenticationState.authResults(isLoading: $isLoading, isSuccess: $isSuccess, isError: $isError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$authResultsImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isSuccess, isSuccess) ||
                other.isSuccess == isSuccess) &&
            (identical(other.isError, isError) || other.isError == isError));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isLoading, isSuccess, isError);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$authResultsImplCopyWith<_$authResultsImpl> get copyWith =>
      __$$authResultsImplCopyWithImpl<_$authResultsImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() authenticationinitial,
    required TResult Function(bool isLoading, bool isSuccess, bool isError)
        authResults,
  }) {
    return authResults(isLoading, isSuccess, isError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? authenticationinitial,
    TResult? Function(bool isLoading, bool isSuccess, bool isError)?
        authResults,
  }) {
    return authResults?.call(isLoading, isSuccess, isError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? authenticationinitial,
    TResult Function(bool isLoading, bool isSuccess, bool isError)? authResults,
    required TResult orElse(),
  }) {
    if (authResults != null) {
      return authResults(isLoading, isSuccess, isError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(authenticationinitial value)
        authenticationinitial,
    required TResult Function(authResults value) authResults,
  }) {
    return authResults(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(authenticationinitial value)? authenticationinitial,
    TResult? Function(authResults value)? authResults,
  }) {
    return authResults?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(authenticationinitial value)? authenticationinitial,
    TResult Function(authResults value)? authResults,
    required TResult orElse(),
  }) {
    if (authResults != null) {
      return authResults(this);
    }
    return orElse();
  }
}

abstract class authResults implements AuthenticationState {
  const factory authResults(
      {required final bool isLoading,
      required final bool isSuccess,
      required final bool isError}) = _$authResultsImpl;

  bool get isLoading;
  bool get isSuccess;
  bool get isError;
  @JsonKey(ignore: true)
  _$$authResultsImplCopyWith<_$authResultsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
