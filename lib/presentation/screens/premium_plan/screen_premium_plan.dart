import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/Razorpay/razorpay_bloc.dart';
import 'package:linq_pe/application/users/user_permission/user_permission_bloc.dart';
import 'package:linq_pe/application/users/users/users_bloc.dart';
import 'package:linq_pe/presentation/screens/about_us/screen_about_us.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/loading_overlay.dart';
import 'package:linq_pe/presentation/screens/home/screen_home.dart';
import 'package:linq_pe/presentation/screens/ledger/screen_ledger.dart';
import 'package:linq_pe/presentation/screens/privacy_policy/screen_privacy_policy.dart';
import 'package:linq_pe/presentation/screens/terms&conditions/screen_terms_and_conditions.dart';
import 'package:linq_pe/presentation/widgets/alert_snackbar.dart';
import 'package:linq_pe/presentation/widgets/issue_box.dart';
import 'package:linq_pe/utilities/colors.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class PremiumPlanScreen extends StatefulWidget {
  const PremiumPlanScreen({
    super.key,
    required this.userName,
    required this.isPermitted,
    this.isNotSatrted = false,
  });

  final String userName;
  final bool isPermitted;
  final bool isNotSatrted;

  @override
  State<PremiumPlanScreen> createState() => _PremiumPlanScreenState();
}

class _PremiumPlanScreenState extends State<PremiumPlanScreen> {
  final razorPay = Razorpay();
  @override
  void initState() {
    //  BlocProvider.of<ViewUserBloc>(context).add(const ViewUserEvent.getUserDetail());
    razorPay.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlePaymentSuccess);
    razorPay.on(Razorpay.EVENT_PAYMENT_ERROR, handlePaymentOnError);
    razorPay.on(Razorpay.EVENT_EXTERNAL_WALLET, handleExternalWallet);
    super.initState();
  }

  void handlePaymentSuccess(PaymentSuccessResponse response) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      BlocProvider.of<UsersBloc>(context)
          .add(const UsersEvent.makeUsersPremium());
      Future.delayed(Duration.zero, () {
        showLoadingOverlay(context);
      });
      Future.delayed(const Duration(seconds: 3), () {
        BlocProvider.of<UserPermissionBloc>(context)
            .add(const UserPermissionEvent.isUserAccountPermitted());
        Navigator.pushReplacement(
            context,
            CupertinoPageRoute(
              builder: (context) => const LedgerScreen(),
            ));
      });
    });
  }

  void handlePaymentOnError(PaymentFailureResponse response) {
    //log(response.message.toString());

    alertSnackbar(context, 'Payment Failed');
  }

  void handleExternalWallet(ExternalWalletResponse response) {}
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: Drawer(
        width: size.width * 0.7,
        backgroundColor: LinqPeColors.kPinkColor,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: size.height * 0.3,
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  'AB',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.1,
                      color: LinqPeColors.kWhiteColor,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ),
            ),
            DrawerListTileWidget(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const TermsAndConditionsScreen(),
                    ));
              },
              size: size,
              icon: Icons.tab_rounded,
              tileName: 'Term&Condition',
            ),
            DrawerListTileWidget(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const PrivacyPolicyScreen(),
                    ));
              },
              size: size,
              icon: Icons.policy,
              tileName: 'Policies',
            ),
                  DrawerListTileWidget(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const AboutUsScreen(),
                    ));
              },
              size: size,
              icon: Icons.business,
              tileName: 'About Us',
            ),
          ],
        ),
      ),
      appBar: PreferredSize(
          preferredSize: size * 0.1,
          child: AppBar(
            backgroundColor: LinqPeColors.kPinkColor,
            leadingWidth: size.width,
            leading: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // SizedBox(
                    //   width: size.width * 0.1,
                    // ),
                    Builder(builder: (context) {
                      return IconButton(
                          onPressed: () {
                            Scaffold.of(context).openDrawer();
                          },
                          icon: const Icon(
                            Icons.menu,
                            color: LinqPeColors.kWhiteColor,
                          ));
                    }),

                    SizedBox(
                      width: size.width * 0.7,
                      child: Text('Hello, ${widget.userName}',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              letterSpacing: .8,
                              fontSize: size.width * 0.045,
                              color: LinqPeColors.kWhiteColor.withOpacity(0.9),
                              fontWeight: FontWeight.w500,
                            ),
                          )),
                    ),
                    // Container(
                    //   width: size.width * 0.1,
                    //   height: size.width * 0.1,
                    //   decoration: BoxDecoration(
                    //       borderRadius: BorderRadius.circular(100),
                    //       color: LinqPeColors.kWhiteColor),
                    //   alignment: Alignment.centerRight,
                    //   child: IconButton(
                    //       onPressed: () {
                    //         Navigator.pop(context);
                    //       },
                    //       icon: Icon(
                    //         Icons.arrow_back_rounded,
                    //         size: size.width * 0.07,
                    //       )),
                    // ),
                  ],
                ),
              ],
            ),
          )),
      body: BlocBuilder<UsersBloc, UsersState>(
        builder: (context, userState) {
          if (userState is addTrialDateState) {
            if (userState.isLoading) {
              Future.delayed(Duration.zero, () {
                showLoadingOverlay(context);
              });
            } else if (userState.isError) {
              Future.delayed(Duration.zero, () {
                issueBox(context, size);
              });
            } else if (userState.isSuccess) {
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                Navigator.pushReplacement(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const LedgerScreen(),
                    ));
              });
            }
          }
          return Column(
            children: [
              SizedBox(
                height: size.height * 0.01,
              ),
              SizedBox(
                height: size.height * 0.01,
              ),
              Text('CHOOSE YOUR PLAN',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      letterSpacing: .8,
                      fontSize: size.width * 0.07,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w700,
                    ),
                  )),
              SizedBox(
                height: size.height * 0.05,
              ),
              BlocBuilder<RazorpayBloc, RazorpayState>(
                builder: (context, razorstate) {
                  return PlanCardWidget(
                    cardColor: Colors.amber.shade500,
                    amount: '799',
                    plan: 'PREMIUM',
                    planIcon: Icons.workspace_premium,
                    onTap: () {
                      //
                      BlocProvider.of<RazorpayBloc>(context).add(
                          RazorpayEvent.razorpayPayments(razorpay: razorPay));
                    },
                  );
                },
              ),
              SizedBox(
                height: size.height * 0.05,
              ),
              widget.isPermitted || widget.isNotSatrted
                  ? PlanCardWidget(
                      onTap: () {
                        BlocProvider.of<UsersBloc>(context)
                            .add(const UsersEvent.addUserTrialDate());
                        // Navigator.pushReplacement(
                        //     context,
                        //     CupertinoPageRoute(
                        //       builder: (context) => const LedgerScreen(),
                        //     ));
                      },
                      cardColor: Colors.teal,
                      amount: '0',
                      plan: '7 Day Free Trial',
                      planIcon: Icons.offline_bolt,
                    )
                  : const SizedBox(),
            ],
          );
        },
      ),
    );
  }
}

class PlanCardWidget extends StatelessWidget {
  const PlanCardWidget({
    super.key,
    required this.planIcon,
    required this.plan,
    required this.amount,
    required this.cardColor,
    required this.onTap,
  });
  final IconData planIcon;
  final String plan;
  final String amount;
  final Color cardColor;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return InkWell(
      focusColor: LinqPeColors.kPinkColor,
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: size.width * 0.05),
        height: size.height * 0.25,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(
                colors: [cardColor.withOpacity(0.7), cardColor])),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: size.width * 0.4,
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(100),
                      bottomRight: Radius.circular(100)),
                  gradient: RadialGradient(
                      colors: [cardColor.withOpacity(0.7), cardColor])),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    planIcon,
                    size: size.width * 0.1,
                    color: LinqPeColors.kBlackColor,
                  ),
                  Text(plan,
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          letterSpacing: .01,
                          fontSize: size.width * 0.045,
                          color: LinqPeColors.kWhiteColor,
                          fontWeight: FontWeight.w600,
                        ),
                      )),
                ],
              ),
            ),
            SizedBox(
              width: size.width * 0.1,
            ),
            SizedBox(
              width: size.width * 0.35,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('\$$amount/',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              letterSpacing: .8,
                              fontSize: size.width * 0.08,
                              color: LinqPeColors.kWhiteColor,
                              fontWeight: FontWeight.w600,
                            ),
                          )),
                      Column(
                        children: [
                          SizedBox(
                            height: size.height * 0.01,
                          ),
                          Text('Yr',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  letterSpacing: .1,
                                  fontSize: size.width * 0.045,
                                  color: LinqPeColors.kWhiteColor,
                                  fontWeight: FontWeight.w500,
                                ),
                              )),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
            // Column(
            //   mainAxisAlignment: MainAxisAlignment.end,
            //   children: [
            //     Row(
            //       children: [
            //         Text('Choose Plan',
            //             style: GoogleFonts.poppins(
            //               textStyle: TextStyle(
            //                 letterSpacing: .1,
            //                 fontSize: size.width * 0.03,
            //                 color: LinqPeColors.kWhiteColor,
            //                 fontWeight: FontWeight.w500,
            //               ),
            //             )),
            //         Icon(
            //           Icons.arrow_forward_ios,
            //           size: size.width * 0.05,
            //           color: LinqPeColors.kWhiteColor,
            //         )
            //       ],
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}
