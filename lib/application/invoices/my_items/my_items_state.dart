part of 'my_items_bloc.dart';

@freezed
class MyItemsState with _$MyItemsState {
  const factory MyItemsState.myItemsInitial() = myItemsInitial;
  const factory MyItemsState.displayMyItems({required List<ItemDTO> myItems}) =
      displayMyItems;
}
