part of 'all_invoices_bloc.dart';

@freezed
class AllInvoicesState with _$AllInvoicesState {
  const factory AllInvoicesState.allInvoiceInitial() = allInvoiceInitial;
  const factory AllInvoicesState.displayInvoiceList({required List<InvoiceEditDTO> invoiceList}) =
      displayInvoiceList;
}
