part of 'user_permission_bloc.dart';

@freezed
class UserPermissionState with _$UserPermissionState {
  const factory UserPermissionState.userPermissionInitial() = userPermissionInitial;
  const factory UserPermissionState.isUserPermitted(
      {required bool isPermitted,
      required bool isLoading,
      required bool isNotSatrted}) = isUserPermitted;
}
