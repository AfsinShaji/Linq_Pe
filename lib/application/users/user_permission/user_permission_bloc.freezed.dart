// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_permission_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UserPermissionEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() isUserAccountPermitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? isUserAccountPermitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? isUserAccountPermitted,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(isUserAccountPermitted value)
        isUserAccountPermitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(isUserAccountPermitted value)? isUserAccountPermitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(isUserAccountPermitted value)? isUserAccountPermitted,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserPermissionEventCopyWith<$Res> {
  factory $UserPermissionEventCopyWith(
          UserPermissionEvent value, $Res Function(UserPermissionEvent) then) =
      _$UserPermissionEventCopyWithImpl<$Res, UserPermissionEvent>;
}

/// @nodoc
class _$UserPermissionEventCopyWithImpl<$Res, $Val extends UserPermissionEvent>
    implements $UserPermissionEventCopyWith<$Res> {
  _$UserPermissionEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$isUserAccountPermittedImplCopyWith<$Res> {
  factory _$$isUserAccountPermittedImplCopyWith(
          _$isUserAccountPermittedImpl value,
          $Res Function(_$isUserAccountPermittedImpl) then) =
      __$$isUserAccountPermittedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$isUserAccountPermittedImplCopyWithImpl<$Res>
    extends _$UserPermissionEventCopyWithImpl<$Res,
        _$isUserAccountPermittedImpl>
    implements _$$isUserAccountPermittedImplCopyWith<$Res> {
  __$$isUserAccountPermittedImplCopyWithImpl(
      _$isUserAccountPermittedImpl _value,
      $Res Function(_$isUserAccountPermittedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$isUserAccountPermittedImpl implements isUserAccountPermitted {
  const _$isUserAccountPermittedImpl();

  @override
  String toString() {
    return 'UserPermissionEvent.isUserAccountPermitted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$isUserAccountPermittedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() isUserAccountPermitted,
  }) {
    return isUserAccountPermitted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? isUserAccountPermitted,
  }) {
    return isUserAccountPermitted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? isUserAccountPermitted,
    required TResult orElse(),
  }) {
    if (isUserAccountPermitted != null) {
      return isUserAccountPermitted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(isUserAccountPermitted value)
        isUserAccountPermitted,
  }) {
    return isUserAccountPermitted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(isUserAccountPermitted value)? isUserAccountPermitted,
  }) {
    return isUserAccountPermitted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(isUserAccountPermitted value)? isUserAccountPermitted,
    required TResult orElse(),
  }) {
    if (isUserAccountPermitted != null) {
      return isUserAccountPermitted(this);
    }
    return orElse();
  }
}

abstract class isUserAccountPermitted implements UserPermissionEvent {
  const factory isUserAccountPermitted() = _$isUserAccountPermittedImpl;
}

/// @nodoc
mixin _$UserPermissionState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() userPermissionInitial,
    required TResult Function(
            bool isPermitted, bool isLoading, bool isNotSatrted)
        isUserPermitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? userPermissionInitial,
    TResult? Function(bool isPermitted, bool isLoading, bool isNotSatrted)?
        isUserPermitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? userPermissionInitial,
    TResult Function(bool isPermitted, bool isLoading, bool isNotSatrted)?
        isUserPermitted,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(userPermissionInitial value)
        userPermissionInitial,
    required TResult Function(isUserPermitted value) isUserPermitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(userPermissionInitial value)? userPermissionInitial,
    TResult? Function(isUserPermitted value)? isUserPermitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(userPermissionInitial value)? userPermissionInitial,
    TResult Function(isUserPermitted value)? isUserPermitted,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserPermissionStateCopyWith<$Res> {
  factory $UserPermissionStateCopyWith(
          UserPermissionState value, $Res Function(UserPermissionState) then) =
      _$UserPermissionStateCopyWithImpl<$Res, UserPermissionState>;
}

/// @nodoc
class _$UserPermissionStateCopyWithImpl<$Res, $Val extends UserPermissionState>
    implements $UserPermissionStateCopyWith<$Res> {
  _$UserPermissionStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$userPermissionInitialImplCopyWith<$Res> {
  factory _$$userPermissionInitialImplCopyWith(
          _$userPermissionInitialImpl value,
          $Res Function(_$userPermissionInitialImpl) then) =
      __$$userPermissionInitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$userPermissionInitialImplCopyWithImpl<$Res>
    extends _$UserPermissionStateCopyWithImpl<$Res, _$userPermissionInitialImpl>
    implements _$$userPermissionInitialImplCopyWith<$Res> {
  __$$userPermissionInitialImplCopyWithImpl(_$userPermissionInitialImpl _value,
      $Res Function(_$userPermissionInitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$userPermissionInitialImpl implements userPermissionInitial {
  const _$userPermissionInitialImpl();

  @override
  String toString() {
    return 'UserPermissionState.userPermissionInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$userPermissionInitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() userPermissionInitial,
    required TResult Function(
            bool isPermitted, bool isLoading, bool isNotSatrted)
        isUserPermitted,
  }) {
    return userPermissionInitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? userPermissionInitial,
    TResult? Function(bool isPermitted, bool isLoading, bool isNotSatrted)?
        isUserPermitted,
  }) {
    return userPermissionInitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? userPermissionInitial,
    TResult Function(bool isPermitted, bool isLoading, bool isNotSatrted)?
        isUserPermitted,
    required TResult orElse(),
  }) {
    if (userPermissionInitial != null) {
      return userPermissionInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(userPermissionInitial value)
        userPermissionInitial,
    required TResult Function(isUserPermitted value) isUserPermitted,
  }) {
    return userPermissionInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(userPermissionInitial value)? userPermissionInitial,
    TResult? Function(isUserPermitted value)? isUserPermitted,
  }) {
    return userPermissionInitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(userPermissionInitial value)? userPermissionInitial,
    TResult Function(isUserPermitted value)? isUserPermitted,
    required TResult orElse(),
  }) {
    if (userPermissionInitial != null) {
      return userPermissionInitial(this);
    }
    return orElse();
  }
}

abstract class userPermissionInitial implements UserPermissionState {
  const factory userPermissionInitial() = _$userPermissionInitialImpl;
}

/// @nodoc
abstract class _$$isUserPermittedImplCopyWith<$Res> {
  factory _$$isUserPermittedImplCopyWith(_$isUserPermittedImpl value,
          $Res Function(_$isUserPermittedImpl) then) =
      __$$isUserPermittedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isPermitted, bool isLoading, bool isNotSatrted});
}

/// @nodoc
class __$$isUserPermittedImplCopyWithImpl<$Res>
    extends _$UserPermissionStateCopyWithImpl<$Res, _$isUserPermittedImpl>
    implements _$$isUserPermittedImplCopyWith<$Res> {
  __$$isUserPermittedImplCopyWithImpl(
      _$isUserPermittedImpl _value, $Res Function(_$isUserPermittedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isPermitted = null,
    Object? isLoading = null,
    Object? isNotSatrted = null,
  }) {
    return _then(_$isUserPermittedImpl(
      isPermitted: null == isPermitted
          ? _value.isPermitted
          : isPermitted // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isNotSatrted: null == isNotSatrted
          ? _value.isNotSatrted
          : isNotSatrted // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$isUserPermittedImpl implements isUserPermitted {
  const _$isUserPermittedImpl(
      {required this.isPermitted,
      required this.isLoading,
      required this.isNotSatrted});

  @override
  final bool isPermitted;
  @override
  final bool isLoading;
  @override
  final bool isNotSatrted;

  @override
  String toString() {
    return 'UserPermissionState.isUserPermitted(isPermitted: $isPermitted, isLoading: $isLoading, isNotSatrted: $isNotSatrted)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$isUserPermittedImpl &&
            (identical(other.isPermitted, isPermitted) ||
                other.isPermitted == isPermitted) &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isNotSatrted, isNotSatrted) ||
                other.isNotSatrted == isNotSatrted));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isPermitted, isLoading, isNotSatrted);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$isUserPermittedImplCopyWith<_$isUserPermittedImpl> get copyWith =>
      __$$isUserPermittedImplCopyWithImpl<_$isUserPermittedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() userPermissionInitial,
    required TResult Function(
            bool isPermitted, bool isLoading, bool isNotSatrted)
        isUserPermitted,
  }) {
    return isUserPermitted(isPermitted, isLoading, isNotSatrted);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? userPermissionInitial,
    TResult? Function(bool isPermitted, bool isLoading, bool isNotSatrted)?
        isUserPermitted,
  }) {
    return isUserPermitted?.call(isPermitted, isLoading, isNotSatrted);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? userPermissionInitial,
    TResult Function(bool isPermitted, bool isLoading, bool isNotSatrted)?
        isUserPermitted,
    required TResult orElse(),
  }) {
    if (isUserPermitted != null) {
      return isUserPermitted(isPermitted, isLoading, isNotSatrted);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(userPermissionInitial value)
        userPermissionInitial,
    required TResult Function(isUserPermitted value) isUserPermitted,
  }) {
    return isUserPermitted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(userPermissionInitial value)? userPermissionInitial,
    TResult? Function(isUserPermitted value)? isUserPermitted,
  }) {
    return isUserPermitted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(userPermissionInitial value)? userPermissionInitial,
    TResult Function(isUserPermitted value)? isUserPermitted,
    required TResult orElse(),
  }) {
    if (isUserPermitted != null) {
      return isUserPermitted(this);
    }
    return orElse();
  }
}

abstract class isUserPermitted implements UserPermissionState {
  const factory isUserPermitted(
      {required final bool isPermitted,
      required final bool isLoading,
      required final bool isNotSatrted}) = _$isUserPermittedImpl;

  bool get isPermitted;
  bool get isLoading;
  bool get isNotSatrted;
  @JsonKey(ignore: true)
  _$$isUserPermittedImplCopyWith<_$isUserPermittedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
