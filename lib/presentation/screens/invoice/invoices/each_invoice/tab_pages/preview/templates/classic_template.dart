import 'dart:developer';

import 'package:flutter/services.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/datetime_to_date_string_converter.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

Future<Uint8List> classicPreviewPdf(
    PdfColor previewBasecolor, InvoiceEditDTO invoice, Size size) async {
  final pdf = pw.Document();

  double subTotal = 0.0;
  double discountAmount = 0.00;
  double tax = 0.00;
  double totalAmount = 0.0;
  double totalPaymentAmount = 0.0;
  double balanceDue = 0.0;
  // ByteData fontdata = await rootBundle.load("assets/fonts/IndianRupee.ttf");

  //start
  if (invoice.itemsList != null) {
    for (var items in invoice.itemsList!) {
      subTotal = subTotal + items.total;
    }
  }

  if (invoice.discountDetails != null) {
    if (invoice.discountDetails!.discountType == 'Percentage' &&
        invoice.discountDetails!.percentage > 0) {
      discountAmount = subTotal * (invoice.discountDetails!.percentage / 100);
    } else if (invoice.discountDetails!.discountType == 'Flat Amount' &&
        invoice.discountDetails!.percentage > 0) {
      discountAmount = invoice.discountDetails!.percentage;
    }
  }

  if (invoice.tax != null &&
      invoice.itemsList != null &&
      invoice.itemsList!.isNotEmpty) {
    log(invoice.tax!.taxType);
    if (invoice.tax!.taxType == 'Deducted') {
      double deductingTotal = 0.0;
      for (var items in invoice.itemsList!) {
        if (items.isTaxable) {
          deductingTotal = deductingTotal + items.total;
        }
      }
      if (deductingTotal > 0 && invoice.tax!.rate != null) {
        tax = (deductingTotal - discountAmount) * (invoice.tax!.rate! / 100);
        tax = -tax;
      }
    } else if (invoice.tax!.taxType == 'On The Total') {
      double deductingTotal = 0.0;
      for (var items in invoice.itemsList!) {
        if (items.isTaxable) {
          //  log(items.total.toString());
          deductingTotal = deductingTotal + items.total;
        }
      }
      log('deduct${deductingTotal.toString()}');
      if (deductingTotal > 0 && invoice.tax!.rate != null) {
        log('discount${discountAmount.toString()}');
        tax = (deductingTotal - discountAmount) * (invoice.tax!.rate! / 100);
      }
      if (invoice.tax!.isInclusive) {
        tax = tax / (1 + (invoice.tax!.rate! / 100));
        //decimel rounding
        tax = double.parse(tax.toStringAsFixed(2));
      }
    } else if (invoice.tax!.taxType == 'Per Item') {
      double totalTax = 0.00;
      for (var items in invoice.itemsList!) {
        if (items.isTaxable && items.taxRate != null && items.taxRate! > 0) {
          double eachTotal = (items.total * (items.taxRate! / 100));
          if (invoice.tax!.isInclusive) {
            eachTotal = eachTotal / (1 + (items.taxRate! / 100));
          }
          totalTax = totalTax + eachTotal;
        }
      }
      tax = totalTax;
      //decimel rounding
      tax = double.parse(tax.toStringAsFixed(2));
    }
  }
  if (invoice.paymentsList != null && invoice.paymentsList!.isNotEmpty) {
    for (var payment in invoice.paymentsList!) {
      totalPaymentAmount = totalPaymentAmount + payment.amount;
    }
  }

  totalAmount = (subTotal - (discountAmount));
  if (invoice.tax != null &&
      (invoice.tax!.isInclusive == false ||
          invoice.tax!.taxType == 'Deducted')) {
    totalAmount = totalAmount + tax;
  }
  totalAmount = double.parse(totalAmount.toStringAsFixed(2));
  final taxable = totalAmount - tax;

  balanceDue = totalAmount - totalPaymentAmount;
  balanceDue = double.parse(balanceDue.toStringAsFixed(2));
//final  images =await decodeImageFromList((  invoice.businessDetails!.businessLogo!.readAsBytesSync());

//end
  double fullWidth = 0.0;
  pw.Widget subRow({required String field, required String amount}) {
    return pw.Container(
        width: fullWidth * 0.4,
        child: pw.Row(
            mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
            children: [
              pw.Text(field, style: pw.TextStyle(fontSize: fullWidth * 0.028)),
              pw.Text(amount, style: pw.TextStyle(fontSize: fullWidth * 0.028)),
            ]));
  }

  pdf.addPage(pw.Page(
      pageFormat: PdfPageFormat.a4,
      //   pageFormat: PdfPageFormat(size.width * 1.6, size.height * 1.5),
      build: (pw.Context context) {
        fullWidth = context.page.pageFormat.width / 2;
        double fullHeight = context.page.pageFormat.height;
        return pw.Container(
            color: PdfColors.white,
            padding: pw.EdgeInsets.all(fullWidth * 0.0),
            child: pw.Column(
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: [
                  pw.Container(
                      width: fullWidth * 1.6,
                      height: fullHeight * 0.001,
                      decoration: pw.BoxDecoration(
                        color: previewBasecolor,
                      )),
                  pw.SizedBox(height: fullHeight * 0.005),
                  pw.Row(
                      crossAxisAlignment: pw.CrossAxisAlignment.start,
                      children: [
                        invoice.businessDetails != null &&
                                invoice.businessDetails!.businessLogo != null
                            ? pw.Image(
                                pw.MemoryImage(
                                  invoice.businessDetails!.businessLogo!
                                      .readAsBytesSync(),
                                ),
                                width: fullWidth * 0.15)
                            : pw.SizedBox(width: fullWidth * 0.15),
                        pw.SizedBox(width: fullWidth * 0.05),
                        invoice.businessDetails != null
                            ? pw.Column(
                                crossAxisAlignment: pw.CrossAxisAlignment.start,
                                children: [
                                    pw.Text(
                                        invoice.businessDetails!.businessName,
                                        style: pw.TextStyle(
                                            fontWeight: pw.FontWeight.bold,
                                            fontSize: fullWidth * 0.03)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!
                                                .businessOwnerName ==
                                            null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice.businessDetails!
                                                .businessOwnerName!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!.businessNumber ==
                                            null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice.businessDetails!
                                                .businessNumber!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!.addressLine1 ==
                                            null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice
                                                .businessDetails!.addressLine1!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!.addressLine2 ==
                                            null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice
                                                .businessDetails!.addressLine2!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!.addressLine3 ==
                                            null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice
                                                .businessDetails!.addressLine3!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!.phone == null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice.businessDetails!.phone!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!.mobile == null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice.businessDetails!.mobile!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!.website == null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice.businessDetails!.website!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                    invoice.businessDetails!.email == null
                                        ? pw.SizedBox()
                                        : pw.Text(
                                            invoice.businessDetails!.email!,
                                            style: pw.TextStyle(
                                                fontSize: fullWidth * 0.028)),
                                    pw.SizedBox(height: fullHeight * 0.005),
                                  ])
                            : pw.SizedBox(),
                        pw.SizedBox(width: fullWidth * 0.9),
                        pw.Column(
                            mainAxisAlignment: pw.MainAxisAlignment.start,
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            children: [
                              pw.Text('INVOICE',
                                  style: pw.TextStyle(
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.005),
                              pw.Text(
                                  invoice.invoiceNumberDetails.invoiceNumber,
                                  style: pw.TextStyle(
                                      fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.01),
                              pw.Text('DATE',
                                  style: pw.TextStyle(
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.005),
                              pw.Text(
                                  dateTimeToStringConverter(
                                      invoice.invoiceNumberDetails.invoiceDate),
                                  style: pw.TextStyle(
                                      fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.01),
                              invoice.invoiceNumberDetails.dueDate == null ||
                                      invoice.invoiceNumberDetails.terms ==
                                          null ||
                                      invoice.invoiceNumberDetails.terms ==
                                          'None'
                                  ? pw.SizedBox()
                                  : pw.Text('DUE DATE',
                                      style: pw.TextStyle(
                                          fontWeight: pw.FontWeight.bold,
                                          fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.005),
                              invoice.invoiceNumberDetails.dueDate == null ||
                                      invoice.invoiceNumberDetails.terms ==
                                          null ||
                                      invoice.invoiceNumberDetails.terms ==
                                          'None'
                                  ? pw.SizedBox()
                                  : pw.Text(
                                      dateTimeToStringConverter(invoice
                                          .invoiceNumberDetails.dueDate!),
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.01),
                              invoice.invoiceNumberDetails.poNumber == null ||
                                      invoice.invoiceNumberDetails.poNumber!
                                          .isEmpty
                                  ? pw.SizedBox()
                                  : pw.Text('PO #',
                                      style: pw.TextStyle(
                                          fontWeight: pw.FontWeight.bold,
                                          fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.005),
                              invoice.invoiceNumberDetails.poNumber == null ||
                                      invoice.invoiceNumberDetails.poNumber!
                                          .isEmpty
                                  ? pw.SizedBox()
                                  : pw.Text(
                                      invoice.invoiceNumberDetails.poNumber!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.01),
                              pw.Text('BALANCE DUE',
                                  style: pw.TextStyle(
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.01),
                              pw.Row(children: [
                                pw.Text('INR $balanceDue',
                                    style: pw.TextStyle(
                                        fontSize: fullWidth * 0.028)),
                              ])
                            ])
                      ]),
                  // pw.SizedBox(height: fullHeight * 0.01),
                  pw.Divider(),
                  invoice.client != null
                      ? pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                              pw.Text('BILL TO',
                                  style: pw.TextStyle(
                                      fontSize: fullWidth * 0.028)),
                              pw.Text(invoice.client!.clientName,
                                  style: pw.TextStyle(
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028)),
                              pw.SizedBox(height: fullHeight * 0.01),
                              invoice.client!.address1 == null
                                  ? pw.SizedBox()
                                  : pw.Text(invoice.client!.address1!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                              invoice.client!.address2 == null
                                  ? pw.SizedBox()
                                  : pw.Text(invoice.client!.address2!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                              invoice.client!.address3 == null
                                  ? pw.SizedBox()
                                  : pw.Text(invoice.client!.address3!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                              invoice.client!.mobileNumber == null
                                  ? pw.SizedBox()
                                  : pw.Text(invoice.client!.mobileNumber!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                              invoice.client!.phoneNumber == null
                                  ? pw.SizedBox()
                                  : pw.Text(invoice.client!.phoneNumber!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                              invoice.client!.faxNumber == null
                                  ? pw.SizedBox()
                                  : pw.Text(invoice.client!.faxNumber!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                              invoice.client!.clientEmail == null
                                  ? pw.SizedBox()
                                  : pw.Text(invoice.client!.clientEmail!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028)),
                            ])
                      : pw.SizedBox(),
                  pw.SizedBox(height: fullHeight * 0.02),
                  pw.Table(
                      border: pw.TableBorder.all(color: PdfColors.black),
                      children: List.generate(
                          invoice.itemsList == null
                              ? 1
                              : invoice.itemsList!.length + 1, (index) {
                        if (index == 0) {
                          return pw.TableRow(children: [
                            pw.Container(
                                padding: pw.EdgeInsets.only(
                                    left: fullWidth * 0.025,
                                    top: fullWidth * 0.03),
                                width: fullWidth * 0.2,
                                height: fullHeight * 0.03,
                                // alignment: pw.Alignment.start,
                                child: pw.Text(
                                  'DESCRIPTION',
                                  style: pw.TextStyle(
                                      color: PdfColors.black,
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028),
                                )),
                            pw.Container(
                                height: fullHeight * 0.03,
                                alignment: pw.Alignment.center,
                                child: pw.Text(
                                  'Rate'.toUpperCase(),
                                  style: pw.TextStyle(
                                      color: PdfColors.black,
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028),
                                )),
                            pw.Container(
                                height: fullHeight * 0.03,
                                alignment: pw.Alignment.center,
                                child: pw.Text(
                                  'QTY',
                                  style: pw.TextStyle(
                                      color: PdfColors.black,
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028),
                                )),
                            pw.Container(
                                height: fullHeight * 0.03,
                                alignment: pw.Alignment.center,
                                child: pw.Text(
                                  'discount'.toUpperCase(),
                                  style: pw.TextStyle(
                                      color: PdfColors.black,
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028),
                                )),
                            invoice.tax != null &&
                                    invoice.tax!.taxType == 'Per Item'
                                ? pw.Container(
                                    height: fullHeight * 0.03,
                                    alignment: pw.Alignment.center,
                                    child: pw.Text(
                                      invoice.tax!.label,
                                      style: pw.TextStyle(
                                          color: PdfColors.black,
                                          fontWeight: pw.FontWeight.bold,
                                          fontSize: fullWidth * 0.028),
                                    ))
                                : pw.SizedBox(),
                            pw.Container(
                                height: fullHeight * 0.03,
                                alignment: pw.Alignment.center,
                                child: pw.Text(
                                  'Amount'.toUpperCase(),
                                  style: pw.TextStyle(
                                      color: PdfColors.black,
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028),
                                )),
                          ]);
                        }
                        final newIndex = index - 1;
                        if (invoice.itemsList != null &&
                            invoice.itemsList!.isNotEmpty) {
                          // subTotal=subTotal+invoice.itemsList![newIndex].total;
                          String discountAmounts = '';
                          String discountPercentage = '';
                          if (invoice.itemsList![newIndex].discount != null &&
                              invoice.itemsList![newIndex].discount !=
                                  'No Discount' &&
                              invoice.itemsList![newIndex].discountAmount !=
                                  null &&
                              invoice.itemsList![newIndex].discountAmount! >
                                  0) {
                            if (invoice.itemsList![newIndex].discount ==
                                'Percentage') {
                              final discount =
                                  (invoice.itemsList![newIndex].unitCost) *
                                      (invoice.itemsList![newIndex].quantity) *
                                      ((invoice.itemsList![newIndex]
                                                  .discountAmount ??
                                              0) /
                                          100);
                              discountAmounts = 'INR $discount';
                              discountPercentage =
                                  '${invoice.itemsList![newIndex].discountAmount!}%';
                            } else if (invoice.itemsList![newIndex].discount ==
                                'Flat Amount') {
                              final discount = ((invoice
                                          .itemsList![newIndex].unitCost) *
                                      (invoice.itemsList![newIndex].quantity)) -
                                  invoice.itemsList![newIndex].discountAmount!;
                              discountAmounts = 'INR $discount';
                              discountPercentage =
                                  'INR -${invoice.itemsList![newIndex].discountAmount!}';
                            }
                          }
                          log(discountPercentage);
                          return pw.TableRow(children: [
                            pw.Container(
                                padding: pw.EdgeInsets.only(
                                    left: fullWidth * 0.025,
                                    top: fullWidth * 0.01),
                                height: fullHeight * 0.03,
                                child: pw.Text(
                                    invoice.itemsList![newIndex].description,
                                    style: pw.TextStyle(
                                        fontSize: fullWidth * 0.028))),
                            pw.Container(
                                height: fullHeight * 0.03,
                                alignment: pw.Alignment.center,
                                child: pw.Text(
                                    'INR ${invoice.itemsList![newIndex].unitCost.toString()}',
                                    style: pw.TextStyle(
                                        fontSize: fullWidth * 0.028))),
                            pw.Container(
                                height: fullHeight * 0.03,
                                alignment: pw.Alignment.center,
                                child: pw.Text(
                                    invoice.itemsList![newIndex].quantity
                                        .toString(),
                                    style: pw.TextStyle(
                                        fontSize: fullWidth * 0.028))),
                            pw.Container(
                                height: fullHeight * 0.04,
                                alignment: pw.Alignment.center,
                                child: pw.Column(
                                    mainAxisAlignment:
                                        pw.MainAxisAlignment.center,
                                    children: [
                                      pw.Text(discountAmounts,
                                          style: pw.TextStyle(
                                              fontSize: fullWidth * 0.028)),
                                      pw.Text(discountPercentage,
                                          style: pw.TextStyle(
                                              fontSize: fullWidth * 0.028))
                                    ])),
                            invoice.tax != null &&
                                    invoice.tax!.taxType == 'Per Item'
                                ? pw.Container(
                                    height: fullHeight * 0.04,
                                    alignment: pw.Alignment.center,
                                    child: pw.Column(
                                       mainAxisAlignment: pw.MainAxisAlignment.center,
                                      children: [
                                      pw.Text(
                                        invoice.tax != null &&
                                                invoice.tax!.isInclusive
                                            ? 'INR ${(((invoice.itemsList![newIndex].total) * 
                                            ((invoice.itemsList![newIndex].taxRate) ?? 0 / 100))
                                             / (1 + ((invoice.itemsList![newIndex].taxRate) ?? 0 / 100))).toStringAsFixed(2)}'
                                            : 'INR ${((invoice.itemsList![newIndex].total) *
                                             ((invoice.itemsList![newIndex].taxRate) ?? 0 / 100)).toStringAsFixed(2)}',
                                        style: pw.TextStyle(
                                            color: PdfColors.black,
                                            fontSize: fullWidth * 0.028),
                                      ),
                                      pw.Text(
                                          '${invoice.itemsList![newIndex].taxRate ?? 0}%',
                                          style: pw.TextStyle(
                                              fontSize: fullWidth * 0.028))
                                    ]))
                                : pw.SizedBox(),
                            pw.Container(
                                height: fullHeight * 0.03,
                                alignment: pw.Alignment.center,
                                child: pw.Text(
                                    'INR ${invoice.itemsList![newIndex].total}',
                                    style: pw.TextStyle(
                                        fontSize: fullWidth * 0.028)))
                          ]);
                        } else {
                          return const pw.TableRow(children: []);
                        }
                      })),
                  pw.Divider(),
                  pw.SizedBox(
                    height: fullHeight * 0.01,
                  ),
                  pw.Row(
                      crossAxisAlignment: pw.CrossAxisAlignment.start,
                      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                      children: [
                        pw.Column(
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            children: [
                              pw.Text('* Indicates non-taxable line item',
                                  style: pw.TextStyle(
                                      fontSize: fullWidth * 0.024)),
                              pw.SizedBox(
                                height: fullHeight * 0.01,
                              ),
                              pw.Text('Payment Info',
                                  style: pw.TextStyle(
                                      fontWeight: pw.FontWeight.bold,
                                      fontSize: fullWidth * 0.028)),
                              pw.SizedBox(
                                height: fullHeight * 0.01,
                              ),
                              invoice.paymentInfo != null &&
                                      invoice.paymentInfo!.paypalEmail != null
                                  ? pw.Text('PAYPAL',
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                              pw.SizedBox(
                                height: fullHeight * 0.001,
                              ),
                              invoice.paymentInfo != null &&
                                      invoice.paymentInfo!.paypalEmail != null
                                  ? pw.Text(invoice.paymentInfo!.paypalEmail!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                              pw.SizedBox(
                                height: fullHeight * 0.01,
                              ),
                              invoice.paymentInfo != null &&
                                      invoice.paymentInfo!
                                              .paymentInstructions !=
                                          null
                                  ? pw.Text('PAYMENT INSTRUCTONS',
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                              pw.SizedBox(
                                height: fullHeight * 0.001,
                              ),
                              invoice.paymentInfo != null &&
                                      invoice.paymentInfo!
                                              .paymentInstructions !=
                                          null
                                  ? pw.Text(
                                      invoice.paymentInfo!.paymentInstructions!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                              pw.SizedBox(
                                height: fullHeight * 0.01,
                              ),
                              invoice.paymentInfo != null &&
                                      invoice.paymentInfo!.businessName != null
                                  ? pw.Text('BY CHECK',
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                              pw.SizedBox(
                                height: fullHeight * 0.001,
                              ),
                              invoice.paymentInfo != null &&
                                      invoice.paymentInfo!.businessName != null
                                  ? pw.Text(invoice.paymentInfo!.businessName!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                              pw.SizedBox(
                                height: fullHeight * 0.01,
                              ),
                              invoice.paymentInfo != null &&
                                      invoice.paymentInfo!
                                              .additionalPaymentInstructions !=
                                          null
                                  ? pw.Text('OTHER',
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                              pw.SizedBox(
                                height: fullHeight * 0.001,
                              ),
                              invoice.paymentInfo != null &&
                                      invoice.paymentInfo!
                                              .additionalPaymentInstructions !=
                                          null
                                  ? pw.Text(
                                      invoice.paymentInfo!
                                          .additionalPaymentInstructions!,
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                            ]),
                        pw.Column(
                            mainAxisAlignment: pw.MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            children: [
                              subRow(
                                  field: 'SUBTOTAL', amount: 'INR $subTotal'),
                              pw.SizedBox(height: fullHeight * 0.01),
                              invoice.tax != null && invoice.tax!.isInclusive
                                  ? subRow(
                                      field: 'TAXABLE', amount: 'INR $taxable')
                                  : pw.SizedBox(),
                              pw.SizedBox(height: fullHeight * 0.01),
                              subRow(
                                  field: 'DISCOUNT',
                                  amount: 'INR -$discountAmount'),
                              pw.SizedBox(height: fullHeight * 0.01),
                              subRow(
                                  field: invoice.tax != null
                                      ? invoice.tax!.label
                                      : '',
                                  amount: invoice.tax != null &&
                                          invoice.tax!.isInclusive
                                      ? 'Inc INR $tax'
                                      : 'INR $tax'),
                              pw.SizedBox(height: fullHeight * 0.01),
                              subRow(
                                  field: 'TOTAL', amount: 'INR $totalAmount'),
                              pw.SizedBox(height: fullHeight * 0.01),
                              subRow(
                                  field: 'PAID',
                                  amount: 'INR $totalPaymentAmount'),
                              pw.SizedBox(height: fullHeight * 0.01),
                              pw.Container(
                                  color: PdfColors.grey200,
                                  width: fullWidth * 0.43,
                                  padding: pw.EdgeInsets.symmetric(
                                      horizontal: fullWidth * 0.01),
                                  height: fullHeight * 0.04,
                                  child: pw.Row(
                                      mainAxisAlignment:
                                          pw.MainAxisAlignment.spaceBetween,
                                      children: [
                                        pw.Text('BALANCE DUE',
                                            style: pw.TextStyle(
                                                fontWeight: pw.FontWeight.bold,
                                                fontSize: fullWidth * 0.028)),
                                        pw.Text('INR $balanceDue',
                                            style: pw.TextStyle(
                                                fontWeight: pw.FontWeight.bold,
                                                fontSize: fullWidth * 0.028)),
                                      ])),
                              pw.SizedBox(height: fullHeight * 0.01),
                              invoice.signature != null &&
                                      invoice.signature!.signature != null
                                  ? pw.Image(
                                      pw.MemoryImage(
                                        invoice.signature!.signature!
                                            .readAsBytesSync(),
                                      ),
                                      width: fullWidth * 0.15)
                                  : pw.SizedBox(),
                              pw.SizedBox(height: fullHeight * 0.01),
                              invoice.signature != null &&
                                      invoice.signature!.signature != null
                                  ? pw.Text('DATE SIGNED',
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                              invoice.signature != null &&
                                      invoice.signature!.signature != null
                                  ? pw.Text(
                                      dateTimeToStringConverter(
                                          invoice.signature!.signDate!),
                                      style: pw.TextStyle(
                                          fontSize: fullWidth * 0.028))
                                  : pw.SizedBox(),
                            ])
                      ]),
                  pw.SizedBox(height: fullHeight * 0.01),
                  invoice.reviewLink != null && invoice.reviewLink!.isNotEmpty
                      ? pw.Text('Please leave a rating/review on',
                          style: pw.TextStyle(fontSize: fullWidth * 0.028))
                      : pw.SizedBox(),
                  invoice.reviewLink != null && invoice.reviewLink!.isNotEmpty
                      ? pw.Text(invoice.reviewLink!,
                          style: pw.TextStyle(fontSize: fullWidth * 0.028))
                      : pw.SizedBox(),
                  pw.SizedBox(height: fullHeight * 0.01),
                  invoice.notes != null && invoice.notes!.notes.isNotEmpty
                      ? pw.Text(invoice.notes!.notes,
                          style: pw.TextStyle(fontSize: fullWidth * 0.028))
                      : pw.SizedBox(),
                  pw.SizedBox(height: fullHeight * 0.01),
                  invoice.photoList != null && invoice.photoList!.isNotEmpty
                      ? pw.Row(
                          children: List.generate(
                              invoice.photoList!.length,
                              (index) => pw.Image(
                                  pw.MemoryImage(
                                    invoice.photoList![index].photo
                                        .readAsBytesSync(),
                                  ),
                                  width: fullWidth * 0.15)))
                      : pw.SizedBox(),
                ])); // Center
      }));

// pdf.addPage(pw.Page(build: (context) => pw.Column(children: [
//   //pw.SizedBox(height * 0.01),
//           invoice.notes != null && invoice.notes!.notes.isNotEmpty
//               ? pw.Text(invoice.notes!.notes)
//               : pw.SizedBox(),
//         //  pw.SizedBox(height: fullHeight * 0.01),
//           invoice.photoList != null && invoice.photoList!.isNotEmpty
//               ? pw.Row(
//                   children: List.generate(
//                       invoice.photoList!.length,
//                       (index) => pw.Image(
//                             pw.MemoryImage(invoice.photoList![index].photo
//                                 .readAsBytesSync(),),width: fullWidth*0.15
//                           )))
//               : pw.SizedBox(),]),));
  return pdf.save(); // Page
}
