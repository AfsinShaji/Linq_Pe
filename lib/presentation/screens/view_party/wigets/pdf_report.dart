import 'dart:io';
import 'dart:typed_data';

import 'package:open_file_plus/open_file_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

Future<void> generateAndSavePdf(Uint8List image,double height,double width) async {
  final pdf = pw.Document();

  // Capture the current screen as an image


  // Add the captured image to the PDF
  pdf.addPage(
    pw.Page(
      pageFormat: PdfPageFormat(width, height),
      build: (pw.Context context) {
        return 
        pw.Center(
          child:
          pw.Image(pw.MemoryImage(image),fit: pw.BoxFit.contain)
          ,
        )
        ;
      },
    ),
  );

  // Save the PDF to a file
  final output = await getTemporaryDirectory();
  final file = File('${output.path}/report.pdf');
  await file.writeAsBytes(await pdf.save());

  // Open the PDF using a PDF viewer or any preferred method
  // For example, you can use the 'open_file' package:
  await OpenFile.open(file.path);
}




