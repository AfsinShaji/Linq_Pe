
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/photo/screen_photo.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/business_details.dart';
import 'package:linq_pe/utilities/colors.dart';

openImagesPickingSheet(
  BuildContext context,
  Size size,
  bool isFromBusinessDetails,
  bool isToEdit,
  WidgetRef? ref,String? imageId
) {
  showModalBottomSheet(
    backgroundColor: LinqPeColors.kWhiteColor,
    context: context,
    builder: (context) => Container(
      decoration: const BoxDecoration(
          color: LinqPeColors.kWhiteColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50), topRight: Radius.circular(50))),
      height: size.height * 0.2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  InkWell(
                    onTap: () async {
                      await openCamera().then((value) {
                        if (value != null) {
                          if (isFromBusinessDetails) {
                            addBusinessDetailLogo(value, ref!);
                          } else {
                            if(isToEdit){
                                 Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      PhotoScreen(imageFilePath: value,isToEdit: isToEdit),
                                ));
                            }
                     else   {    Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      PhotoScreen(imageFilePath: value,isToEdit: isToEdit),
                                ));}
                          }
                        }
                      });
                    },
                    child: Container(
                      height: size.height * 0.1,
                      width: size.width * 0.23,
                      decoration: BoxDecoration(
                        color: LinqPeColors.kPinkColor,
                        borderRadius: BorderRadius.circular(
                          15,
                        ),
                      ),
                      child: Icon(
                        Icons.camera,
                        color: LinqPeColors.kWhiteColor,
                        size: size.width * 0.1,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  Text('Camera',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.036,
                          color: LinqPeColors.kBlackColor,
                          fontWeight: FontWeight.w500,
                        ),
                      )),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(),
                child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        // openGallery();
                        await openGallery().then((value) {
                          if (value != null) {
                            if (isFromBusinessDetails) {
                              addBusinessDetailLogo(value, ref!);
                            } else {
                                  if(isToEdit){
                                 Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      PhotoScreen(imageFilePath: value,isToEdit: isToEdit,
                                      imageId: imageId!),
                                ));
                            }
                     else   {    Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      PhotoScreen(imageFilePath: value,isToEdit: isToEdit),
                                ));}
                            }
                          }
                        });
                      },
                      child: Container(
                        height: size.height * 0.1,
                        width: size.width * 0.23,
                        decoration: BoxDecoration(
                          color: LinqPeColors.kPinkColor,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Icon(
                          Icons.image,
                          color: LinqPeColors.kWhiteColor,
                          size: size.width * 0.1,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Text('Gallery',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            letterSpacing: .5,
                            fontSize: size.width * 0.036,
                            color: LinqPeColors.kBlackColor,
                            fontWeight: FontWeight.w500,
                          ),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}

Future<String?> openCamera() async {
  final picker = ImagePicker();
  final pickedImage = await picker.pickImage(source: ImageSource.camera);

  if (pickedImage != null) {
    return pickedImage.path;
    // addBusinessLogo(pickedImage, ref);
    // Use the picked file (e.g., upload or display the image)
  }
  return null;
}

Future<String?> openGallery() async {
  final picker = ImagePicker();
  final pickedImage = await picker.pickImage(source: ImageSource.gallery);

  if (pickedImage != null) {
    return pickedImage.path;
    //  addBusinessLogo(pickedImage, ref);
    // Use the picked file (e.g., upload or display the image)
  }
  return null;
}
