import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/screen_invoices.dart';
import 'package:linq_pe/presentation/screens/invoice/reports/screen_invoice_reports.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

class InvoiceScreen extends ConsumerWidget {
  const InvoiceScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    final listOfPages = [
      const InvoicesScreen(
        isEstimate: false,
        isInvoice: true,
      ),
      const InvoicesScreen(
        isEstimate: true,
        isInvoice: false,
      ),
      const InvoiceReportsScreen(),
    ];
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: LinqPeColors.kPinkColor,
        onTap: (value) {
          addinvoicePage(value, ref);
        },
        selectedItemColor: LinqPeColors.kWhiteColor,
        currentIndex: ref.watch(invoicePageProvider),
        selectedFontSize: size.width * 0.04,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.book,
            ),
            label: 'Invoices',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calculate_outlined),
            label: 'Estimates',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.bar_chart),
            label: 'Reports',
          ),
        ],
      ),
      body: listOfPages.elementAt(ref.watch(invoicePageProvider)),
    );
  }
}
