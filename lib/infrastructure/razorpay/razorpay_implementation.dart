import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:linq_pe/domain/repositories/razorpay/razorpay_repository.dart';
import 'package:linq_pe/infrastructure/users/users_implementation.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class RazorPayImplementation extends RazorPayRepository {
  // creating a singleton
  RazorPayImplementation.internal();
  static RazorPayImplementation instance = RazorPayImplementation.internal();
  RazorPayImplementation factory() {
    return instance;
  }

///////////////////////////////////////////
  @override
  Future<String> getRazorPayKey() async {
    final dataColllection = FirebaseFirestore.instance.collection('AppData');
    final razopayDocument = dataColllection.doc('razorpay');
    final dataMap = await razopayDocument.get();
    String key = '';
    if (dataMap.data() != null) {
      key = dataMap.data()!['key'];
    }

    return key;
  }

  @override
  Future<void> razorpayPayment({required Razorpay razorpay}) async {
    final razorpayKey = await getRazorPayKey();
    final userResponse = await UsersImplementation.instance.getUserDetails();

    if (razorpayKey.isNotEmpty) {
      userResponse.fold((l) => null, (user) {
        Map<String, dynamic> options = {
          'key': razorpayKey,
          'amount': 799 * 100,
          'name': 'Accounts Book',
          'timeout': 300,
          'description': 'Premium',
          'prefill': {'contact': user.phoneNumber, 'email': user.userEmail},
        };
        try {
          razorpay.open(options);
        } catch (e) {
          log(e.toString());
          return;
        }
        razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, (PaymentFailureResponse response) {
               log('Error');
          return;
     
        });
      });
    }
  }
}
