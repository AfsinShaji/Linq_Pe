import 'package:flutter/cupertino.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size =MediaQuery.of(context).size;
    return Center(
      child: Image.asset('assets/images/ab-high-resolution-logo-removebg-preview.png',
      height: size.height*0.25,
      ),
    );
  }
}