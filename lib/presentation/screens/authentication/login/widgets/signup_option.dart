import 'package:flutter/cupertino.dart';
import 'package:linq_pe/presentation/screens/authentication/registration/registration.dart';
import 'package:linq_pe/utilities/colors.dart';



class SignupOption extends StatelessWidget {
  const SignupOption({
    super.key,
  });

  @override
  
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Don'thave an account?",
          style: TextStyle(
            color: LinqPeColors.kWhiteColor,
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.pushReplacement(
              context,
              CupertinoPageRoute(
                builder: (context) => RegistrationScreen(),
              ),
            );
          },
          child: const Text(
            "Sign up",
            style: TextStyle(
              color:LinqPeColors.kWhiteColor ,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}