import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/utilities/colors.dart';
//
class ReportPdf extends StatelessWidget {
  const ReportPdf({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      bottomSheet:    Container(
            height: size.height * 0.1,
            color: LinqPeColors.kPinkColor,
          ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: size.height * 0.1,
              padding: EdgeInsets.only(right:size.width*0.04),
              color: LinqPeColors.kPinkColor,
              alignment: Alignment.centerRight,
              child: Text('AB',
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
    letterSpacing: .5,
    fontSize: size.width * 0.06,
    color: LinqPeColors.kWhiteColor.withOpacity(0.8),
    fontWeight: FontWeight.w700,
                      ),
                    )),
            ),
             SizedBox(
                        height: size.height*0.03,
                      ),
            Container(
              color: LinqPeColors.kWhiteColor,
              margin: EdgeInsets.all(size.width * 0.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('Report',
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.06,
                          color: LinqPeColors.kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      )),
                  Text(
                      '(As of Today - ${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year})',
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.03,
                          color: LinqPeColors.kBlackColor,
                          fontWeight: FontWeight.w400,
                        ),
                      )),
                      SizedBox(
                        height: size.height*0.02,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: size.width*0.05),
                        padding:EdgeInsets.symmetric(vertical: size.height*0.01) ,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: LinqPeColors.kBlackColor45)
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ReportAmountNotifierWidget(size: size,fieldName: 'Total Received',
                            fieldAmount: '20000000',
                            ),
                            Container(
                              color: LinqPeColors.kBlackColor45,
                              height: size.height*0.05,
                              width: size.width*0.005,
                            ),
                             ReportAmountNotifierWidget(size: size,fieldName: 'Total Payed',
                            fieldAmount: '20000000',
                            ),
                            Container(
                              color: LinqPeColors.kBlackColor45,
                              height: size.height*0.05,
                              width: size.width*0.005,
                            ),
                              ReportAmountNotifierWidget(size: size,fieldName: 'Total Balance',
                            fieldAmount: '20000000',
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: size.height*0.02,
                      ),
                  const MyTable(),
                  SizedBox(
                        height: size.height*0.02,
                      ),
                  const GrandTotalTable()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ReportAmountNotifierWidget extends StatelessWidget {
  const ReportAmountNotifierWidget({
    super.key,
    required this.size, required this.fieldAmount, required this.fieldName,
    
  });

  final Size size;
  final   String fieldAmount;
  final  String fieldName;

  @override
  Widget build(BuildContext context) {
 
    return Column(
      children: [
        Text(fieldName,
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
    letterSpacing: .5,
    fontSize: size.width * 0.025,
    color: LinqPeColors.kBlackColor,
    fontWeight: FontWeight.w400,
                      ),
                    )),Text('₹$fieldAmount',
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
    letterSpacing: .5,
    fontSize: size.width * 0.035,
    color: LinqPeColors.kBlackColor,
    fontWeight: FontWeight.w500,
                      ),
                    )),
      ],
    );
  }
}

class MyTable extends StatelessWidget {
  const MyTable({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.02),
      width: size.width ,
      color: LinqPeColors.kLightBluwWhite,
      child: Table(
       // defaultColumnWidth: FixedColumnWidth(size.width * 0.19),

        columnWidths:{
       0:FixedColumnWidth(size.width * 0.2),
           1:FixedColumnWidth(size.width * 0.2),
              2:FixedColumnWidth(size.width * 0.16),
                 3:FixedColumnWidth(size.width * 0.16),
                    4:FixedColumnWidth(size.width * 0.16),  
        } ,
        border: TableBorder.all(borderRadius: BorderRadius.circular(5),
            color: LinqPeColors.kBlackColor, width: size.width * 0.001),
        children:  [
          TableRow(
            
            decoration: BoxDecoration(
              color: LinqPeColors.kWhiteColor.withOpacity(0.7)
            ), children: const [
            TableHeadColumnText(
              header: 'Name',
            ),
            TableHeadColumnText(header: 'Details'),
            TableHeadColumnText(header: 'Received'),
            TableHeadColumnText(header: 'Payment'),
            TableHeadColumnText(header: 'Balance'),
          ]),
          const TableRow(children: [
            TableRowText(
              header: 'Afsal Hostelmate 2.0',
            ),
            TableRowText(header: '9656971622'),
            TableRowText(header: '1000000'),
            TableRowText(header: '1000000'),
            TableRowText(header: '1000000'),
          ]), const TableRow(children: [
            TableRowText(
              header: 'Afsal Hostelmate 2.0',
            ),
            TableRowText(header: '9656971622'),
            TableRowText(header: '1000000'),
            TableRowText(header: '1000000'),
            TableRowText(header: '1000000'),
          ]), const TableRow(children: [
            TableRowText(
              header: 'Afsal Hostelmate 2.0',
            ),
            TableRowText(header: '9656971622'),
            TableRowText(header: '1000000'),
            TableRowText(header: '1000000'),
            TableRowText(header: '1000000'),
          ]),
        ],
      ),
    );
  }
}
class GrandTotalTable extends StatelessWidget {
  const GrandTotalTable({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.02),
      width: size.width ,
      color: LinqPeColors.kLightBluwWhite,
      child: Table(
       // defaultColumnWidth: FixedColumnWidth(size.width * 0.19),

        columnWidths:{
       0:FixedColumnWidth(size.width * 0.42),
           1:FixedColumnWidth(size.width * 0.16),
              2:FixedColumnWidth(size.width * 0.16),
                 3:FixedColumnWidth(size.width * 0.16),
                
        } ,
        border: TableBorder.all(borderRadius: BorderRadius.circular(5),
            color: LinqPeColors.kBlackColor, width: size.width * 0.001),
        children:  [
          TableRow(
            
            decoration: BoxDecoration(
              color: LinqPeColors.kWhiteColor.withOpacity(0.9)
            ), children: const [
            TableHeadColumnText(
              header: 'Grand Total',
            ),
         
            TableHeadColumnText(header: '1000000'),
            TableHeadColumnText(header: '1000000'),
            TableHeadColumnText(header: '1000000'),
          ]),
     
         
        ],
      ),
    );
  }
}

class TableHeadColumnText extends StatelessWidget {
  const TableHeadColumnText({super.key, required this.header});
  final String header;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(
          vertical: size.width * 0.03,),
      child: Text(header,
          style: GoogleFonts.roboto(
            textStyle: TextStyle(
              letterSpacing: .5,
              fontSize: size.width * 0.035,
              color: LinqPeColors.kBlackColor,
              fontWeight: FontWeight.w600,
            ),
          )),
    );
  }
}

class TableRowText extends StatelessWidget {
  const TableRowText({super.key, required this.header});
  final String header;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,  padding: EdgeInsets.symmetric(
          vertical: size.width * 0.03,horizontal: size.width*0.01),
     
      child: Text(header,
          style: GoogleFonts.roboto(
            textStyle: TextStyle(
              letterSpacing: .5,
              fontSize: size.width * 0.03,
              color: LinqPeColors.kBlackColor,
              fontWeight: FontWeight.w500,
            ),
          )),
    );
  }
}
