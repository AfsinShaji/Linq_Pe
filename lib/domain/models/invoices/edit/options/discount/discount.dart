import 'package:hive/hive.dart';



part 'discount.g.dart';

@HiveType(typeId: 14)
class DiscountModel {
  @HiveField(0)
 final String invoiceId;
  @HiveField(1)
  final String discountType;
  @HiveField(2)
  final double percentage;
  



  

  DiscountModel({
  
   required this.invoiceId,
   required this.discountType,
   required this.percentage,
  });
}

class DiscountBox {
  static Box<DiscountModel>? _getbox;
  static Box<DiscountModel> getInstance() {
    return _getbox ??= Hive.box('DiscountBox');
  }
}
