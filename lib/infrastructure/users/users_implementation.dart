import 'dart:developer';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:linq_pe/domain/core/failure/failure.dart';
import 'package:linq_pe/domain/models/users/users.dart';
import 'package:linq_pe/domain/repositories/users/users_repository.dart';
import 'package:linq_pe/infrastructure/services/secure_storage.dart';

class UsersImplementation extends UsersRepository {
  // creating a singleton
  UsersImplementation.internal();
  static UsersImplementation instance = UsersImplementation.internal();
  UsersImplementation factory() {
    return instance;
  }

///////////////////////////////////////////

  @override
  Future<void> addUser(
      {required String name,
      required String email,
      required String phoneNumber,
      required String businessType,
      required String purpose}) async {
    final currentUser = FirebaseAuth.instance.currentUser;
    // final trialStart = DateTime.now();
    final user =
        FirebaseFirestore.instance.collection('Users').doc(currentUser!.uid);
    final userGet = await user.get();
    if (!userGet.exists) {
      final userProfile = UserProfile(
          businessLogo: '',
          businessName: '',
          isPremium: false,
          trialStartDate: null,
          userName: name,
          userId: currentUser.uid,
          phoneNumber: phoneNumber,
          userEmail: email,
          businessType: businessType,
          purpose: purpose);
      final firebaseUser = userProfile.toFirebase();
      await user.set(firebaseUser);
    }
    await StorageService.instance.writeSecureData(StorageItem(
      'loginStatus',
      'true',
    ));

    await StorageService.instance.writeSecureData(StorageItem(
      'userId',
      currentUser.uid,
    ));

    await StorageService.instance.writeSecureData(StorageItem(
      'userName',
      name,
    ));
  }

  Future<void> addLoginSecureData() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    final userColllection = FirebaseFirestore.instance.collection('Users');
    final userDocument = userColllection.doc(currentUser!.uid);
    final userDataMap = await userDocument.get();

    final UserProfile userData = UserProfile.fromFirebas(userDataMap.data()!);
    await StorageService.instance.writeSecureData(StorageItem(
      'loginStatus',
      'true',
    ));
    await StorageService.instance.writeSecureData(StorageItem(
      'userId',
      currentUser.uid,
    ));
    if (userData.trialStartDate != null) {
      await StorageService.instance.writeSecureData(StorageItem(
        'trialStartDate',
        userData.trialStartDate.toString(),
      ));
    }
    await StorageService.instance.writeSecureData(StorageItem(
      'userName',
      userData.userName,
    ));
  }

  @override
  Future<Either<MainFailure, UserProfile>> getUserDetails() async {
    try {
      final currentUser = FirebaseAuth.instance.currentUser;
      final userColllection = FirebaseFirestore.instance.collection('Users');
      final userDocument = userColllection.doc(currentUser!.uid);
      final userDataMap = await userDocument.get();

      if (userDataMap.data() == null) {
        return const Left(MainFailure.serverFailure());
      }
      final UserProfile userData = UserProfile.fromFirebas(userDataMap.data()!);

      return Right(userData);
    } catch (e) {
      return const Left(MainFailure.clientFailure());
    }
  }

  @override
  Future<Either<MainFailure, UserProfile?>> addTrialDate() async {
    try {
      final currentUser = FirebaseAuth.instance.currentUser;
      final userColllection = FirebaseFirestore.instance.collection('Users');
      final userDocument = userColllection.doc(currentUser!.uid);
      final userDataMap = await userDocument.get();
      final UserProfile userData = UserProfile.fromFirebas(userDataMap.data()!);
      final usermap = userData.addTrialDate(DateTime.now());
      await StorageService.instance.writeSecureData(StorageItem(
        'trialStartDate',
        DateTime.now().toString(),
      ));
      UserProfile? result = userData;
      await userDocument
          .set(usermap)
          .onError((error, stackTrace) => result = null);
      return Right(result);
    } catch (e) {
      return const Left(MainFailure.clientFailure());
    }
  }

  @override
  Future<Either<MainFailure, String>> updateUserBusiness(
      {required String userPusrpose,
      required String userBusinessName,
      required String userBusinessLogo,
      required String userBusinessType}) async {
    try {
      final currentUser = FirebaseAuth.instance.currentUser;

      final userColllection = FirebaseFirestore.instance.collection('Users');

      final userDocument = userColllection.doc(currentUser!.uid);
      final userDataMap = await userDocument.get();
      log(userDataMap.data().toString());
      final UserProfile userData = UserProfile.fromFirebas(userDataMap.data()!);
      final uploadedImageUrl = await uploadImage(File(userBusinessLogo));
      String result = 'success';
      final usermap = userData.addUserBusiness(
          userBusinessType: userBusinessType,
          userPusrpose: userPusrpose,
          userBusinessName: userBusinessName,
          userBusinessLogo: uploadedImageUrl);
      await userDocument.set(usermap).onError((error, stackTrace) {
        result = 'Failure';
      });
      return Right(result);
    } catch (e) {
      return const Left(MainFailure.clientFailure());
    }
  }

  Future<String> uploadImage(File file) async {
    DateTime now = DateTime.now();
    final currentUser = FirebaseAuth.instance.currentUser;
    String timeStamp = now.microsecondsSinceEpoch.toString();
    final storage = FirebaseStorage.instance;
    final storageRef = storage.ref();
    final imageRef =
        storageRef.child("ImageEntity/${currentUser!.uid}/$timeStamp");

    // log(imageRef.toString());
    // log(file.toString());
    final task = await imageRef.putFile(file);

    final String downloadUrl = await task.ref.getDownloadURL();
    return downloadUrl;
  }

  Future<String> makeUserPremium() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    final userColllection = FirebaseFirestore.instance.collection('Users');
    final userDocument = userColllection.doc(currentUser!.uid);
    final userDataMap = await userDocument.get();
    final UserProfile userData = UserProfile.fromFirebas(userDataMap.data()!);

    String result = 'success';
    final usermap = userData.addPremium();
    await userDocument.set(usermap).onError((error, stackTrace) {
      result = 'Failure';
    });
    await addTrialDate();
    await StorageService.instance.writeSecureData(StorageItem(
      'isPremium',
      'true',
    ));

    return result;
  }

  @override
  Future<bool?> isAccountPermitted() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    final userColllection = FirebaseFirestore.instance.collection('Users');
    final userDocument = userColllection.doc(currentUser!.uid);
    final userDataMap = await userDocument.get();

    final UserProfile userData = UserProfile.fromFirebas(userDataMap.data()!);
    if (userData.isPremium == true) {
      final trialStartDate = userData.trialStartDate;
      if(trialStartDate!=null){
        final currentDate = DateTime.now();
            final difference =
                currentDate.difference(trialStartDate).inDays;
            log('timeDifference:$difference');
            if (difference >= 365) {
              return false;
            } else {
              return true;
            }
      }

      return true;
    } else {
      final trialDate =
          await StorageService.instance.readSecureData('trialStartDate');
      if (await StorageService.instance.containsKeyInSecureData('isPremium')) {
        final isPremium =
            await StorageService.instance.readSecureData('isPremium');
        if (isPremium != null && isPremium == 'true') {
          if (trialDate != null) {
            final currentDate = DateTime.now();
            final difference =
                currentDate.difference(DateTime.parse(trialDate)).inDays;
            log('timeDifference:$difference');
            if (difference >= 365) {
              return false;
            } else {
              return true;
            }
          }
        }
      }

      log('trialDate:$trialDate');
      if (trialDate != null) {
        final currentDate = DateTime.now();
        final difference =
            currentDate.difference(DateTime.parse(trialDate)).inDays;
        log('timeDifference:$difference');
        if (difference >= 7) {
          return false;
        } else {
          return true;
        }
      } else {
        return null;
      }
    }
  }
}
