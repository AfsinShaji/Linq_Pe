import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/payment_info/screen_payment_info.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/image_picking_sheet.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/presentation/widgets/alert_box.dart';
import 'package:linq_pe/utilities/colors.dart';

class PhotoScreen extends StatelessWidget {
  PhotoScreen(
      {super.key,
      required this.imageFilePath,
      this.imageId,
      this.details,
      this.additionalDetails,
      required this.isToEdit});
  final String imageFilePath;
  final String? details;
  final String? additionalDetails;
  final TextEditingController descriptionTextController =
      TextEditingController();
  final TextEditingController additionalDetailsTextController =
      TextEditingController();
  final bool isToEdit;
  final String? imageId;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    log(isToEdit.toString());
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      bottomSheet: SaveButtonWidget(
          photoId: imageId == null ? '' : imageId!,
          isToEdit: isToEdit,
          imageFilePath: imageFilePath,
          descriptionTextController: descriptionTextController,
          additionalDetailsTextController: additionalDetailsTextController),
      appBar: AppBar(
        actions: [
          isToEdit
              ? DeletePhotoButton(imageId: imageId, size: size)
              : const SizedBox(),
        ],
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.26,
        title: Text('Photo',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
                height: size.height * 0.3,
                width: size.width,
                child: InkWell(
                  onTap: () {
                    if (isToEdit) {
                      openImagesPickingSheet(
                          context, size, false, isToEdit, null, imageId);
                    }
                  },
                  child: EditContainerWidget(
                      child: Image.file(File(imageFilePath))),
                )),
            EditContainerWidget(
                padding: size.width * 0.03,
                child: Column(
                  children: [
                    DetailsTextFieldWidget(
                      initailValue: details ?? '',
                      hintText: 'Details',
                      maxLines: 1,
                      size: size,
                      textController: descriptionTextController,
                    ),
                    Divider(
                      color: LinqPeColors.kBlackColor,
                      height: size.height * 0.001,
                      thickness: size.height * 0.0001,
                    ),
                    DetailsTextFieldWidget(
                      initailValue: additionalDetails ?? '',
                      hintText: 'Additional Details',
                      maxLines: null,
                      size: size,
                      textController: additionalDetailsTextController,
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}

class DeletePhotoButton extends ConsumerWidget {
  const DeletePhotoButton({
    super.key,
    required this.imageId,
    required this.size,
  });

  final String? imageId;
  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IconButton(
        onPressed: () {
          alertBox(context, 'Delete Item', () {
            BlocProvider.of<InvoicesBloc>(context).add(
                InvoicesEvent.deletePhotos(
                    photoId: imageId!,
                    invoiceId: ref.watch(invoiceIdProvider)));
            Navigator.pop(context);
            Navigator.pop(context);
          }, size);
        },
        icon: const Icon(
          Icons.delete,
          color: LinqPeColors.kWhiteColor,
        ));
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget({
    super.key,
    required this.imageFilePath,
    required this.descriptionTextController,
    required this.additionalDetailsTextController,
    required this.isToEdit,
    required this.photoId,
  });

  final String imageFilePath;
  final TextEditingController descriptionTextController;
  final TextEditingController additionalDetailsTextController;
  final bool isToEdit;
  final String photoId;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(onTap: () {
      if (imageFilePath.isNotEmpty && ref.watch(invoiceIdProvider).isNotEmpty) {
        if (isToEdit) {
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.editsPhoto(
              photoId: photoId,
              invoiceId: ref.watch(invoiceIdProvider),
              photo: File(imageFilePath),
              description: descriptionTextController.text,
              addionalDetails: additionalDetailsTextController.text));
        } else {
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.addsPhoto(
              invoiceId: ref.watch(invoiceIdProvider),
              photo: File(imageFilePath),
              description: descriptionTextController.text,
              addionalDetails: additionalDetailsTextController.text));
        }
        Navigator.pop(context);
        Navigator.pop(context);
      }
    });
  }
}
