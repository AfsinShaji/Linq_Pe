import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/payments_dto.dart';

final addPaymentProvider = StateProvider(
  (ref) {
    PaymentsDTO? payment;
    return payment;
  },
);
addPaymentDTO(PaymentsDTO? type, WidgetRef ref) {
  ref.read(addPaymentProvider.notifier).state = type;
}

final paymentMethodProvider = StateProvider(
  (ref) {
    return 'Select Payment Method';
  },
);
addpaymentMethod(String type, WidgetRef ref) {
  ref.read(paymentMethodProvider.notifier).state = type;
}

final paymentDateProvider = StateProvider(
  (ref) {
    DateTime? date;
    return date;
  },
);
addpaymentDate(DateTime date, WidgetRef ref) {
  ref.read(paymentDateProvider.notifier).state = date;
}
