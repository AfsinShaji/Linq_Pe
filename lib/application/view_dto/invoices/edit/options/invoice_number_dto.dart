import 'package:linq_pe/domain/models/invoices/edit/options/invoice_number/invoice_number.dart';

class InvoiceNumberDTO {
  final String invoiceId;

  final String invoiceNumber;

  final DateTime invoiceDate;

  final DateTime? dueDate;

  final String? poNumber;

  final String? terms;

  InvoiceNumberDTO({
    required this.dueDate,
    required this.invoiceDate,
    required this.invoiceId,
    required this.invoiceNumber,
    required this.poNumber,
    required this.terms,
  });
}

InvoiceNumberDTO? convertInvoiceNumberModelToDTO(
    InvoiceNumberModel? invoiceNumber) {
       if (invoiceNumber == null) {
    return null;
  }
  return InvoiceNumberDTO(
      dueDate: invoiceNumber.dueDate,
      invoiceDate: invoiceNumber.invoiceDate,
      invoiceId: invoiceNumber.invoiceId,
      invoiceNumber: invoiceNumber.invoiceNumber,
      poNumber: invoiceNumber.poNumber,
      terms: invoiceNumber.terms);
}
