import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/invoices/my_items/my_items_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/item_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/discount/screen_discount.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/items/screen_my_items.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/discount.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/item.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/presentation/widgets/alert_box.dart';
import 'package:linq_pe/utilities/colors.dart';
import 'package:motion/motion.dart';

import '../business_details/screen_business_details.dart';

class ItemsScreen extends StatelessWidget {
  ItemsScreen({super.key, this.item, required this.isToEdit});
  final ItemDTO? item;
  final TextEditingController descriptionTextController =
      TextEditingController();
  final TextEditingController additionalDetailsTextController =
      TextEditingController();
  final TextEditingController unitTextController = TextEditingController();
  final TextEditingController unitCostTextController = TextEditingController();
  final TextEditingController quantityTextController = TextEditingController();
  final TextEditingController discountAmountTextController =
      TextEditingController();
  final TextEditingController taxRateTextController = TextEditingController();
  final bool isToEdit;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      //floatingActionButton: MyItemsButton(size: size),
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),

      appBar: AppBar(
        actions: [
          isToEdit?IconButton(
        onPressed: () {
          alertBox(context, 'Delete Item', () {
            BlocProvider.of<InvoicesBloc>(context).add(
                InvoicesEvent.deleteItem(itemId: item!.itemId, invoiceId: item!.invoiceId));
            Navigator.pop(context);
            Navigator.pop(context);
          }, size);
        },
        icon: const Icon(
          Icons.delete,
          color: LinqPeColors.kWhiteColor,
        )):
       const SizedBox() ,
        isToEdit?const SizedBox():   IconButton(
            onPressed:() {
             Navigator.push(context, MaterialPageRoute(builder: (context) =>const MyItemsScreen() ,));
            },
            icon: const Icon(
              Icons.install_mobile,
              color: LinqPeColors.kWhiteColor,
            ))
        ],
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            } ,
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.27,
        title: Text('Item',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButtonWidget(
          itemId: item == null ? '' : item!.itemId,
          isToEdit: isToEdit,
          unitCostTextController: unitCostTextController,
          unitTextController: unitTextController,
          taxRateTextController: taxRateTextController,
          quantityTextController: quantityTextController,
          discountAmountTextController: discountAmountTextController,
          descriptionTextController: descriptionTextController,
          additionalDetailsTextController: additionalDetailsTextController),
      body: SafeArea(
          child: Container(
        padding: EdgeInsets.all(size.width * 0.01),
        width: size.width,
        // height: size.height * 0.17,
        child: SingleChildScrollView(
          child: Column(
            children: [
              EditContainerWidget(
                padding: 0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: size.width * 0.05,
                          vertical: size.width * 0.025),
                      child: Column(
                        children: [
                          DetailsField(
                            initialValue: item == null ? '' : item!.description,
                            fontSize: size.width * 0.045,
                            height: size.height * 0.05,
                            hintText: 'Description',
                            isNumber: false,
                            size: size,
                            textEditingController: descriptionTextController,
                          ),
                          DetailFieldRow(
                            initialValue:
                                item == null ? '' : item!.unitCost.toString(),
                            size: size,
                            fieldName: 'Unit Cost:',
                            hintText: '₹0.00',
                            isNumberField: true,
                            textController: unitCostTextController,
                          ),
                          DetailFieldRow(
                            initialValue: item == null
                                ? ''
                                : item!.unit == null
                                    ? ''
                                    : item!.unit!,
                            size: size,
                            fieldName: 'Unit:',
                            hintText: 'hours,days',
                            isNumberField: false,
                            textController: unitTextController,
                          ),
                          DetailFieldRow(
                            initialValue:
                                item == null ? '' : item!.quantity.toString(),
                            size: size,
                            fieldName: 'Quantity:',
                            hintText: '1',
                            isNumberField: true,
                            textController: quantityTextController,
                          ),
                          SizedBox(
                            height: size.height * 0.01,
                          ),
                          DiscountRow(size: size),
                          SizedBox(
                            height: size.height * 0.01,
                          ),
                          DetailFieldRow(
                            initialValue: item == null
                                ? ''
                                : item!.discountAmount == null
                                    ? ''
                                    : item!.discountAmount!.toString(),
                            size: size,
                            fieldName: 'Discount Amount:',
                            hintText: '0',
                            isNumberField: true,
                            textController: discountAmountTextController,
                          ),
                          SizedBox(
                            height: size.height * 0.005,
                          ),
                          DetailSwitchRow(
                            fieldName: 'Taxable',
                            size: size,
                          ),
                          SizedBox(
                            height: size.height * 0.005,
                          ),
                          TaxRateWidget(
                              item: item,
                              size: size,
                              taxRateTextController: taxRateTextController),
                        ],
                      ),
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: LinqPeColors.kGreyColor,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(5),
                              bottomRight: Radius.circular(5))),
                      padding: EdgeInsets.symmetric(
                          vertical: size.height * 0.01,
                          horizontal: size.width * 0.05),
                      child: TotalRowWidget(size: size),
                    )
                  ],
                ),
              ),
              EditContainerWidget(
                child: SizedBox(
                  width: size.width,
                  height: size.height * 0.2,
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: TextField(
                      //    textAlign: TextAlign.right, // Align text to the right
                      maxLines: null,

                      //   textDirection: TextDirection.ltr,

                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Additional Details',
                          hintStyle: GoogleFonts.roboto(
                            textStyle: TextStyle(
                              letterSpacing: .5,
                              fontSize: size.width * 0.04,
                              color: LinqPeColors.kBlackColor.withOpacity(0.3),
                              fontWeight: FontWeight.w600,
                            ),
                          )),

                      controller: additionalDetailsTextController,

                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          letterSpacing: .5,
                          fontSize: size.width * 0.04,
                          color: LinqPeColors.kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              EditContainerWidget(
                child: MyItemsSwitchRow(
                    fieldName: 'Save to "My Items"', size: size),
              )
            ],
          ),
        ),
      )),
    );
  }
}

class MyItemsButton extends StatelessWidget {
  const MyItemsButton({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: size.width * 0.15, vertical: size.height * 0.25),
      child: Motion(
        shadow: null,
        child: FloatingActionButton.extended(
            backgroundColor: LinqPeColors.kPinkColor,
            onPressed: () {
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //       builder: (context) => MyScreen(),
              //     ));
            },
            label: Row(
              children: [
                Text('Add from my items',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        letterSpacing: .1,
                        fontSize: size.width * 0.04,
                        color: LinqPeColors.kWhiteColor,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                const Icon(
                  Icons.arrow_forward,
                  color: LinqPeColors.kWhiteColor,
                )
              ],
            )),
      ),
    );
  }
}

class TaxRateWidget extends ConsumerWidget {
  const TaxRateWidget({
    super.key,
    required this.item,
    required this.size,
    required this.taxRateTextController,
  });

  final ItemDTO? item;
  final Size size;
  final TextEditingController taxRateTextController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return BlocBuilder<InvoicesBloc, InvoicesState>(
      builder: (context, invoiceState) {
        InvoiceEditDTO? invoice;
        if (invoiceState is displayInvoice) {
          invoice = invoiceState.invoice;
        }
        if (invoice == null) {
          return const SizedBox();
        }
        if (invoice.tax != null &&
            invoice.tax!.taxType == 'Per Item' &&
            ref.watch(isTaxableProvider)) {
          return DetailFieldRow(
            initialValue: item == null
                ? ''
                : item!.taxRate == null
                    ? ''
                    : item!.taxRate!.toString(),
            size: size,
            fieldName: 'Tax Rate:',
            hintText: '0%',
            isNumberField: true,
            textController: taxRateTextController,
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }
}

class TotalRowWidget extends ConsumerWidget {
  const TotalRowWidget({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    String total = ref.watch(totalProvider);
    log(total.toString());

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text('Total',
            style: GoogleFonts.roboto(
              textStyle: TextStyle(
                letterSpacing: .5,
                fontSize: size.width * 0.04,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w600,
              ),
            )),
        Text('₹$total',
            style: GoogleFonts.roboto(
              textStyle: TextStyle(
                letterSpacing: .5,
                fontSize: size.width * 0.04,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w600,
              ),
            )),
      ],
    );
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget(
      {super.key,
      required this.unitCostTextController,
      required this.unitTextController,
      required this.taxRateTextController,
      required this.quantityTextController,
      required this.discountAmountTextController,
      required this.descriptionTextController,
      required this.additionalDetailsTextController,
      required this.isToEdit,
      required this.itemId});

  final TextEditingController unitCostTextController;
  final TextEditingController unitTextController;
  final TextEditingController taxRateTextController;
  final TextEditingController quantityTextController;
  final TextEditingController discountAmountTextController;
  final TextEditingController descriptionTextController;
  final TextEditingController additionalDetailsTextController;
  final bool isToEdit;
  final String itemId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(onTap: () {
      log('invoiceId-${ref.watch(invoiceIdProvider)}');
      if (descriptionTextController.text.isNotEmpty &&
          unitCostTextController.text.isNotEmpty &&
          quantityTextController.text.isNotEmpty &&
          ref.watch(invoiceIdProvider).isNotEmpty) {
        if (isToEdit == false) {
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.addItem(
              total: double.parse(ref.watch(totalProvider)),
              invoiceId: ref.watch(invoiceIdProvider),
              unitCost: double.parse(unitCostTextController.text),
              unit: unitTextController.text,
              taxRate: taxRateTextController.text.isNotEmpty
                  ? double.parse(taxRateTextController.text)
                  : null,
              quantity: int.parse(quantityTextController.text),
              isTaxable: true,
              discountAmount: discountAmountTextController.text.isNotEmpty
                  ? double.parse(discountAmountTextController.text)
                  : null,
              description: descriptionTextController.text,
              discount: ref.watch(discountTypeProvider),
              addiotionalDetails: additionalDetailsTextController.text,
              isAddToMyItems: true));
        } else {
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.editItem(
              itemId: itemId,
              total: double.parse(ref.watch(totalProvider)),
              invoiceId: ref.watch(invoiceIdProvider),
              unitCost: double.parse(unitCostTextController.text),
              unit: unitTextController.text,
              taxRate: taxRateTextController.text.isNotEmpty
                  ? double.parse(taxRateTextController.text)
                  : null,
              quantity: int.parse(quantityTextController.text),
              isTaxable: true,
              discountAmount: discountAmountTextController.text.isNotEmpty
                  ? double.parse(discountAmountTextController.text)
                  : null,
              description: descriptionTextController.text,
              discount: ref.watch(discountTypeProvider),
              addiotionalDetails: additionalDetailsTextController.text,
              isAddToMyItems: true));
        }
        if (ref.watch(myItemsProvider) == true) {
          BlocProvider.of<MyItemsBloc>(context).add(MyItemsEvent.addToMyItem(
              item: ItemDTO(
            itemId:
                'Item-${ref.watch(invoiceIdProvider)}-${DateTime.now().millisecondsSinceEpoch}',
            invoiceId: ref.watch(invoiceIdProvider),
            unitCost: double.parse(unitCostTextController.text),
            unit: unitTextController.text,
            taxRate: taxRateTextController.text.isNotEmpty
                ? double.parse(taxRateTextController.text)
                : null,
            quantity: int.parse(quantityTextController.text),
            isTaxable: true,
            discountAmount: discountAmountTextController.text.isNotEmpty
                ? double.parse(discountAmountTextController.text)
                : null,
            description: descriptionTextController.text,
            discount: ref.watch(discountTypeProvider),
            addiotionalDetails: additionalDetailsTextController.text,
            total: double.parse(ref.watch(totalProvider)),
          )));
        }
        Navigator.pop(context);
      }
    });
  }
}

class DetailFieldRow extends ConsumerStatefulWidget {
  const DetailFieldRow({
    super.key,
    required this.size,
    required this.fieldName,
    required this.textController,
    required this.isNumberField,
    required this.hintText,
    required this.initialValue,
  });

  final Size size;
  final String fieldName;
  final TextEditingController textController;
  final bool isNumberField;
  final String hintText;
  final String initialValue;

  @override
  DetailFieldRowState createState() => DetailFieldRowState();
}

class DetailFieldRowState extends ConsumerState<DetailFieldRow> {
  @override
  void initState() {
    if (widget.initialValue.isNotEmpty) {
      widget.textController.text = widget.initialValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: widget.size.height * 0.045,
          child: Center(
            child: Text(widget.fieldName,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: widget.size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                )),
          ),
        ),
        SizedBox(
          width: widget.size.width * 0.48,
          height: widget.size.height * 0.045,
          child: Align(
            alignment: Alignment.topRight,
            child: TextField(
              textAlign: TextAlign.right, // Align text to the right

              textDirection: TextDirection.ltr,

              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: widget.hintText,
                hintStyle: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: widget.size.width * 0.04,
                    color: LinqPeColors.kBlackColor.withOpacity(0.3),
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              onChanged: (value) {
                log('Here');
                if (widget.fieldName == 'Unit Cost:') {
                  addunitCost(value, ref);
                } else if (widget.fieldName == 'Quantity:') {
                  addquantity(value, ref);
                } else if (widget.fieldName == 'Discount Amount:') {
                  adddiscount(value, ref);
                }
              },
              controller: widget.textController,
              keyboardType: widget.isNumberField
                  ? TextInputType.number
                  : TextInputType.emailAddress,
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: widget.size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}

class DetailSwitchRow extends ConsumerWidget {
  const DetailSwitchRow({
    super.key,
    required this.size,
    required this.fieldName,
  });

  final Size size;
  final String fieldName;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: size.height * 0.03,
          child: Center(
            child: Text(fieldName,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w500,
                  ),
                )),
          ),
        ),
        SizedBox(
          height: size.height * 0.045,
          child: FittedBox(
            fit: BoxFit.fill,
            child: Switch(
                value: ref.watch(isTaxableProvider),
                activeColor: LinqPeColors.kBlueColor,
                onChanged: (value) {
                  addisTaxable(value, ref);
                }),
          ),
        )
      ],
    );
  }
}

class MyItemsSwitchRow extends ConsumerWidget {
  const MyItemsSwitchRow({
    super.key,
    required this.size,
    required this.fieldName,
  });

  final Size size;
  final String fieldName;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: size.height * 0.03,
          child: Center(
            child: Text(fieldName,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w500,
                  ),
                )),
          ),
        ),
        SizedBox(
          height: size.height * 0.045,
          child: FittedBox(
            fit: BoxFit.fill,
            child: Switch(
                value: ref.watch(myItemsProvider),
                activeColor: LinqPeColors.kBlueColor,
                onChanged: (value) {
                  addmyItems(value, ref);
                }),
          ),
        )
      ],
    );
  }
}
