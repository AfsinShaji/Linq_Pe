import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linq_pe/infrastructure/authentication/authentication_implementation.dart';
import 'package:linq_pe/infrastructure/services/secure_storage.dart';
import 'package:linq_pe/presentation/screens/authentication/add_business/screen_add_business.dart';
import 'package:linq_pe/presentation/screens/ledger/screen_ledger.dart';
import 'package:linq_pe/presentation/widgets/alert_snackbar.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';
part 'authentication_bloc.freezed.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(const authenticationinitial()) {
    on<logIn>((event, emit) async {
      emit(
          const authResults(isLoading: true, isSuccess: false, isError: false));
      final result = await AuthenticationImplementation.instance
          .login(email: event.email, password: event.password);
      result.fold((l) {
        alertSnackbar(event.context, 'Something Went Wrong');
      }, (r) async {
        if (r == 'success') {
          emit(const authResults(
              isLoading: false, isSuccess: true, isError: false));
          await StorageService.instance
              .readSecureData('userName')
              .then((value) {
            Navigator.pushAndRemoveUntil(
              event.context,
              CupertinoPageRoute(
                builder: (context) => const LedgerScreen(),
                //  PremiumPlanScreen(userName: value ?? '',isPermitted: ),
              ),
              (route) => false,
            );
          });
        } else {
          alertSnackbar(event.context, 'Error: ${r.toString()}');
          log(r.toString());
        }
      });
    });
    on<registration>((event, emit) async {
      emit(
          const authResults(isLoading: true, isSuccess: false, isError: false));
      final result = await AuthenticationImplementation.instance
          .userRegistration(
              email: event.email,
              password: event.password,
              phoneNumber: event.phoneNumber,
              businessType: event.businessType,
              purpose: event.purpose,
              name: event.name);
      result.fold((l) {
        alertSnackbar(event.context, 'Something Went Wrong');
      }, (r) {
        if (r == 'success') {
          emit(const authResults(
              isLoading: false, isSuccess: true, isError: false));
          Navigator.push(
            event.context,
            CupertinoPageRoute(
              builder: (context) => AddBusinessScreen(),
            ),
          );
        } else {
          emit(const authResults(
              isLoading: false, isSuccess: false, isError: true));
          alertSnackbar(event.context, 'Error: ${r.toString()}');
          log(r.toString());
        }
      });
    });
    on<loggingOut>((event, emit) async {
      await AuthenticationImplementation.instance.logOut();
    });
  }
}
