import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/my_items/my_items_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/item_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/items/screen_items.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/item.dart';
import 'package:linq_pe/utilities/colors.dart';

class MyItemsScreen extends StatefulWidget {
  const MyItemsScreen({super.key});

  @override
  State<MyItemsScreen> createState() => _MyItemsScreenState();
}

class _MyItemsScreenState extends State<MyItemsScreen> {
  @override
  void initState() {
    BlocProvider.of<MyItemsBloc>(context)
        .add(const MyItemsEvent.getAllMyItem());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.2,
        title: Text('My Items',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      body: SafeArea(child: BlocBuilder<MyItemsBloc, MyItemsState>(
        builder: (context, itemstate) {
          List<ItemDTO> myItems = [];
          if (itemstate is displayMyItems) {
            myItems = itemstate.myItems;
          }
          log(myItems.length.toString());
          if (myItems.isEmpty) {
            return Center(
              child: Image.asset('assets/images/No data-amico.png'),
            );
          }
          return ListView.builder(
            itemBuilder: (context, index) =>
                ItemContainer(myItems: myItems, size: size, index: index),
            itemCount: myItems.length,
          );
        },
      )),
    );
  }
}

class ItemContainer extends ConsumerWidget {
  const ItemContainer(
      {super.key,
      required this.myItems,
      required this.size,
      required this.index});

  final List<ItemDTO> myItems;
  final Size size;
  final int index;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
        addtotal(myItems[index].total.toString(), ref);
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  ItemsScreen(isToEdit: false, item: myItems[index]),
            ));
      },
      child: Container(
        width: size.width,
        height: size.height * 0.08,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: LinqPeColors.kWhiteColor,
        ),
        padding: EdgeInsets.all(size.width * 0.05),
        margin: EdgeInsets.all(size.width * 0.02),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(myItems[index].description,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .1,
                    fontSize: size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w500,
                  ),
                )),
            Text('₹${myItems[index].unitCost}',
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .1,
                    fontSize: size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w500,
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
