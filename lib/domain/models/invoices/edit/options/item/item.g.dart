// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ItemModelAdapter extends TypeAdapter<ItemModel> {
  @override
  final int typeId = 19;

  @override
  ItemModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ItemModel(
      itemId: fields[11] as String,
      invoiceId: fields[0] as String,
      addiotionalDetails: fields[9] as String?,
      discount: fields[5] as String?,
      description: fields[1] as String,
      discountAmount: fields[6] as double?,
      isTaxable: fields[7] as bool,
      quantity: fields[4] as int,
      taxRate: fields[8] as double?,
      unit: fields[3] as String?,
      unitCost: fields[2] as double,
      total: fields[10] as double,
    );
  }

  @override
  void write(BinaryWriter writer, ItemModel obj) {
    writer
      ..writeByte(12)
      ..writeByte(0)
      ..write(obj.invoiceId)
      ..writeByte(1)
      ..write(obj.description)
      ..writeByte(2)
      ..write(obj.unitCost)
      ..writeByte(3)
      ..write(obj.unit)
      ..writeByte(4)
      ..write(obj.quantity)
      ..writeByte(5)
      ..write(obj.discount)
      ..writeByte(6)
      ..write(obj.discountAmount)
      ..writeByte(7)
      ..write(obj.isTaxable)
      ..writeByte(8)
      ..write(obj.taxRate)
      ..writeByte(9)
      ..write(obj.addiotionalDetails)
      ..writeByte(10)
      ..write(obj.total)
      ..writeByte(11)
      ..write(obj.itemId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ItemModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
