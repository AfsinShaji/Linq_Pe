import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/all_invoices/all_invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/client_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/reports/tabs/paid/screen_paid.dart';
import 'package:linq_pe/presentation/view_state/invoice/report/invoice_report.dart';
import 'package:linq_pe/utilities/colors.dart';

class ClientScreen extends ConsumerWidget {
  const ClientScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    return BlocBuilder<AllInvoicesBloc, AllInvoicesState>(
      builder: (context, invoicestate) {
        List<InvoiceEditDTO> invoiceList = [];
        List<ClientDTO> clientList = [];

        double totalBalanceDue = 0.00;
        List<int> yearList = [];
        if (invoicestate is displayInvoiceList) {
          invoiceList = invoicestate.invoiceList
              .where((element) => element.isInvoice == true)
              .toList();
        }
        if (invoiceList.isNotEmpty) {
          for (var invoice in invoiceList) {
            if (invoice.invoiceNumberDetails.invoiceDate.year ==
                ref.watch(yearProvider)) {
              if (invoice.client != null) {
                if (clientList
                    .where((element) =>
                        element.clientName == invoice.client!.clientName &&
                        element.mobileNumber == invoice.client!.mobileNumber)
                    .isEmpty) {
                  clientList.add(invoice.client!);
                }
              }
              if (invoice.balanceDue != null && invoice.isPaid == true) {
                totalBalanceDue = totalBalanceDue + invoice.balanceDue!;
              }
            }

            if (!yearList
                .contains(invoice.invoiceNumberDetails.invoiceDate.year)) {
              yearList.add(invoice.invoiceNumberDetails.invoiceDate.year);
            }
          }
        }
        return SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(size.width * 0.05),
            child: Column(
              children: [
                Row(
                  children: [
                    TableHeaders(size: size, width: size.width * 0.325),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.25,
                      headerText: 'Invoices',
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.325,
                      headerText: 'Paid',
                    )
                  ],
                ),
                const Divider(),
                Row(
                  children: [
                    TableHeaders(
                        size: size,
                        width: size.width * 0.325,
                        headerText: 'Tax Year ${ref.watch(yearProvider)}'),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.25,
                      headerText:
                          '${invoiceList.where((element) => element.invoiceNumberDetails.invoiceDate.year == ref.watch(yearProvider) ).length}',
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.325,
                      headerText: '₹ $totalBalanceDue',
                    )
                  ],
                ),
                const Divider(),
                SizedBox(
                  height: size.height * 0.005,
                ),
                Column(
                  children: List.generate(clientList.length, (index) {
                    int clientInvoiceNumber = 0;
                    double clientBalanceDue = 0.00;
                    for (var invoice in invoiceList) {
                      if (
                          invoice.client != null &&
                          invoice.invoiceNumberDetails.invoiceDate.year ==
                              ref.watch(yearProvider) &&
                          invoice.client!.clientName ==
                              clientList[index].clientName &&
                          invoice.client!.mobileNumber ==
                              clientList[index].mobileNumber) {
                        clientInvoiceNumber = clientInvoiceNumber + 1;
                        if (invoice.balanceDue != null&&invoice.isPaid) {
                          clientBalanceDue =
                              clientBalanceDue + invoice.balanceDue!;
                        }
                      }
                    }

                    return Column(
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: size.width * 0.325,
                              child: Text(clientList[index].clientName,
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                      letterSpacing: .1,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            ),
                            SizedBox(
                              width: size.width * 0.25,
                              child: Text('$clientInvoiceNumber',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                      letterSpacing: .1,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            ),
                            SizedBox(
                              width: size.width * 0.325,
                              child: Text('₹ $clientBalanceDue',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                      letterSpacing: .1,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            )
                          ],
                        ),const Divider()
                      ],
                    );
                  }),
                ),

                SizedBox(
                  height: size.height * 0.005,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                      yearList.length,
                      (index) => YearContainer(
                            yearList: yearList,
                            size: size,
                            index: index,
                          )),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
