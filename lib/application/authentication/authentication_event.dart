part of 'authentication_bloc.dart';

@freezed
class AuthenticationEvent with _$AuthenticationEvent {
  const factory AuthenticationEvent.logIn(
      {required String email,
      required String password,
      required BuildContext context}) = logIn;
  const factory AuthenticationEvent.registration(
      {required String name,
      required String email,
      required String phoneNumber,
      required BuildContext context,
      required String businessType,
      required String purpose,
      required String password}) = registration;
  // const factory AuthenticationEvent.resetPassword() = resetPassword;
  const factory AuthenticationEvent.loggingOut() = loggingOut;
}
