import 'dart:io';

import 'package:linq_pe/application/view_dto/transaction/secondary_transaction_dto.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/business_details/business_details.dart';

class BusinessDetailsDTO {
  final String? businessOwnerName;
  final String businessName;
  final String? businessNumber;
  final String? addressLine1;
  final String? addressLine2;
  final String? addressLine3;
  final String? email;
  final String? website;
  final String? mobile;
  final String? phone;
  final String invoiceId;
  final File? businessLogo;

  BusinessDetailsDTO({
    required this.businessLogo,
    required this.addressLine1,
    required this.addressLine2,
    required this.addressLine3,
    required this.businessName,
    required this.businessNumber,
    required this.businessOwnerName,
    required this.email,
    required this.mobile,
    required this.phone,
    required this.website,
    required this.invoiceId,
  });
}

Future<BusinessDetailsDTO? >convertBusinessDetailsModelToDTO(
    BusinessDetailsModel? businessDetail)async {
  if (businessDetail == null) {
    return null;
  }

  return BusinessDetailsDTO(
      businessLogo:await uint8ListToFile(businessDetail.businessLogo),
      addressLine1: businessDetail.addressLine1,
      addressLine2: businessDetail.addressLine2,
      addressLine3: businessDetail.addressLine3,
      businessName: businessDetail.businessName,
      businessNumber: businessDetail.businessNumber,
      businessOwnerName: businessDetail.businessOwnerName,
      email: businessDetail.email,
      mobile: businessDetail.mobile,
      phone: businessDetail.phone,
      website: businessDetail.website,
      invoiceId: businessDetail.invoiceId);
}
