import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/preview/screen_preview.dart';
import 'package:linq_pe/presentation/view_state/invoice/preview.dart/preview.dart';
import 'package:linq_pe/utilities/colors.dart';
import 'package:pdf/pdf.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class StyleScreen extends ConsumerWidget {
  const StyleScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: SafeArea(child: BlocBuilder<InvoicesBloc, InvoicesState>(
        builder: (context, invoicestate) {
          InvoiceEditDTO? invoiceEditDTO;
          if (invoicestate is displayInvoice) {
            invoiceEditDTO = invoicestate.invoice;
          }
          final Size size = MediaQuery.of(context).size;
          return Column(
            children: [
              FutureBuilder(
                key: const Key('Template Design'),
                  future: creatingpdf(
                    invoiceEditDTO!,
                    size,
                    ref.watch(previewTypeProvider),
                    ref.watch(previewColorProvider),
                  ),
                  builder: (context, filesnap) {
                    if (filesnap.hasData) {
                      return SizedBox(
                        width: size.width*0.7,
                        height: size.height*0.65,
                        child: Padding(
                          padding: EdgeInsets.all(size.width * 0),
                          child: SfPdfViewer.file(filesnap.data!,key:const Key('Template Designs') ,),
                        ),
                      );
                    } else {
                      return const SizedBox();
                    }
                  }),
              Container(
                width: size.width,
                padding: EdgeInsets.all(size.width * 0.05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Color',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            letterSpacing: .1,
                            fontSize: size.width * 0.045,
                            color: LinqPeColors.kBlackColor,
                            fontWeight: FontWeight.w500,
                          ),
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [ColorContainer(size: size,color: LinqPeColors.kBlackColor,),
                      ColorContainer(size: size,color: const Color.fromARGB(255, 54, 91, 55),),
                      ColorContainer(size: size,color: LinqPeColors.kGreenColor,),
                      ColorContainer(size: size,color: LinqPeColors.kGreyColor,),
                      ColorContainer(size: size,color: LinqPeColors.kredColor,),
                      ColorContainer(size: size,color: LinqPeColors.kPinkColor,),
                      ColorContainer(size: size,color: LinqPeColors.kBlueColor,),
                      ColorContainer(size: size,color: Colors.indigoAccent,),
                      
                      
                      ],
                    ),
                      Row(
                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [ColorContainer(size: size,color: LinqPeColors.kLightBluwWhite,),
                      ColorContainer(size: size,color: const Color.fromARGB(255, 238, 246, 7),),
                      ColorContainer(size: size,color: const Color.fromARGB(255, 92, 82, 223),),
                      ColorContainer(size: size,color: const Color.fromARGB(255, 11, 241, 107),),
                      ColorContainer(size: size,color: Colors.cyan,),
                      ColorContainer(size: size,color: Colors.deepOrange,),
                      ColorContainer(size: size,color: Colors.white,),
                      ColorContainer(size: size,color: Colors.teal,),
                      
                      
                      ],
                    ),
                  ],
                ),
              )
            ],
          );
        },
      )),
    );
  }
}

class ColorContainer extends ConsumerWidget {
  const ColorContainer({super.key, required this.size, required this.color});

  final Size size;
  final Color color;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pdfColor =
    //  Color.fromARGB(
    //   (
        ref.watch(previewColorProvider)
      
    //   .alpha * 255).round(),
    //   (ref.watch(previewColorProvider).red * 255).round(),
    //   (ref.watch(previewColorProvider).green * 255).round(),
    //   (ref.watch(previewColorProvider).blue * 255).round(),
    // )
    ;
    return InkWell(
      onTap: () {
        addpreviewColor(
            PdfColor(
              color.red / 255.0,
              color.green / 255.0,
              color.blue / 255.0,
              color.alpha / 255.0,
            ),
            ref);
      },
      child: Container(
        width: size.width * 0.08,
        height: size.width * 0.08,
        margin: EdgeInsets.all(size.width*0.01),
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(5), color: color,
            border: PdfColor(
              color.red / 255.0,
              color.green / 255.0,
              color.blue / 255.0,
              color.alpha / 255.0,
            )==pdfColor?Border.all(color: LinqPeColors.kWhiteColor):null
            ),
            child: Center(child:PdfColor(
              color.red / 255.0,
              color.green / 255.0,
              color.blue / 255.0,
              color.alpha / 255.0,
            )==pdfColor?
             const Icon(Icons.check_circle,color: LinqPeColors.kWhiteColor,):null),
      ),
    );
  }
}
