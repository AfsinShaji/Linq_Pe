import 'package:dartz/dartz.dart';
import 'package:linq_pe/domain/core/failure/failure.dart';

abstract class AuthenticationRepository {
  Future<Either<MainFailure, String>> login({
    required String email,
    required String password,
  });
  Future<Either<MainFailure, String>>  userRegistration(
      {required String name,
      required String email,
      required String phoneNumber,
      required String businessType,
      required String purpose,
      required String password});
}
