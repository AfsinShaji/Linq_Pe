import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pdf/pdf.dart';

enum DesignType { classic, compact, clean, sharp ,pos}

final previewTypeProvider = StateProvider(
  (ref) {
    return DesignType.sharp;
  },
);
addpreviewType(DesignType id, WidgetRef ref) {
  ref.read(previewTypeProvider.notifier).state = id;
}

final previewColorProvider = StateProvider(
  (ref) {
    return PdfColors.pink;
  },
);
addpreviewColor(PdfColor id, WidgetRef ref) {
  ref.read(previewColorProvider.notifier).state = id;
}


