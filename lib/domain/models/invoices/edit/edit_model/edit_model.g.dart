// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class InvoiceEditModelAdapter extends TypeAdapter<InvoiceEditModel> {
  @override
  final int typeId = 21;

  @override
  InvoiceEditModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return InvoiceEditModel(
      invoiceId: fields[0] as String,
      businessDetails: fields[2] as BusinessDetailsModel?,
      discountDetails: fields[3] as DiscountModel?,
      notes: fields[5] as NotesModel?,
      invoiceNumberDetails: fields[1] as InvoiceNumberModel,
      itemsList: (fields[4] as List?)?.cast<ItemModel>(),
      paymentInfo: fields[6] as PaymentInfoModel?,
      paymentsList: (fields[7] as List?)?.cast<PaymentsModel>(),
      photoList: (fields[8] as List?)?.cast<PhotoModel>(),
      signature: fields[9] as SignatureModel?,
      tax: fields[10] as TaxModel?,
      balanceDue: fields[16] as double?,
      client: fields[11] as ClientModel?,
      isToReview: fields[12] as bool?,
      reviewLink: fields[13] as String?,
      isPaid: fields[14] as bool,
      paidMethod: fields[15] as String?,
      isEstimate: fields[18] as bool,
      isInvoice: fields[17] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, InvoiceEditModel obj) {
    writer
      ..writeByte(19)
      ..writeByte(0)
      ..write(obj.invoiceId)
      ..writeByte(1)
      ..write(obj.invoiceNumberDetails)
      ..writeByte(2)
      ..write(obj.businessDetails)
      ..writeByte(3)
      ..write(obj.discountDetails)
      ..writeByte(4)
      ..write(obj.itemsList)
      ..writeByte(5)
      ..write(obj.notes)
      ..writeByte(6)
      ..write(obj.paymentInfo)
      ..writeByte(7)
      ..write(obj.paymentsList)
      ..writeByte(8)
      ..write(obj.photoList)
      ..writeByte(9)
      ..write(obj.signature)
      ..writeByte(10)
      ..write(obj.tax)
      ..writeByte(11)
      ..write(obj.client)
      ..writeByte(12)
      ..write(obj.isToReview)
      ..writeByte(13)
      ..write(obj.reviewLink)
      ..writeByte(14)
      ..write(obj.isPaid)
      ..writeByte(15)
      ..write(obj.paidMethod)
      ..writeByte(16)
      ..write(obj.balanceDue)
      ..writeByte(17)
      ..write(obj.isInvoice)
      ..writeByte(18)
      ..write(obj.isEstimate);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is InvoiceEditModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
