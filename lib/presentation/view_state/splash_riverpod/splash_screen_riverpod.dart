import 'package:flutter_riverpod/flutter_riverpod.dart';

final isPermittedProvider = StateProvider(
  (ref) {
    bool? isPermitted;
    return isPermitted;
  },
);
addIsPermitted(bool isPermitted, WidgetRef ref) {
  ref.read(isPermittedProvider.notifier).state = isPermitted;
}
