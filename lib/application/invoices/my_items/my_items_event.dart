part of 'my_items_bloc.dart';

@freezed
class MyItemsEvent with _$MyItemsEvent {
  const factory MyItemsEvent.addToMyItem({required ItemDTO item}) =
      addToMyItem;
  const factory MyItemsEvent.getAllMyItem() = getAllMyItem;
}
