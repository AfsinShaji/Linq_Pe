import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/date_picker_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/datetime_to_date_string_converter.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/invoice_number.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

class InvoiceNumberScreen extends StatelessWidget {
  InvoiceNumberScreen({super.key, required this.isEstimate,});
  final bool isEstimate;
  final TextEditingController invoiceNumberController = TextEditingController();
  final TextEditingController poNumberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.1,
        title: Text('Invoice Number',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButtonWidget(
          invoiceNumberController: invoiceNumberController,
          poNumberController: poNumberController),
      body: SafeArea(
          child: Container(
        padding: EdgeInsets.all(size.width * 0.01),
        width: size.width,
        height: size.height * 0.36,
        child: EditContainerWidget(
          child: BlocBuilder<InvoicesBloc, InvoicesState>(
            builder: (context, invoiceState) {
              InvoiceEditDTO? invoice;

              if (invoiceState is displayInvoice) {
                invoice = invoiceState.invoice;
              }
              if (invoice == null) {
                return const SizedBox();
              }

              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  NumberAndFieldRow(
                      initialValue: invoice.invoiceNumberDetails.invoiceNumber,
                      fieldName: 'Invoice Number:',
                      size: size,
                      textController: invoiceNumberController),
                  SizedBox(
                    height: size.height * 0.005,
                  ),
                  DatePickerWidget(size: size, invoice: invoice),
                  SizedBox(
                    height: size.height * 0.02,
                  ),
              isEstimate?const SizedBox():    TermsRow(
                    size: size,
                  ),
                  SizedBox(
                    height: isEstimate?0.0: size.height * 0.02,
                  ),
                  DueDatePickerWidget(size: size),
                  SizedBox(
                    height: size.height * 0.0,
                  ),
                  NumberAndFieldRow(
                      initialValue: invoice.invoiceNumberDetails.poNumber ?? '',
                      fieldName: 'PO Number:',
                      size: size,
                      textController: poNumberController),
                  //       SizedBox(
                  //   height: size.height * 0.02,
                  // ),
                ],
              );
            },
          ),
        ),
      )),
    );
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget({
    super.key,
    required this.invoiceNumberController,
    required this.poNumberController,
  });

  final TextEditingController invoiceNumberController;
  final TextEditingController poNumberController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(
      onTap: () {
        if (ref.watch(invoiceIdProvider).isNotEmpty &&
            ref.watch(invoiceDateProvider) != null &&
            ref.watch(invoiceDueDateProvider) != null) {
          BlocProvider.of<InvoicesBloc>(context).add(
              InvoicesEvent.addInvoicesNumber(
                  invoiceNumber: invoiceNumberController.text,
                  invoiceId: ref.watch(invoiceIdProvider),
                  date: ref.watch(invoiceDateProvider)!,
                  terms: ref.watch(termsProvider),
                  dueDate: ref.watch(invoiceDueDateProvider)!,
                  poNumber: poNumberController.text));
          Navigator.pop(context);
        }
      },
    );
  }
}

class DueDatePickerWidget extends ConsumerWidget {
  const DueDatePickerWidget({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (ref.watch(termsProvider) == 'Custom') {
      return DatePickerRow(
          dateScreen: 'InvoiceNumberDueDate',
          fieldName: 'Due Date:',
          size: size,
          initialDate: ref.watch(invoiceDueDateProvider)

          //invoice.invoiceNumberDetails.dueDate ??
          //  invoice.invoiceNumberDetails.invoiceDate,
          );
    } else {
      return const SizedBox();
    }
  }
}

class DatePickerWidget extends ConsumerWidget {
  const DatePickerWidget({
    super.key,
    required this.size,
    required this.invoice,
  });

  final Size size;
  final InvoiceEditDTO? invoice;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DatePickerRow(
        dateScreen: 'InvoiceNumberDate',
        fieldName: 'Date:',
        size: size,
        initialDate: ref.watch(invoiceDateProvider));
  }
}

class NumberAndFieldRow extends StatefulWidget {
  const NumberAndFieldRow(
      {super.key,
      required this.size,
      required this.textController,
      required this.fieldName,
      this.initialValue = ''});

  final Size size;
  final TextEditingController textController;
  final String fieldName;
  final String initialValue;

  @override
  State<NumberAndFieldRow> createState() => _NumberAndFieldRowState();
}

class _NumberAndFieldRowState extends State<NumberAndFieldRow> {
  @override
  void initState() {
    if (widget.initialValue.isNotEmpty) {
      widget.textController.text = widget.initialValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: widget.size.height * 0.058,
          child: Center(
            child: Text(widget.fieldName,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: widget.size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w500,
                  ),
                )),
          ),
        ),
        SizedBox(
          width: widget.size.width * 0.5,
          height: widget.size.height * 0.058,
          child: Align(
            alignment: Alignment.topCenter,
            child: TextField(
              // onChanged: (value) {
              //   widget.textController.text = value;
              // },
              textAlign: TextAlign.right, // Align text to the right
              textDirection: TextDirection.ltr,
              decoration: const InputDecoration(border: InputBorder.none),
              controller: widget.textController,
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: widget.size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}

class DatePickerRow extends StatelessWidget {
  const DatePickerRow(
      {super.key,
      required this.size,
      required this.fieldName,
      this.initialDate,
      required this.dateScreen});

  final Size size;

  final String fieldName;
  final DateTime? initialDate;
  final String dateScreen;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(fieldName,
            style: GoogleFonts.roboto(
              textStyle: TextStyle(
                letterSpacing: .5,
                fontSize: size.width * 0.04,
                color: LinqPeColors.kBlackColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        InkWell(
          onTap: () {
            showDatePickers(context, size, dateScreen);
          },
          child: Text(
              initialDate != null
                  ? dateTimeToStringConverter(initialDate!)
                  : '               ',
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
        ),
      ],
    );
  }
}

class TermsRow extends ConsumerWidget {
  const TermsRow({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return InkWell(
      onTap: () {
        openTermsBottomSheet(context, size, ref);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('Terms:',
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
          Text(ref.watch(termsProvider),
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
        ],
      ),
    );
  }
}

openTermsBottomSheet(BuildContext context, Size size, WidgetRef ref) {
  showModalBottomSheet(
    backgroundColor: LinqPeColors.kWhiteColor,
    context: context,
    builder: (BuildContext context) {
      return Container(
        width: size.width,
        decoration: const BoxDecoration(
            color: LinqPeColors.kWhiteColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50))),
        height: size.height * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () {
                addterms('None', ref);
                Navigator.pop(context);
              },
              child: Text('None',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            InkWell(
              onTap: () {
                addterms('Due On Receipt', ref);
                Navigator.pop(context);
              },
              child: Text('Due On Receipt',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            InkWell(
              onTap: () {
                addterms('Custom', ref);
                Navigator.pop(context);
              },
              child: Text('Custom',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
          ],
        ),
      );
    },
  );
}
