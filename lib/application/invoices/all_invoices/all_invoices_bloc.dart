import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/infrastructure/invoices/invoices_implementation.dart';

part 'all_invoices_event.dart';
part 'all_invoices_state.dart';
part 'all_invoices_bloc.freezed.dart';

class AllInvoicesBloc extends Bloc<AllInvoicesEvent, AllInvoicesState> {
  AllInvoicesBloc() : super(const allInvoiceInitial()) {
      on<getAllInvoicesList>((event, emit) async {
      final invoiceModelList =
          InvoicesImplementation.instance.getAllInvoiceList();
      List<InvoiceEditDTO> invoiceList = [];
      for (var invoiceModel in invoiceModelList) {
        invoiceList.add(await convertInvoiceEditModelToDTO(invoiceModel));
      }

      emit(displayInvoiceList(invoiceList: invoiceList));
    });
  }
}
