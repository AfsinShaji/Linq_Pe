import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/all_invoices/all_invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/item_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/reports/tabs/paid/screen_paid.dart';
import 'package:linq_pe/presentation/view_state/invoice/report/invoice_report.dart';
import 'package:linq_pe/utilities/colors.dart';

class ItemScreen extends ConsumerWidget {
  const ItemScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    return BlocBuilder<AllInvoicesBloc, AllInvoicesState>(
      builder: (context, invoicestate) {
        List<InvoiceEditDTO> invoiceList = [];

        List<ItemDTO> itemList = [];

        List<int> yearList = [];
        double totalItemPayment = 0.00;
        if (invoicestate is displayInvoiceList) {
          invoiceList = invoicestate.invoiceList
              .where((element) => element.isInvoice == true)
              .toList();
        }
        if (invoiceList.isNotEmpty) {
          for (var invoice in invoiceList) {
            if (invoice.invoiceNumberDetails.invoiceDate.year ==
                ref.watch(yearProvider)) {
              if (invoice.itemsList != null && invoice.itemsList!.isNotEmpty&&invoice.isPaid == true) {
                for (var item in invoice.itemsList!) {
                  if (invoice.isPaid == true) {
                    totalItemPayment =
                        totalItemPayment + (item.quantity * item.unitCost);
                  }

                  if (itemList
                      .where(
                          (element) => element.description == item.description&&element.unitCost==item.unitCost)
                      .isEmpty) {
                    itemList.add(item);
                  }
                }
              }
            }

            if (!yearList
                .contains(invoice.invoiceNumberDetails.invoiceDate.year)) {
              yearList.add(invoice.invoiceNumberDetails.invoiceDate.year);
            }
          }
        }
        return SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(size.width * 0.05),
            child: Column(
              children: [
                Row(
                  children: [
                    TableHeaders(size: size, width: size.width * 0.2),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.2,
                      headerText: 'Invoices',
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.2,
                      headerText: 'Qty',
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.3,
                      headerText: 'Paid',
                    )
                  ],
                ),
                const Divider(),
                Row(
                  children: [
                    TableHeaders(
                        size: size,
                        width: size.width * 0.2,
                        headerText: 'Tax Year ${ref.watch(yearProvider)}'),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.2,
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.2,
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.3,
                      headerText: '₹ $totalItemPayment',
                    )
                  ],
                ),
                const Divider(),
                SizedBox(
                  height: size.height * 0.005,
                ),
                Column(
                  children: List.generate(itemList.length, (index) {
                    int itemInvoiceNumber = 0;
                    int itemQuantity = 0;
                    double itemPaidAmount = 0.00;
                    for (var invoice in invoiceList
                        .where((element) =>
                            element.invoiceNumberDetails.invoiceDate.year ==
                            ref.watch(yearProvider))
                        .toList()) {
                      if (invoice.itemsList != null &&
                          invoice.itemsList!.isNotEmpty) {
                        for (var item in invoice.itemsList!) {
                          if (item.description == itemList[index].description&&item.unitCost==itemList[index].unitCost) {
                            itemInvoiceNumber = itemInvoiceNumber + 1;
                            itemQuantity = itemQuantity + item.quantity;
                          }
                        }
                      }
                    }

                    itemPaidAmount = itemList[index].unitCost * itemQuantity;

                    return Column(
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: size.width * 0.2,
                              child: Text(itemList[index].description,
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                      letterSpacing: .1,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            ),
                            SizedBox(
                              width: size.width * 0.2,
                              child: Text('$itemInvoiceNumber',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                      letterSpacing: .1,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            ),
                            SizedBox(
                              width: size.width * 0.2,
                              child: Text('$itemQuantity',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                      letterSpacing: .1,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            ),
                            SizedBox(
                              width: size.width * 0.3,
                              child: Text('₹ $itemPaidAmount',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                      letterSpacing: .1,
                                      fontSize: size.width * 0.04,
                                      color: LinqPeColors.kBlackColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            )
                          ],
                        ),
                        const Divider()
                      ],
                    );
                  }),
                ),
                SizedBox(
                  height: size.height * 0.005,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                      yearList.length,
                      (index) => YearContainer(
                            yearList: yearList,
                            size: size,
                            index: index,
                          )),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
