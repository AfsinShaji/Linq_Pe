// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invoice_number.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class InvoiceNumberModelAdapter extends TypeAdapter<InvoiceNumberModel> {
  @override
  final int typeId = 12;

  @override
  InvoiceNumberModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return InvoiceNumberModel(
      dueDate: fields[3] as DateTime?,
      invoiceDate: fields[2] as DateTime,
      invoiceId: fields[0] as String,
      invoiceNumber: fields[1] as String,
      poNumber: fields[4] as String?,
      terms: fields[5] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, InvoiceNumberModel obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.invoiceId)
      ..writeByte(1)
      ..write(obj.invoiceNumber)
      ..writeByte(2)
      ..write(obj.invoiceDate)
      ..writeByte(3)
      ..write(obj.dueDate)
      ..writeByte(4)
      ..write(obj.poNumber)
      ..writeByte(5)
      ..write(obj.terms);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is InvoiceNumberModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
