import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:linq_pe/domain/core/failure/failure.dart';
import 'package:linq_pe/domain/repositories/authentication/authentication_repository.dart';
import 'package:linq_pe/infrastructure/services/secure_storage.dart';
import 'package:linq_pe/infrastructure/users/users_implementation.dart';

class AuthenticationImplementation extends AuthenticationRepository {
  // creating a singleton
  AuthenticationImplementation.internal();
  static AuthenticationImplementation instance =
      AuthenticationImplementation.internal();
  AuthenticationImplementation factory() {
    return instance;
  }

///////////////////////////////////////////
  @override
  Future<Either<MainFailure, String>> login(
      {required String email, required String password}) async {
    String result = '';
    try {
      //  final resultCredential =
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(
        email: email,
        password: password,
      )
          .then((value) async {
        await UsersImplementation.instance.addLoginSecureData();

        result = 'success';
      }).onError((error, stackTrace) {
        result = error.toString();
      });
      return Right(result);
    } catch (e) {
      log(e.toString());
      return const Left(MainFailure.clientFailure());
    }
  }

  Future<Either<MainFailure, String>> logOut() async {
    try {
      String result = '';
      await FirebaseAuth.instance
          .signOut()
          .onError((error, stackTrace) => result = 'failure');
      if (result == 'failure') {
        return const Left(MainFailure.serverFailure());
      }
      await StorageService.instance.deleteAllSecureData();
      return const Right('success');
    } catch (e) {
      return const Left(MainFailure.clientFailure());
    }
  }

  @override
  Future<Either<MainFailure, String>> userRegistration(
      {required String name,
      required String email,
      required String phoneNumber,
      required String businessType,
      required String purpose,
      required String password}) async {
    String result = '';
    try {
      //  final resultCredential =
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
        email: email,
        password: password,
      )
          .then((value) async {
        await UsersImplementation.instance.addUser(
            name: name,
            email: email,
            phoneNumber: phoneNumber,
            businessType: businessType,
            purpose: purpose);

        result = 'success';
      }).onError((error, stackTrace) {
        result = error.toString();
      });
      return Right(result);
    } catch (e) {
      log(e.toString());
      return const Left(MainFailure.clientFailure());
    }
  }
}
