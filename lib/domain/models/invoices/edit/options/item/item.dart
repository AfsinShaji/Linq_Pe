

import 'package:hive/hive.dart';

part 'item.g.dart';

@HiveType(typeId: 19)
class ItemModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final String description;
  @HiveField(2)
  final double unitCost;
  @HiveField(3)
  final String? unit;
  @HiveField(4)
  final int quantity;
  @HiveField(5)
  final String? discount;
  @HiveField(6)
  final double? discountAmount;
  @HiveField(7)
  final bool isTaxable;
  @HiveField(8)
  final double? taxRate;
  @HiveField(9)
  final String? addiotionalDetails;
  @HiveField(10)
  final double total;
  @HiveField(11)
  final String itemId;

  ItemModel( {
  required  this.itemId,
    required this.invoiceId,
    required this.addiotionalDetails,
    required this.discount,
    required this.description,
    required this.discountAmount,
    required this.isTaxable,
    required this.quantity,
    required this.taxRate,
    required this.unit,
    required this.unitCost, required this.total
  });
}

class ItemBox {
  static Box<ItemModel>? _getbox;
  static Box<ItemModel> getInstance() {
    return _getbox ??= Hive.box('ItemBox');
  }
}
