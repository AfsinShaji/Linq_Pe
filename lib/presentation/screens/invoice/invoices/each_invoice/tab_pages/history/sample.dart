import 'package:flutter/material.dart';

class MyScreen extends StatefulWidget {
  const MyScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _MyScreenState createState() => _MyScreenState();
}
class _MyScreenState extends State<MyScreen> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Offset> _fabAnimation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500), // Adjust animation duration as needed
    );
    _fabAnimation = Tween<Offset>(
      begin: const Offset(-1.0, 0.0), // Start from the left edge
      end: Offset.zero, // End at the center
    ).animate(_animationController);
    _animationController.forward(); // Start animation immediately
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  // ... rest of your widget code
  @override
Widget build(BuildContext context) {
  return Scaffold(
    // ... other Scaffold properties
    body: Stack(
      children: [
        // Your other screen content here
        AnimatedBuilder(
          animation: _fabAnimation,
          builder: (context, child) {
            return Transform.translate(
              offset: _fabAnimation.value,
              child: FloatingActionButton(
                onPressed: () {},
                child: const Icon(Icons.add),
              ),
            );
          },
        ),
      ],
    ),
  );
}

}
