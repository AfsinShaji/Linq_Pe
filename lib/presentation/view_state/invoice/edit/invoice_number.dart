import 'package:flutter_riverpod/flutter_riverpod.dart';

final invoiceDateProvider = StateProvider(
  (ref) {
    DateTime? date;
    return date;
  },
);
addinvoiceDate(DateTime date, WidgetRef ref) {
  ref.read(invoiceDateProvider.notifier).state = date;
}

final invoiceDueDateProvider = StateProvider(
  (ref) {
    DateTime? date;
    return date;
  },
);
addinvoiceDueDate(DateTime date, WidgetRef ref) {
  ref.read(invoiceDueDateProvider.notifier).state = date;
}

final termsProvider = StateProvider(
  (ref) {
   
    return 'None';
  },
);
addterms(String term, WidgetRef ref) {
  ref.read(termsProvider.notifier).state = term;
}