import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/authentication/authentication_bloc.dart';
import 'package:linq_pe/application/users/users/users_bloc.dart';
import 'package:linq_pe/application/view_dto/user/user.dart';
import 'package:linq_pe/presentation/screens/authentication/login/screen_login.dart';
import 'package:linq_pe/presentation/widgets/alert_box.dart';

import 'package:linq_pe/presentation/widgets/shimmer_loading.dart';
import 'package:linq_pe/utilities/colors.dart';

class ViewUserScreen extends StatefulWidget {
  const ViewUserScreen({super.key});

  @override
  State<ViewUserScreen> createState() => _ViewUserScreenState();
}

class _ViewUserScreenState extends State<ViewUserScreen> {
  @override
  void initState() {
    BlocProvider.of<UsersBloc>(context).add(const UsersEvent.getUserDetail());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    UserProfileDTO? user;
    return Shimmer(
      linearGradient: shimmerGradient,
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: size.height * 0.1,
          leading: const SizedBox(),
          leadingWidth: 0,
          title: Text('User Details',
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.07,
                  color: LinqPeColors.kWhiteColor,
                  fontWeight: FontWeight.w700,
                ),
              )),
          backgroundColor: LinqPeColors.kPinkColor,
        ),
        body: BlocBuilder<UsersBloc, UsersState>(builder: (context, userState) {
          bool isLoading = false;
          if (userState is viewUser) {
            if (userState.isLoading) {
              isLoading = true;
              // return const Center(
              //   child: CircularProgressIndicator(),
              // );
            } else if (userState.isError) {
              isLoading = false;
            }
    
            if (userState.user != null) {
              isLoading = false;
              user = userState.user;
            }
          }
    
          return ShimmerLoading(
            isLoading: isLoading,
            child: Container(
              padding: EdgeInsets.all(size.width * 0.04),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: LinqPeColors.kPinkColor,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    padding: EdgeInsets.all(size.width * 0.04),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: size.width * 0.13,
                          backgroundImage:
                              user == null || user!.businessLogo.isEmpty
                                  ? null
                                  : NetworkImage(user!.businessLogo),
                        ),
                        SizedBox(width: size.width * 0.04),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              user == null ? '' : user!.name,
                              style: const TextStyle(
                                fontSize: 24.0,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              user == null ? '' : user!.email,
                              style: const TextStyle(
                                fontSize: 16.0,
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              user == null ? '' : user!.phoneNumber,
                              style: const TextStyle(
                                fontSize: 16.0,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: size.height * 0.025),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    padding: EdgeInsets.all(size.width * 0.04),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Business Details:',
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: size.height * 0.01),
                        Text(
                            'Business Name: ${user == null ? '' : user!.businessName}'),
                        Text('Purpose: ${user == null ? '' : user!.purpose}'),
                        Text(
                            'Business Type: ${user == null ? '' : user!.businessType}'),
                      ],
                    ),
                  ),
                  SizedBox(height: size.height * 0.035),
                  ElevatedButton(
                    onPressed: () {
                      alertBox(context, 'Logging out', () {
                        BlocProvider.of<AuthenticationBloc>(context)
                            .add(const AuthenticationEvent.loggingOut());
                        Navigator.pushAndRemoveUntil(
                            context,
                            CupertinoPageRoute(
                              builder: (context) => LoginScreen(),
                            ),
                            (route) => false);
                      }, size);
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: LinqPeColors.kPinkColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(size.width * 0.03),
                      child: const Text(
                        'Logout',
                        style: TextStyle(
                            fontSize: 18.0, color: LinqPeColors.kWhiteColor),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
