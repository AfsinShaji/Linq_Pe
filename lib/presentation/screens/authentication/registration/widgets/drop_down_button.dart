import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:linq_pe/presentation/view_state/registration_riverpod.dart/registration.dart';
import 'package:linq_pe/utilities/colors.dart';

// ignore: must_be_immutable
class DropdownButtton extends ConsumerWidget {
  const DropdownButtton({
    super.key,
    required this.items,
    required this.isBusiness,
    required this.isPurpose,
  });

  final List<String> items;

  final bool isBusiness;
  final bool isPurpose;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    String selectedValue = '';
    if (isBusiness) {
      selectedValue = ref.watch(businessTypeProvider);
    } else if (isPurpose) {
      selectedValue = ref.watch(purposeTypeProvider);
    }
    final Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.07,
      width: size.width,
      decoration: BoxDecoration(
          color: LinqPeColors.kWhiteColor,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: LinqPeColors.kWhiteColor)),
      alignment: Alignment.center,
      child: SizedBox(
        width: size.width * 0.8,
        child: DropdownButton(
          underline: const SizedBox(),
          focusColor: LinqPeColors.kBlackColor,
          alignment: Alignment.center,
          isExpanded: true,
          dropdownColor: LinqPeColors.kWhiteColor,
          value: selectedValue,
          items: items
              .map<DropdownMenuItem>((value) => DropdownMenuItem(
                    alignment: Alignment.center,
                    value: value,
                    child: Container(
                      width: size.width * 0.9,
                
                      decoration: BoxDecoration(
                          color: LinqPeColors.kWhiteColor,
                          border: Border.all(
                            color: LinqPeColors.kWhiteColor,
                          ),
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomLeft: Radius.circular(20))),
                      child: Center(
                        child: Text(
                          value,
                          textAlign: TextAlign.center,
                          style:
                              // GoogleFonts.poppins(
                              //   textStyle:
                              const TextStyle(
                            // letterSpacing: .5,
                            fontSize: 14,
                            color: LinqPeColors.kBlackColor,
                            //  fontWeight: FontWeight.w500,
                          ),
                          // ),
                        ),
                      ),
                    ),
                  ))
              .toList(),
          onChanged: (newvalue) {
            if (isBusiness) {
              addBusinessType(newvalue, ref);
            } else if (isPurpose) {
              addPurposeType(newvalue, ref);
            }
          },
        ),
      ),
    );
  }
}
