// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payments.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PaymentsModelAdapter extends TypeAdapter<PaymentsModel> {
  @override
  final int typeId = 20;

  @override
  PaymentsModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PaymentsModel(
      paymentId: fields[5] as String,
      invoiceId: fields[0] as String,
      amount: fields[2] as double,
      date: fields[3] as DateTime,
      notes: fields[4] as String,
      paymentMethod: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, PaymentsModel obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.invoiceId)
      ..writeByte(1)
      ..write(obj.paymentMethod)
      ..writeByte(2)
      ..write(obj.amount)
      ..writeByte(3)
      ..write(obj.date)
      ..writeByte(4)
      ..write(obj.notes)
      ..writeByte(5)
      ..write(obj.paymentId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PaymentsModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
