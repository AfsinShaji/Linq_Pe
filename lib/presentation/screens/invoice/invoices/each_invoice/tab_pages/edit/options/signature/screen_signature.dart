import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/signature.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';
import 'dart:ui' as ui;

class SignatureScreen extends ConsumerStatefulWidget {
  const SignatureScreen(this.signature, {Key? key}) : super(key: key);
  final File? signature;

  @override
  SignatureScreenState createState() => SignatureScreenState();
}

class SignatureScreenState extends ConsumerState<SignatureScreen> {
  final GlobalKey<SfSignaturePadState> signatureGlobalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  void handleClearButtonPressed(File? sign) {
    if (sign != null) {
      addsignature(null, ref);
    } else {
      signatureGlobalKey.currentState!.clear();
    }
  }

  void _handleSaveButtonPressed({required String invoiceId}) async {
    if (signatureGlobalKey.currentState == null) {
      Navigator.pop(context);
      return;
    }
    final data = await signatureGlobalKey.currentState!.toImage(pixelRatio: 3.0);
    final bytes = await data.toByteData(format: ui.ImageByteFormat.png);
// Check if bytes is not null before proceeding
    if (bytes != null) {
      final buffer = bytes.buffer.asUint8List();
      final tempDir = await getTemporaryDirectory();
      final file = File('${tempDir.path}/signature.png');

      await file
          .writeAsBytes(buffer)
          .then((value) => BlocProvider.of<InvoicesBloc>(context).add(
              InvoicesEvent.addsSignature(
                  invoiceId: invoiceId, signature: file)))
          .then((value) => Navigator.pop(context));

      // Now 'file' contains the signature as a File
      // You can use 'file' as needed, for example, upload it to a server, etc.
    }

    //  Navigator.of(context).push(
    //   MaterialPageRoute(
    //     builder: (BuildContext context) {
    //       return Scaffold(
    //         appBar: AppBar(),
    //         body: Center(
    //           child: Container(
    //             color: Colors.grey[300],
    //             child: Image.memory(bytes!.buffer.asUint8List()),
    //           ),
    //         ),
    //       );
    //     },
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    // WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    //   if (widget.signature != null) {

    // }
    // });
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.7),
        appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: LinqPeColors.kWhiteColor,
              )),
          titleSpacing: size.width * 0.2,
          title: Text('Signature',
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  letterSpacing: .8,
                  fontSize: size.width * 0.06,
                  color: LinqPeColors.kWhiteColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
          backgroundColor: LinqPeColors.kPinkColor,
          shadowColor: LinqPeColors.kBlackColor,
        ),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                  padding: const EdgeInsets.all(10),
                  child: Container(
                      decoration:
                          BoxDecoration(border: Border.all(color: Colors.grey)),
                      child: ref.watch(signatureProvider) != null
                          ? Image.file(ref.watch(signatureProvider)!)
                          : SfSignaturePad(
                              key: signatureGlobalKey,
                              backgroundColor: Colors.white,
                              strokeColor: Colors.black,
                              minimumStrokeWidth: 1.0,
                              maximumStrokeWidth: 4.0))),
              const SizedBox(height: 10),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    TextButton(
                      onPressed: () {
                        _handleSaveButtonPressed(
                            invoiceId: ref.watch(invoiceIdProvider));
                      },
                      child: Text('Save',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              letterSpacing: .5,
                              fontSize: size.width * 0.04,
                              color: LinqPeColors.kBlackColor,
                              fontWeight: FontWeight.w500,
                            ),
                          )),
                    ),
                    TextButton(
                      onPressed: () {
                        handleClearButtonPressed(ref.watch(signatureProvider));
                      },
                      child: Text('Clear',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              letterSpacing: .5,
                              fontSize: size.width * 0.04,
                              color: LinqPeColors.kBlackColor,
                              fontWeight: FontWeight.w500,
                            ),
                          )),
                    )
                  ])
            ]));
  }
}
