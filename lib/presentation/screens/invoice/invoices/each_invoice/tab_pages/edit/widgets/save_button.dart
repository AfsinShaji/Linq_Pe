import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/utilities/colors.dart';

class SaveButton extends StatelessWidget {
  const SaveButton({
    super.key,
    required this.onTap,
  });
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return ColoredBox(
      color: LinqPeColors.kWhiteColor.withOpacity(0.6),
      child: Padding(
        padding: EdgeInsets.only(
            left: size.width * 0.05,
             right: size.width * 0.05,
              bottom: size.width * 0.05,
              top: size.width * 0.01,
            //  vertical: size.width * 0.05
             
             ),
        child: Material(
          child: InkWell(
            onTap: onTap,
            child: Container(
                // margin: EdgeInsets.symmetric(
                //     horizontal: size.width * 0.05, vertical: size.width * 0.05),
                height: size.height * 0.065,
                width: size.width,
                decoration: const BoxDecoration(color: LinqPeColors.kPinkColor),
                child: Center(
                    child: Text(
                  'SAVE',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kWhiteColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ))),
          ),
        ),
      ),
    );
  }
}
