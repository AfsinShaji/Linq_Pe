import 'package:linq_pe/domain/models/invoices/edit/options/item/item.dart';

abstract class MyItemsRepository {
  Future<void> addToMyItems({required ItemModel item});
  List<ItemModel> getAllMyItems();
}
