
import 'package:flutter_riverpod/flutter_riverpod.dart';

final businessDetailsBusinessLogoProvider = StateProvider(
  (ref) {
    return '';
  },
);
addBusinessDetailLogo(String id, WidgetRef ref) {

  ref.read(businessDetailsBusinessLogoProvider.notifier).state = id;
}