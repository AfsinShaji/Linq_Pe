import 'package:cloud_firestore/cloud_firestore.dart';

class UserProfile {
  final String userName;
  final String userId;
  final String userEmail;
  final String phoneNumber;
  final String businessType;
  final String purpose;
  final DateTime? trialStartDate;
  final bool isPremium;
  final String businessName;
  final String businessLogo;

  UserProfile({
    required this.businessName,
    required this.businessLogo,
    required this.userName,
    required this.userId,
    required this.phoneNumber,
    required this.userEmail,
    required this.businessType,
    required this.purpose,
    required this.trialStartDate,
    required this.isPremium,
  });

  Map<String, dynamic> toFirebase() {
    Timestamp? timeStamp;
    if (trialStartDate == null) {
      timeStamp = null;
    } else {
      timeStamp = Timestamp.fromDate(trialStartDate!.toUtc());
    }
    return {
      'userName': userName,
      'userId': userId,
      'phoneNumber': phoneNumber,
      'userEmail': userEmail,
      'businessType': businessType,
      'purpose': purpose,
      'trialStartDate': timeStamp,
      'isPremium': isPremium,
      'businessName': businessName,
      'businessLogo': businessLogo,
    };
  }

  Map<String, dynamic> addTrialDate(DateTime trialDate) {
    final timeStamp = Timestamp.fromDate(trialDate.toUtc());
    return {
      'userName': userName,
      'userId': userId,
      'phoneNumber': phoneNumber,
      'userEmail': userEmail,
      'businessType': businessType,
      'purpose': purpose,
      'trialStartDate': timeStamp,
      'isPremium': isPremium,
      'businessName': businessName,
      'businessLogo': businessLogo,
    };
  }
  Map<String, dynamic> addPremium() {
   
    return {
      'userName': userName,
      'userId': userId,
      'phoneNumber': phoneNumber,
      'userEmail': userEmail,
      'businessType': businessType,
      'purpose': purpose,
      'trialStartDate': trialStartDate,
      'isPremium': true,
      'businessName': businessName,
      'businessLogo': businessLogo,
    };
  }
  Map<String, dynamic> addUserBusiness({required String userPusrpose,required String userBusinessName,
  required String userBusinessLogo,required String userBusinessType}){
       return {
      'userName': userName,
      'userId': userId,
      'phoneNumber': phoneNumber,
      'userEmail': userEmail,
      'businessType': userBusinessType,
      'purpose': userPusrpose,
      'trialStartDate': trialStartDate,
      'isPremium': isPremium,
      'businessName': userBusinessName,
      'businessLogo': userBusinessLogo,
    };

  }

  static UserProfile fromFirebas(Map<String, dynamic> json) {
    Timestamp? timestamp = json['trialStartDate'];

    final startDate = timestamp?.toDate();
    return UserProfile(
      businessLogo: json['businessLogo'],
      businessName: json['businessName'],
      isPremium: json['isPremium'],
      trialStartDate: startDate,
      businessType: json['businessType'],
      purpose: json['purpose'],
      userName: json['userName'],
      userId: json['userId'],
      phoneNumber: json['phoneNumber'],
      userEmail: json['userEmail'],
    );
  }
}
