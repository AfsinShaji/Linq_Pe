
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linq_pe/infrastructure/razorpay/razorpay_implementation.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

part 'razorpay_event.dart';
part 'razorpay_state.dart';
part 'razorpay_bloc.freezed.dart';

class RazorpayBloc extends Bloc<RazorpayEvent, RazorpayState> {
  RazorpayBloc() : super(const razorpayInitial()) {
    on<getRazorpayKey>((event, emit) async {
      final razorPayKey =
          await RazorPayImplementation.instance.getRazorPayKey();
      if (razorPayKey.isNotEmpty) {
        emit(displayRazorpayKey(razorpayKey: razorPayKey));
      }
    });
    on<razorpayPayments>((event, emit) async {
      emit(const paymentResult(
          isLoading: true, isFailed: false, isError: false, isSuccess: false));
      try {
        await RazorPayImplementation.instance
            .razorpayPayment(razorpay: event.razorpay);
        emit(const paymentResult(
            isLoading: false,
            isFailed: false,
            isError: false,
            isSuccess: false));
        // event.razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS,
        //     (PaymentSuccessResponse response) async {
        //   // Handle payment success, e.g., makeUserPremium()
        // });

        // event.razorpay.on(Razorpay.EVENT_PAYMENT_ERROR,
        //     (PaymentFailureResponse response) {
        //   // Handle payment error, emit appropriate state
        // });

        // event.razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET,
        //     (ExternalWalletResponse response) {
        //   // Handle external wallet, emit appropriate state
        // });

        // razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, () async {
        //   await UsersImplementation.instance.makeUserPremium();
        //   emit(const RazorpayState.paymentResult(
        //       isLoading: false,
        //       isFailed: false,
        //       isError: false,
        //       isSuccess: true));
        // });
        // razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, () {
        //   log('Razorpay Payment Error');
        //   emit(const RazorpayState.paymentResult(
        //       isLoading: false,
        //       isFailed: true,
        //       isError: false,
        //       isSuccess: false));
        // });
        // razorpay.on(
        //     Razorpay.EVENT_EXTERNAL_WALLET,
        //     () => emit(const RazorpayState.paymentResult(
        //         isLoading: false,
        //         isFailed: true,
        //         isError: false,
        //         isSuccess: true)));
      } catch (e) {
        emit(const RazorpayState.paymentResult(
            isLoading: false,
            isFailed: false,
            isError: true,
            isSuccess: false));
      }
    });
  }
}
