import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linq_pe/application/authentication/authentication_bloc.dart';
import 'package:linq_pe/presentation/screens/authentication/login/widgets/signup_option.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/loading_overlay.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/logo_widget.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/sign_button.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/sign_text_field.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/text_before_field.dart';
import 'package:linq_pe/utilities/colors.dart';

class LoginScreen extends StatelessWidget {
  final formKey = GlobalKey<FormState>();
  final TextEditingController passwordTextController = TextEditingController();
  final TextEditingController emailTextController = TextEditingController();

  LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      // backgroundColor: LinqPeColors.kPinkColor.withOpacity(0.95),
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            LinqPeColors.kPinkColor.withOpacity(0.7),
            LinqPeColors.kPinkColor.withOpacity(0.9)
          ],
          // begin: Alignment.topLeft,
          // end: Alignment.bottomRight,
        )),
        child: SafeArea(
          child: Center(
            child: Form(
              key: formKey,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
                      builder: (context, authState) {
                    if (authState is authResults) {
                      if (authState.isLoading) {
                        Future.delayed(Duration.zero, () {
                          showLoadingOverlay(context);
                        });
                      }
                    }
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const LogoWidget(),
                        const TextbeforeField(text: 'Email'),
                        SizedBox(
                          height: size.height * 0.01,
                        ),
                        SignTextField(
                          controller: emailTextController,
                          text: "Enter Email Id",
                          icon: Icons.person_2_outlined,
                          isTextPasswordType: false,
                          isTextEmailType: true,
                        ),
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        const TextbeforeField(text: 'Password'),
                        SizedBox(
                          height: size.height * 0.01,
                        ),
                        SignTextField(
                          controller: passwordTextController,
                          text: "Enter password",
                          icon: Icons.lock_outline,
                          isTextPasswordType: true,
                        ),
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        // const Align(
                        //     alignment: Alignment.centerRight,
                        //     child: ForgotPasswordButton()),

                        SignButton(
                            text: "LOG IN",
                            onTap: () {
                              if (formKey.currentState!.validate()) {
                                BlocProvider.of<AuthenticationBloc>(context)
                                    .add(AuthenticationEvent.logIn(
                                        email: emailTextController.text,
                                        password: passwordTextController.text,
                                        context: context));
                              }
                            },
                            width: size.width),
                        const Align(
                          alignment: Alignment.center,
                          child: Text(
                            'or',
                            style: TextStyle(color: LinqPeColors.kWhiteColor),
                          ),
                        ),

                        const SignupOption()
                      ],
                    );
                  }),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
