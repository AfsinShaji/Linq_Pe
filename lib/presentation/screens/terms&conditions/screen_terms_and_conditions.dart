
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/utilities/colors.dart';
import 'package:linq_pe/utilities/strings.dart';

class TermsAndConditionsScreen extends StatelessWidget {
  const TermsAndConditionsScreen({super.key});

  @override
  Widget build(BuildContext context) {
     final Size size = MediaQuery.of(context).size;
    return Scaffold(
            backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.1,
        title: Text('Terms&Conditions',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      body: SingleChildScrollView(
        child: SafeArea(child:   Center(
                    child: Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Text(LinqPeStrings.termsAndConditions,
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              letterSpacing: .5,
                              fontSize: 16,
                              color: LinqPeColors.kBlackColor,
                              fontWeight: FontWeight.w500,
                            ),
                          )),
                    ),
                  )),
      ),
    );
  }
}