import 'package:hive/hive.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/item/item.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/my_items/my_items.dart';
import 'package:linq_pe/domain/repositories/invoices/opions/my_items/my_items_repository.dart';

class MyItemsImplementation extends MyItemsRepository {
  // creating a singleton
  MyItemsImplementation.internal();
  static MyItemsImplementation instance = MyItemsImplementation.internal();
  MyItemsImplementation factory() {
    return instance;
  }

///////////////////////////////////////////
  late Box<MyItemsModel> myItemsBox;
  openMyItemsBox() async {
    myItemsBox = await Hive.openBox("MyItemsBox");
  }

  @override
  Future<void> addToMyItems({required ItemModel item}) async {
    if (myItemsBox.isEmpty) {
      final myItem = MyItemsModel(itemsList: [item]);
      await myItemsBox.add(myItem);
    } else {
      final myItem = myItemsBox.getAt(0);
      final itemList = myItem!.itemsList;
      itemList.add(item);
      myItemsBox.putAt(0, MyItemsModel(itemsList: itemList));
    }
  }

  @override
  List<ItemModel> getAllMyItems() {
    if(myItemsBox.isEmpty){
       return [];
    }
    final myItem = myItemsBox.getAt(0);
    
    if (myItem == null) {
      return [];
    } else {
      return [...myItem.itemsList];
    }
  }
}
