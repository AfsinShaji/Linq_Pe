import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/discount.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

class ScreenDiscount extends StatelessWidget {
  ScreenDiscount({super.key});
  final TextEditingController discountTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.2,
        title: Text('Discount',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButtonWidget(percentage: discountTextController.text),
      body: SafeArea(
          child: Container(
        padding: EdgeInsets.all(size.width * 0.01),
        width: size.width,
        height: size.height * 0.17,
        child: EditContainerWidget(
          child: BlocBuilder<InvoicesBloc, InvoicesState>(
            builder: (context, invoiceState) {
              InvoiceEditDTO? invoice;

              if (invoiceState is displayInvoice) {
                invoice = invoiceState.invoice;
              }
              if (invoice == null) {
                return const SizedBox();
              }
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  DiscountRow(size: size),
                  SizedBox(
                    height: size.height * 0.005,
                  ),
                  DiscountFieldRowWidget(
                      invoice: invoice,
                      size: size,
                      discountTextController: discountTextController)
                ],
              );
            },
          ),
        ),
      )),
    );
  }
}

class DiscountFieldRowWidget extends ConsumerStatefulWidget {
  const DiscountFieldRowWidget({
    super.key,
    required this.invoice,
    required this.size,
    required this.discountTextController,
  });

  final InvoiceEditDTO? invoice;
  final Size size;
  final TextEditingController discountTextController;

  @override
  DiscountFieldRowWidgetState createState() => DiscountFieldRowWidgetState();
}

class DiscountFieldRowWidgetState extends ConsumerState<DiscountFieldRowWidget> {
  @override
  Widget build(BuildContext context) {
    if (ref.watch(discountTypeProvider) == 'No Discount') {
      return const SizedBox();
    }
    return DiscountAndFieldRow(
      initialValue: widget.invoice!.discountDetails == null
          ? ''
          : widget.invoice!.discountDetails!.percentage.toString(),
      fieldName: ref.watch(discountTypeProvider) == 'Flat Amount'
          ? 'Amount'
          : 'Percentage',
      size: widget.size,
      textController: widget.discountTextController,
    );
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget({
    super.key,
    required this.percentage,
  });
  final String percentage;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(
      onTap: () {
        if (
            ref.watch(invoiceIdProvider).isNotEmpty 
           ) {
          BlocProvider.of<InvoicesBloc>(context).add(
              InvoicesEvent.addDiscountDetail(
                  invoiceId: ref.watch(invoiceIdProvider),
                  discountType: ref.watch(discountTypeProvider),
                  percentage:percentage.isNotEmpty? double.parse(percentage):0.00));
          Navigator.pop(context);
        }
      },
    );
  }
}

class DiscountRow extends ConsumerWidget {
  const DiscountRow({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return InkWell(
      onTap: () {
        openDiscountBottomSheet(context, size, ref);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('Discount:',
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
          Text(ref.watch(discountTypeProvider),
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
        ],
      ),
    );
  }
}

class DiscountAndFieldRow extends StatefulWidget {
  const DiscountAndFieldRow({
    super.key,
    required this.size,
    required this.textController,
    required this.fieldName,
    required this.initialValue,
  });

  final Size size;
  final TextEditingController textController;
  final String fieldName;
  final String initialValue;

  @override
  State<DiscountAndFieldRow> createState() => _DiscountAndFieldRowState();
}

class _DiscountAndFieldRowState extends State<DiscountAndFieldRow> {
  @override
  void initState() {
    if (widget.initialValue.isNotEmpty) {
      widget.textController.text = widget.initialValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: widget.size.height * 0.058,
          child: Center(
            child: Text(widget.fieldName,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: widget.size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w500,
                  ),
                )),
          ),
        ),
        SizedBox(
          width: widget.size.width * 0.5,
          height: widget.size.height * 0.058,
          child: Align(
            alignment: Alignment.topCenter,
            child: TextField(
              textAlign: TextAlign.right, // Align text to the right
              textDirection: TextDirection.ltr,
              decoration: const InputDecoration(border: InputBorder.none),
              controller: widget.textController,
              keyboardType: TextInputType.number,
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: widget.size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}

openDiscountBottomSheet(BuildContext context, Size size, WidgetRef ref) {
  showModalBottomSheet(
    backgroundColor: LinqPeColors.kWhiteColor,
    context: context,
    builder: (BuildContext context) {
      return Container(
        width: size.width,
        decoration: const BoxDecoration(
            color: LinqPeColors.kWhiteColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50))),
        height: size.height * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () {
                addDiscountType('No Discount', ref);
                Navigator.pop(context);
              },
              child: Text('No Discount',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            InkWell(
              onTap: () {
                addDiscountType('Percentage', ref);
                Navigator.pop(context);
              },
              child: Text('Percentage',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            InkWell(
              onTap: () {
                addDiscountType('Flat Amount', ref);
                Navigator.pop(context);
              },
              child: Text('Flat Amount',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
          ],
        ),
      );
    },
  );
}
