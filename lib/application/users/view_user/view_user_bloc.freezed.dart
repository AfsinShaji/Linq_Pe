// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'view_user_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ViewUserEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getUserDetail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getUserDetail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getUserDetail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getUserDetail value) getUserDetail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getUserDetail value)? getUserDetail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getUserDetail value)? getUserDetail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ViewUserEventCopyWith<$Res> {
  factory $ViewUserEventCopyWith(
          ViewUserEvent value, $Res Function(ViewUserEvent) then) =
      _$ViewUserEventCopyWithImpl<$Res, ViewUserEvent>;
}

/// @nodoc
class _$ViewUserEventCopyWithImpl<$Res, $Val extends ViewUserEvent>
    implements $ViewUserEventCopyWith<$Res> {
  _$ViewUserEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$getUserDetailImplCopyWith<$Res> {
  factory _$$getUserDetailImplCopyWith(
          _$getUserDetailImpl value, $Res Function(_$getUserDetailImpl) then) =
      __$$getUserDetailImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$getUserDetailImplCopyWithImpl<$Res>
    extends _$ViewUserEventCopyWithImpl<$Res, _$getUserDetailImpl>
    implements _$$getUserDetailImplCopyWith<$Res> {
  __$$getUserDetailImplCopyWithImpl(
      _$getUserDetailImpl _value, $Res Function(_$getUserDetailImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$getUserDetailImpl implements getUserDetail {
  const _$getUserDetailImpl();

  @override
  String toString() {
    return 'ViewUserEvent.getUserDetail()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$getUserDetailImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getUserDetail,
  }) {
    return getUserDetail();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getUserDetail,
  }) {
    return getUserDetail?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getUserDetail,
    required TResult orElse(),
  }) {
    if (getUserDetail != null) {
      return getUserDetail();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getUserDetail value) getUserDetail,
  }) {
    return getUserDetail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getUserDetail value)? getUserDetail,
  }) {
    return getUserDetail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getUserDetail value)? getUserDetail,
    required TResult orElse(),
  }) {
    if (getUserDetail != null) {
      return getUserDetail(this);
    }
    return orElse();
  }
}

abstract class getUserDetail implements ViewUserEvent {
  const factory getUserDetail() = _$getUserDetailImpl;
}

/// @nodoc
mixin _$ViewUserState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() viewuserinitial,
    required TResult Function(
            UserProfileDTO? user, bool isLoading, bool isError)
        viewUsers,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? viewuserinitial,
    TResult? Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUsers,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? viewuserinitial,
    TResult Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUsers,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(viewuserinitial value) viewuserinitial,
    required TResult Function(viewUsers value) viewUsers,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(viewuserinitial value)? viewuserinitial,
    TResult? Function(viewUsers value)? viewUsers,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(viewuserinitial value)? viewuserinitial,
    TResult Function(viewUsers value)? viewUsers,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ViewUserStateCopyWith<$Res> {
  factory $ViewUserStateCopyWith(
          ViewUserState value, $Res Function(ViewUserState) then) =
      _$ViewUserStateCopyWithImpl<$Res, ViewUserState>;
}

/// @nodoc
class _$ViewUserStateCopyWithImpl<$Res, $Val extends ViewUserState>
    implements $ViewUserStateCopyWith<$Res> {
  _$ViewUserStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$viewuserinitialImplCopyWith<$Res> {
  factory _$$viewuserinitialImplCopyWith(_$viewuserinitialImpl value,
          $Res Function(_$viewuserinitialImpl) then) =
      __$$viewuserinitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$viewuserinitialImplCopyWithImpl<$Res>
    extends _$ViewUserStateCopyWithImpl<$Res, _$viewuserinitialImpl>
    implements _$$viewuserinitialImplCopyWith<$Res> {
  __$$viewuserinitialImplCopyWithImpl(
      _$viewuserinitialImpl _value, $Res Function(_$viewuserinitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$viewuserinitialImpl implements viewuserinitial {
  const _$viewuserinitialImpl();

  @override
  String toString() {
    return 'ViewUserState.viewuserinitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$viewuserinitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() viewuserinitial,
    required TResult Function(
            UserProfileDTO? user, bool isLoading, bool isError)
        viewUsers,
  }) {
    return viewuserinitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? viewuserinitial,
    TResult? Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUsers,
  }) {
    return viewuserinitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? viewuserinitial,
    TResult Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUsers,
    required TResult orElse(),
  }) {
    if (viewuserinitial != null) {
      return viewuserinitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(viewuserinitial value) viewuserinitial,
    required TResult Function(viewUsers value) viewUsers,
  }) {
    return viewuserinitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(viewuserinitial value)? viewuserinitial,
    TResult? Function(viewUsers value)? viewUsers,
  }) {
    return viewuserinitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(viewuserinitial value)? viewuserinitial,
    TResult Function(viewUsers value)? viewUsers,
    required TResult orElse(),
  }) {
    if (viewuserinitial != null) {
      return viewuserinitial(this);
    }
    return orElse();
  }
}

abstract class viewuserinitial implements ViewUserState {
  const factory viewuserinitial() = _$viewuserinitialImpl;
}

/// @nodoc
abstract class _$$viewUsersImplCopyWith<$Res> {
  factory _$$viewUsersImplCopyWith(
          _$viewUsersImpl value, $Res Function(_$viewUsersImpl) then) =
      __$$viewUsersImplCopyWithImpl<$Res>;
  @useResult
  $Res call({UserProfileDTO? user, bool isLoading, bool isError});
}

/// @nodoc
class __$$viewUsersImplCopyWithImpl<$Res>
    extends _$ViewUserStateCopyWithImpl<$Res, _$viewUsersImpl>
    implements _$$viewUsersImplCopyWith<$Res> {
  __$$viewUsersImplCopyWithImpl(
      _$viewUsersImpl _value, $Res Function(_$viewUsersImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user = freezed,
    Object? isLoading = null,
    Object? isError = null,
  }) {
    return _then(_$viewUsersImpl(
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as UserProfileDTO?,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$viewUsersImpl implements viewUsers {
  const _$viewUsersImpl(
      {required this.user, required this.isLoading, required this.isError});

  @override
  final UserProfileDTO? user;
  @override
  final bool isLoading;
  @override
  final bool isError;

  @override
  String toString() {
    return 'ViewUserState.viewUsers(user: $user, isLoading: $isLoading, isError: $isError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$viewUsersImpl &&
            (identical(other.user, user) || other.user == user) &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isError, isError) || other.isError == isError));
  }

  @override
  int get hashCode => Object.hash(runtimeType, user, isLoading, isError);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$viewUsersImplCopyWith<_$viewUsersImpl> get copyWith =>
      __$$viewUsersImplCopyWithImpl<_$viewUsersImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() viewuserinitial,
    required TResult Function(
            UserProfileDTO? user, bool isLoading, bool isError)
        viewUsers,
  }) {
    return viewUsers(user, isLoading, isError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? viewuserinitial,
    TResult? Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUsers,
  }) {
    return viewUsers?.call(user, isLoading, isError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? viewuserinitial,
    TResult Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUsers,
    required TResult orElse(),
  }) {
    if (viewUsers != null) {
      return viewUsers(user, isLoading, isError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(viewuserinitial value) viewuserinitial,
    required TResult Function(viewUsers value) viewUsers,
  }) {
    return viewUsers(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(viewuserinitial value)? viewuserinitial,
    TResult? Function(viewUsers value)? viewUsers,
  }) {
    return viewUsers?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(viewuserinitial value)? viewuserinitial,
    TResult Function(viewUsers value)? viewUsers,
    required TResult orElse(),
  }) {
    if (viewUsers != null) {
      return viewUsers(this);
    }
    return orElse();
  }
}

abstract class viewUsers implements ViewUserState {
  const factory viewUsers(
      {required final UserProfileDTO? user,
      required final bool isLoading,
      required final bool isError}) = _$viewUsersImpl;

  UserProfileDTO? get user;
  bool get isLoading;
  bool get isError;
  @JsonKey(ignore: true)
  _$$viewUsersImplCopyWith<_$viewUsersImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
