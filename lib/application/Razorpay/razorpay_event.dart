part of 'razorpay_bloc.dart';

@freezed
class RazorpayEvent with _$RazorpayEvent {
  const factory RazorpayEvent.getRazorpayKey() = getRazorpayKey;
  const factory RazorpayEvent.razorpayPayments({required Razorpay razorpay}) =
      razorpayPayments;
}
