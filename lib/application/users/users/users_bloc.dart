import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linq_pe/application/view_dto/user/user.dart';
import 'package:linq_pe/infrastructure/users/users_implementation.dart';

part 'users_event.dart';
part 'users_state.dart';
part 'users_bloc.freezed.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  UsersBloc() : super(const usersInitial()) {
    // on<isUserAccountPermit>((event, emit) async {
    //   emit(const isUserPermitted(
    //       isPermitted: false, isLoading: true, isNotSatrted: false));
    //   final isPermitted =
    //       await UsersImplementation.instance.isAccountPermitted();
    //   log('permit:$isPermitted');
    //   emit(isUserPermitted(
    //     isPermitted: isPermitted ?? false,
    //     isLoading: false,
    //     isNotSatrted: isPermitted == null ? true : false,
    //   ));
    // });
    on<addUserTrialDate>((event, emit) async {
      emit(const addTrialDateState(
          isError: false, isLoading: true, isSuccess: false, user: null));
      final result = await UsersImplementation.instance.addTrialDate();
      result.fold(
          (l) => emit(const addTrialDateState(
              isError: true,
              isLoading: false,
              isSuccess: false,
              user: null)), (r) {
        if (r != null) {
          emit(addTrialDateState(
              isError: false,
              isLoading: false,
              isSuccess: true,
              user: converstUserModelToDTO(r)));
        } else {
          emit(const addTrialDateState(
              isError: true, isLoading: false, isSuccess: false, user: null));
        }
      });
    });
    on<updateUsersBusiness>((event, emit) async {
      log('here');
      emit(const updateBusinessResult(
          isLoading: true, isSuccess: false, isError: false));

      final result = await UsersImplementation.instance.updateUserBusiness(
          userBusinessType: event.userBusinessType,
          userPusrpose: event.userPusrpose,
          userBusinessName: event.userBusinessName,
          userBusinessLogo: event.userBusinessLogo);
      result.fold((l) {
        emit(const updateBusinessResult(
            isLoading: false, isSuccess: false, isError: true));
      }, (r) {
        if (r == 'success') {
          emit(const updateBusinessResult(
              isLoading: false, isSuccess: true, isError: false));
        } else {
          emit(const updateBusinessResult(
              isLoading: false, isSuccess: false, isError: true));
        }
      });
    });
    on<getUserDetail>((event, emit) async {
      emit(const viewUser(isLoading: true, user: null, isError: false));
      final result = await UsersImplementation.instance.getUserDetails();
      result.fold(
          (l) =>
              emit(const viewUser(isLoading: false, user: null, isError: true)),
          (r) {
        final user = converstUserModelToDTO(r);

        emit(viewUser(isLoading: false, user: user, isError: false));
      });
    });
    on<makeUsersPremium>((event, emit) async {
     
      await UsersImplementation.instance.makeUserPremium();
  
    });
  }
}
