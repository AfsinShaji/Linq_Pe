import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linq_pe/infrastructure/users/users_implementation.dart';

part 'user_permission_event.dart';
part 'user_permission_state.dart';
part 'user_permission_bloc.freezed.dart';

class UserPermissionBloc
    extends Bloc<UserPermissionEvent, UserPermissionState> {
  UserPermissionBloc() : super(const userPermissionInitial()) {
    on<isUserAccountPermitted>((event, emit) async {
      emit(const isUserPermitted(
          isPermitted: false, isLoading: true, isNotSatrted: false));
      final isPermitted =
          await UsersImplementation.instance.isAccountPermitted();
      log('permit:$isPermitted');
      emit(isUserPermitted(
        isPermitted: isPermitted ?? false,
        isLoading: false,
        isNotSatrted: isPermitted == null ? true : false,
      ));
    });
  }
}
