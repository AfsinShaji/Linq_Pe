

import 'package:hive/hive.dart';

part 'payments.g.dart';

@HiveType(typeId: 20)
class PaymentsModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final String paymentMethod;
  @HiveField(2)
  final double amount;
  @HiveField(3)
  final DateTime date;
  @HiveField(4)
  final String notes;
  @HiveField(5)
  final String paymentId;


  PaymentsModel({required this.paymentId,
    required this.invoiceId,
    required this.amount,required this.date,required this.notes,required this.paymentMethod
  });
}

class PaymentsBox {
  static Box<PaymentsModel>? _getbox;
  static Box<PaymentsModel> getInstance() {
    return _getbox ??= Hive.box('PaymentsBox');
  }
}
