import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/invoice_number/screen_invoice_number.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/items/screen_items.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/payment_info/screen_payment_info.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/payments.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/presentation/widgets/alert_box.dart';
import 'package:linq_pe/utilities/colors.dart';

class AddPaymentScreen extends StatelessWidget {
  AddPaymentScreen({super.key, required this.isToEdit});
  final TextEditingController amountTextController = TextEditingController();
  final TextEditingController notesTextController = TextEditingController();
  final bool isToEdit;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        actions: [isToEdit ? DeleteWidget(size: size) : const SizedBox()],
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.12,
        title: Text('Add Payment',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButtonWidget(
          isToEdit: isToEdit,
          amountTextController: amountTextController,
          notesTextController: notesTextController),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: size.height * 0.02,
            ),
            EditContainerWidget(
                child: Column(
              children: [
                DtailFieldRowWidget(
                    size: size, amountTextController: amountTextController),
                PaymentMethodRow(
                  size: size,
                ),
                SizedBox(
                  height: size.height * 0.01,
                ),
                DatePickerRowWidget(size: size)
              ],
            )),
            EditContainerWidget(
                padding: 0,
                child: DetailsTextFieldConsumerWidget(
                    size: size, notesTextController: notesTextController)),
          ],
        ),
      )),
    );
  }
}

class DeleteWidget extends ConsumerWidget {
  const DeleteWidget({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IconButton(
        onPressed: () {
          alertBox(context, 'Delete Payment', () {
            BlocProvider.of<InvoicesBloc>(context).add(
                InvoicesEvent.deletePayment(
                    paymentId: ref.watch(addPaymentProvider)!.paymentId,
                    invoiceId: ref.watch(addPaymentProvider)!.invoiceId));
            Navigator.pop(context);
            Navigator.pop(context);
          }, size);
        },
        icon: const Icon(
          Icons.delete,
          color: LinqPeColors.kWhiteColor,
        ));
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget({
    super.key,
    required this.amountTextController,
    required this.notesTextController,
    required this.isToEdit,
  });

  final TextEditingController amountTextController;
  final TextEditingController notesTextController;
  final bool isToEdit;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(onTap: () {
      if (amountTextController.text.isNotEmpty &&
          ref.watch(invoiceIdProvider).isNotEmpty &&
          ref.watch(paymentDateProvider) != null &&
          ref.watch(paymentMethodProvider) != 'Select Payment Method') {
        if (isToEdit) {
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.editPayment(
              paymentId: ref.watch(addPaymentProvider) == null
                  ? ''
                  : ref.watch(addPaymentProvider)!.paymentId,
              invoiceId: ref.watch(invoiceIdProvider),
              amount: double.parse(amountTextController.text),
              date: ref.watch(paymentDateProvider)!,
              notes: notesTextController.text,
              paymentMethod: ref.watch(paymentMethodProvider)));
        } else {
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.addPayment(
              invoiceId: ref.watch(invoiceIdProvider),
              amount: double.parse(amountTextController.text),
              date: ref.watch(paymentDateProvider)!,
              notes: notesTextController.text,
              paymentMethod: ref.watch(paymentMethodProvider)));
        }
        Navigator.pop(context);
      }
    });
  }
}

class DatePickerRowWidget extends ConsumerWidget {
  const DatePickerRowWidget({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DatePickerRow(
      initialDate: ref.watch(paymentDateProvider),
      fieldName: 'Date',
      size: size,
      dateScreen: 'Add Payment',
    );
  }
}

class DetailsTextFieldConsumerWidget extends ConsumerWidget {
  const DetailsTextFieldConsumerWidget({
    super.key,
    required this.size,
    required this.notesTextController,
  });

  final Size size;
  final TextEditingController notesTextController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DetailsTextFieldWidget(
        initailValue: ref.watch(addPaymentProvider) == null
            ? ''
            : ref.watch(addPaymentProvider)!.notes,
        size: size,
        maxLines: null,
        hintText: 'Notes',
        textController: notesTextController);
  }
}

class DtailFieldRowWidget extends ConsumerWidget {
  const DtailFieldRowWidget({
    super.key,
    required this.size,
    required this.amountTextController,
  });

  final Size size;
  final TextEditingController amountTextController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DetailFieldRow(
      initialValue: ref.watch(addPaymentProvider) == null
          ? ''
          : ref.watch(addPaymentProvider)!.amount.toString(),
      size: size,
      fieldName: 'Amount',
      hintText: '₹0.00',
      isNumberField: true,
      textController: amountTextController,
    );
  }
}

class PaymentMethodRow extends ConsumerWidget {
  const PaymentMethodRow({
    super.key,
    required this.size,
  });

  final Size size;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return InkWell(
      onTap: () {
        openPaymentMethodBottomSheet(context, size, ref);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('Payment Method:',
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
          Text(ref.watch(paymentMethodProvider),
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
        ],
      ),
    );
  }
}

openPaymentMethodBottomSheet(BuildContext context, Size size, WidgetRef ref) {
  showModalBottomSheet(
    backgroundColor: LinqPeColors.kWhiteColor,
    context: context,
    builder: (BuildContext context) {
      return Container(
        width: size.width,
        decoration: const BoxDecoration(
            color: LinqPeColors.kWhiteColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50))),
        height: size.height * 0.4,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            PaymentMethodText(
              method: 'Select Payment Method',
              size: size,
              onTap: () {
                addpaymentMethod('Select Payment Method', ref);
                Navigator.pop(context);
              },
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            PaymentMethodText(
              method: 'Bank Transfer',
              size: size,
              onTap: () {
                addpaymentMethod('Bank Transfer', ref);
                Navigator.pop(context);
              },
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            PaymentMethodText(
              method: 'Cash',
              size: size,
              onTap: () {
                addpaymentMethod('Cash', ref);
                Navigator.pop(context);
              },
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            PaymentMethodText(
              method: 'Card',
              size: size,
              onTap: () {
                addpaymentMethod('Card', ref);
                Navigator.pop(context);
              },
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            PaymentMethodText(
              method: 'Credit Card',
              size: size,
              onTap: () {
                addpaymentMethod('Credit Card', ref);
                Navigator.pop(context);
              },
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            PaymentMethodText(
              method: 'Paypal',
              size: size,
              onTap: () {
                addpaymentMethod('Paypal', ref);
                Navigator.pop(context);
              },
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            PaymentMethodText(
              method: 'Peer-to-peer payment',
              size: size,
              onTap: () {
                addpaymentMethod('Peer-to-peer payment', ref);
                Navigator.pop(context);
              },
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            PaymentMethodText(
              method: 'Point-of-sale system',
              size: size,
              onTap: () {
                addpaymentMethod('Point-of-sale system', ref);
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
    },
  );
}

class PaymentMethodText extends StatelessWidget {
  const PaymentMethodText({
    super.key,
    required this.size,
    required this.method,
    required this.onTap,
  });
  final Size size;
  final String method;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Text(method,
          style: GoogleFonts.roboto(
            textStyle: TextStyle(
              letterSpacing: .5,
              fontSize: size.width * 0.04,
              color: LinqPeColors.kBlackColor,
              fontWeight: FontWeight.w500,
            ),
          )),
    );
  }
}
