part of 'invoices_bloc.dart';

@freezed
class InvoicesState with _$InvoicesState {
  const factory InvoicesState.invoicesInitial() = invoicesInitial;
  const factory InvoicesState.displayInvoiceId({required String invoiceId}) =
      displayInvoiceId;
  const factory InvoicesState.displayInvoice({required InvoiceEditDTO invoice}) =
      displayInvoice;
     
}
