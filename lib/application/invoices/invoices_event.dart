part of 'invoices_bloc.dart';

@freezed
class InvoicesEvent with _$InvoicesEvent {
  const factory InvoicesEvent.createInvoicesNumber({
    required String invoiceNumber,
  }) = createInvoicesNumber;
  const factory InvoicesEvent.addInvoicesNumber(
      {required String invoiceNumber,
      required String invoiceId,
      required DateTime date,
      required String terms,
      required DateTime? dueDate,
      required String? poNumber}) = addInvoicesNumber;
  const factory InvoicesEvent.addBusinessDetail({
    required String businessName,
    required String invoiceId,
    required File? businessLogo,
    required String? businessOwnerName,
    required String? businessNumber,
    required String? addressLine1,
    required String? addressLine2,
    required String? addressLine3,
    required String? email,
    required String? phone,
    required String? mobile,
    required String? website,
  }) = addBusinessDetail;
  const factory InvoicesEvent.addPaymentsInfo(
      {required String? additionalPaymentInstructions,
      required String? businessName,
      required String? paymentInstructions,
      required String? paypalEmail,
      required String invoiceId}) = addPaymentsInfo;
  const factory InvoicesEvent.addsPhoto(
      {required String invoiceId,
      required File photo,
      required String? description,
      required String? addionalDetails}) = addsPhoto;
  const factory InvoicesEvent.addPayment(
      {required String invoiceId,
      required double amount,
      required DateTime date,
      required String notes,
      required String paymentMethod}) = addPayment;
  const factory InvoicesEvent.addsClient(
      {required String invoiceId,
      required String? phoneNumber,
      required String? mobileNumber,
      required String? faxNumber,
      required String clientName,
      required String? clientEmail,
      required String? address3,
      required String? address2,
      required String? address1}) = addsClient;
  const factory InvoicesEvent.addItem(
      {required String invoiceId,
      required double unitCost,
      required String? unit,
      required double? taxRate,
      required int quantity,
      required bool isTaxable,
      required double? discountAmount,
      required String description,
      required String? discount,
      required String? addiotionalDetails,
      required bool isAddToMyItems,
      required double total}) = addItem;
  const factory InvoicesEvent.addTaxDetail(
      {required String invoiceId,
      required double? rate,
      required String label,
      required String taxType,
      required bool isInclusive}) = addTaxDetail;
  const factory InvoicesEvent.addNote(
      {required String notes,
      required String invoiceId,
      required bool isDefault}) = addNote;
  const factory InvoicesEvent.addsSignature(
      {required String invoiceId, required File signature}) = addsSignature;
  const factory InvoicesEvent.addDiscountDetail(
      {required String invoiceId,
      required String discountType,
      required double percentage}) = addDiscountDetail;
  const factory InvoicesEvent.getInvoicesDetails({required String invoiceId}) =
      getInvoicesDetails;
  const factory InvoicesEvent.editItem(
      {required String itemId,
      required String invoiceId,
      required double unitCost,
      required String? unit,
      required double? taxRate,
      required int quantity,
      required bool isTaxable,
      required double? discountAmount,
      required String description,
      required String? discount,
      required String? addiotionalDetails,
      required bool isAddToMyItems,
      required double total}) = editItem;
  const factory InvoicesEvent.editPayment(
      {required String invoiceId,
      required double amount,
      required DateTime date,
      required String notes,
      required String paymentMethod,
      required String paymentId}) = editPayment;
  const factory InvoicesEvent.editsPhoto(
      {required String invoiceId,
      required File photo,
      required String? description,
      required String? addionalDetails,
      required String photoId}) = editsPhoto;
  const factory InvoicesEvent.addReviews({
    required String invoiceId,
    required String reviewLink,
    required bool isToReview,
  }) = addReviews;

  const factory InvoicesEvent.deleteItem({
    required String itemId,
    required String invoiceId,
  }) = deleteItem;
  const factory InvoicesEvent.deletePayment({
    required String paymentId,
    required String invoiceId,
  }) = deletePayment;
  const factory InvoicesEvent.deletePhotos({
    required String photoId,
    required String invoiceId,
  }) = deletePhotos;
  const factory InvoicesEvent.markAsPaidOrUnPaid(
      {required String invoiceId,
      required String? paymentMethod,
      required bool isPaid}) = markAsPaidOrUnPaid;
  const factory InvoicesEvent.addBalanceDues({
    required String invoiceId,
    required double balanceDue,
  }) = addBalanceDues;
  const factory InvoicesEvent.createEstimatesNumber(
      {required String estimateNumber}) = createEstimatesNumber;
  const factory InvoicesEvent.convertingEstimateToInvoice({
    required String invoiceId,
  }) = convertingEstimateToInvoice;
}
