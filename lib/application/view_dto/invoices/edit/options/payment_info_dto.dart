import 'package:linq_pe/domain/models/invoices/edit/options/payment_info/payment_info.dart';

class PaymentInfoDTO {
  final String invoiceId;

  final String? paypalEmail;

  final String? businessName;

  final String? paymentInstructions;

  final String? additionalPaymentInstructions;

  PaymentInfoDTO({
    required this.invoiceId,
    required this.additionalPaymentInstructions,
    required this.businessName,
    required this.paymentInstructions,
    required this.paypalEmail,
  });
}

PaymentInfoDTO? convertPaymentInfoModelToDTO(PaymentInfoModel? paymentInfo) {
   if (paymentInfo == null) {
    return null;
  }
  return PaymentInfoDTO(
      invoiceId: paymentInfo.invoiceId,
      additionalPaymentInstructions: paymentInfo.additionalPaymentInstructions,
      businessName: paymentInfo.businessName,
      paymentInstructions: paymentInfo.paymentInstructions,
      paypalEmail: paymentInfo.paypalEmail);
}
