import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

class PaymentInfoScreen extends StatelessWidget {
  PaymentInfoScreen({super.key});
  final TextEditingController paypalTextController = TextEditingController();
  final TextEditingController nameTextController = TextEditingController();
  final TextEditingController paymentInstructionTextController =
      TextEditingController();
  final TextEditingController otherTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.12,
        title: Text('Payment Info',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButtonWidget(
          paypalTextController: paypalTextController,
          nameTextController: nameTextController,
          paymentInstructionTextController: paymentInstructionTextController,
          otherTextController: otherTextController),
      body: SafeArea(child: SingleChildScrollView(
        child: BlocBuilder<InvoicesBloc, InvoicesState>(
          builder: (context, invoiceState) {
            InvoiceEditDTO? invoice;

            if (invoiceState is displayInvoice) {
              invoice = invoiceState.invoice;
            }
            if (invoice == null) {
              return const SizedBox();
            }

            return Column(
              children: [
                SizedBox(
                  height: size.height * 0.02,
                ),
                Text('Do not enter sensitive information',
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        letterSpacing: .5,
                        fontSize: size.width * 0.04,
                        color: LinqPeColors.kBlackColor.withOpacity(0.5),
                        fontWeight: FontWeight.w600,
                      ),
                    )),
                SizedBox(
                  height: size.height * 0.01,
                ),
                PaymentInfoContainer(
                  initailValue: invoice.paymentInfo == null
                      ? ''
                      : invoice.paymentInfo!.paypalEmail == null
                          ? ''
                          : invoice.paymentInfo!.paypalEmail!,
                  size: size,
                  headerText: 'Paypal email',
                  hintText: 'Enter your paypal email address',
                  textController: paypalTextController,
                ),
                PaymentInfoContainer(
                  initailValue: invoice.paymentInfo == null
                      ? ''
                      : invoice.paymentInfo!.businessName == null
                          ? ''
                          : invoice.paymentInfo!.businessName!,
                  size: size,
                  headerText: 'Make checks payable to',
                  hintText: "Your or your business's name",
                  textController: nameTextController,
                ),
                PaymentInfoContainer(
                  initailValue: invoice.paymentInfo == null
                      ? ''
                      : invoice.paymentInfo!.paymentInstructions == null
                          ? ''
                          : invoice.paymentInfo!.paymentInstructions!,
                  maxLines: null,
                  size: size,
                  headerText: 'Payment Instructions',
                  hintText: 'Specify instructions for payments or deposits',
                  textController: paymentInstructionTextController,
                ),
                PaymentInfoContainer(
                  initailValue: invoice.paymentInfo == null
                      ? ''
                      : invoice.paymentInfo!.additionalPaymentInstructions ==
                              null
                          ? ''
                          : invoice.paymentInfo!.additionalPaymentInstructions!,
                  maxLines: null,
                  size: size,
                  headerText: 'Other',
                  hintText: 'Additional payment instructions',
                  textController: otherTextController,
                ),
                SizedBox(
                  height: size.height * 0.1,
                ),
              ],
            );
          },
        ),
      )),
    );
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget({
    super.key,
    required this.paypalTextController,
    required this.nameTextController,
    required this.paymentInstructionTextController,
    required this.otherTextController,
  });

  final TextEditingController paypalTextController;
  final TextEditingController nameTextController;
  final TextEditingController paymentInstructionTextController;
  final TextEditingController otherTextController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(onTap: () {
      if (paypalTextController.text.isEmpty &&
          nameTextController.text.isEmpty &&
          paymentInstructionTextController.text.isEmpty &&
          otherTextController.text.isEmpty &&
          ref.watch(invoiceIdProvider).isEmpty) {
      } else {
        BlocProvider.of<InvoicesBloc>(context).add(
            InvoicesEvent.addPaymentsInfo(
                additionalPaymentInstructions: otherTextController.text,
                businessName: nameTextController.text,
                paymentInstructions: paymentInstructionTextController.text,
                paypalEmail: paypalTextController.text,
                invoiceId: ref.watch(invoiceIdProvider)));
        Navigator.pop(context);
      }
    });
  }
}

class PaymentInfoContainer extends StatelessWidget {
  const PaymentInfoContainer({
    super.key,
    required this.size,
    required this.headerText,
    this.maxLines = 1,
    required this.hintText,
    required this.textController,
    required this.initailValue,
  });

  final Size size;
  final String headerText;
  final int? maxLines;
  final String hintText;
  final TextEditingController textController;
  final String initailValue;

  @override
  Widget build(BuildContext context) {
    return EditContainerWidget(
        padding: 0,
        child: Column(
          children: [
            Container(
              width: size.width,
              decoration: BoxDecoration(
                  color: LinqPeColors.kBlackColor.withOpacity(0.5),
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5))),
              padding: EdgeInsets.symmetric(
                  vertical: size.height * 0.01, horizontal: size.width * 0.05),
              child: Text(headerText,
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kWhiteColor,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
            ),
            SizedBox(
              height: size.height * 0.01,
            ),
            DetailsTextFieldWidget(
                initailValue: initailValue,
                size: size,
                maxLines: maxLines,
                hintText: hintText,
                textController: textController),
          ],
        ));
  }
}

class DetailsTextFieldWidget extends StatefulWidget {
  const DetailsTextFieldWidget({
    super.key,
    required this.size,
    required this.maxLines,
    required this.hintText,
    required this.textController,
    required this.initailValue,
  });

  final Size size;
  final int? maxLines;
  final String hintText;
  final TextEditingController textController;
  final String initailValue;

  @override
  State<DetailsTextFieldWidget> createState() => _DetailsTextFieldWidgetState();
}

class _DetailsTextFieldWidgetState extends State<DetailsTextFieldWidget> {
  @override
  void initState() {
    if (widget.initailValue.isNotEmpty) {
      widget.textController.text = widget.initailValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: widget.size.height * 0.005,
          horizontal: widget.size.width * 0.05),
      width: widget.size.width,
      height: widget.maxLines == null
          ? widget.size.height * 0.2
          : widget.size.height * 0.05,
      child: Align(
        alignment: Alignment.topCenter,
        child: TextField(
          //    textAlign: TextAlign.right, // Align text to the right
          maxLines: widget.maxLines,

          //   textDirection: TextDirection.ltr,

          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: widget.hintText,
              hintStyle: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: widget.size.width * 0.039,
                  color: LinqPeColors.kBlackColor.withOpacity(0.3),
                  fontWeight: FontWeight.w600,
                ),
              )),

          controller: widget.textController,

          style: GoogleFonts.roboto(
            textStyle: TextStyle(
              letterSpacing: .5,
              fontSize: widget.size.width * 0.04,
              color: LinqPeColors.kBlackColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
