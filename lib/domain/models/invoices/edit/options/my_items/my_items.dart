import 'package:hive/hive.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/item/item.dart';

part 'my_items.g.dart';

@HiveType(typeId: 24)
class MyItemsModel {
  @HiveField(0)
  final List<ItemModel> itemsList;


  MyItemsModel({

    required this.itemsList,

  });
}

class MyItemsBox {
  static Box<MyItemsModel>? _getbox;
  static Box<MyItemsModel> getInstance() {
    return _getbox ??= Hive.box('MyItemsBox');
  }
}
