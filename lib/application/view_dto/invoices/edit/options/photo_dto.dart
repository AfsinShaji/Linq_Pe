import 'dart:io';

import 'package:linq_pe/application/view_dto/transaction/secondary_transaction_dto.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/photo/photo.dart';

class PhotoDTO {
  final String invoiceId;

  final File photo;

  final String? description;

  final String? addionalDetails;
  final String photoId;

  PhotoDTO(
      {required this.invoiceId,
      required this.photo,
      required this.addionalDetails,
      required this.description, required this.photoId});
}

Future<List<PhotoDTO>?> convertPhotosModelToDTO(List<PhotoModel>? photos)async {
  if (photos == null) {
    return null;
  }
  List<PhotoDTO> photoList = [];
  for (var photo in photos) {
    photoList.add(PhotoDTO(
      photoId: photo.photoId,
        invoiceId: photo.invoiceId,
        photo:await uint8ListToFile(photo.photo)??File(''),
        addionalDetails: photo.addionalDetails,
        description: photo.description));
  }
  return photoList;
}
