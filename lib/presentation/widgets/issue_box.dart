import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/utilities/colors.dart';

issueBox(BuildContext context, Size size) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: LinqPeColors.kredColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
      content: Text(
        'Something went wrong!',
        style: GoogleFonts.poppins(
          textStyle: const TextStyle(
            letterSpacing: .5,
            fontSize: 18,
            color: LinqPeColors.kWhiteColor,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.pop(context);
              Navigator.pop(context);
            },
            child: Text(
              'okay',
              style: GoogleFonts.poppins(
                textStyle: const TextStyle(
                  letterSpacing: .5,
                  fontSize: 14,
                  color: LinqPeColors.kWhiteColor,
                  fontWeight: FontWeight.w500,
                ),
              ),
            )),
      ],
    ),
  );
}
