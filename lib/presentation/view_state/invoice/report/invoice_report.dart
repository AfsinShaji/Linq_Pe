import 'package:flutter_riverpod/flutter_riverpod.dart';

enum ReportType { paid, clients, items }

final reportTypeProvider = StateProvider(
  (ref) {
    return ReportType.clients;
  },
);
addreportType(ReportType id, WidgetRef ref) {
  ref.read(reportTypeProvider.notifier).state = id;
}
final yearProvider = StateProvider(
  (ref) {
    return DateTime.now().year;
  },
);
addyear(int id, WidgetRef ref) {
  ref.read(yearProvider.notifier).state = id;
}
