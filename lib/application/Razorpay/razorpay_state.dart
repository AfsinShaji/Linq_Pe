part of 'razorpay_bloc.dart';

@freezed
class RazorpayState with _$RazorpayState {
  const factory RazorpayState.razorpayInitial() = razorpayInitial;
  const factory RazorpayState.displayRazorpayKey(
      {required String razorpayKey}) = displayRazorpayKey;
  const factory RazorpayState.paymentResult(
      {required bool isLoading,
      required bool isFailed,
      required bool isError,
      required bool isSuccess}) = paymentResult;
}
