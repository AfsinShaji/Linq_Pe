// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'users_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UsersEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() addUserTrialDate,
    required TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)
        updateUsersBusiness,
    required TResult Function() getUserDetail,
    required TResult Function() makeUsersPremium,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? addUserTrialDate,
    TResult? Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult? Function()? getUserDetail,
    TResult? Function()? makeUsersPremium,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? addUserTrialDate,
    TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult Function()? getUserDetail,
    TResult Function()? makeUsersPremium,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(addUserTrialDate value) addUserTrialDate,
    required TResult Function(updateUsersBusiness value) updateUsersBusiness,
    required TResult Function(getUserDetail value) getUserDetail,
    required TResult Function(makeUsersPremium value) makeUsersPremium,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(addUserTrialDate value)? addUserTrialDate,
    TResult? Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult? Function(getUserDetail value)? getUserDetail,
    TResult? Function(makeUsersPremium value)? makeUsersPremium,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(addUserTrialDate value)? addUserTrialDate,
    TResult Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult Function(getUserDetail value)? getUserDetail,
    TResult Function(makeUsersPremium value)? makeUsersPremium,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UsersEventCopyWith<$Res> {
  factory $UsersEventCopyWith(
          UsersEvent value, $Res Function(UsersEvent) then) =
      _$UsersEventCopyWithImpl<$Res, UsersEvent>;
}

/// @nodoc
class _$UsersEventCopyWithImpl<$Res, $Val extends UsersEvent>
    implements $UsersEventCopyWith<$Res> {
  _$UsersEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$addUserTrialDateImplCopyWith<$Res> {
  factory _$$addUserTrialDateImplCopyWith(_$addUserTrialDateImpl value,
          $Res Function(_$addUserTrialDateImpl) then) =
      __$$addUserTrialDateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$addUserTrialDateImplCopyWithImpl<$Res>
    extends _$UsersEventCopyWithImpl<$Res, _$addUserTrialDateImpl>
    implements _$$addUserTrialDateImplCopyWith<$Res> {
  __$$addUserTrialDateImplCopyWithImpl(_$addUserTrialDateImpl _value,
      $Res Function(_$addUserTrialDateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$addUserTrialDateImpl implements addUserTrialDate {
  const _$addUserTrialDateImpl();

  @override
  String toString() {
    return 'UsersEvent.addUserTrialDate()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$addUserTrialDateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() addUserTrialDate,
    required TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)
        updateUsersBusiness,
    required TResult Function() getUserDetail,
    required TResult Function() makeUsersPremium,
  }) {
    return addUserTrialDate();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? addUserTrialDate,
    TResult? Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult? Function()? getUserDetail,
    TResult? Function()? makeUsersPremium,
  }) {
    return addUserTrialDate?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? addUserTrialDate,
    TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult Function()? getUserDetail,
    TResult Function()? makeUsersPremium,
    required TResult orElse(),
  }) {
    if (addUserTrialDate != null) {
      return addUserTrialDate();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(addUserTrialDate value) addUserTrialDate,
    required TResult Function(updateUsersBusiness value) updateUsersBusiness,
    required TResult Function(getUserDetail value) getUserDetail,
    required TResult Function(makeUsersPremium value) makeUsersPremium,
  }) {
    return addUserTrialDate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(addUserTrialDate value)? addUserTrialDate,
    TResult? Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult? Function(getUserDetail value)? getUserDetail,
    TResult? Function(makeUsersPremium value)? makeUsersPremium,
  }) {
    return addUserTrialDate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(addUserTrialDate value)? addUserTrialDate,
    TResult Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult Function(getUserDetail value)? getUserDetail,
    TResult Function(makeUsersPremium value)? makeUsersPremium,
    required TResult orElse(),
  }) {
    if (addUserTrialDate != null) {
      return addUserTrialDate(this);
    }
    return orElse();
  }
}

abstract class addUserTrialDate implements UsersEvent {
  const factory addUserTrialDate() = _$addUserTrialDateImpl;
}

/// @nodoc
abstract class _$$updateUsersBusinessImplCopyWith<$Res> {
  factory _$$updateUsersBusinessImplCopyWith(_$updateUsersBusinessImpl value,
          $Res Function(_$updateUsersBusinessImpl) then) =
      __$$updateUsersBusinessImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String userPusrpose,
      String userBusinessName,
      String userBusinessLogo,
      String userBusinessType});
}

/// @nodoc
class __$$updateUsersBusinessImplCopyWithImpl<$Res>
    extends _$UsersEventCopyWithImpl<$Res, _$updateUsersBusinessImpl>
    implements _$$updateUsersBusinessImplCopyWith<$Res> {
  __$$updateUsersBusinessImplCopyWithImpl(_$updateUsersBusinessImpl _value,
      $Res Function(_$updateUsersBusinessImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userPusrpose = null,
    Object? userBusinessName = null,
    Object? userBusinessLogo = null,
    Object? userBusinessType = null,
  }) {
    return _then(_$updateUsersBusinessImpl(
      userPusrpose: null == userPusrpose
          ? _value.userPusrpose
          : userPusrpose // ignore: cast_nullable_to_non_nullable
              as String,
      userBusinessName: null == userBusinessName
          ? _value.userBusinessName
          : userBusinessName // ignore: cast_nullable_to_non_nullable
              as String,
      userBusinessLogo: null == userBusinessLogo
          ? _value.userBusinessLogo
          : userBusinessLogo // ignore: cast_nullable_to_non_nullable
              as String,
      userBusinessType: null == userBusinessType
          ? _value.userBusinessType
          : userBusinessType // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$updateUsersBusinessImpl implements updateUsersBusiness {
  const _$updateUsersBusinessImpl(
      {required this.userPusrpose,
      required this.userBusinessName,
      required this.userBusinessLogo,
      required this.userBusinessType});

  @override
  final String userPusrpose;
  @override
  final String userBusinessName;
  @override
  final String userBusinessLogo;
  @override
  final String userBusinessType;

  @override
  String toString() {
    return 'UsersEvent.updateUsersBusiness(userPusrpose: $userPusrpose, userBusinessName: $userBusinessName, userBusinessLogo: $userBusinessLogo, userBusinessType: $userBusinessType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$updateUsersBusinessImpl &&
            (identical(other.userPusrpose, userPusrpose) ||
                other.userPusrpose == userPusrpose) &&
            (identical(other.userBusinessName, userBusinessName) ||
                other.userBusinessName == userBusinessName) &&
            (identical(other.userBusinessLogo, userBusinessLogo) ||
                other.userBusinessLogo == userBusinessLogo) &&
            (identical(other.userBusinessType, userBusinessType) ||
                other.userBusinessType == userBusinessType));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userPusrpose, userBusinessName,
      userBusinessLogo, userBusinessType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$updateUsersBusinessImplCopyWith<_$updateUsersBusinessImpl> get copyWith =>
      __$$updateUsersBusinessImplCopyWithImpl<_$updateUsersBusinessImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() addUserTrialDate,
    required TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)
        updateUsersBusiness,
    required TResult Function() getUserDetail,
    required TResult Function() makeUsersPremium,
  }) {
    return updateUsersBusiness(
        userPusrpose, userBusinessName, userBusinessLogo, userBusinessType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? addUserTrialDate,
    TResult? Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult? Function()? getUserDetail,
    TResult? Function()? makeUsersPremium,
  }) {
    return updateUsersBusiness?.call(
        userPusrpose, userBusinessName, userBusinessLogo, userBusinessType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? addUserTrialDate,
    TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult Function()? getUserDetail,
    TResult Function()? makeUsersPremium,
    required TResult orElse(),
  }) {
    if (updateUsersBusiness != null) {
      return updateUsersBusiness(
          userPusrpose, userBusinessName, userBusinessLogo, userBusinessType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(addUserTrialDate value) addUserTrialDate,
    required TResult Function(updateUsersBusiness value) updateUsersBusiness,
    required TResult Function(getUserDetail value) getUserDetail,
    required TResult Function(makeUsersPremium value) makeUsersPremium,
  }) {
    return updateUsersBusiness(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(addUserTrialDate value)? addUserTrialDate,
    TResult? Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult? Function(getUserDetail value)? getUserDetail,
    TResult? Function(makeUsersPremium value)? makeUsersPremium,
  }) {
    return updateUsersBusiness?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(addUserTrialDate value)? addUserTrialDate,
    TResult Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult Function(getUserDetail value)? getUserDetail,
    TResult Function(makeUsersPremium value)? makeUsersPremium,
    required TResult orElse(),
  }) {
    if (updateUsersBusiness != null) {
      return updateUsersBusiness(this);
    }
    return orElse();
  }
}

abstract class updateUsersBusiness implements UsersEvent {
  const factory updateUsersBusiness(
      {required final String userPusrpose,
      required final String userBusinessName,
      required final String userBusinessLogo,
      required final String userBusinessType}) = _$updateUsersBusinessImpl;

  String get userPusrpose;
  String get userBusinessName;
  String get userBusinessLogo;
  String get userBusinessType;
  @JsonKey(ignore: true)
  _$$updateUsersBusinessImplCopyWith<_$updateUsersBusinessImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$getUserDetailImplCopyWith<$Res> {
  factory _$$getUserDetailImplCopyWith(
          _$getUserDetailImpl value, $Res Function(_$getUserDetailImpl) then) =
      __$$getUserDetailImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$getUserDetailImplCopyWithImpl<$Res>
    extends _$UsersEventCopyWithImpl<$Res, _$getUserDetailImpl>
    implements _$$getUserDetailImplCopyWith<$Res> {
  __$$getUserDetailImplCopyWithImpl(
      _$getUserDetailImpl _value, $Res Function(_$getUserDetailImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$getUserDetailImpl implements getUserDetail {
  const _$getUserDetailImpl();

  @override
  String toString() {
    return 'UsersEvent.getUserDetail()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$getUserDetailImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() addUserTrialDate,
    required TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)
        updateUsersBusiness,
    required TResult Function() getUserDetail,
    required TResult Function() makeUsersPremium,
  }) {
    return getUserDetail();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? addUserTrialDate,
    TResult? Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult? Function()? getUserDetail,
    TResult? Function()? makeUsersPremium,
  }) {
    return getUserDetail?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? addUserTrialDate,
    TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult Function()? getUserDetail,
    TResult Function()? makeUsersPremium,
    required TResult orElse(),
  }) {
    if (getUserDetail != null) {
      return getUserDetail();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(addUserTrialDate value) addUserTrialDate,
    required TResult Function(updateUsersBusiness value) updateUsersBusiness,
    required TResult Function(getUserDetail value) getUserDetail,
    required TResult Function(makeUsersPremium value) makeUsersPremium,
  }) {
    return getUserDetail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(addUserTrialDate value)? addUserTrialDate,
    TResult? Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult? Function(getUserDetail value)? getUserDetail,
    TResult? Function(makeUsersPremium value)? makeUsersPremium,
  }) {
    return getUserDetail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(addUserTrialDate value)? addUserTrialDate,
    TResult Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult Function(getUserDetail value)? getUserDetail,
    TResult Function(makeUsersPremium value)? makeUsersPremium,
    required TResult orElse(),
  }) {
    if (getUserDetail != null) {
      return getUserDetail(this);
    }
    return orElse();
  }
}

abstract class getUserDetail implements UsersEvent {
  const factory getUserDetail() = _$getUserDetailImpl;
}

/// @nodoc
abstract class _$$makeUsersPremiumImplCopyWith<$Res> {
  factory _$$makeUsersPremiumImplCopyWith(_$makeUsersPremiumImpl value,
          $Res Function(_$makeUsersPremiumImpl) then) =
      __$$makeUsersPremiumImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$makeUsersPremiumImplCopyWithImpl<$Res>
    extends _$UsersEventCopyWithImpl<$Res, _$makeUsersPremiumImpl>
    implements _$$makeUsersPremiumImplCopyWith<$Res> {
  __$$makeUsersPremiumImplCopyWithImpl(_$makeUsersPremiumImpl _value,
      $Res Function(_$makeUsersPremiumImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$makeUsersPremiumImpl implements makeUsersPremium {
  const _$makeUsersPremiumImpl();

  @override
  String toString() {
    return 'UsersEvent.makeUsersPremium()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$makeUsersPremiumImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() addUserTrialDate,
    required TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)
        updateUsersBusiness,
    required TResult Function() getUserDetail,
    required TResult Function() makeUsersPremium,
  }) {
    return makeUsersPremium();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? addUserTrialDate,
    TResult? Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult? Function()? getUserDetail,
    TResult? Function()? makeUsersPremium,
  }) {
    return makeUsersPremium?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? addUserTrialDate,
    TResult Function(String userPusrpose, String userBusinessName,
            String userBusinessLogo, String userBusinessType)?
        updateUsersBusiness,
    TResult Function()? getUserDetail,
    TResult Function()? makeUsersPremium,
    required TResult orElse(),
  }) {
    if (makeUsersPremium != null) {
      return makeUsersPremium();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(addUserTrialDate value) addUserTrialDate,
    required TResult Function(updateUsersBusiness value) updateUsersBusiness,
    required TResult Function(getUserDetail value) getUserDetail,
    required TResult Function(makeUsersPremium value) makeUsersPremium,
  }) {
    return makeUsersPremium(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(addUserTrialDate value)? addUserTrialDate,
    TResult? Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult? Function(getUserDetail value)? getUserDetail,
    TResult? Function(makeUsersPremium value)? makeUsersPremium,
  }) {
    return makeUsersPremium?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(addUserTrialDate value)? addUserTrialDate,
    TResult Function(updateUsersBusiness value)? updateUsersBusiness,
    TResult Function(getUserDetail value)? getUserDetail,
    TResult Function(makeUsersPremium value)? makeUsersPremium,
    required TResult orElse(),
  }) {
    if (makeUsersPremium != null) {
      return makeUsersPremium(this);
    }
    return orElse();
  }
}

abstract class makeUsersPremium implements UsersEvent {
  const factory makeUsersPremium() = _$makeUsersPremiumImpl;
}

/// @nodoc
mixin _$UsersState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() usersInitial,
    required TResult Function(bool isLoading, bool isSuccess, bool isError)
        updateBusinessResult,
    required TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)
        addTrialDateState,
    required TResult Function(
            UserProfileDTO? user, bool isLoading, bool isError)
        viewUser,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? usersInitial,
    TResult? Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult? Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult? Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? usersInitial,
    TResult Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(usersInitial value) usersInitial,
    required TResult Function(updateBusinessResult value) updateBusinessResult,
    required TResult Function(addTrialDateState value) addTrialDateState,
    required TResult Function(viewUser value) viewUser,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(usersInitial value)? usersInitial,
    TResult? Function(updateBusinessResult value)? updateBusinessResult,
    TResult? Function(addTrialDateState value)? addTrialDateState,
    TResult? Function(viewUser value)? viewUser,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(usersInitial value)? usersInitial,
    TResult Function(updateBusinessResult value)? updateBusinessResult,
    TResult Function(addTrialDateState value)? addTrialDateState,
    TResult Function(viewUser value)? viewUser,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UsersStateCopyWith<$Res> {
  factory $UsersStateCopyWith(
          UsersState value, $Res Function(UsersState) then) =
      _$UsersStateCopyWithImpl<$Res, UsersState>;
}

/// @nodoc
class _$UsersStateCopyWithImpl<$Res, $Val extends UsersState>
    implements $UsersStateCopyWith<$Res> {
  _$UsersStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$usersInitialImplCopyWith<$Res> {
  factory _$$usersInitialImplCopyWith(
          _$usersInitialImpl value, $Res Function(_$usersInitialImpl) then) =
      __$$usersInitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$usersInitialImplCopyWithImpl<$Res>
    extends _$UsersStateCopyWithImpl<$Res, _$usersInitialImpl>
    implements _$$usersInitialImplCopyWith<$Res> {
  __$$usersInitialImplCopyWithImpl(
      _$usersInitialImpl _value, $Res Function(_$usersInitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$usersInitialImpl implements usersInitial {
  const _$usersInitialImpl();

  @override
  String toString() {
    return 'UsersState.usersInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$usersInitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() usersInitial,
    required TResult Function(bool isLoading, bool isSuccess, bool isError)
        updateBusinessResult,
    required TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)
        addTrialDateState,
    required TResult Function(
            UserProfileDTO? user, bool isLoading, bool isError)
        viewUser,
  }) {
    return usersInitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? usersInitial,
    TResult? Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult? Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult? Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
  }) {
    return usersInitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? usersInitial,
    TResult Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
    required TResult orElse(),
  }) {
    if (usersInitial != null) {
      return usersInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(usersInitial value) usersInitial,
    required TResult Function(updateBusinessResult value) updateBusinessResult,
    required TResult Function(addTrialDateState value) addTrialDateState,
    required TResult Function(viewUser value) viewUser,
  }) {
    return usersInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(usersInitial value)? usersInitial,
    TResult? Function(updateBusinessResult value)? updateBusinessResult,
    TResult? Function(addTrialDateState value)? addTrialDateState,
    TResult? Function(viewUser value)? viewUser,
  }) {
    return usersInitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(usersInitial value)? usersInitial,
    TResult Function(updateBusinessResult value)? updateBusinessResult,
    TResult Function(addTrialDateState value)? addTrialDateState,
    TResult Function(viewUser value)? viewUser,
    required TResult orElse(),
  }) {
    if (usersInitial != null) {
      return usersInitial(this);
    }
    return orElse();
  }
}

abstract class usersInitial implements UsersState {
  const factory usersInitial() = _$usersInitialImpl;
}

/// @nodoc
abstract class _$$updateBusinessResultImplCopyWith<$Res> {
  factory _$$updateBusinessResultImplCopyWith(_$updateBusinessResultImpl value,
          $Res Function(_$updateBusinessResultImpl) then) =
      __$$updateBusinessResultImplCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isLoading, bool isSuccess, bool isError});
}

/// @nodoc
class __$$updateBusinessResultImplCopyWithImpl<$Res>
    extends _$UsersStateCopyWithImpl<$Res, _$updateBusinessResultImpl>
    implements _$$updateBusinessResultImplCopyWith<$Res> {
  __$$updateBusinessResultImplCopyWithImpl(_$updateBusinessResultImpl _value,
      $Res Function(_$updateBusinessResultImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isSuccess = null,
    Object? isError = null,
  }) {
    return _then(_$updateBusinessResultImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isSuccess: null == isSuccess
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$updateBusinessResultImpl implements updateBusinessResult {
  const _$updateBusinessResultImpl(
      {required this.isLoading,
      required this.isSuccess,
      required this.isError});

  @override
  final bool isLoading;
  @override
  final bool isSuccess;
  @override
  final bool isError;

  @override
  String toString() {
    return 'UsersState.updateBusinessResult(isLoading: $isLoading, isSuccess: $isSuccess, isError: $isError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$updateBusinessResultImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isSuccess, isSuccess) ||
                other.isSuccess == isSuccess) &&
            (identical(other.isError, isError) || other.isError == isError));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isLoading, isSuccess, isError);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$updateBusinessResultImplCopyWith<_$updateBusinessResultImpl>
      get copyWith =>
          __$$updateBusinessResultImplCopyWithImpl<_$updateBusinessResultImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() usersInitial,
    required TResult Function(bool isLoading, bool isSuccess, bool isError)
        updateBusinessResult,
    required TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)
        addTrialDateState,
    required TResult Function(
            UserProfileDTO? user, bool isLoading, bool isError)
        viewUser,
  }) {
    return updateBusinessResult(isLoading, isSuccess, isError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? usersInitial,
    TResult? Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult? Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult? Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
  }) {
    return updateBusinessResult?.call(isLoading, isSuccess, isError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? usersInitial,
    TResult Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
    required TResult orElse(),
  }) {
    if (updateBusinessResult != null) {
      return updateBusinessResult(isLoading, isSuccess, isError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(usersInitial value) usersInitial,
    required TResult Function(updateBusinessResult value) updateBusinessResult,
    required TResult Function(addTrialDateState value) addTrialDateState,
    required TResult Function(viewUser value) viewUser,
  }) {
    return updateBusinessResult(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(usersInitial value)? usersInitial,
    TResult? Function(updateBusinessResult value)? updateBusinessResult,
    TResult? Function(addTrialDateState value)? addTrialDateState,
    TResult? Function(viewUser value)? viewUser,
  }) {
    return updateBusinessResult?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(usersInitial value)? usersInitial,
    TResult Function(updateBusinessResult value)? updateBusinessResult,
    TResult Function(addTrialDateState value)? addTrialDateState,
    TResult Function(viewUser value)? viewUser,
    required TResult orElse(),
  }) {
    if (updateBusinessResult != null) {
      return updateBusinessResult(this);
    }
    return orElse();
  }
}

abstract class updateBusinessResult implements UsersState {
  const factory updateBusinessResult(
      {required final bool isLoading,
      required final bool isSuccess,
      required final bool isError}) = _$updateBusinessResultImpl;

  bool get isLoading;
  bool get isSuccess;
  bool get isError;
  @JsonKey(ignore: true)
  _$$updateBusinessResultImplCopyWith<_$updateBusinessResultImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addTrialDateStateImplCopyWith<$Res> {
  factory _$$addTrialDateStateImplCopyWith(_$addTrialDateStateImpl value,
          $Res Function(_$addTrialDateStateImpl) then) =
      __$$addTrialDateStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError});
}

/// @nodoc
class __$$addTrialDateStateImplCopyWithImpl<$Res>
    extends _$UsersStateCopyWithImpl<$Res, _$addTrialDateStateImpl>
    implements _$$addTrialDateStateImplCopyWith<$Res> {
  __$$addTrialDateStateImplCopyWithImpl(_$addTrialDateStateImpl _value,
      $Res Function(_$addTrialDateStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isSuccess = null,
    Object? user = freezed,
    Object? isError = null,
  }) {
    return _then(_$addTrialDateStateImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isSuccess: null == isSuccess
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as UserProfileDTO?,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$addTrialDateStateImpl implements addTrialDateState {
  const _$addTrialDateStateImpl(
      {required this.isLoading,
      required this.isSuccess,
      required this.user,
      required this.isError});

  @override
  final bool isLoading;
  @override
  final bool isSuccess;
  @override
  final UserProfileDTO? user;
  @override
  final bool isError;

  @override
  String toString() {
    return 'UsersState.addTrialDateState(isLoading: $isLoading, isSuccess: $isSuccess, user: $user, isError: $isError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addTrialDateStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isSuccess, isSuccess) ||
                other.isSuccess == isSuccess) &&
            (identical(other.user, user) || other.user == user) &&
            (identical(other.isError, isError) || other.isError == isError));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isLoading, isSuccess, user, isError);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addTrialDateStateImplCopyWith<_$addTrialDateStateImpl> get copyWith =>
      __$$addTrialDateStateImplCopyWithImpl<_$addTrialDateStateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() usersInitial,
    required TResult Function(bool isLoading, bool isSuccess, bool isError)
        updateBusinessResult,
    required TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)
        addTrialDateState,
    required TResult Function(
            UserProfileDTO? user, bool isLoading, bool isError)
        viewUser,
  }) {
    return addTrialDateState(isLoading, isSuccess, user, isError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? usersInitial,
    TResult? Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult? Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult? Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
  }) {
    return addTrialDateState?.call(isLoading, isSuccess, user, isError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? usersInitial,
    TResult Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
    required TResult orElse(),
  }) {
    if (addTrialDateState != null) {
      return addTrialDateState(isLoading, isSuccess, user, isError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(usersInitial value) usersInitial,
    required TResult Function(updateBusinessResult value) updateBusinessResult,
    required TResult Function(addTrialDateState value) addTrialDateState,
    required TResult Function(viewUser value) viewUser,
  }) {
    return addTrialDateState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(usersInitial value)? usersInitial,
    TResult? Function(updateBusinessResult value)? updateBusinessResult,
    TResult? Function(addTrialDateState value)? addTrialDateState,
    TResult? Function(viewUser value)? viewUser,
  }) {
    return addTrialDateState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(usersInitial value)? usersInitial,
    TResult Function(updateBusinessResult value)? updateBusinessResult,
    TResult Function(addTrialDateState value)? addTrialDateState,
    TResult Function(viewUser value)? viewUser,
    required TResult orElse(),
  }) {
    if (addTrialDateState != null) {
      return addTrialDateState(this);
    }
    return orElse();
  }
}

abstract class addTrialDateState implements UsersState {
  const factory addTrialDateState(
      {required final bool isLoading,
      required final bool isSuccess,
      required final UserProfileDTO? user,
      required final bool isError}) = _$addTrialDateStateImpl;

  bool get isLoading;
  bool get isSuccess;
  UserProfileDTO? get user;
  bool get isError;
  @JsonKey(ignore: true)
  _$$addTrialDateStateImplCopyWith<_$addTrialDateStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$viewUserImplCopyWith<$Res> {
  factory _$$viewUserImplCopyWith(
          _$viewUserImpl value, $Res Function(_$viewUserImpl) then) =
      __$$viewUserImplCopyWithImpl<$Res>;
  @useResult
  $Res call({UserProfileDTO? user, bool isLoading, bool isError});
}

/// @nodoc
class __$$viewUserImplCopyWithImpl<$Res>
    extends _$UsersStateCopyWithImpl<$Res, _$viewUserImpl>
    implements _$$viewUserImplCopyWith<$Res> {
  __$$viewUserImplCopyWithImpl(
      _$viewUserImpl _value, $Res Function(_$viewUserImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user = freezed,
    Object? isLoading = null,
    Object? isError = null,
  }) {
    return _then(_$viewUserImpl(
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as UserProfileDTO?,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$viewUserImpl implements viewUser {
  const _$viewUserImpl(
      {required this.user, required this.isLoading, required this.isError});

  @override
  final UserProfileDTO? user;
  @override
  final bool isLoading;
  @override
  final bool isError;

  @override
  String toString() {
    return 'UsersState.viewUser(user: $user, isLoading: $isLoading, isError: $isError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$viewUserImpl &&
            (identical(other.user, user) || other.user == user) &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isError, isError) || other.isError == isError));
  }

  @override
  int get hashCode => Object.hash(runtimeType, user, isLoading, isError);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$viewUserImplCopyWith<_$viewUserImpl> get copyWith =>
      __$$viewUserImplCopyWithImpl<_$viewUserImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() usersInitial,
    required TResult Function(bool isLoading, bool isSuccess, bool isError)
        updateBusinessResult,
    required TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)
        addTrialDateState,
    required TResult Function(
            UserProfileDTO? user, bool isLoading, bool isError)
        viewUser,
  }) {
    return viewUser(user, isLoading, isError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? usersInitial,
    TResult? Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult? Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult? Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
  }) {
    return viewUser?.call(user, isLoading, isError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? usersInitial,
    TResult Function(bool isLoading, bool isSuccess, bool isError)?
        updateBusinessResult,
    TResult Function(
            bool isLoading, bool isSuccess, UserProfileDTO? user, bool isError)?
        addTrialDateState,
    TResult Function(UserProfileDTO? user, bool isLoading, bool isError)?
        viewUser,
    required TResult orElse(),
  }) {
    if (viewUser != null) {
      return viewUser(user, isLoading, isError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(usersInitial value) usersInitial,
    required TResult Function(updateBusinessResult value) updateBusinessResult,
    required TResult Function(addTrialDateState value) addTrialDateState,
    required TResult Function(viewUser value) viewUser,
  }) {
    return viewUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(usersInitial value)? usersInitial,
    TResult? Function(updateBusinessResult value)? updateBusinessResult,
    TResult? Function(addTrialDateState value)? addTrialDateState,
    TResult? Function(viewUser value)? viewUser,
  }) {
    return viewUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(usersInitial value)? usersInitial,
    TResult Function(updateBusinessResult value)? updateBusinessResult,
    TResult Function(addTrialDateState value)? addTrialDateState,
    TResult Function(viewUser value)? viewUser,
    required TResult orElse(),
  }) {
    if (viewUser != null) {
      return viewUser(this);
    }
    return orElse();
  }
}

abstract class viewUser implements UsersState {
  const factory viewUser(
      {required final UserProfileDTO? user,
      required final bool isLoading,
      required final bool isError}) = _$viewUserImpl;

  UserProfileDTO? get user;
  bool get isLoading;
  bool get isError;
  @JsonKey(ignore: true)
  _$$viewUserImplCopyWith<_$viewUserImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
