import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/presentation/screens/invoice/reports/tabs/clients/screen_clients.dart';
import 'package:linq_pe/presentation/screens/invoice/reports/tabs/items/screen_item.dart';
import 'package:linq_pe/presentation/screens/invoice/reports/tabs/paid/screen_paid.dart';
import 'package:linq_pe/presentation/view_state/invoice/report/invoice_report.dart';
import 'package:linq_pe/utilities/colors.dart';

class InvoiceReportsScreen extends ConsumerWidget {
  const InvoiceReportsScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    final screenList = [const PaidScreen(),const ClientScreen() , const ItemScreen()];
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.2,
        title: Text('Reports',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.09,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
                  children: [
            SizedBox(
              height: size.height * 0.02,
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ReportTypeContainer(
                    reportType: ReportType.paid, reportTypeName: 'Paid'),
                ReportTypeContainer(
                    reportType: ReportType.clients, reportTypeName: 'Clients'),
                ReportTypeContainer(
                    reportType: ReportType.items, reportTypeName: 'Items'),
              ],
            ),
            screenList.elementAt(ref.watch(reportTypeProvider) == ReportType.paid
                ? 0
                : ref.watch(reportTypeProvider) == ReportType.clients
                    ? 1
                    : 2),
                  ],
                ),
          )),
    );
  }
}

class ReportTypeContainer extends ConsumerWidget {
  const ReportTypeContainer(
      {super.key, required this.reportType, required this.reportTypeName});
  final ReportType reportType;
  final String reportTypeName;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    bool isSelected =
        ref.watch(reportTypeProvider) == reportType ? true : false;
    return InkWell(
      onTap: () {
        addreportType(reportType, ref);
      },
      child: Container(
        width: size.width * 0.3,
        height: size.height * 0.05,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: LinqPeColors.kPinkColor),
            color: isSelected
                ? LinqPeColors.kPinkColor
                : LinqPeColors.kWhiteColor),
        child: Text(reportTypeName,
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .1,
                fontSize: size.width * 0.04,
                color: isSelected
                    ? LinqPeColors.kWhiteColor
                    : LinqPeColors.kPinkColor,
                fontWeight: FontWeight.w500,
              ),
            )),
      ),
    );
  }
}
