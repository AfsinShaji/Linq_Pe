// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'my_items_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MyItemsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ItemDTO item) addToMyItem,
    required TResult Function() getAllMyItem,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ItemDTO item)? addToMyItem,
    TResult? Function()? getAllMyItem,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ItemDTO item)? addToMyItem,
    TResult Function()? getAllMyItem,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(addToMyItem value) addToMyItem,
    required TResult Function(getAllMyItem value) getAllMyItem,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(addToMyItem value)? addToMyItem,
    TResult? Function(getAllMyItem value)? getAllMyItem,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(addToMyItem value)? addToMyItem,
    TResult Function(getAllMyItem value)? getAllMyItem,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MyItemsEventCopyWith<$Res> {
  factory $MyItemsEventCopyWith(
          MyItemsEvent value, $Res Function(MyItemsEvent) then) =
      _$MyItemsEventCopyWithImpl<$Res, MyItemsEvent>;
}

/// @nodoc
class _$MyItemsEventCopyWithImpl<$Res, $Val extends MyItemsEvent>
    implements $MyItemsEventCopyWith<$Res> {
  _$MyItemsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$addToMyItemImplCopyWith<$Res> {
  factory _$$addToMyItemImplCopyWith(
          _$addToMyItemImpl value, $Res Function(_$addToMyItemImpl) then) =
      __$$addToMyItemImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ItemDTO item});
}

/// @nodoc
class __$$addToMyItemImplCopyWithImpl<$Res>
    extends _$MyItemsEventCopyWithImpl<$Res, _$addToMyItemImpl>
    implements _$$addToMyItemImplCopyWith<$Res> {
  __$$addToMyItemImplCopyWithImpl(
      _$addToMyItemImpl _value, $Res Function(_$addToMyItemImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? item = null,
  }) {
    return _then(_$addToMyItemImpl(
      item: null == item
          ? _value.item
          : item // ignore: cast_nullable_to_non_nullable
              as ItemDTO,
    ));
  }
}

/// @nodoc

class _$addToMyItemImpl implements addToMyItem {
  const _$addToMyItemImpl({required this.item});

  @override
  final ItemDTO item;

  @override
  String toString() {
    return 'MyItemsEvent.addToMyItem(item: $item)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addToMyItemImpl &&
            (identical(other.item, item) || other.item == item));
  }

  @override
  int get hashCode => Object.hash(runtimeType, item);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addToMyItemImplCopyWith<_$addToMyItemImpl> get copyWith =>
      __$$addToMyItemImplCopyWithImpl<_$addToMyItemImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ItemDTO item) addToMyItem,
    required TResult Function() getAllMyItem,
  }) {
    return addToMyItem(item);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ItemDTO item)? addToMyItem,
    TResult? Function()? getAllMyItem,
  }) {
    return addToMyItem?.call(item);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ItemDTO item)? addToMyItem,
    TResult Function()? getAllMyItem,
    required TResult orElse(),
  }) {
    if (addToMyItem != null) {
      return addToMyItem(item);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(addToMyItem value) addToMyItem,
    required TResult Function(getAllMyItem value) getAllMyItem,
  }) {
    return addToMyItem(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(addToMyItem value)? addToMyItem,
    TResult? Function(getAllMyItem value)? getAllMyItem,
  }) {
    return addToMyItem?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(addToMyItem value)? addToMyItem,
    TResult Function(getAllMyItem value)? getAllMyItem,
    required TResult orElse(),
  }) {
    if (addToMyItem != null) {
      return addToMyItem(this);
    }
    return orElse();
  }
}

abstract class addToMyItem implements MyItemsEvent {
  const factory addToMyItem({required final ItemDTO item}) = _$addToMyItemImpl;

  ItemDTO get item;
  @JsonKey(ignore: true)
  _$$addToMyItemImplCopyWith<_$addToMyItemImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$getAllMyItemImplCopyWith<$Res> {
  factory _$$getAllMyItemImplCopyWith(
          _$getAllMyItemImpl value, $Res Function(_$getAllMyItemImpl) then) =
      __$$getAllMyItemImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$getAllMyItemImplCopyWithImpl<$Res>
    extends _$MyItemsEventCopyWithImpl<$Res, _$getAllMyItemImpl>
    implements _$$getAllMyItemImplCopyWith<$Res> {
  __$$getAllMyItemImplCopyWithImpl(
      _$getAllMyItemImpl _value, $Res Function(_$getAllMyItemImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$getAllMyItemImpl implements getAllMyItem {
  const _$getAllMyItemImpl();

  @override
  String toString() {
    return 'MyItemsEvent.getAllMyItem()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$getAllMyItemImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ItemDTO item) addToMyItem,
    required TResult Function() getAllMyItem,
  }) {
    return getAllMyItem();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ItemDTO item)? addToMyItem,
    TResult? Function()? getAllMyItem,
  }) {
    return getAllMyItem?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ItemDTO item)? addToMyItem,
    TResult Function()? getAllMyItem,
    required TResult orElse(),
  }) {
    if (getAllMyItem != null) {
      return getAllMyItem();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(addToMyItem value) addToMyItem,
    required TResult Function(getAllMyItem value) getAllMyItem,
  }) {
    return getAllMyItem(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(addToMyItem value)? addToMyItem,
    TResult? Function(getAllMyItem value)? getAllMyItem,
  }) {
    return getAllMyItem?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(addToMyItem value)? addToMyItem,
    TResult Function(getAllMyItem value)? getAllMyItem,
    required TResult orElse(),
  }) {
    if (getAllMyItem != null) {
      return getAllMyItem(this);
    }
    return orElse();
  }
}

abstract class getAllMyItem implements MyItemsEvent {
  const factory getAllMyItem() = _$getAllMyItemImpl;
}

/// @nodoc
mixin _$MyItemsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() myItemsInitial,
    required TResult Function(List<ItemDTO> myItems) displayMyItems,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? myItemsInitial,
    TResult? Function(List<ItemDTO> myItems)? displayMyItems,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? myItemsInitial,
    TResult Function(List<ItemDTO> myItems)? displayMyItems,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(myItemsInitial value) myItemsInitial,
    required TResult Function(displayMyItems value) displayMyItems,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(myItemsInitial value)? myItemsInitial,
    TResult? Function(displayMyItems value)? displayMyItems,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(myItemsInitial value)? myItemsInitial,
    TResult Function(displayMyItems value)? displayMyItems,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MyItemsStateCopyWith<$Res> {
  factory $MyItemsStateCopyWith(
          MyItemsState value, $Res Function(MyItemsState) then) =
      _$MyItemsStateCopyWithImpl<$Res, MyItemsState>;
}

/// @nodoc
class _$MyItemsStateCopyWithImpl<$Res, $Val extends MyItemsState>
    implements $MyItemsStateCopyWith<$Res> {
  _$MyItemsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$myItemsInitialImplCopyWith<$Res> {
  factory _$$myItemsInitialImplCopyWith(_$myItemsInitialImpl value,
          $Res Function(_$myItemsInitialImpl) then) =
      __$$myItemsInitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$myItemsInitialImplCopyWithImpl<$Res>
    extends _$MyItemsStateCopyWithImpl<$Res, _$myItemsInitialImpl>
    implements _$$myItemsInitialImplCopyWith<$Res> {
  __$$myItemsInitialImplCopyWithImpl(
      _$myItemsInitialImpl _value, $Res Function(_$myItemsInitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$myItemsInitialImpl implements myItemsInitial {
  const _$myItemsInitialImpl();

  @override
  String toString() {
    return 'MyItemsState.myItemsInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$myItemsInitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() myItemsInitial,
    required TResult Function(List<ItemDTO> myItems) displayMyItems,
  }) {
    return myItemsInitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? myItemsInitial,
    TResult? Function(List<ItemDTO> myItems)? displayMyItems,
  }) {
    return myItemsInitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? myItemsInitial,
    TResult Function(List<ItemDTO> myItems)? displayMyItems,
    required TResult orElse(),
  }) {
    if (myItemsInitial != null) {
      return myItemsInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(myItemsInitial value) myItemsInitial,
    required TResult Function(displayMyItems value) displayMyItems,
  }) {
    return myItemsInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(myItemsInitial value)? myItemsInitial,
    TResult? Function(displayMyItems value)? displayMyItems,
  }) {
    return myItemsInitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(myItemsInitial value)? myItemsInitial,
    TResult Function(displayMyItems value)? displayMyItems,
    required TResult orElse(),
  }) {
    if (myItemsInitial != null) {
      return myItemsInitial(this);
    }
    return orElse();
  }
}

abstract class myItemsInitial implements MyItemsState {
  const factory myItemsInitial() = _$myItemsInitialImpl;
}

/// @nodoc
abstract class _$$displayMyItemsImplCopyWith<$Res> {
  factory _$$displayMyItemsImplCopyWith(_$displayMyItemsImpl value,
          $Res Function(_$displayMyItemsImpl) then) =
      __$$displayMyItemsImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<ItemDTO> myItems});
}

/// @nodoc
class __$$displayMyItemsImplCopyWithImpl<$Res>
    extends _$MyItemsStateCopyWithImpl<$Res, _$displayMyItemsImpl>
    implements _$$displayMyItemsImplCopyWith<$Res> {
  __$$displayMyItemsImplCopyWithImpl(
      _$displayMyItemsImpl _value, $Res Function(_$displayMyItemsImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? myItems = null,
  }) {
    return _then(_$displayMyItemsImpl(
      myItems: null == myItems
          ? _value._myItems
          : myItems // ignore: cast_nullable_to_non_nullable
              as List<ItemDTO>,
    ));
  }
}

/// @nodoc

class _$displayMyItemsImpl implements displayMyItems {
  const _$displayMyItemsImpl({required final List<ItemDTO> myItems})
      : _myItems = myItems;

  final List<ItemDTO> _myItems;
  @override
  List<ItemDTO> get myItems {
    if (_myItems is EqualUnmodifiableListView) return _myItems;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_myItems);
  }

  @override
  String toString() {
    return 'MyItemsState.displayMyItems(myItems: $myItems)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$displayMyItemsImpl &&
            const DeepCollectionEquality().equals(other._myItems, _myItems));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_myItems));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$displayMyItemsImplCopyWith<_$displayMyItemsImpl> get copyWith =>
      __$$displayMyItemsImplCopyWithImpl<_$displayMyItemsImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() myItemsInitial,
    required TResult Function(List<ItemDTO> myItems) displayMyItems,
  }) {
    return displayMyItems(myItems);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? myItemsInitial,
    TResult? Function(List<ItemDTO> myItems)? displayMyItems,
  }) {
    return displayMyItems?.call(myItems);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? myItemsInitial,
    TResult Function(List<ItemDTO> myItems)? displayMyItems,
    required TResult orElse(),
  }) {
    if (displayMyItems != null) {
      return displayMyItems(myItems);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(myItemsInitial value) myItemsInitial,
    required TResult Function(displayMyItems value) displayMyItems,
  }) {
    return displayMyItems(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(myItemsInitial value)? myItemsInitial,
    TResult? Function(displayMyItems value)? displayMyItems,
  }) {
    return displayMyItems?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(myItemsInitial value)? myItemsInitial,
    TResult Function(displayMyItems value)? displayMyItems,
    required TResult orElse(),
  }) {
    if (displayMyItems != null) {
      return displayMyItems(this);
    }
    return orElse();
  }
}

abstract class displayMyItems implements MyItemsState {
  const factory displayMyItems({required final List<ItemDTO> myItems}) =
      _$displayMyItemsImpl;

  List<ItemDTO> get myItems;
  @JsonKey(ignore: true)
  _$$displayMyItemsImplCopyWith<_$displayMyItemsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
