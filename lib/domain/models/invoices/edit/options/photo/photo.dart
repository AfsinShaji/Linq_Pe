import 'dart:typed_data';

import 'package:hive/hive.dart';

part 'photo.g.dart';

@HiveType(typeId: 18)
class PhotoModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final Uint8List photo;
  @HiveField(2)
  final String? description;
  @HiveField(3)
  final String? addionalDetails;
  @HiveField(4)
  final String photoId;


  PhotoModel({required this.photoId,
    required this.invoiceId,
    required this.photo,
    required this.addionalDetails,
    required this.description

  });
}

class PhotoBox {
  static Box<PhotoModel>? _getbox;
  static Box<PhotoModel> getInstance() {
    return _getbox ??= Hive.box('PhotoBox');
  }
}
