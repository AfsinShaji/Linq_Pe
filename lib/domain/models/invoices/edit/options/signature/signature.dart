import 'dart:typed_data';

import 'package:hive/hive.dart';

part 'signature.g.dart';

@HiveType(typeId: 17)
class SignatureModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final Uint8List? signature;
  @HiveField(2)
  final DateTime? signDate;

  SignatureModel({
    required this.invoiceId,
    required this.signature,required this.signDate
  });
}

class SignatureBox {
  static Box<SignatureModel>? _getbox;
  static Box<SignatureModel> getInstance() {
    return _getbox ??= Hive.box('SignatureBox');
  }
}
