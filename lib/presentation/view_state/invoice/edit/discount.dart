import 'package:flutter_riverpod/flutter_riverpod.dart';

final discountTypeProvider = StateProvider(
  (ref) {
    return 'No Discount';
  },
);
addDiscountType(String type, WidgetRef ref) {

  ref.read(discountTypeProvider.notifier).state = type;
}