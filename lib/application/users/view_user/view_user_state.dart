part of 'view_user_bloc.dart';

@freezed
class ViewUserState with _$ViewUserState {
  const factory ViewUserState.viewuserinitial() = viewuserinitial;
   const factory ViewUserState.viewUsers(
      {required UserProfileDTO? user,
      required bool isLoading,
      required bool isError}) = viewUsers;
}
