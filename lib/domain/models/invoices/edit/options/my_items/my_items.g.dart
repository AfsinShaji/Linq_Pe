// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_items.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MyItemsModelAdapter extends TypeAdapter<MyItemsModel> {
  @override
  final int typeId = 24;

  @override
  MyItemsModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MyItemsModel(
      itemsList: (fields[0] as List).cast<ItemModel>(),
    );
  }

  @override
  void write(BinaryWriter writer, MyItemsModel obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.itemsList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MyItemsModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
