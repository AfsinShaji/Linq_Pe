import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/contacts/contacts_bloc.dart';
import 'package:linq_pe/application/transactions/transactions_bloc.dart';

import 'package:linq_pe/application/view_dto/contact/contact_dto.dart';
import 'package:linq_pe/application/view_dto/transaction/secondary_transaction_dto.dart';
import 'package:linq_pe/presentation/screens/view_party/screen_view_party.dart';
import 'package:linq_pe/presentation/screens/view_party/wigets/pdf_report.dart';
import 'package:linq_pe/utilities/colors.dart';
import 'package:screenshot/screenshot.dart';

class ViewPartyScreens extends StatefulWidget {
  const ViewPartyScreens(
      {super.key,
      required this.contact,
      required this.isExpense,
      required this.lengthNum});
  final ContactsDTO contact;
  final bool isExpense;
  final int lengthNum;
  static final ScreenshotController screenshotController =
      ScreenshotController();
  @override
  State<ViewPartyScreens> createState() => _ViewPartyScreensState();
}

class _ViewPartyScreensState extends State<ViewPartyScreens> {
  double balanceForPay = 0.0;
  //Create an instance of ScreenshotController
  final screenshotKey = GlobalKey();
  final ScrollController controller = ScrollController();
  @override
  void initState() {
    super.initState();
    BlocProvider.of<TransactionsBloc>(context).add(
        TransactionsEvent.getTransactionsList(
            ledgerId: widget.contact.ledgerId,
            contactId: widget.contact.contactId));
  }

  @override
  Widget build(BuildContext context) {
    log(widget.lengthNum.toString());
    final Size size = MediaQuery.of(context).size;
    double splittedAmount = 0.0;
    String received = '0';
    String balance = '0';
    String payed = '0';
    return Scaffold(
      backgroundColor: LinqPeColors.kPinkColor,
      bottomSheet: InkWell(
        onTap: () async {
          await captureAndDownloads(
              widget.lengthNum > 5
                  ? (size.height +
                      ((size.height * 0.2) * (widget.lengthNum - 5)))
                  : size.height,
              size.width);
        },
        child: Container(
          margin: EdgeInsets.all(size.width * 0.05),
          height: size.height * 0.065,
          width: size.width,
          decoration: const BoxDecoration(color: LinqPeColors.kPinkColor),
          child: Center(
              child: Text(
            'Download Report',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .5,
                fontSize: size.width * 0.04,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          )),
        ),
      ),
      body: ListView(
        controller: controller,
        children: [
          SingleChildScrollView(
            child: Screenshot(
              controller: ViewPartyScreens.screenshotController,
              child: SizedBox(
                height: widget.lengthNum > 5
                    ? (size.height +
                        ((size.height * 0.2) * (widget.lengthNum - 5)))
                    : size.height,
                child: Scaffold(
                  appBar: PreferredSize(
                    preferredSize: widget.isExpense ? size * 0.1 : size * 0.3,
                    child: ColoredBox(
                      color: LinqPeColors.kPinkColor,
                      child: Column(
                        children: [
                          SizedBox(
                            height: size.height * 0.01,
                          ),
                          Text('Report',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                  letterSpacing: .5,
                                  fontSize: size.width * 0.07,
                                  color: LinqPeColors.kWhiteColor,
                                  fontWeight: FontWeight.w500,
                                ),
                              )),
                          Text(
                              '(As of Today - ${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year})',
                              style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                  letterSpacing: .5,
                                  fontSize: size.width * 0.03,
                                  color: LinqPeColors.kWhiteColor,
                                  fontWeight: FontWeight.w400,
                                ),
                              )),
                          SizedBox(
                            height: size.height * 0.01,
                          ),
                          AppBar(
                            leading: const SizedBox(),
                            backgroundColor: LinqPeColors.kPinkColor,
                            leadingWidth: size.width * 0.2,
                            title: Row(
                              children: [
                                (widget.contact.avatar != null &&
                                        widget.contact.avatar!.isNotEmpty)
                                    ? Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              color: LinqPeColors.kWhiteColor
                                                  .withOpacity(0.5),
                                              width: size.width * 0.005,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(100)),
                                        child: CircleAvatar(
                                            radius: size.width * 0.06,
                                            backgroundImage: MemoryImage(
                                                widget.contact.avatar!)),
                                      )
                                    : CircleAvatar(
                                        child: Text(widget.contact.initails)),
                                SizedBox(
                                  width: size.width * 0.01,
                                ),
                                SizedBox(
                                  width: size.width * 0.5,
                                  child: Text(widget.contact.displayName,
                                      style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                          overflow: TextOverflow.ellipsis,
                                          letterSpacing: .5,
                                          fontSize: size.width * 0.05,
                                          color: LinqPeColors.kWhiteColor,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )),
                                ),
                              ],
                            ),
                            actions:
                                //  widget.isExpense
                                //     ? null
                                //     :
                                [
                              // IconButton(
                              //     onPressed: () async {
                              //       await captureAndDownloads();
                              //     },
                              //     icon: Icon(
                              //       Icons.download,
                              //       color: LinqPeColors.kWhiteColor,
                              //       size: size.width * 0.06,
                              //     ))
                              //MenuIconPopupButton
                              // MenuIconButtonWidget(size: size,
                              //  widget: widget, balance: balance,
                              //  splittedAmount: splittedAmount, )
                              //----------------------------------------------------------------------

                              // ClickButton(
                              //   textColor: LinqPeColors.kPinkColor,
                              //   onTap: () {
                              //     Navigator.push(
                              //         context,
                              //         CupertinoPageRoute(
                              //           builder: (context) => AddAmountScreen(
                              //             isGive: false,
                              //             isSplit: false,
                              //             isSecondaryPay: false,
                              //             isPay: true,
                              //             isAddBalance: false,
                              //             partyName: widget.contact.displayName,
                              //           ),
                              //         ));
                              //   },
                              //   width: size.width * 0.33,
                              //   text: 'PAY FOR ₹',
                              //   radius: 5,
                              //   backGroundColor: LinqPeColors.kWhiteColor,
                              //   changeColor: LinqPeColors.kWhiteColor.withOpacity(0.5),
                              // ),
                              //,

                              SizedBox(
                                width: size.width * 0.05,
                              )
                            ],
                          ),
                          widget.isExpense
                              ? const SizedBox()
                              : BlocBuilder<TransactionsBloc,
                                  TransactionsState>(
                                  builder: (context, state) {
                                    if (state is displayTransactions) {
                                      if (state.partyAccount != null) {
                                        received = state
                                            .partyAccount!.recievedAmt
                                            .toString();
                                        balance = state.partyAccount!.balanceAmt
                                            .toString();
                                        payed = state.partyAccount!.payedAmt
                                            .toString();
                                      } else {
                                        received = '0.0';
                                        balance = '0.0';
                                        payed = '0.0';
                                      }
                                    }
                                    log(balance.toString());
                                    return AmountNotifyingContainer(
                                        size: size,
                                        received: received,
                                        payed: payed,
                                        balance: balance);
                                  },
                                ),
                        ],
                      ),
                    ),
                  ),
                  body: ColoredBox(
                    color: LinqPeColors.kLightBluwWhite,
                    child: BlocBuilder<ContactsBloc, ContactsState>(
                      builder: (context, contactstate) {
                        List<ContactsDTO> contactList = [];
                        if (contactstate is displayContacts) {
                          contactList = contactstate.contactList;
                        }
                        return BlocBuilder<TransactionsBloc, TransactionsState>(
                          builder: (context, state) {
                            List<NestedSecondaryTransactionsDTO>
                                transactionList = [];
                            if (state is displayTransactions) {
                              if (state.partyAccount != null &&
                                  state.partyAccount!.secondaryTransaction !=
                                      null &&
                                  state.transactionList.isNotEmpty) {
                                for (var element in state.transactionList) {
                                  if (!widget.isExpense) {
                                    if (!element.isExpense) {
                                      transactionList.add(element);
                                    }
                                  } else {
                                    if (element.isExpense) {
                                      transactionList.add(element);
                                    }
                                  }
                                }
                                //  transactionList = state.transactionList;
                              }
                            }
                            final splitList = transactionList
                                .where((element) => element.isSplit == true)
                                .toList();
                            if (splitList.isNotEmpty) {
                              for (var e in splitList) {
                                splittedAmount = splittedAmount + e.givenAmt;
                              }
                            }
                            return ListView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: transactionList.length + 1,
                              itemBuilder: (context, index) {
                                bool isSecondaryParty = false;
                                log('length${transactionList.length}');
                                if (index == transactionList.length) {
                                  return SizedBox(
                                    height: size.height * 0.15,
                                  );
                                }
                                String transaction = '';
                                String toName = '';
                                String fromName = '';

                                if (transactionList[index].isPayed) {
                                  if (transactionList[index].toContactId ==
                                          'You' ||
                                      transactionList[index]
                                          .toContactId
                                          .isEmpty) {
                                    toName = 'You';
                                  } else {
                                    toName = contactList
                                        .firstWhere((element) =>
                                            element.contactId ==
                                            transactionList[index].toContactId)
                                        .displayName;
                                  }
                                  if (transactionList[index].fromContactId ==
                                          'You' ||
                                      transactionList[index]
                                          .fromContactId
                                          .isEmpty ||
                                      transactionList[index].fromContactId ==
                                          widget.contact.contactId) {
                                    fromName = 'You';
                                  } else {
                                    // log('${transactionList[index].fromContactId}');
                                    fromName = contactList
                                        .firstWhere((element) =>
                                            element.contactId ==
                                            transactionList[index]
                                                .fromContactId)
                                        .displayName;
                                  }
                                  transaction = '$fromName payed $toName';
                                } else {
                                  if (transactionList[index].fromContactId ==
                                      'You') {
                                    transaction =
                                        'You gave to ${widget.contact.displayName}';
                                    if (transactionList[index].isAddBalance) {
                                      transaction =
                                          'You Added Balance to ${widget.contact.displayName}';
                                    } else if (transactionList[index].isSplit) {
                                      toName = contactList
                                          .firstWhere((element) =>
                                              element.contactId ==
                                              transactionList[index]
                                                  .toContactId)
                                          .displayName;
                                      transaction =
                                          'You Splitted Balance to $toName';
                                    }
                                  } else {
                                    if (transactionList[index].toContactId ==
                                        'You') {
                                      transaction =
                                          'Received from ${widget.contact.displayName}';
                                    } else {
                                      final toNameContact =
                                          contactList.firstWhere(
                                              (element) =>
                                                  element.contactId ==
                                                  transactionList[index]
                                                      .toContactId,
                                              orElse: () => ContactsDTO(
                                                  ledgerId: '',
                                                  contactId: '',
                                                  displayName: '',
                                                  contactNumber: '',
                                                  avatar: null,
                                                  initails: ''));
                                      toName = toNameContact.displayName;

                                      transaction =
                                          //${widget.contact.displayName}
                                          'Received from $toName';
                                      //The game begins
                                      isSecondaryParty = true;
                                    }
                                  }
                                }

                                return EachTransactionContainer(
                                    isFromExpense: widget.isExpense,
                                    isGive: transactionList[index].isGive,
                                    toName: toName,
                                    contact: widget.contact,
                                    displayName: widget.contact.displayName,
                                    isPay: transactionList[index].isPayed,
                                    isGet: transactionList[index].isGet,
                                    primaryContactName:
                                        widget.contact.displayName,
                                    index: index,
                                    size: size,
                                    isSecondaryParty: isSecondaryParty,
                                    transactionList: transactionList,
                                    transaction: transaction);
                              },
                            );
                          },
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<void> captureAndDownloads(
  double height,
  double width,
) async {
  log('kkkkkkk');
  await ViewPartyScreens.screenshotController
      .capture()
      .then((Uint8List? image) async {
    log('kkkkkkk2');
    if (image != null) {
      // final imageFile = await uint8ListToFile(image);
      log('kkkkkkk3');
      await generateAndSavePdf(image, height, width);
    }
  });
}
