import 'package:linq_pe/domain/models/invoices/edit/options/discount/discount.dart';

class DiscountDTO {
  final String invoiceId;

  final String discountType;

  final double percentage;

  DiscountDTO({
    required this.invoiceId,
    required this.discountType,
    required this.percentage,
  });
}

DiscountDTO? convertDiscountModelToDTO(DiscountModel? discount) {
   if (discount == null) {
    return null;
  }
  return DiscountDTO(
      invoiceId: discount.invoiceId, 
      discountType: discount.discountType, percentage: discount.percentage);
}
