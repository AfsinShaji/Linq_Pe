import 'package:linq_pe/domain/models/invoices/edit/options/notes/notes.dart';

class NotesDTO {
  final String invoiceId;

  final String notes;

  NotesDTO({required this.invoiceId, required this.notes});
}

NotesDTO? convertNotesModelToDTO(NotesModel? notes) {
   if (notes == null) {
    return null;
  }
  return NotesDTO(invoiceId: notes.invoiceId, notes: notes.notes);
}
