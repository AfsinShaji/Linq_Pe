import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/payment_info/screen_payment_info.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

class AdditionalDetailsScreen extends StatelessWidget {
  AdditionalDetailsScreen({super.key});
  final TextEditingController notesTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.1,
        title: Text('Additional Details',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButtonWidget(notesTextController: notesTextController),
      body: SafeArea(child: BlocBuilder<InvoicesBloc, InvoicesState>(
        builder: (context, invoiceState) {
          InvoiceEditDTO? invoice;

          if (invoiceState is displayInvoice) {
            invoice = invoiceState.invoice;
          }
          if (invoice == null) {
            return const SizedBox();
          }
          return Column(
            children: [
              EditContainerWidget(
                  padding: 0,
                  child: DetailsTextFieldWidget(
                      initailValue:
                          invoice.notes == null ? '' : invoice.notes!.notes,
                      size: size,
                      maxLines: null,
                      hintText: 'Notes',
                      textController: notesTextController)),
              // EditContainerWidget(
              //   child: DetailSwitchRow(
              //     fieldName: 'Set as default',
              //     size: size,
              //   ),
              // )
            ],
          );
        },
      )),
    );
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget({
    super.key,
    required this.notesTextController,
  });

  final TextEditingController notesTextController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(onTap: () {
      if (notesTextController.text.isNotEmpty &&
          ref.watch(invoiceIdProvider).isNotEmpty) {
        BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.addNote(
            notes: notesTextController.text,
            invoiceId: ref.watch(invoiceIdProvider),
            isDefault: true));
        Navigator.pop(context);
      }
    });
  }
}
