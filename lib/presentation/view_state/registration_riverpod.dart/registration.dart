import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

//enum BusinessType { businessType, none1, none2, none3}

final businessTypeProvider = StateProvider(
  (ref) {
    return 'Business Type';
  },
);
addBusinessType(String businessType, WidgetRef ref) {
  ref.read(businessTypeProvider.notifier).state = businessType;
}
//enum PurposeType { purposeType, none1, none2, none3}

final purposeTypeProvider = StateProvider(
  (ref) {
    return 'Purpose';
  },
);
addPurposeType(String purposeType, WidgetRef ref) {
  ref.read(purposeTypeProvider.notifier).state = purposeType;
}

final fullNameProvider = StateProvider(
  (ref) {
    return '';
  },
);
addFullName(String name, WidgetRef ref) {
  ref.read(fullNameProvider.notifier).state = name;
}

final emailProvider = StateProvider(
  (ref) {
    return '';
  },
);
addEmail(String email, WidgetRef ref) {
  ref.read(emailProvider.notifier).state = email;
}

final phoneNumberProvider = StateProvider(
  (ref) {
    return '';
  },
);
addPhoneNumber(String phoneNumber, WidgetRef ref) {
  ref.read(phoneNumberProvider.notifier).state = phoneNumber;
}

final businessLogoProvider = StateProvider(
  (ref) {
    XFile? pickedImage;
    return pickedImage;
  },
);
addBusinessLogo(XFile? pickedImage, WidgetRef ref) {
  ref.read(businessLogoProvider.notifier).state = pickedImage;
}
