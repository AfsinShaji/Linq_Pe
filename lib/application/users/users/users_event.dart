part of 'users_bloc.dart';

@freezed
class UsersEvent with _$UsersEvent {
  // const factory UsersEvent.isUserAccountPermit() = isUserAccountPermit;
  const factory UsersEvent.addUserTrialDate() = addUserTrialDate;
  const factory UsersEvent.updateUsersBusiness(
      {required String userPusrpose,
      required String userBusinessName,
      required String userBusinessLogo,
      required String userBusinessType}) = updateUsersBusiness;
  const factory UsersEvent.getUserDetail() = getUserDetail;
  const factory UsersEvent.makeUsersPremium() = makeUsersPremium;
}
