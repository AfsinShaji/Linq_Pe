import 'dart:io';

import 'package:linq_pe/domain/models/invoices/edit/edit_model/edit_model.dart';

abstract class InvoicesRepository {
  Future<String> createInvoiceNumber({required String invoiceNumber});
  Future<void> addInvoiceNumber(
      {required String invoiceNumber,
      required String invoiceId,
      required DateTime date,
      required String terms,
      required DateTime? dueDate,
      required String? poNumber});
  Future<void> addBusinessDetails({
    required String businessName,
    required String invoiceId,
    required File? businessLogo,
    required String? businessOwnerName,
    required String? businessNumber,
    required String? addressLine1,
    required String? addressLine2,
    required String? addressLine3,
    required String? email,
    required String? phone,
    required String? mobile,
    required String? website,
  });
  Future<void> addPaymentInfo(
      {required String? additionalPaymentInstructions,
      required String? businessName,
      required String? paymentInstructions,
      required String? paypalEmail,
      required String invoiceId});
  Future<void> addPhoto(
      {required String invoiceId,
      required File photo,
      required String? description,
      required String? addionalDetails});
  Future<void> addPayments(
      {required String invoiceId,
      required double amount,
      required DateTime date,
      required String notes,
      required String paymentMethod});
  Future<void> addClient(
      {required String invoiceId,
      required String? phoneNumber,
      required String? mobileNumber,
      required String? faxNumber,
      required String clientName,
      required String? clientEmail,
      required String? address3,
      required String? address2,
      required String? address1});
  Future<void> addItems(
      {required String invoiceId,
      required double unitCost,
      required String? unit,
      required double? taxRate,
      required int quantity,
      required bool isTaxable,
      required double? discountAmount,
      required String description,
      required String? discount,
      required String? addiotionalDetails,
      required bool isAddToMyItems,
      required double total});
  Future<void> addTaxDetails(
      {required String invoiceId,
      required double? rate,
      required String label,
      required String taxType,
      required bool isInclusive});
  Future<void> addNotes(
      {required String notes,
      required String invoiceId,
      required bool isDefault});
  Future<void> addSignature(
      {required String invoiceId, required File signature});
  Future<void> addDiscountDetails(
      {required String invoiceId,
      required String discountType,
      required double percentage});
  InvoiceEditModel getInvoiceDetails({required String invoiceId});
  List<InvoiceEditModel> getAllInvoiceList();
  Future<void> editItems(
      {required String itemId,
      required String invoiceId,
      required double unitCost,
      required String? unit,
      required double? taxRate,
      required int quantity,
      required bool isTaxable,
      required double? discountAmount,
      required String description,
      required String? discount,
      required String? addiotionalDetails,
      required bool isAddToMyItems,
      required double total});
  Future<void> editPhoto(
      {required String photoId,
      required String invoiceId,
      required File photo,
      required String? description,
      required String? addionalDetails});
  Future<void> editPayments(
      {required String paymentId,
      required String invoiceId,
      required double amount,
      required DateTime date,
      required String notes,
      required String paymentMethod});
  Future<void> addReview(
      {required String invoiceId,
      required String reviewLink,
      required bool isToReview});

  Future<void> deleteItems({
    required String itemId,
    required String invoiceId,
  });
  Future<void> deletePayments({
    required String paymentId,
    required String invoiceId,
  });
  Future<void> deletePhoto({
    required String photoId,
    required String invoiceId,
  });
  Future<void> marKPaidOrUnPaid(
      {required String invoiceId,
      required String? paymentMethod,
      required bool isPaid});
  Future<void> addBalanceDue({
    required String invoiceId,
    required double balanceDue,
  });
  Future<String> createEstimateNumber({required String estimateNumber});
  Future<void> convertEstimateToInvoice({
    required String invoiceId,
  });
}
