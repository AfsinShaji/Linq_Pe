import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/all_invoices/all_invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/client_dto.dart';
import 'package:linq_pe/presentation/view_state/invoice/report/invoice_report.dart';
import 'package:linq_pe/utilities/colors.dart';

class PaidScreen extends ConsumerWidget {
  const PaidScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    return BlocBuilder<AllInvoicesBloc, AllInvoicesState>(
      builder: (context, invoicestate) {
        List<InvoiceEditDTO> invoiceList = [];
        List<ClientDTO> clientList = [];
        int totalClients = 0;
        double totalBalanceDue = 0.00;
        List<int> yearList = [];
        if (invoicestate is displayInvoiceList) {
          invoiceList = invoicestate.invoiceList
              .where((element) => element.isInvoice == true)
              .toList();
        }
        if (invoiceList.isNotEmpty) {
          for (var invoice in invoiceList) {
            if (invoice.invoiceNumberDetails.invoiceDate.year ==
                ref.watch(yearProvider)) {
              if (invoice.client != null) {
                if (clientList
                    .where((element) =>
                        element.clientName == invoice.client!.clientName &&
                        element.mobileNumber == invoice.client!.mobileNumber)
                    .isEmpty) {
                  totalClients = totalClients + 1;
                  clientList.add(invoice.client!);
                }
              }
              if (invoice.balanceDue != null && invoice.isPaid == true) {
                totalBalanceDue = totalBalanceDue + invoice.balanceDue!;
              }
            }

            if (!yearList
                .contains(invoice.invoiceNumberDetails.invoiceDate.year)) {
              yearList.add(invoice.invoiceNumberDetails.invoiceDate.year);
            }
          }
        }

        // WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        //   if (yearList.isNotEmpty) {
        //     addyear(yearList[0], ref);
        //   }
        // });
        return SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(size.width * 0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TableHeaders(size: size, width: size.width * 0.25),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.2,
                      headerText: 'Clients',
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.2,
                      headerText: 'Invoices',
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.25,
                      headerText: 'Paid',
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        TableHeaders(
                          size: size,
                          width: size.width * 0.25,
                          headerText: 'Tax Year',
                        ),
                        TableHeaders(
                          size: size,
                          width: size.width * 0.25,
                          headerText: '${ref.watch(yearProvider)}',
                        ),
                      ],
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.2,
                      headerText: '$totalClients',
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.2,
                      headerText:
                          '${invoiceList.where((element) => element.isPaid == true && element.invoiceNumberDetails.invoiceDate.year == ref.watch(yearProvider)).toList().length}',
                    ),
                    TableHeaders(
                      size: size,
                      width: size.width * 0.25,
                      headerText: '₹ $totalBalanceDue',
                    ),
                  ],
                ),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Dec',
                    monthNum: 12),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Nov',
                    monthNum: 11),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Oct',
                    monthNum: 10),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Sep',
                    monthNum: 9),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Aug',
                    monthNum: 8),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Jul',
                    monthNum: 7),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Jun',
                    monthNum: 6),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'May',
                    monthNum: 5),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Apr',
                    monthNum: 4),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Mar',
                    monthNum: 3),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Feb',
                    monthNum: 2),
                const Divider(),
                MonthlyRowWidget(
                    size: size,
                    invoiceList: invoiceList,
                    month: 'Jan',
                    monthNum: 1),
                const Divider(),
                // SizedBox(
                //   height: size.height * 0.005,
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                      yearList.length,
                      (index) => YearContainer(
                            yearList: yearList,
                            size: size,
                            index: index,
                          )),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class YearContainer extends ConsumerWidget {
  const YearContainer(
      {super.key,
      required this.yearList,
      required this.size,
      required this.index});

  final List<int> yearList;
  final Size size;
  final int index;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return InkWell(
      onTap: () {
        addyear(yearList[index], ref);
      },
      child: Container(
        padding: EdgeInsets.all(size.width * 0.02),
        margin: EdgeInsets.all(size.width * 0.01),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: ref.watch(yearProvider) == yearList[index]
                ? LinqPeColors.kPinkColor
                : LinqPeColors.kWhiteColor,
            border: Border.all(color: LinqPeColors.kPinkColor)),
        child: Text('${yearList[index]}',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .1,
                fontSize: size.width * 0.04,
                color: ref.watch(yearProvider) == yearList[index]
                    ? LinqPeColors.kWhiteColor
                    : LinqPeColors.kPinkColor,
                fontWeight: FontWeight.w400,
              ),
            )),
      ),
    );
  }
}

class MonthlyRowWidget extends ConsumerWidget {
  const MonthlyRowWidget(
      {super.key,
      required this.size,
      required this.invoiceList,
      required this.month,
      required this.monthNum});

  final Size size;
  final List<InvoiceEditDTO> invoiceList;
  final int monthNum;
  final String month;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    List<InvoiceEditDTO> monthlyNonDuplicateClientList = [];
    final monthlyClients = invoiceList
        .where((element) =>
            element.invoiceNumberDetails.invoiceDate.month == monthNum &&
            element.client != null &&
            element.invoiceNumberDetails.invoiceDate.year ==
                ref.watch(yearProvider))
        .toList();
    for (var client in monthlyClients) {
// if(monthlyNonDuplicateClientList.where((element) =>
//  element.client!.clientName==client.client!.clientName&&
//  element.client!.mobileNumber==client.client!.mobileNumber).isEmpty){
//   monthlyClients.add(client);

//  }
      bool isDuplicate = monthlyNonDuplicateClientList.any((element) =>
          element.client!.clientName == client.client!.clientName &&
          element.client!.mobileNumber == client.client!.mobileNumber);
      log('client:${client.client!.clientName}');
      if (!isDuplicate) {
        monthlyNonDuplicateClientList.add(client);
      }
    }
    return MonthlyRow(
      size: size,
      invoiceList: invoiceList
          .where((element) =>
              element.isPaid == true &&
              element.invoiceNumberDetails.invoiceDate.month == monthNum &&
              element.invoiceNumberDetails.invoiceDate.year ==
                  ref.watch(yearProvider))
          .toList(),
      month: month,
      numberOfInvoices: invoiceList
          .where((element) =>
              element.isPaid == true &&
              element.invoiceNumberDetails.invoiceDate.month == monthNum &&
              element.invoiceNumberDetails.invoiceDate.year ==
                  ref.watch(yearProvider))
          .toList()
          .length,
      numberofMonthClients: monthlyNonDuplicateClientList.length,
    );
  }
}

class MonthlyRow extends StatelessWidget {
  const MonthlyRow(
      {super.key,
      required this.size,
      required this.month,
      required this.numberOfInvoices,
      required this.invoiceList,
      required this.numberofMonthClients});

  final Size size;
  final String month;
  final int numberofMonthClients;
  final int numberOfInvoices;
  final List<InvoiceEditDTO> invoiceList;

  @override
  Widget build(BuildContext context) {
    double balanceDue = 0.00;
    for (var invoice in invoiceList) {
      if (invoice.balanceDue != null) {
        balanceDue = balanceDue + invoice.balanceDue!;
      }
    }
    return Row(
      children: [
        SizedBox(
          width: size.width * 0.25,
          child: Text(month,
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  letterSpacing: .1,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w400,
                ),
              )),
        ),
        SizedBox(
          width: size.width * 0.2,
          child: Text('$numberofMonthClients',
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  letterSpacing: .1,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w400,
                ),
              )),
        ),
        SizedBox(
          width: size.width * 0.2,
          child: Text('$numberOfInvoices',
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  letterSpacing: .1,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w400,
                ),
              )),
        ),
        SizedBox(
          width: size.width * 0.25,
          child: Text('₹ $balanceDue',
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  letterSpacing: .1,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w400,
                ),
              )),
        )
      ],
    );
  }
}

class TableHeaders extends StatelessWidget {
  const TableHeaders(
      {super.key, required this.size, required this.width, this.headerText});

  final Size size;
  final double width;
  final String? headerText;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: headerText == null
          ? null
          : Text(headerText!,
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  letterSpacing: .1,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
    );
  }
}
