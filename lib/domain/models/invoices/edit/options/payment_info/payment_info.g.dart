// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_info.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PaymentInfoModelAdapter extends TypeAdapter<PaymentInfoModel> {
  @override
  final int typeId = 23;

  @override
  PaymentInfoModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PaymentInfoModel(
      invoiceId: fields[0] as String,
      additionalPaymentInstructions: fields[4] as String?,
      businessName: fields[2] as String?,
      paymentInstructions: fields[3] as String?,
      paypalEmail: fields[1] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, PaymentInfoModel obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.invoiceId)
      ..writeByte(1)
      ..write(obj.paypalEmail)
      ..writeByte(2)
      ..write(obj.businessName)
      ..writeByte(3)
      ..write(obj.paymentInstructions)
      ..writeByte(4)
      ..write(obj.additionalPaymentInstructions);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PaymentInfoModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
