import 'package:hive/hive.dart';

part 'tax.g.dart';

@HiveType(typeId: 15)
class TaxModel {
  @HiveField(0)
  final String invoiceId;
  @HiveField(1)
  final String label;
  @HiveField(2)
  final double? rate;
  @HiveField(3)
  final String taxType;
  @HiveField(4)
  final bool isInclusive;
  TaxModel({
    required this.invoiceId,
    required this.taxType,
    required this.label,
    required this.rate,required this.isInclusive
  });
}

class TaxBox {
  static Box<TaxModel>? _getbox;
  static Box<TaxModel> getInstance() {
    return _getbox ??= Hive.box('TaxBox');
  }
}
