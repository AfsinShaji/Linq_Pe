import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/preview/screen_preview.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/preview/templates/style.dart/screen_style.dart';
import 'package:linq_pe/presentation/view_state/invoice/preview.dart/preview.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/utilities/colors.dart';

class InvoiceTemplateScreen extends StatefulWidget {
  const InvoiceTemplateScreen({super.key});

  @override
  State<InvoiceTemplateScreen> createState() => _InvoiceTemplateScreenState();
}

class _InvoiceTemplateScreenState extends State<InvoiceTemplateScreen>
    with TickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: LinqPeColors.kWhiteColor,
              )),
          titleSpacing: size.width * 0.19,
          title: Text('Template',
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  letterSpacing: .8,
                  fontSize: size.width * 0.06,
                  color: LinqPeColors.kWhiteColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
          backgroundColor: LinqPeColors.kPinkColor,
          shadowColor: LinqPeColors.kBlackColor,
          bottom: TabBar(
            indicatorSize: TabBarIndicatorSize.tab,
            labelColor: LinqPeColors.kWhiteColor,
            indicatorColor: LinqPeColors.kWhiteColor,
            overlayColor: MaterialStatePropertyAll(
                LinqPeColors.kWhiteColor.withOpacity(0.05)),
            unselectedLabelColor: LinqPeColors.kBlackColor.withOpacity(0.7),
            controller: tabController,
            tabs: const [
              Tab(
                text: "DESIGN",
              ),
              Tab(
                text: "STYLE",
              ),
              // Tab(
              //   text: "HISTORY",
              // )
            ],
          )),
      body: TabBarView(
        controller: tabController,
        children: const [
          Center(
            child: AllTemplatesScreen(),
          ),
          Center(
            child: StyleScreen(),
          ),

          // Center(
          //   child: Text("Settings"),
          // ),
        ],
      ),
    );
  }
}

class AllTemplatesScreen extends StatelessWidget {
  const AllTemplatesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: BlocBuilder<InvoicesBloc, InvoicesState>(
          builder: (context, invoicestate) {
            InvoiceEditDTO? invoiceEditDTO;
            if (invoicestate is displayInvoice) {
              invoiceEditDTO = invoicestate.invoice;
            }
            final Size size = MediaQuery.of(context).size;
            return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      DesignInvoiceWidget(
                          invoiceEditDTO: invoiceEditDTO,
                          title: 'CLASSIC',
                          size: size,
                          design: DesignType.classic),
                      DesignInvoiceWidget(
                          invoiceEditDTO: invoiceEditDTO,
                          title: 'COMPACT',
                          size: size,
                          design: DesignType.compact),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      DesignInvoiceWidget(
                          invoiceEditDTO: invoiceEditDTO,
                          title: 'CLEAN',
                          size: size,
                          design: DesignType.clean),
                      DesignInvoiceWidget(
                          invoiceEditDTO: invoiceEditDTO,
                          title: 'SHARP',
                          size: size,
                          design: DesignType.sharp),
                    ],
                  ),
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      DesignInvoiceWidget(
                          invoiceEditDTO: invoiceEditDTO,
                          title: 'POS',
                          size: size,
                          design: DesignType.pos),
                    ],
                  )
                ]);
          },
        ),
      ),
    );
  }
}

class DesignInvoiceWidget extends ConsumerWidget {
  const DesignInvoiceWidget(
      {super.key,
      required this.invoiceEditDTO,
      required this.size,
      required this.title,
      required this.design});

  final InvoiceEditDTO? invoiceEditDTO;
  final Size size;
  final DesignType design;
  final String title;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    creatingpdf(
      invoiceEditDTO!,
      size,
      design,
      ref.watch(previewColorProvider),
    );
    log('design${design.toString()}');
    return FutureBuilder(
        key: Key('$design '),
        future: creatingpdf(
          invoiceEditDTO!,
          size,
          design,
          ref.watch(previewColorProvider),
        ),
        builder: (context, filesnap) {
          if (filesnap.hasData) {
            log('data${filesnap.data.toString()}');
            return InkWell(
              onTap: () {
                log('tapped');
                addpreviewType(design, ref);
              },
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: ref.watch(previewTypeProvider) == design
                            ? Border.all(
                                color: LinqPeColors.kPinkColor,
                                width: size.width * 0.01)
                            : null),
                    width: size.width * 0.45,
                    height: size.height * 0.3,
                    child: Padding(
                      padding: EdgeInsets.all(size.width * 0),
                      child: SfPdfViewer.file(
                        filesnap.data!,
                        key: Key('$design'),
                        onTap: (val) {
                          log('tapped');
                          addpreviewType(design, ref);
                        },
                      ),
                    ),
                  ),
                  Text(title,
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          letterSpacing: .8,
                          fontSize: size.width * 0.045,
                          color: LinqPeColors.kBlackColor,
                          fontWeight: FontWeight.w500,
                        ),
                      ))
                ],
              ),
            );
          } else {
            return const SizedBox();
          }
        });
  }
}

//xghjhghjshsghghjsfsdjhjdhjgjgfgjkd
//gfhfhfgjgjhgkmhkk