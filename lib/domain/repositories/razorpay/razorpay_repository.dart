import 'package:razorpay_flutter/razorpay_flutter.dart';

abstract class RazorPayRepository {
  Future<String> getRazorPayKey();
  Future<void> razorpayPayment({required Razorpay razorpay});
}
