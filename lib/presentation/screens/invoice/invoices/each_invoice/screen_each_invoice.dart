import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/screen_edit.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/preview/preview.dart';
import 'package:linq_pe/utilities/colors.dart';

class EachInvoiceScreen extends StatefulWidget {
  const EachInvoiceScreen({
    super.key,
    required this.isEstimate,
  });
  final bool isEstimate;

  @override
  State<EachInvoiceScreen> createState() => _EachInvoiceScreenState();
}

class _EachInvoiceScreenState extends State<EachInvoiceScreen>
    with TickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: LinqPeColors.kWhiteColor,
              )),
          titleSpacing: size.width * 0.25,
          title: Text('Invoice',
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  letterSpacing: .8,
                  fontSize: size.width * 0.06,
                  color: LinqPeColors.kWhiteColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
          backgroundColor: LinqPeColors.kPinkColor,
          shadowColor: LinqPeColors.kBlackColor,
          bottom: TabBar(
            indicatorSize: TabBarIndicatorSize.tab,
            labelColor: LinqPeColors.kWhiteColor,
            indicatorColor: LinqPeColors.kWhiteColor,
            overlayColor: MaterialStatePropertyAll(
                LinqPeColors.kWhiteColor.withOpacity(0.05)),
            unselectedLabelColor: LinqPeColors.kBlackColor.withOpacity(0.7),
            controller: tabController,
            tabs: const [
              Tab(
                text: "EDIT",
              ),
              Tab(
                text: "PREVIEW",
              ),
              // Tab(
              //   text: "HISTORY",
              // )
            ],
          )),
      body: TabBarView(
        controller: tabController,
        children: [
          Center(
            child: EditScreen(isEstimate: widget.isEstimate),
          ),
          const Center(
            child: PreviewScreen(),
          ),

          // Center(
          //   child: Text("Settings"),
          // ),
        ],
      ),
    );
  }
}
