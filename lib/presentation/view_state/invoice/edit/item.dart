import 'dart:developer';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/discount.dart';

final unitCostProvider = StateProvider(
  (ref) {
    return '0.0';
  },
);
addunitCost(String type, WidgetRef ref) {
  log('addUnit');
  ref.read(unitCostProvider.notifier).state = type;
  calculateTotal(ref);
}

final quantityProvider = StateProvider(
  (ref) {
    return '0.0';
  },
);
addquantity(String type, WidgetRef ref) {
  ref.read(quantityProvider.notifier).state = type;
  calculateTotal(ref);
}

final discountProvider = StateProvider(
  (ref) {
    return '0.0';
  },
);
adddiscount(String type, WidgetRef ref) {
  ref.read(discountProvider.notifier).state = type;
  calculateTotal(ref);
}

final totalProvider = StateProvider(
  (ref) {
    return '0.0';
  },
);
addtotal(String type, WidgetRef ref) {
  ref.read(totalProvider.notifier).state = type;
}

calculateTotal(WidgetRef ref) {
  double total = 0.0;
  final unitCost = double.parse(ref.watch(unitCostProvider).isNotEmpty
      ? ref.watch(unitCostProvider)
      : '0.0');
  final quantity = double.parse(ref.watch(quantityProvider).isNotEmpty
      ? ref.watch(quantityProvider)
      : '0.0');

  final discount = double.parse(ref.watch(discountProvider).isNotEmpty
      ? ref.watch(discountProvider)
      : '0.0');
  total = unitCost * quantity;
  if (ref.watch(discountTypeProvider) == 'Percentage') {
    total = total - (total * (discount / 100));
  } else if (ref.watch(discountTypeProvider) == 'Flat Amount') {
    total = total - discount;
  }
  addtotal(total.toString(), ref);
}

final isTaxableProvider = StateProvider(
  (ref) {
    return false;
  },
);
addisTaxable(bool type, WidgetRef ref) {
  ref.read(isTaxableProvider.notifier).state = type;
}

final myItemsProvider = StateProvider(
  (ref) {
    return false;
  },
);
addmyItems(bool type, WidgetRef ref) {
  ref.read(myItemsProvider.notifier).state = type;
}
