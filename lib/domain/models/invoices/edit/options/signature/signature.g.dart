// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signature.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SignatureModelAdapter extends TypeAdapter<SignatureModel> {
  @override
  final int typeId = 17;

  @override
  SignatureModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SignatureModel(
      invoiceId: fields[0] as String,
      signature: fields[1] as Uint8List?,
      signDate: fields[2] as DateTime?,
    );
  }

  @override
  void write(BinaryWriter writer, SignatureModel obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.invoiceId)
      ..writeByte(1)
      ..write(obj.signature)
      ..writeByte(2)
      ..write(obj.signDate);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SignatureModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
