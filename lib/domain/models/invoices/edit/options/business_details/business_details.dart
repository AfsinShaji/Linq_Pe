import 'dart:typed_data';

import 'package:hive/hive.dart';



part 'business_details.g.dart';

@HiveType(typeId: 13)
class BusinessDetailsModel {
  @HiveField(0)
  final String? businessOwnerName;
  @HiveField(1)
  final String businessName;
  @HiveField(2)
  final String? businessNumber;
  @HiveField(3)
  final String? addressLine1;
  @HiveField(4)
  final String? addressLine2;
  @HiveField(5)
  final String? addressLine3;
  @HiveField(6)
  final String? email;
  @HiveField(7)
  final String? website;
  @HiveField(8)
  final String? mobile;
  @HiveField(9)
  final String? phone;
  @HiveField(10)
  final String invoiceId;
  @HiveField(11)
  final Uint8List? businessLogo;

  BusinessDetailsModel( {required this.businessLogo,
    required this.addressLine1,
    required this.addressLine2,
    required this.addressLine3,
    required this.businessName,
    required this.businessNumber,
   required this.businessOwnerName,
   required this.email,
   required this.mobile,
   required this.phone,
   required this.website,
   required this.invoiceId,
  });
}

class BusinessDetailsBox {
  static Box<BusinessDetailsModel>? _getbox;
  static Box<BusinessDetailsModel> getInstance() {
    return _getbox ??= Hive.box('BusinessDetailsBox');
  }
}
