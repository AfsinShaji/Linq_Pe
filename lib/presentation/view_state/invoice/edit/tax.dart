import 'package:flutter_riverpod/flutter_riverpod.dart';

final taxTypeProvider = StateProvider(
  (ref) {
    return 'None';
  },
);
addtaxType(String type, WidgetRef ref) {

  ref.read(taxTypeProvider.notifier).state = type;
}
final labelProvider = StateProvider(
  (ref) {
    return 'None';
  },
);
addlabel(String type, WidgetRef ref) {

  ref.read(labelProvider.notifier).state = type;
}
final rateProvider = StateProvider(
  (ref) {
    return 'None';
  },
);
addrate(String type, WidgetRef ref) {

  ref.read(rateProvider.notifier).state = type;
}

final inclusiveProvider = StateProvider(
  (ref) {
    return false;
  },
);
addinclusive(bool type, WidgetRef ref) {
  ref.read(inclusiveProvider.notifier).state = type;
}