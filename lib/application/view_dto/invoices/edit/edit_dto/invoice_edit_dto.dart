import 'package:linq_pe/application/view_dto/invoices/edit/options/business_details_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/client_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/discount_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/invoice_number_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/item_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/notes_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/payment_info_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/payments_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/photo_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/signature_dto.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/tax_dto.dart';
import 'package:linq_pe/domain/models/invoices/edit/edit_model/edit_model.dart';

class InvoiceEditDTO {
  final String invoiceId;
  final InvoiceNumberDTO invoiceNumberDetails;
  final BusinessDetailsDTO? businessDetails;
  final DiscountDTO? discountDetails;
  final List<ItemDTO>? itemsList;
  final NotesDTO? notes;
  final PaymentInfoDTO? paymentInfo;
  final List<PaymentsDTO>? paymentsList;
  final List<PhotoDTO>? photoList;
  final SignatureDTO? signature;

  final TaxDTO? tax;

  final ClientDTO? client;
  final String? reviewLink;
  final bool? isToReview;
  final bool isPaid;
  final String? paidMethod;
   final double? balanceDue;
final bool isInvoice;
 
  final bool isEstimate;
  InvoiceEditDTO({
    required this.invoiceId,
    required this.businessDetails,
    required this.discountDetails,
    required this.notes,
    required this.invoiceNumberDetails,
    required this.itemsList,
    required this.paymentInfo,
    required this.paymentsList,
    required this.photoList,
    required this.signature,
    required this.tax,
    required this.client,
    required this.isToReview,
    required this.reviewLink,
    required this.isPaid,
    required this.paidMethod,required this.balanceDue, required this.isEstimate,
        required this.isInvoice,
  });
}

Future<InvoiceEditDTO> convertInvoiceEditModelToDTO(
    InvoiceEditModel invoice) async {
  return InvoiceEditDTO(
    isEstimate: invoice.isEstimate,
    isInvoice: invoice.isInvoice,
    balanceDue:invoice.balanceDue ,
    isPaid: invoice.isPaid ,
    paidMethod:  invoice.paidMethod,
      isToReview: invoice.isToReview,
      reviewLink: invoice.reviewLink,
      invoiceId: invoice.invoiceId,
      businessDetails:
          await convertBusinessDetailsModelToDTO(invoice.businessDetails),
      discountDetails: convertDiscountModelToDTO(invoice.discountDetails),
      notes: convertNotesModelToDTO(invoice.notes),
      invoiceNumberDetails:
          convertInvoiceNumberModelToDTO(invoice.invoiceNumberDetails)!,
      itemsList: convertItemModelToDTO(invoice.itemsList),
      paymentInfo: convertPaymentInfoModelToDTO(invoice.paymentInfo),
      paymentsList: convertPaymentsModelToDTO(invoice.paymentsList),
      photoList: await convertPhotosModelToDTO(invoice.photoList),
      signature: await convertSignatureModelToDTO(invoice.signature),
      tax: convertTaxModelToDTO(invoice.tax),
      client: convertClientModelToDTO(invoice.client));
}
