import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/preview/screen_preview.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/preview/templates/all_templates_screen.dart';
import 'package:linq_pe/presentation/view_state/invoice/preview.dart/preview.dart';
import 'package:linq_pe/utilities/colors.dart';
import 'package:share_plus/share_plus.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PreviewScreen extends ConsumerWidget {
  const PreviewScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return BlocBuilder<InvoicesBloc, InvoicesState>(
      builder: (context, invoicestate) {
        InvoiceEditDTO? invoiceEditDTO;
        if (invoicestate is displayInvoice) {
          invoiceEditDTO = invoicestate.invoice;
        }
        final Size size = MediaQuery.of(context).size;
        return Scaffold(
          bottomSheet: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => const InvoiceTemplateScreen(),
                  ));
            },
            child: Container(
              color: LinqPeColors.kPinkColor,
              height: size.height * 0.06,
              width: size.width,
              child: Center(
                child: Text('Template',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        letterSpacing: .5,
                        fontSize: size.width * 0.045,
                        color: LinqPeColors.kWhiteColor,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
              ),
            ),
          ),
          floatingActionButton: FutureBuilder(
              future: creatingpdf(
                invoiceEditDTO!,
                size,
                ref.watch(previewTypeProvider),
                ref.watch(previewColorProvider),
              ),
              builder: (context, filesnap) {
                return FloatingActionButton(
                  backgroundColor: Colors.white,
                  onPressed: () {
                    if (filesnap.hasData) {
                      Share.shareXFiles([XFile(filesnap.data!.path)],
                          text: invoiceEditDTO!
                              .invoiceNumberDetails.invoiceNumber,
                          subject: 'Invoice',
                          sharePositionOrigin:
                              Rect.fromPoints(Offset.zero, Offset.zero));
                    }
                  },
                  child: Image.asset(
                    'assets/images/Untitled design.png',
                    width: size.width * 0.1,
                  ),
                );
              }),
          body: FutureBuilder(
            key:const Key('Template preview'),
              future: creatingpdf(
                invoiceEditDTO,
                size,
                ref.watch(previewTypeProvider),
                ref.watch(previewColorProvider),
              ),
              builder: (context, filesnap) {
                if (filesnap.hasData) {
                  return Padding(
                    padding: EdgeInsets.all(size.width * 0),
                    child: SfPdfViewer.file(filesnap.data!,  key:const Key('Template previews'),),
                  );
                } else {
                  return const SizedBox();
                }
              }),
        );
      },
    );
  }
}
