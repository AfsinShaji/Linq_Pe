import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/tax.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

class TaxScreen extends ConsumerWidget {
  TaxScreen({super.key});
  final TextEditingController labelTextController = TextEditingController();
  final TextEditingController rateTextController = TextEditingController();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Size size = MediaQuery.of(context).size;
    final taxType = ref.watch(taxTypeProvider);
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.3,
        title: Text('Tax',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButton(onTap: () {
        bool add = false;
        String? label;
        double? rate;
        if (ref.watch(taxTypeProvider) == "On The Total" ||
            ref.watch(taxTypeProvider) == "Deducted") {
          if (labelTextController.text.isNotEmpty &&
              rateTextController.text.isNotEmpty) {
            add = true;
            label = labelTextController.text;
            rate = double.parse(rateTextController.text);
          }
        } else if (ref.watch(taxTypeProvider) == "Per Item") {
          if (labelTextController.text.isNotEmpty) {
            add = true;
            label = labelTextController.text;
          }
        } else if (ref.watch(taxTypeProvider) == "None") {
          add = true;
        }

        if (add && ref.watch(invoiceIdProvider).isNotEmpty) {
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.addTaxDetail(
            isInclusive: ref.watch(inclusiveProvider),
              invoiceId: ref.watch(invoiceIdProvider),
              rate: rate,
              label: label ?? '',
              taxType: ref.watch(taxTypeProvider)));
          Navigator.pop(context);
        }
      }),
      body: SafeArea(
          child: Container(
        padding: EdgeInsets.all(size.width * 0.01),
        width: size.width,
        //  height: size.height * 0.17,
        child: BlocBuilder<InvoicesBloc, InvoicesState>(
          builder: (context, invoiceState) {
            InvoiceEditDTO? invoice;

            if (invoiceState is displayInvoice) {
              invoice = invoiceState.invoice;
            }
            if (invoice == null) {
              return const SizedBox();
            }
            return Column(
              children: [
                EditContainerWidget(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      TaxRow(size: size, ref: ref),
                      SizedBox(
                        height: size.height * 0.005,
                      ),
                      taxType == 'None'
                          ? const SizedBox()
                          : TaxAndFieldRow(
                              initialValue:
                                  invoice.tax == null ? '' : invoice.tax!.label,
                              isNumberField: false,
                              fieldName: 'Label:',
                              size: size,
                              textController: labelTextController,
                            ),
                      // SizedBox(
                      //   height: size.height * 0.005,
                      // ),
                      taxType == 'Per Item' || taxType == 'None'
                          ? const SizedBox()
                          : TaxAndFieldRow(
                              initialValue: invoice.tax == null
                                  ? ''
                                  : invoice.tax!.rate == null
                                      ? ''
                                      : invoice.tax!.rate.toString(),
                              isNumberField: true,
                              fieldName: 'Rate:',
                              size: size,
                              textController: rateTextController,
                            ),
                    ],
                  ),
                ),
                taxType == 'None'|| taxType =='Deducted'
                    ? const SizedBox()
                    : EditContainerWidget(
                        child: Column(
                          children: [
                            InclusiveSwitchRow(
                              fieldName: 'Inclusive',
                              size: size,
                            ),
                            SizedBox(
                              height: size.height * 0.005,
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child:
                                  Text('Turn on if prices already include Tax',
                                      style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                          letterSpacing: .5,
                                          fontSize: size.width * 0.035,
                                          color: LinqPeColors.kBlackColor
                                              .withOpacity(0.3),
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )),
                            ),
                          ],
                        ),
                      )
              ],
            );
          },
        ),
      )),
    );
  }
}

class TaxRow extends StatelessWidget {
  const TaxRow({
    super.key,
    required this.size,
    required this.ref,
  });

  final Size size;
  final WidgetRef ref;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        openTaxBottomSheet(context, size, ref);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('Tax:',
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
          Text(ref.watch(taxTypeProvider),
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w500,
                ),
              )),
        ],
      ),
    );
  }
}

class TaxAndFieldRow extends StatefulWidget {
  const TaxAndFieldRow({
    super.key,
    required this.size,
    required this.textController,
    required this.fieldName,
    required this.isNumberField,
    required this.initialValue,
  });

  final Size size;
  final TextEditingController textController;
  final String fieldName;
  final bool isNumberField;
  final String initialValue;

  @override
  State<TaxAndFieldRow> createState() => _TaxAndFieldRowState();
}

class _TaxAndFieldRowState extends State<TaxAndFieldRow> {
  @override
  void initState() {
    if (widget.initialValue.isNotEmpty) {
      widget.textController.text = widget.initialValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: widget.size.height * 0.04,
          child: Center(
            child: Text(widget.fieldName,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: widget.size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w500,
                  ),
                )),
          ),
        ),
        SizedBox(
          width: widget.size.width * 0.5,
          height: widget.size.height * 0.04,
          child: Align(
            alignment: Alignment.topCenter,
            child: TextField(
              textAlign: TextAlign.right, // Align text to the right
              textDirection: TextDirection.ltr,
              decoration: const InputDecoration(border: InputBorder.none),
              controller: widget.textController,
              keyboardType: widget.isNumberField ? TextInputType.number : null,
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  letterSpacing: .5,
                  fontSize: widget.size.width * 0.04,
                  color: LinqPeColors.kBlackColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}

openTaxBottomSheet(BuildContext context, Size size, WidgetRef ref) {
  showModalBottomSheet(
    backgroundColor: LinqPeColors.kWhiteColor,
    context: context,
    builder: (BuildContext context) {
      return Container(
        width: size.width,
        decoration: const BoxDecoration(
            color: LinqPeColors.kWhiteColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50))),
        height: size.height * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () {
                addtaxType('None', ref);
                Navigator.pop(context);
              },
              child: Text('None',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            InkWell(
              onTap: () {
                addtaxType('On The Total', ref);
                Navigator.pop(context);
              },
              child: Text('On The Total',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            InkWell(
              onTap: () {
                addtaxType('Deducted', ref);
                Navigator.pop(context);
              },
              child: Text('Deducted',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
            Divider(
              color: LinqPeColors.kBlackColor,
              height: size.height * 0.001,
              thickness: size.height * 0.0001,
            ),
            InkWell(
              onTap: () {
                addtaxType('Per Item', ref);
                Navigator.pop(context);
              },
              child: Text('Per Item',
                  style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      letterSpacing: .5,
                      fontSize: size.width * 0.04,
                      color: LinqPeColors.kBlackColor,
                      fontWeight: FontWeight.w500,
                    ),
                  )),
            ),
          ],
        ),
      );
    },
  );
}

class InclusiveSwitchRow extends ConsumerWidget {
  const InclusiveSwitchRow({
    super.key,
    required this.size,
    required this.fieldName,
  });

  final Size size;
  final String fieldName;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: size.height * 0.03,
          child: Center(
            child: Text(fieldName,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: size.width * 0.04,
                    color: LinqPeColors.kBlackColor,
                    fontWeight: FontWeight.w500,
                  ),
                )),
          ),
        ),
        SizedBox(
          height: size.height * 0.045,
          child: FittedBox(
            fit: BoxFit.fill,
            child: Switch(
                value: ref.watch(inclusiveProvider),
                activeColor: LinqPeColors.kBlueColor,
                onChanged: (value) {
                  addinclusive(value, ref);
                }),
          ),
        )
      ],
    );
  }
}