import 'package:linq_pe/domain/models/invoices/edit/options/payments/payments.dart';

class PaymentsDTO {
  final String invoiceId;

  final String paymentMethod;

  final double amount;

  final DateTime date;

  final String notes;
  final String paymentId;

  PaymentsDTO(
      {required this.invoiceId,
      required this.amount,
      required this.date,
      required this.notes,
      required this.paymentMethod, required this.paymentId});
}

List<PaymentsDTO>? convertPaymentsModelToDTO(List<PaymentsModel>? payments) {
  if (payments == null) {
    return null;
  }
  List<PaymentsDTO> paymentList = [];
  for (var payment in payments) {
    paymentList.add(PaymentsDTO(
      paymentId: payment.paymentId,
        invoiceId: payment.invoiceId,
        amount: payment.amount,
        date: payment.date,
        notes: payment.notes,
        paymentMethod: payment.paymentMethod));
  }
  return paymentList;
}
