// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'razorpay_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RazorpayEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getRazorpayKey,
    required TResult Function(Razorpay razorpay) razorpayPayments,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getRazorpayKey,
    TResult? Function(Razorpay razorpay)? razorpayPayments,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getRazorpayKey,
    TResult Function(Razorpay razorpay)? razorpayPayments,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getRazorpayKey value) getRazorpayKey,
    required TResult Function(razorpayPayments value) razorpayPayments,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getRazorpayKey value)? getRazorpayKey,
    TResult? Function(razorpayPayments value)? razorpayPayments,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getRazorpayKey value)? getRazorpayKey,
    TResult Function(razorpayPayments value)? razorpayPayments,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RazorpayEventCopyWith<$Res> {
  factory $RazorpayEventCopyWith(
          RazorpayEvent value, $Res Function(RazorpayEvent) then) =
      _$RazorpayEventCopyWithImpl<$Res, RazorpayEvent>;
}

/// @nodoc
class _$RazorpayEventCopyWithImpl<$Res, $Val extends RazorpayEvent>
    implements $RazorpayEventCopyWith<$Res> {
  _$RazorpayEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$getRazorpayKeyImplCopyWith<$Res> {
  factory _$$getRazorpayKeyImplCopyWith(_$getRazorpayKeyImpl value,
          $Res Function(_$getRazorpayKeyImpl) then) =
      __$$getRazorpayKeyImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$getRazorpayKeyImplCopyWithImpl<$Res>
    extends _$RazorpayEventCopyWithImpl<$Res, _$getRazorpayKeyImpl>
    implements _$$getRazorpayKeyImplCopyWith<$Res> {
  __$$getRazorpayKeyImplCopyWithImpl(
      _$getRazorpayKeyImpl _value, $Res Function(_$getRazorpayKeyImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$getRazorpayKeyImpl implements getRazorpayKey {
  const _$getRazorpayKeyImpl();

  @override
  String toString() {
    return 'RazorpayEvent.getRazorpayKey()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$getRazorpayKeyImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getRazorpayKey,
    required TResult Function(Razorpay razorpay) razorpayPayments,
  }) {
    return getRazorpayKey();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getRazorpayKey,
    TResult? Function(Razorpay razorpay)? razorpayPayments,
  }) {
    return getRazorpayKey?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getRazorpayKey,
    TResult Function(Razorpay razorpay)? razorpayPayments,
    required TResult orElse(),
  }) {
    if (getRazorpayKey != null) {
      return getRazorpayKey();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getRazorpayKey value) getRazorpayKey,
    required TResult Function(razorpayPayments value) razorpayPayments,
  }) {
    return getRazorpayKey(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getRazorpayKey value)? getRazorpayKey,
    TResult? Function(razorpayPayments value)? razorpayPayments,
  }) {
    return getRazorpayKey?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getRazorpayKey value)? getRazorpayKey,
    TResult Function(razorpayPayments value)? razorpayPayments,
    required TResult orElse(),
  }) {
    if (getRazorpayKey != null) {
      return getRazorpayKey(this);
    }
    return orElse();
  }
}

abstract class getRazorpayKey implements RazorpayEvent {
  const factory getRazorpayKey() = _$getRazorpayKeyImpl;
}

/// @nodoc
abstract class _$$razorpayPaymentsImplCopyWith<$Res> {
  factory _$$razorpayPaymentsImplCopyWith(_$razorpayPaymentsImpl value,
          $Res Function(_$razorpayPaymentsImpl) then) =
      __$$razorpayPaymentsImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Razorpay razorpay});
}

/// @nodoc
class __$$razorpayPaymentsImplCopyWithImpl<$Res>
    extends _$RazorpayEventCopyWithImpl<$Res, _$razorpayPaymentsImpl>
    implements _$$razorpayPaymentsImplCopyWith<$Res> {
  __$$razorpayPaymentsImplCopyWithImpl(_$razorpayPaymentsImpl _value,
      $Res Function(_$razorpayPaymentsImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? razorpay = null,
  }) {
    return _then(_$razorpayPaymentsImpl(
      razorpay: null == razorpay
          ? _value.razorpay
          : razorpay // ignore: cast_nullable_to_non_nullable
              as Razorpay,
    ));
  }
}

/// @nodoc

class _$razorpayPaymentsImpl implements razorpayPayments {
  const _$razorpayPaymentsImpl({required this.razorpay});

  @override
  final Razorpay razorpay;

  @override
  String toString() {
    return 'RazorpayEvent.razorpayPayments(razorpay: $razorpay)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$razorpayPaymentsImpl &&
            (identical(other.razorpay, razorpay) ||
                other.razorpay == razorpay));
  }

  @override
  int get hashCode => Object.hash(runtimeType, razorpay);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$razorpayPaymentsImplCopyWith<_$razorpayPaymentsImpl> get copyWith =>
      __$$razorpayPaymentsImplCopyWithImpl<_$razorpayPaymentsImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getRazorpayKey,
    required TResult Function(Razorpay razorpay) razorpayPayments,
  }) {
    return razorpayPayments(razorpay);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getRazorpayKey,
    TResult? Function(Razorpay razorpay)? razorpayPayments,
  }) {
    return razorpayPayments?.call(razorpay);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getRazorpayKey,
    TResult Function(Razorpay razorpay)? razorpayPayments,
    required TResult orElse(),
  }) {
    if (razorpayPayments != null) {
      return razorpayPayments(razorpay);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getRazorpayKey value) getRazorpayKey,
    required TResult Function(razorpayPayments value) razorpayPayments,
  }) {
    return razorpayPayments(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getRazorpayKey value)? getRazorpayKey,
    TResult? Function(razorpayPayments value)? razorpayPayments,
  }) {
    return razorpayPayments?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getRazorpayKey value)? getRazorpayKey,
    TResult Function(razorpayPayments value)? razorpayPayments,
    required TResult orElse(),
  }) {
    if (razorpayPayments != null) {
      return razorpayPayments(this);
    }
    return orElse();
  }
}

abstract class razorpayPayments implements RazorpayEvent {
  const factory razorpayPayments({required final Razorpay razorpay}) =
      _$razorpayPaymentsImpl;

  Razorpay get razorpay;
  @JsonKey(ignore: true)
  _$$razorpayPaymentsImplCopyWith<_$razorpayPaymentsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$RazorpayState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() razorpayInitial,
    required TResult Function(String razorpayKey) displayRazorpayKey,
    required TResult Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)
        paymentResult,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? razorpayInitial,
    TResult? Function(String razorpayKey)? displayRazorpayKey,
    TResult? Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)?
        paymentResult,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? razorpayInitial,
    TResult Function(String razorpayKey)? displayRazorpayKey,
    TResult Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)?
        paymentResult,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(razorpayInitial value) razorpayInitial,
    required TResult Function(displayRazorpayKey value) displayRazorpayKey,
    required TResult Function(paymentResult value) paymentResult,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(razorpayInitial value)? razorpayInitial,
    TResult? Function(displayRazorpayKey value)? displayRazorpayKey,
    TResult? Function(paymentResult value)? paymentResult,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(razorpayInitial value)? razorpayInitial,
    TResult Function(displayRazorpayKey value)? displayRazorpayKey,
    TResult Function(paymentResult value)? paymentResult,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RazorpayStateCopyWith<$Res> {
  factory $RazorpayStateCopyWith(
          RazorpayState value, $Res Function(RazorpayState) then) =
      _$RazorpayStateCopyWithImpl<$Res, RazorpayState>;
}

/// @nodoc
class _$RazorpayStateCopyWithImpl<$Res, $Val extends RazorpayState>
    implements $RazorpayStateCopyWith<$Res> {
  _$RazorpayStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$razorpayInitialImplCopyWith<$Res> {
  factory _$$razorpayInitialImplCopyWith(_$razorpayInitialImpl value,
          $Res Function(_$razorpayInitialImpl) then) =
      __$$razorpayInitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$razorpayInitialImplCopyWithImpl<$Res>
    extends _$RazorpayStateCopyWithImpl<$Res, _$razorpayInitialImpl>
    implements _$$razorpayInitialImplCopyWith<$Res> {
  __$$razorpayInitialImplCopyWithImpl(
      _$razorpayInitialImpl _value, $Res Function(_$razorpayInitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$razorpayInitialImpl implements razorpayInitial {
  const _$razorpayInitialImpl();

  @override
  String toString() {
    return 'RazorpayState.razorpayInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$razorpayInitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() razorpayInitial,
    required TResult Function(String razorpayKey) displayRazorpayKey,
    required TResult Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)
        paymentResult,
  }) {
    return razorpayInitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? razorpayInitial,
    TResult? Function(String razorpayKey)? displayRazorpayKey,
    TResult? Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)?
        paymentResult,
  }) {
    return razorpayInitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? razorpayInitial,
    TResult Function(String razorpayKey)? displayRazorpayKey,
    TResult Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)?
        paymentResult,
    required TResult orElse(),
  }) {
    if (razorpayInitial != null) {
      return razorpayInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(razorpayInitial value) razorpayInitial,
    required TResult Function(displayRazorpayKey value) displayRazorpayKey,
    required TResult Function(paymentResult value) paymentResult,
  }) {
    return razorpayInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(razorpayInitial value)? razorpayInitial,
    TResult? Function(displayRazorpayKey value)? displayRazorpayKey,
    TResult? Function(paymentResult value)? paymentResult,
  }) {
    return razorpayInitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(razorpayInitial value)? razorpayInitial,
    TResult Function(displayRazorpayKey value)? displayRazorpayKey,
    TResult Function(paymentResult value)? paymentResult,
    required TResult orElse(),
  }) {
    if (razorpayInitial != null) {
      return razorpayInitial(this);
    }
    return orElse();
  }
}

abstract class razorpayInitial implements RazorpayState {
  const factory razorpayInitial() = _$razorpayInitialImpl;
}

/// @nodoc
abstract class _$$displayRazorpayKeyImplCopyWith<$Res> {
  factory _$$displayRazorpayKeyImplCopyWith(_$displayRazorpayKeyImpl value,
          $Res Function(_$displayRazorpayKeyImpl) then) =
      __$$displayRazorpayKeyImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String razorpayKey});
}

/// @nodoc
class __$$displayRazorpayKeyImplCopyWithImpl<$Res>
    extends _$RazorpayStateCopyWithImpl<$Res, _$displayRazorpayKeyImpl>
    implements _$$displayRazorpayKeyImplCopyWith<$Res> {
  __$$displayRazorpayKeyImplCopyWithImpl(_$displayRazorpayKeyImpl _value,
      $Res Function(_$displayRazorpayKeyImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? razorpayKey = null,
  }) {
    return _then(_$displayRazorpayKeyImpl(
      razorpayKey: null == razorpayKey
          ? _value.razorpayKey
          : razorpayKey // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$displayRazorpayKeyImpl implements displayRazorpayKey {
  const _$displayRazorpayKeyImpl({required this.razorpayKey});

  @override
  final String razorpayKey;

  @override
  String toString() {
    return 'RazorpayState.displayRazorpayKey(razorpayKey: $razorpayKey)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$displayRazorpayKeyImpl &&
            (identical(other.razorpayKey, razorpayKey) ||
                other.razorpayKey == razorpayKey));
  }

  @override
  int get hashCode => Object.hash(runtimeType, razorpayKey);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$displayRazorpayKeyImplCopyWith<_$displayRazorpayKeyImpl> get copyWith =>
      __$$displayRazorpayKeyImplCopyWithImpl<_$displayRazorpayKeyImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() razorpayInitial,
    required TResult Function(String razorpayKey) displayRazorpayKey,
    required TResult Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)
        paymentResult,
  }) {
    return displayRazorpayKey(razorpayKey);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? razorpayInitial,
    TResult? Function(String razorpayKey)? displayRazorpayKey,
    TResult? Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)?
        paymentResult,
  }) {
    return displayRazorpayKey?.call(razorpayKey);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? razorpayInitial,
    TResult Function(String razorpayKey)? displayRazorpayKey,
    TResult Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)?
        paymentResult,
    required TResult orElse(),
  }) {
    if (displayRazorpayKey != null) {
      return displayRazorpayKey(razorpayKey);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(razorpayInitial value) razorpayInitial,
    required TResult Function(displayRazorpayKey value) displayRazorpayKey,
    required TResult Function(paymentResult value) paymentResult,
  }) {
    return displayRazorpayKey(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(razorpayInitial value)? razorpayInitial,
    TResult? Function(displayRazorpayKey value)? displayRazorpayKey,
    TResult? Function(paymentResult value)? paymentResult,
  }) {
    return displayRazorpayKey?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(razorpayInitial value)? razorpayInitial,
    TResult Function(displayRazorpayKey value)? displayRazorpayKey,
    TResult Function(paymentResult value)? paymentResult,
    required TResult orElse(),
  }) {
    if (displayRazorpayKey != null) {
      return displayRazorpayKey(this);
    }
    return orElse();
  }
}

abstract class displayRazorpayKey implements RazorpayState {
  const factory displayRazorpayKey({required final String razorpayKey}) =
      _$displayRazorpayKeyImpl;

  String get razorpayKey;
  @JsonKey(ignore: true)
  _$$displayRazorpayKeyImplCopyWith<_$displayRazorpayKeyImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$paymentResultImplCopyWith<$Res> {
  factory _$$paymentResultImplCopyWith(
          _$paymentResultImpl value, $Res Function(_$paymentResultImpl) then) =
      __$$paymentResultImplCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isLoading, bool isFailed, bool isError, bool isSuccess});
}

/// @nodoc
class __$$paymentResultImplCopyWithImpl<$Res>
    extends _$RazorpayStateCopyWithImpl<$Res, _$paymentResultImpl>
    implements _$$paymentResultImplCopyWith<$Res> {
  __$$paymentResultImplCopyWithImpl(
      _$paymentResultImpl _value, $Res Function(_$paymentResultImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isFailed = null,
    Object? isError = null,
    Object? isSuccess = null,
  }) {
    return _then(_$paymentResultImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isFailed: null == isFailed
          ? _value.isFailed
          : isFailed // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      isSuccess: null == isSuccess
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$paymentResultImpl implements paymentResult {
  const _$paymentResultImpl(
      {required this.isLoading,
      required this.isFailed,
      required this.isError,
      required this.isSuccess});

  @override
  final bool isLoading;
  @override
  final bool isFailed;
  @override
  final bool isError;
  @override
  final bool isSuccess;

  @override
  String toString() {
    return 'RazorpayState.paymentResult(isLoading: $isLoading, isFailed: $isFailed, isError: $isError, isSuccess: $isSuccess)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$paymentResultImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isFailed, isFailed) ||
                other.isFailed == isFailed) &&
            (identical(other.isError, isError) || other.isError == isError) &&
            (identical(other.isSuccess, isSuccess) ||
                other.isSuccess == isSuccess));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isLoading, isFailed, isError, isSuccess);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$paymentResultImplCopyWith<_$paymentResultImpl> get copyWith =>
      __$$paymentResultImplCopyWithImpl<_$paymentResultImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() razorpayInitial,
    required TResult Function(String razorpayKey) displayRazorpayKey,
    required TResult Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)
        paymentResult,
  }) {
    return paymentResult(isLoading, isFailed, isError, isSuccess);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? razorpayInitial,
    TResult? Function(String razorpayKey)? displayRazorpayKey,
    TResult? Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)?
        paymentResult,
  }) {
    return paymentResult?.call(isLoading, isFailed, isError, isSuccess);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? razorpayInitial,
    TResult Function(String razorpayKey)? displayRazorpayKey,
    TResult Function(
            bool isLoading, bool isFailed, bool isError, bool isSuccess)?
        paymentResult,
    required TResult orElse(),
  }) {
    if (paymentResult != null) {
      return paymentResult(isLoading, isFailed, isError, isSuccess);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(razorpayInitial value) razorpayInitial,
    required TResult Function(displayRazorpayKey value) displayRazorpayKey,
    required TResult Function(paymentResult value) paymentResult,
  }) {
    return paymentResult(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(razorpayInitial value)? razorpayInitial,
    TResult? Function(displayRazorpayKey value)? displayRazorpayKey,
    TResult? Function(paymentResult value)? paymentResult,
  }) {
    return paymentResult?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(razorpayInitial value)? razorpayInitial,
    TResult Function(displayRazorpayKey value)? displayRazorpayKey,
    TResult Function(paymentResult value)? paymentResult,
    required TResult orElse(),
  }) {
    if (paymentResult != null) {
      return paymentResult(this);
    }
    return orElse();
  }
}

abstract class paymentResult implements RazorpayState {
  const factory paymentResult(
      {required final bool isLoading,
      required final bool isFailed,
      required final bool isError,
      required final bool isSuccess}) = _$paymentResultImpl;

  bool get isLoading;
  bool get isFailed;
  bool get isError;
  bool get isSuccess;
  @JsonKey(ignore: true)
  _$$paymentResultImplCopyWith<_$paymentResultImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
