import 'dart:io';

import 'package:hive/hive.dart';
import 'package:linq_pe/application/view_dto/transaction/secondary_transaction_dto.dart';
import 'package:linq_pe/domain/models/invoices/edit/edit_model/edit_model.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/business_details/business_details.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/client/client.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/discount/discount.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/invoice_number/invoice_number.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/item/item.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/notes/notes.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/payment_info/payment_info.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/payments/payments.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/photo/photo.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/signature/signature.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/tax/tax.dart';
import 'package:linq_pe/domain/repositories/invoices/invoices_repository.dart';

class InvoicesImplementation extends InvoicesRepository {
  // creating a singleton
  InvoicesImplementation.internal();
  static InvoicesImplementation instance = InvoicesImplementation.internal();
  InvoicesImplementation factory() {
    return instance;
  }

///////////////////////////////////////////
  late Box<InvoiceEditModel> invoicesBox;
  openInvoicesBox() async {
    invoicesBox = await Hive.openBox("invoicesBox");
  }

// await invoicesBox.putAt(
//         invoiceIndex,
//         InvoiceEditModel(
//             invoiceId: invoiceId,
//             businessDetails: invoice.businessDetails,
//             discountDetails: invoice.discountDetails,
//             notes: invoice.notes,
//             invoiceNumberDetails: invoice.invoiceNumberDetails,
//             itemsList: invoice.itemsList,
//             paymentInfo: invoice.paymentInfo,
//             paymentsList: invoice.paymentsList,
//             photoList: invoice.photoList,
//             signature: invoice.signature,
//             tax: invoice.tax,
//             client: invoice.client));





  @override
  Future<String> createInvoiceNumber({required String invoiceNumber}) async {
    final invoiceId = DateTime.now().millisecondsSinceEpoch.toString();
    await invoicesBox.add(InvoiceEditModel(
        isEstimate: false,
        isInvoice: true,
        balanceDue: null,
        isPaid: false,
        paidMethod: null,
        isToReview: null,
        reviewLink: null,
        invoiceId: invoiceId,
        businessDetails: null,
        discountDetails: null,
        notes: null,
        invoiceNumberDetails: InvoiceNumberModel(
            dueDate: null,
            invoiceDate: DateTime.now(),
            invoiceId: invoiceId,
            invoiceNumber: invoiceNumber,
            poNumber: null,
            terms: null),
        itemsList: null,
        paymentInfo: null,
        paymentsList: null,
        photoList: null,
        signature: null,
        tax: null,
   client: ClientModel(invoiceId: invoiceId, 
        address1: null, address2: null,
         address3: null, clientEmail: null,
          clientName: 'No Client', faxNumber: null,
           mobileNumber: null, phoneNumber: null)));

    return invoiceId;
  }



 @override
  Future<String> createEstimateNumber({required String estimateNumber}) async {
    final invoiceId = DateTime.now().millisecondsSinceEpoch.toString();
    await invoicesBox.add(InvoiceEditModel(
        isEstimate: true,
        isInvoice: false,
        balanceDue: null,
        isPaid: false,
        paidMethod: null,
        isToReview: null,
        reviewLink: null,
        invoiceId: invoiceId,
        businessDetails: null,
        discountDetails: null,
        notes: null,
        invoiceNumberDetails: InvoiceNumberModel(
            dueDate: null,
            invoiceDate: DateTime.now(),
            invoiceId: invoiceId,
            invoiceNumber: estimateNumber,
            poNumber: null,
            terms: null),
        itemsList: null,
        paymentInfo: null,
        paymentsList: null,
        photoList: null,
        signature: null,
        tax: null,
        client: ClientModel(invoiceId: invoiceId, 
        address1: null, address2: null,
         address3: null, clientEmail: null,
          clientName: 'No Client', faxNumber: null,
           mobileNumber: null, phoneNumber: null)));

    return invoiceId;
  }
  @override
  Future<void> addDiscountDetails(
      {required String invoiceId,
      required String discountType,
      required double percentage}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel(
            isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: DiscountModel(
                invoiceId: invoiceId,
                discountType: discountType,
                percentage: percentage),
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> addSignature(
      {required String invoiceId, required File signature}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: SignatureModel(
                invoiceId: invoiceId,
                signDate: DateTime.now(),
                signature: await convertToUni8List(signature)),
            tax: invoice.tax,
            client: invoice.client));
  }
  @override
  Future<void> convertEstimateToInvoice({ required String invoiceId,})async{
      final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: false,
            isInvoice: true,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));

  }

  @override
  Future<void> addNotes(
      {required String notes,
      required String invoiceId,
      required bool isDefault}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: NotesModel(invoiceId: invoiceId, notes: notes),
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
    if (isDefault) {}
  }

  @override
  Future<void> addTaxDetails(
      {required String invoiceId,
      required double? rate,
      required String label,
      required String taxType,
      required bool isInclusive}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: TaxModel(
                isInclusive: isInclusive,
                invoiceId: invoiceId,
                taxType: taxType,
                label: label,
                rate: rate),
            client: invoice.client));
  }

  @override
  Future<void> addItems(
      {required String invoiceId,
      required double unitCost,
      required String? unit,
      required double? taxRate,
      required int quantity,
      required bool isTaxable,
      required double? discountAmount,
      required String description,
      required String? discount,
      required String? addiotionalDetails,
      required bool isAddToMyItems,
      required double total}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<ItemModel> itemsList = [];
    if (invoice.itemsList != null) {
      itemsList = invoice.itemsList!;
    }
    itemsList.add(ItemModel(
        itemId: 'Item-$invoiceId-${DateTime.now().millisecondsSinceEpoch}',
        total: total,
        invoiceId: invoiceId,
        addiotionalDetails: addiotionalDetails,
        discount: discount,
        description: description,
        discountAmount: discountAmount,
        isTaxable: isTaxable,
        quantity: quantity,
        taxRate: taxRate,
        unit: unit,
        unitCost: unitCost));
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
    if (isAddToMyItems) {}
  }

  @override
  Future<void> editItems(
      {required String itemId,
      required String invoiceId,
      required double unitCost,
      required String? unit,
      required double? taxRate,
      required int quantity,
      required bool isTaxable,
      required double? discountAmount,
      required String description,
      required String? discount,
      required String? addiotionalDetails,
      required bool isAddToMyItems,
      required double total}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<ItemModel> itemsList = [];
    if (invoice.itemsList != null) {
      itemsList = invoice.itemsList!;
    }
    final itemIndex =
        itemsList.indexWhere((element) => element.itemId == itemId);
    if (itemIndex < 0) {
      return;
    }
    itemsList[itemIndex] = ItemModel(
        itemId: itemId,
        total: total,
        invoiceId: invoiceId,
        addiotionalDetails: addiotionalDetails,
        discount: discount,
        description: description,
        discountAmount: discountAmount,
        isTaxable: isTaxable,
        quantity: quantity,
        taxRate: taxRate,
        unit: unit,
        unitCost: unitCost);
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
    if (isAddToMyItems) {}
  }

  @override
  Future<void> deleteItems({
    required String itemId,
    required String invoiceId,
  }) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<ItemModel> itemsList = [];
    if (invoice.itemsList != null) {
      itemsList = invoice.itemsList!;
    }
    final itemIndex =
        itemsList.indexWhere((element) => element.itemId == itemId);
    if (itemIndex < 0) {
      return;
    }
    itemsList.removeAt(itemIndex);
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> addClient(
      {required String invoiceId,
      required String? phoneNumber,
      required String? mobileNumber,
      required String? faxNumber,
      required String clientName,
      required String? clientEmail,
      required String? address3,
      required String? address2,
      required String? address1}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: ClientModel(
                invoiceId: invoiceId,
                address1: address1,
                address2: address2,
                address3: address3,
                clientEmail: clientEmail,
                clientName: clientName,
                faxNumber: faxNumber,
                mobileNumber: mobileNumber,
                phoneNumber: phoneNumber)));
  }

  @override
  Future<void> addPayments(
      {required String invoiceId,
      required double amount,
      required DateTime date,
      required String notes,
      required String paymentMethod}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<PaymentsModel> paymentsList = [];
    if (invoice.paymentsList != null) {
      paymentsList = invoice.paymentsList!;
    }
    paymentsList.add(PaymentsModel(
        paymentId:
            'Payment-$invoiceId-${DateTime.now().millisecondsSinceEpoch}',
        invoiceId: invoiceId,
        amount: amount,
        date: date,
        notes: notes,
        paymentMethod: paymentMethod));

    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> editPayments(
      {required String paymentId,
      required String invoiceId,
      required double amount,
      required DateTime date,
      required String notes,
      required String paymentMethod}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<PaymentsModel> paymentsList = [];
    if (invoice.paymentsList != null) {
      paymentsList = invoice.paymentsList!;
    }
    final paymentIndex =
        paymentsList.indexWhere((element) => element.paymentId == paymentId);
    if (paymentIndex < 0) {
      return;
    }
    paymentsList[paymentIndex] = PaymentsModel(
        paymentId: paymentId,
        invoiceId: invoiceId,
        amount: amount,
        date: date,
        notes: notes,
        paymentMethod: paymentMethod);

    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> deletePayments({
    required String paymentId,
    required String invoiceId,
  }) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<PaymentsModel> paymentsList = [];
    if (invoice.paymentsList != null) {
      paymentsList = invoice.paymentsList!;
    }
    final paymentIndex =
        paymentsList.indexWhere((element) => element.paymentId == paymentId);
    if (paymentIndex < 0) {
      return;
    }
    paymentsList.removeAt(paymentIndex);

    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> addPhoto(
      {required String invoiceId,
      required File photo,
      required String? description,
      required String? addionalDetails}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<PhotoModel> photoList = [];
    if (invoice.photoList != null) {
      photoList = invoice.photoList!;
    }
    final photosUnit = await convertToUni8List(photo);
    photoList.add(PhotoModel(
        photoId: 'Photo-$invoiceId-${DateTime.now().millisecondsSinceEpoch}',
        invoiceId: invoiceId,
        photo: photosUnit!,
        addionalDetails: addionalDetails,
        description: description));
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> editPhoto(
      {required String photoId,
      required String invoiceId,
      required File photo,
      required String? description,
      required String? addionalDetails}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<PhotoModel> photoList = [];
    if (invoice.photoList != null) {
      photoList = invoice.photoList!;
    }
    final photoIndex =
        photoList.indexWhere((element) => element.photoId == photoId);
    if (photoIndex < 0) {
      return;
    }
    final photosUnit = await convertToUni8List(photo);
    photoList[photoIndex] = PhotoModel(
        photoId: photoId,
        invoiceId: invoiceId,
        photo: photosUnit!,
        addionalDetails: addionalDetails,
        description: description);
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> deletePhoto({
    required String photoId,
    required String invoiceId,
  }) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    List<PhotoModel> photoList = [];
    if (invoice.photoList != null) {
      photoList = invoice.photoList!;
    }
    final photoIndex =
        photoList.indexWhere((element) => element.photoId == photoId);
    if (photoIndex < 0) {
      return;
    }

    photoList.removeAt(photoIndex);
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> addPaymentInfo(
      {required String? additionalPaymentInstructions,
      required String? businessName,
      required String? paymentInstructions,
      required String? paypalEmail,
      required String invoiceId}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: PaymentInfoModel(
                invoiceId: invoiceId,
                additionalPaymentInstructions: additionalPaymentInstructions,
                businessName: businessName,
                paymentInstructions: paymentInstructions,
                paypalEmail: paypalEmail),
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> addBusinessDetails(
      {required String businessName,
      required File? businessLogo,
      required String? businessOwnerName,
      required String? businessNumber,
      required String? addressLine1,
      required String? addressLine2,
      required String? addressLine3,
      required String invoiceId,
      required String? email,
      required String? phone,
      required String? mobile,
      required String? website}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: BusinessDetailsModel(
                businessLogo: await convertToUni8List(businessLogo),
                addressLine1: addressLine1,
                addressLine2: addressLine2,
                addressLine3: addressLine3,
                businessName: businessName,
                businessNumber: businessNumber,
                businessOwnerName: businessOwnerName,
                email: email,
                mobile: mobile,
                phone: phone,
                website: website,
                invoiceId: invoice.invoiceId),
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> addInvoiceNumber(
      {required String invoiceNumber,
      required String invoiceId,
      required DateTime date,
      required String terms,
      required DateTime? dueDate,
      required String? poNumber}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: InvoiceNumberModel(
                dueDate: dueDate,
                invoiceDate: date,
                invoiceId: invoice.invoiceId,
                invoiceNumber: invoiceNumber,
                poNumber: poNumber,
                terms: terms),
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  InvoiceEditModel getInvoiceDetails({required String invoiceId}) {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {}
    final invoice = allInvoiceList[invoiceIndex];
    return invoice;
  }

  @override
  Future<void> addReview(
      {required String invoiceId,
      required String reviewLink,
      required bool isToReview}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: isToReview,
            reviewLink: reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> addBalanceDue({
    required String invoiceId,
    required double balanceDue,
  }) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: balanceDue,
            isPaid: invoice.isPaid,
            paidMethod: invoice.paidMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  Future<void> marKPaidOrUnPaid(
      {required String invoiceId,
      required String? paymentMethod,
      required bool isPaid}) async {
    final allInvoiceList = invoicesBox.values.toList();
    final invoiceIndex =
        allInvoiceList.indexWhere((element) => element.invoiceId == invoiceId);
    if (invoiceIndex < 0) {
      return;
    }
    final invoice = allInvoiceList[invoiceIndex];
    await invoicesBox.putAt(
        invoiceIndex,
        InvoiceEditModel( isEstimate: invoice.isEstimate,
            isInvoice: invoice.isInvoice,
            balanceDue: invoice.balanceDue,
            isPaid: isPaid,
            paidMethod: paymentMethod,
            isToReview: invoice.isToReview,
            reviewLink: invoice.reviewLink,
            invoiceId: invoiceId,
            businessDetails: invoice.businessDetails,
            discountDetails: invoice.discountDetails,
            notes: invoice.notes,
            invoiceNumberDetails: invoice.invoiceNumberDetails,
            itemsList: invoice.itemsList,
            paymentInfo: invoice.paymentInfo,
            paymentsList: invoice.paymentsList,
            photoList: invoice.photoList,
            signature: invoice.signature,
            tax: invoice.tax,
            client: invoice.client));
  }

  @override
  List<InvoiceEditModel> getAllInvoiceList() {
    final allInvoiceList = invoicesBox.values.toList();

    return allInvoiceList;
  }
}
