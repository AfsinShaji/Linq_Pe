// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'business_details.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BusinessDetailsModelAdapter extends TypeAdapter<BusinessDetailsModel> {
  @override
  final int typeId = 13;

  @override
  BusinessDetailsModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BusinessDetailsModel(
      businessLogo: fields[11] as Uint8List?,
      addressLine1: fields[3] as String?,
      addressLine2: fields[4] as String?,
      addressLine3: fields[5] as String?,
      businessName: fields[1] as String,
      businessNumber: fields[2] as String?,
      businessOwnerName: fields[0] as String?,
      email: fields[6] as String?,
      mobile: fields[8] as String?,
      phone: fields[9] as String?,
      website: fields[7] as String?,
      invoiceId: fields[10] as String,
    );
  }

  @override
  void write(BinaryWriter writer, BusinessDetailsModel obj) {
    writer
      ..writeByte(12)
      ..writeByte(0)
      ..write(obj.businessOwnerName)
      ..writeByte(1)
      ..write(obj.businessName)
      ..writeByte(2)
      ..write(obj.businessNumber)
      ..writeByte(3)
      ..write(obj.addressLine1)
      ..writeByte(4)
      ..write(obj.addressLine2)
      ..writeByte(5)
      ..write(obj.addressLine3)
      ..writeByte(6)
      ..write(obj.email)
      ..writeByte(7)
      ..write(obj.website)
      ..writeByte(8)
      ..write(obj.mobile)
      ..writeByte(9)
      ..write(obj.phone)
      ..writeByte(10)
      ..write(obj.invoiceId)
      ..writeByte(11)
      ..write(obj.businessLogo);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BusinessDetailsModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
