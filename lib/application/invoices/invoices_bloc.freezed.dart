// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'invoices_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$InvoicesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvoicesEventCopyWith<$Res> {
  factory $InvoicesEventCopyWith(
          InvoicesEvent value, $Res Function(InvoicesEvent) then) =
      _$InvoicesEventCopyWithImpl<$Res, InvoicesEvent>;
}

/// @nodoc
class _$InvoicesEventCopyWithImpl<$Res, $Val extends InvoicesEvent>
    implements $InvoicesEventCopyWith<$Res> {
  _$InvoicesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$createInvoicesNumberImplCopyWith<$Res> {
  factory _$$createInvoicesNumberImplCopyWith(_$createInvoicesNumberImpl value,
          $Res Function(_$createInvoicesNumberImpl) then) =
      __$$createInvoicesNumberImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceNumber});
}

/// @nodoc
class __$$createInvoicesNumberImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$createInvoicesNumberImpl>
    implements _$$createInvoicesNumberImplCopyWith<$Res> {
  __$$createInvoicesNumberImplCopyWithImpl(_$createInvoicesNumberImpl _value,
      $Res Function(_$createInvoicesNumberImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceNumber = null,
  }) {
    return _then(_$createInvoicesNumberImpl(
      invoiceNumber: null == invoiceNumber
          ? _value.invoiceNumber
          : invoiceNumber // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$createInvoicesNumberImpl implements createInvoicesNumber {
  const _$createInvoicesNumberImpl({required this.invoiceNumber});

  @override
  final String invoiceNumber;

  @override
  String toString() {
    return 'InvoicesEvent.createInvoicesNumber(invoiceNumber: $invoiceNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$createInvoicesNumberImpl &&
            (identical(other.invoiceNumber, invoiceNumber) ||
                other.invoiceNumber == invoiceNumber));
  }

  @override
  int get hashCode => Object.hash(runtimeType, invoiceNumber);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$createInvoicesNumberImplCopyWith<_$createInvoicesNumberImpl>
      get copyWith =>
          __$$createInvoicesNumberImplCopyWithImpl<_$createInvoicesNumberImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return createInvoicesNumber(invoiceNumber);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return createInvoicesNumber?.call(invoiceNumber);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (createInvoicesNumber != null) {
      return createInvoicesNumber(invoiceNumber);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return createInvoicesNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return createInvoicesNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (createInvoicesNumber != null) {
      return createInvoicesNumber(this);
    }
    return orElse();
  }
}

abstract class createInvoicesNumber implements InvoicesEvent {
  const factory createInvoicesNumber({required final String invoiceNumber}) =
      _$createInvoicesNumberImpl;

  String get invoiceNumber;
  @JsonKey(ignore: true)
  _$$createInvoicesNumberImplCopyWith<_$createInvoicesNumberImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addInvoicesNumberImplCopyWith<$Res> {
  factory _$$addInvoicesNumberImplCopyWith(_$addInvoicesNumberImpl value,
          $Res Function(_$addInvoicesNumberImpl) then) =
      __$$addInvoicesNumberImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String invoiceNumber,
      String invoiceId,
      DateTime date,
      String terms,
      DateTime? dueDate,
      String? poNumber});
}

/// @nodoc
class __$$addInvoicesNumberImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addInvoicesNumberImpl>
    implements _$$addInvoicesNumberImplCopyWith<$Res> {
  __$$addInvoicesNumberImplCopyWithImpl(_$addInvoicesNumberImpl _value,
      $Res Function(_$addInvoicesNumberImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceNumber = null,
    Object? invoiceId = null,
    Object? date = null,
    Object? terms = null,
    Object? dueDate = freezed,
    Object? poNumber = freezed,
  }) {
    return _then(_$addInvoicesNumberImpl(
      invoiceNumber: null == invoiceNumber
          ? _value.invoiceNumber
          : invoiceNumber // ignore: cast_nullable_to_non_nullable
              as String,
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      terms: null == terms
          ? _value.terms
          : terms // ignore: cast_nullable_to_non_nullable
              as String,
      dueDate: freezed == dueDate
          ? _value.dueDate
          : dueDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      poNumber: freezed == poNumber
          ? _value.poNumber
          : poNumber // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$addInvoicesNumberImpl implements addInvoicesNumber {
  const _$addInvoicesNumberImpl(
      {required this.invoiceNumber,
      required this.invoiceId,
      required this.date,
      required this.terms,
      required this.dueDate,
      required this.poNumber});

  @override
  final String invoiceNumber;
  @override
  final String invoiceId;
  @override
  final DateTime date;
  @override
  final String terms;
  @override
  final DateTime? dueDate;
  @override
  final String? poNumber;

  @override
  String toString() {
    return 'InvoicesEvent.addInvoicesNumber(invoiceNumber: $invoiceNumber, invoiceId: $invoiceId, date: $date, terms: $terms, dueDate: $dueDate, poNumber: $poNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addInvoicesNumberImpl &&
            (identical(other.invoiceNumber, invoiceNumber) ||
                other.invoiceNumber == invoiceNumber) &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.terms, terms) || other.terms == terms) &&
            (identical(other.dueDate, dueDate) || other.dueDate == dueDate) &&
            (identical(other.poNumber, poNumber) ||
                other.poNumber == poNumber));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, invoiceNumber, invoiceId, date, terms, dueDate, poNumber);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addInvoicesNumberImplCopyWith<_$addInvoicesNumberImpl> get copyWith =>
      __$$addInvoicesNumberImplCopyWithImpl<_$addInvoicesNumberImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addInvoicesNumber(
        invoiceNumber, invoiceId, date, terms, dueDate, poNumber);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addInvoicesNumber?.call(
        invoiceNumber, invoiceId, date, terms, dueDate, poNumber);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addInvoicesNumber != null) {
      return addInvoicesNumber(
          invoiceNumber, invoiceId, date, terms, dueDate, poNumber);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addInvoicesNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addInvoicesNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addInvoicesNumber != null) {
      return addInvoicesNumber(this);
    }
    return orElse();
  }
}

abstract class addInvoicesNumber implements InvoicesEvent {
  const factory addInvoicesNumber(
      {required final String invoiceNumber,
      required final String invoiceId,
      required final DateTime date,
      required final String terms,
      required final DateTime? dueDate,
      required final String? poNumber}) = _$addInvoicesNumberImpl;

  String get invoiceNumber;
  String get invoiceId;
  DateTime get date;
  String get terms;
  DateTime? get dueDate;
  String? get poNumber;
  @JsonKey(ignore: true)
  _$$addInvoicesNumberImplCopyWith<_$addInvoicesNumberImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addBusinessDetailImplCopyWith<$Res> {
  factory _$$addBusinessDetailImplCopyWith(_$addBusinessDetailImpl value,
          $Res Function(_$addBusinessDetailImpl) then) =
      __$$addBusinessDetailImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String businessName,
      String invoiceId,
      File? businessLogo,
      String? businessOwnerName,
      String? businessNumber,
      String? addressLine1,
      String? addressLine2,
      String? addressLine3,
      String? email,
      String? phone,
      String? mobile,
      String? website});
}

/// @nodoc
class __$$addBusinessDetailImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addBusinessDetailImpl>
    implements _$$addBusinessDetailImplCopyWith<$Res> {
  __$$addBusinessDetailImplCopyWithImpl(_$addBusinessDetailImpl _value,
      $Res Function(_$addBusinessDetailImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? businessName = null,
    Object? invoiceId = null,
    Object? businessLogo = freezed,
    Object? businessOwnerName = freezed,
    Object? businessNumber = freezed,
    Object? addressLine1 = freezed,
    Object? addressLine2 = freezed,
    Object? addressLine3 = freezed,
    Object? email = freezed,
    Object? phone = freezed,
    Object? mobile = freezed,
    Object? website = freezed,
  }) {
    return _then(_$addBusinessDetailImpl(
      businessName: null == businessName
          ? _value.businessName
          : businessName // ignore: cast_nullable_to_non_nullable
              as String,
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      businessLogo: freezed == businessLogo
          ? _value.businessLogo
          : businessLogo // ignore: cast_nullable_to_non_nullable
              as File?,
      businessOwnerName: freezed == businessOwnerName
          ? _value.businessOwnerName
          : businessOwnerName // ignore: cast_nullable_to_non_nullable
              as String?,
      businessNumber: freezed == businessNumber
          ? _value.businessNumber
          : businessNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine1: freezed == addressLine1
          ? _value.addressLine1
          : addressLine1 // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine2: freezed == addressLine2
          ? _value.addressLine2
          : addressLine2 // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine3: freezed == addressLine3
          ? _value.addressLine3
          : addressLine3 // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String?,
      mobile: freezed == mobile
          ? _value.mobile
          : mobile // ignore: cast_nullable_to_non_nullable
              as String?,
      website: freezed == website
          ? _value.website
          : website // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$addBusinessDetailImpl implements addBusinessDetail {
  const _$addBusinessDetailImpl(
      {required this.businessName,
      required this.invoiceId,
      required this.businessLogo,
      required this.businessOwnerName,
      required this.businessNumber,
      required this.addressLine1,
      required this.addressLine2,
      required this.addressLine3,
      required this.email,
      required this.phone,
      required this.mobile,
      required this.website});

  @override
  final String businessName;
  @override
  final String invoiceId;
  @override
  final File? businessLogo;
  @override
  final String? businessOwnerName;
  @override
  final String? businessNumber;
  @override
  final String? addressLine1;
  @override
  final String? addressLine2;
  @override
  final String? addressLine3;
  @override
  final String? email;
  @override
  final String? phone;
  @override
  final String? mobile;
  @override
  final String? website;

  @override
  String toString() {
    return 'InvoicesEvent.addBusinessDetail(businessName: $businessName, invoiceId: $invoiceId, businessLogo: $businessLogo, businessOwnerName: $businessOwnerName, businessNumber: $businessNumber, addressLine1: $addressLine1, addressLine2: $addressLine2, addressLine3: $addressLine3, email: $email, phone: $phone, mobile: $mobile, website: $website)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addBusinessDetailImpl &&
            (identical(other.businessName, businessName) ||
                other.businessName == businessName) &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.businessLogo, businessLogo) ||
                other.businessLogo == businessLogo) &&
            (identical(other.businessOwnerName, businessOwnerName) ||
                other.businessOwnerName == businessOwnerName) &&
            (identical(other.businessNumber, businessNumber) ||
                other.businessNumber == businessNumber) &&
            (identical(other.addressLine1, addressLine1) ||
                other.addressLine1 == addressLine1) &&
            (identical(other.addressLine2, addressLine2) ||
                other.addressLine2 == addressLine2) &&
            (identical(other.addressLine3, addressLine3) ||
                other.addressLine3 == addressLine3) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.mobile, mobile) || other.mobile == mobile) &&
            (identical(other.website, website) || other.website == website));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      businessName,
      invoiceId,
      businessLogo,
      businessOwnerName,
      businessNumber,
      addressLine1,
      addressLine2,
      addressLine3,
      email,
      phone,
      mobile,
      website);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addBusinessDetailImplCopyWith<_$addBusinessDetailImpl> get copyWith =>
      __$$addBusinessDetailImplCopyWithImpl<_$addBusinessDetailImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addBusinessDetail(
        businessName,
        invoiceId,
        businessLogo,
        businessOwnerName,
        businessNumber,
        addressLine1,
        addressLine2,
        addressLine3,
        email,
        phone,
        mobile,
        website);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addBusinessDetail?.call(
        businessName,
        invoiceId,
        businessLogo,
        businessOwnerName,
        businessNumber,
        addressLine1,
        addressLine2,
        addressLine3,
        email,
        phone,
        mobile,
        website);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addBusinessDetail != null) {
      return addBusinessDetail(
          businessName,
          invoiceId,
          businessLogo,
          businessOwnerName,
          businessNumber,
          addressLine1,
          addressLine2,
          addressLine3,
          email,
          phone,
          mobile,
          website);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addBusinessDetail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addBusinessDetail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addBusinessDetail != null) {
      return addBusinessDetail(this);
    }
    return orElse();
  }
}

abstract class addBusinessDetail implements InvoicesEvent {
  const factory addBusinessDetail(
      {required final String businessName,
      required final String invoiceId,
      required final File? businessLogo,
      required final String? businessOwnerName,
      required final String? businessNumber,
      required final String? addressLine1,
      required final String? addressLine2,
      required final String? addressLine3,
      required final String? email,
      required final String? phone,
      required final String? mobile,
      required final String? website}) = _$addBusinessDetailImpl;

  String get businessName;
  String get invoiceId;
  File? get businessLogo;
  String? get businessOwnerName;
  String? get businessNumber;
  String? get addressLine1;
  String? get addressLine2;
  String? get addressLine3;
  String? get email;
  String? get phone;
  String? get mobile;
  String? get website;
  @JsonKey(ignore: true)
  _$$addBusinessDetailImplCopyWith<_$addBusinessDetailImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addPaymentsInfoImplCopyWith<$Res> {
  factory _$$addPaymentsInfoImplCopyWith(_$addPaymentsInfoImpl value,
          $Res Function(_$addPaymentsInfoImpl) then) =
      __$$addPaymentsInfoImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String? additionalPaymentInstructions,
      String? businessName,
      String? paymentInstructions,
      String? paypalEmail,
      String invoiceId});
}

/// @nodoc
class __$$addPaymentsInfoImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addPaymentsInfoImpl>
    implements _$$addPaymentsInfoImplCopyWith<$Res> {
  __$$addPaymentsInfoImplCopyWithImpl(
      _$addPaymentsInfoImpl _value, $Res Function(_$addPaymentsInfoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? additionalPaymentInstructions = freezed,
    Object? businessName = freezed,
    Object? paymentInstructions = freezed,
    Object? paypalEmail = freezed,
    Object? invoiceId = null,
  }) {
    return _then(_$addPaymentsInfoImpl(
      additionalPaymentInstructions: freezed == additionalPaymentInstructions
          ? _value.additionalPaymentInstructions
          : additionalPaymentInstructions // ignore: cast_nullable_to_non_nullable
              as String?,
      businessName: freezed == businessName
          ? _value.businessName
          : businessName // ignore: cast_nullable_to_non_nullable
              as String?,
      paymentInstructions: freezed == paymentInstructions
          ? _value.paymentInstructions
          : paymentInstructions // ignore: cast_nullable_to_non_nullable
              as String?,
      paypalEmail: freezed == paypalEmail
          ? _value.paypalEmail
          : paypalEmail // ignore: cast_nullable_to_non_nullable
              as String?,
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$addPaymentsInfoImpl implements addPaymentsInfo {
  const _$addPaymentsInfoImpl(
      {required this.additionalPaymentInstructions,
      required this.businessName,
      required this.paymentInstructions,
      required this.paypalEmail,
      required this.invoiceId});

  @override
  final String? additionalPaymentInstructions;
  @override
  final String? businessName;
  @override
  final String? paymentInstructions;
  @override
  final String? paypalEmail;
  @override
  final String invoiceId;

  @override
  String toString() {
    return 'InvoicesEvent.addPaymentsInfo(additionalPaymentInstructions: $additionalPaymentInstructions, businessName: $businessName, paymentInstructions: $paymentInstructions, paypalEmail: $paypalEmail, invoiceId: $invoiceId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addPaymentsInfoImpl &&
            (identical(other.additionalPaymentInstructions,
                    additionalPaymentInstructions) ||
                other.additionalPaymentInstructions ==
                    additionalPaymentInstructions) &&
            (identical(other.businessName, businessName) ||
                other.businessName == businessName) &&
            (identical(other.paymentInstructions, paymentInstructions) ||
                other.paymentInstructions == paymentInstructions) &&
            (identical(other.paypalEmail, paypalEmail) ||
                other.paypalEmail == paypalEmail) &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, additionalPaymentInstructions,
      businessName, paymentInstructions, paypalEmail, invoiceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addPaymentsInfoImplCopyWith<_$addPaymentsInfoImpl> get copyWith =>
      __$$addPaymentsInfoImplCopyWithImpl<_$addPaymentsInfoImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addPaymentsInfo(additionalPaymentInstructions, businessName,
        paymentInstructions, paypalEmail, invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addPaymentsInfo?.call(additionalPaymentInstructions, businessName,
        paymentInstructions, paypalEmail, invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addPaymentsInfo != null) {
      return addPaymentsInfo(additionalPaymentInstructions, businessName,
          paymentInstructions, paypalEmail, invoiceId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addPaymentsInfo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addPaymentsInfo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addPaymentsInfo != null) {
      return addPaymentsInfo(this);
    }
    return orElse();
  }
}

abstract class addPaymentsInfo implements InvoicesEvent {
  const factory addPaymentsInfo(
      {required final String? additionalPaymentInstructions,
      required final String? businessName,
      required final String? paymentInstructions,
      required final String? paypalEmail,
      required final String invoiceId}) = _$addPaymentsInfoImpl;

  String? get additionalPaymentInstructions;
  String? get businessName;
  String? get paymentInstructions;
  String? get paypalEmail;
  String get invoiceId;
  @JsonKey(ignore: true)
  _$$addPaymentsInfoImplCopyWith<_$addPaymentsInfoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addsPhotoImplCopyWith<$Res> {
  factory _$$addsPhotoImplCopyWith(
          _$addsPhotoImpl value, $Res Function(_$addsPhotoImpl) then) =
      __$$addsPhotoImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String invoiceId,
      File photo,
      String? description,
      String? addionalDetails});
}

/// @nodoc
class __$$addsPhotoImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addsPhotoImpl>
    implements _$$addsPhotoImplCopyWith<$Res> {
  __$$addsPhotoImplCopyWithImpl(
      _$addsPhotoImpl _value, $Res Function(_$addsPhotoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? photo = null,
    Object? description = freezed,
    Object? addionalDetails = freezed,
  }) {
    return _then(_$addsPhotoImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      photo: null == photo
          ? _value.photo
          : photo // ignore: cast_nullable_to_non_nullable
              as File,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      addionalDetails: freezed == addionalDetails
          ? _value.addionalDetails
          : addionalDetails // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$addsPhotoImpl implements addsPhoto {
  const _$addsPhotoImpl(
      {required this.invoiceId,
      required this.photo,
      required this.description,
      required this.addionalDetails});

  @override
  final String invoiceId;
  @override
  final File photo;
  @override
  final String? description;
  @override
  final String? addionalDetails;

  @override
  String toString() {
    return 'InvoicesEvent.addsPhoto(invoiceId: $invoiceId, photo: $photo, description: $description, addionalDetails: $addionalDetails)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addsPhotoImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.photo, photo) || other.photo == photo) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.addionalDetails, addionalDetails) ||
                other.addionalDetails == addionalDetails));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, invoiceId, photo, description, addionalDetails);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addsPhotoImplCopyWith<_$addsPhotoImpl> get copyWith =>
      __$$addsPhotoImplCopyWithImpl<_$addsPhotoImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addsPhoto(invoiceId, photo, description, addionalDetails);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addsPhoto?.call(invoiceId, photo, description, addionalDetails);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addsPhoto != null) {
      return addsPhoto(invoiceId, photo, description, addionalDetails);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addsPhoto(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addsPhoto?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addsPhoto != null) {
      return addsPhoto(this);
    }
    return orElse();
  }
}

abstract class addsPhoto implements InvoicesEvent {
  const factory addsPhoto(
      {required final String invoiceId,
      required final File photo,
      required final String? description,
      required final String? addionalDetails}) = _$addsPhotoImpl;

  String get invoiceId;
  File get photo;
  String? get description;
  String? get addionalDetails;
  @JsonKey(ignore: true)
  _$$addsPhotoImplCopyWith<_$addsPhotoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addPaymentImplCopyWith<$Res> {
  factory _$$addPaymentImplCopyWith(
          _$addPaymentImpl value, $Res Function(_$addPaymentImpl) then) =
      __$$addPaymentImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String invoiceId,
      double amount,
      DateTime date,
      String notes,
      String paymentMethod});
}

/// @nodoc
class __$$addPaymentImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addPaymentImpl>
    implements _$$addPaymentImplCopyWith<$Res> {
  __$$addPaymentImplCopyWithImpl(
      _$addPaymentImpl _value, $Res Function(_$addPaymentImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? amount = null,
    Object? date = null,
    Object? notes = null,
    Object? paymentMethod = null,
  }) {
    return _then(_$addPaymentImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as double,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      notes: null == notes
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String,
      paymentMethod: null == paymentMethod
          ? _value.paymentMethod
          : paymentMethod // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$addPaymentImpl implements addPayment {
  const _$addPaymentImpl(
      {required this.invoiceId,
      required this.amount,
      required this.date,
      required this.notes,
      required this.paymentMethod});

  @override
  final String invoiceId;
  @override
  final double amount;
  @override
  final DateTime date;
  @override
  final String notes;
  @override
  final String paymentMethod;

  @override
  String toString() {
    return 'InvoicesEvent.addPayment(invoiceId: $invoiceId, amount: $amount, date: $date, notes: $notes, paymentMethod: $paymentMethod)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addPaymentImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.notes, notes) || other.notes == notes) &&
            (identical(other.paymentMethod, paymentMethod) ||
                other.paymentMethod == paymentMethod));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, invoiceId, amount, date, notes, paymentMethod);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addPaymentImplCopyWith<_$addPaymentImpl> get copyWith =>
      __$$addPaymentImplCopyWithImpl<_$addPaymentImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addPayment(invoiceId, amount, date, notes, paymentMethod);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addPayment?.call(invoiceId, amount, date, notes, paymentMethod);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addPayment != null) {
      return addPayment(invoiceId, amount, date, notes, paymentMethod);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addPayment(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addPayment?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addPayment != null) {
      return addPayment(this);
    }
    return orElse();
  }
}

abstract class addPayment implements InvoicesEvent {
  const factory addPayment(
      {required final String invoiceId,
      required final double amount,
      required final DateTime date,
      required final String notes,
      required final String paymentMethod}) = _$addPaymentImpl;

  String get invoiceId;
  double get amount;
  DateTime get date;
  String get notes;
  String get paymentMethod;
  @JsonKey(ignore: true)
  _$$addPaymentImplCopyWith<_$addPaymentImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addsClientImplCopyWith<$Res> {
  factory _$$addsClientImplCopyWith(
          _$addsClientImpl value, $Res Function(_$addsClientImpl) then) =
      __$$addsClientImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String invoiceId,
      String? phoneNumber,
      String? mobileNumber,
      String? faxNumber,
      String clientName,
      String? clientEmail,
      String? address3,
      String? address2,
      String? address1});
}

/// @nodoc
class __$$addsClientImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addsClientImpl>
    implements _$$addsClientImplCopyWith<$Res> {
  __$$addsClientImplCopyWithImpl(
      _$addsClientImpl _value, $Res Function(_$addsClientImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? phoneNumber = freezed,
    Object? mobileNumber = freezed,
    Object? faxNumber = freezed,
    Object? clientName = null,
    Object? clientEmail = freezed,
    Object? address3 = freezed,
    Object? address2 = freezed,
    Object? address1 = freezed,
  }) {
    return _then(_$addsClientImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: freezed == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      mobileNumber: freezed == mobileNumber
          ? _value.mobileNumber
          : mobileNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      faxNumber: freezed == faxNumber
          ? _value.faxNumber
          : faxNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      clientName: null == clientName
          ? _value.clientName
          : clientName // ignore: cast_nullable_to_non_nullable
              as String,
      clientEmail: freezed == clientEmail
          ? _value.clientEmail
          : clientEmail // ignore: cast_nullable_to_non_nullable
              as String?,
      address3: freezed == address3
          ? _value.address3
          : address3 // ignore: cast_nullable_to_non_nullable
              as String?,
      address2: freezed == address2
          ? _value.address2
          : address2 // ignore: cast_nullable_to_non_nullable
              as String?,
      address1: freezed == address1
          ? _value.address1
          : address1 // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$addsClientImpl implements addsClient {
  const _$addsClientImpl(
      {required this.invoiceId,
      required this.phoneNumber,
      required this.mobileNumber,
      required this.faxNumber,
      required this.clientName,
      required this.clientEmail,
      required this.address3,
      required this.address2,
      required this.address1});

  @override
  final String invoiceId;
  @override
  final String? phoneNumber;
  @override
  final String? mobileNumber;
  @override
  final String? faxNumber;
  @override
  final String clientName;
  @override
  final String? clientEmail;
  @override
  final String? address3;
  @override
  final String? address2;
  @override
  final String? address1;

  @override
  String toString() {
    return 'InvoicesEvent.addsClient(invoiceId: $invoiceId, phoneNumber: $phoneNumber, mobileNumber: $mobileNumber, faxNumber: $faxNumber, clientName: $clientName, clientEmail: $clientEmail, address3: $address3, address2: $address2, address1: $address1)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addsClientImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.mobileNumber, mobileNumber) ||
                other.mobileNumber == mobileNumber) &&
            (identical(other.faxNumber, faxNumber) ||
                other.faxNumber == faxNumber) &&
            (identical(other.clientName, clientName) ||
                other.clientName == clientName) &&
            (identical(other.clientEmail, clientEmail) ||
                other.clientEmail == clientEmail) &&
            (identical(other.address3, address3) ||
                other.address3 == address3) &&
            (identical(other.address2, address2) ||
                other.address2 == address2) &&
            (identical(other.address1, address1) ||
                other.address1 == address1));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      invoiceId,
      phoneNumber,
      mobileNumber,
      faxNumber,
      clientName,
      clientEmail,
      address3,
      address2,
      address1);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addsClientImplCopyWith<_$addsClientImpl> get copyWith =>
      __$$addsClientImplCopyWithImpl<_$addsClientImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addsClient(invoiceId, phoneNumber, mobileNumber, faxNumber,
        clientName, clientEmail, address3, address2, address1);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addsClient?.call(invoiceId, phoneNumber, mobileNumber, faxNumber,
        clientName, clientEmail, address3, address2, address1);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addsClient != null) {
      return addsClient(invoiceId, phoneNumber, mobileNumber, faxNumber,
          clientName, clientEmail, address3, address2, address1);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addsClient(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addsClient?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addsClient != null) {
      return addsClient(this);
    }
    return orElse();
  }
}

abstract class addsClient implements InvoicesEvent {
  const factory addsClient(
      {required final String invoiceId,
      required final String? phoneNumber,
      required final String? mobileNumber,
      required final String? faxNumber,
      required final String clientName,
      required final String? clientEmail,
      required final String? address3,
      required final String? address2,
      required final String? address1}) = _$addsClientImpl;

  String get invoiceId;
  String? get phoneNumber;
  String? get mobileNumber;
  String? get faxNumber;
  String get clientName;
  String? get clientEmail;
  String? get address3;
  String? get address2;
  String? get address1;
  @JsonKey(ignore: true)
  _$$addsClientImplCopyWith<_$addsClientImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addItemImplCopyWith<$Res> {
  factory _$$addItemImplCopyWith(
          _$addItemImpl value, $Res Function(_$addItemImpl) then) =
      __$$addItemImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String invoiceId,
      double unitCost,
      String? unit,
      double? taxRate,
      int quantity,
      bool isTaxable,
      double? discountAmount,
      String description,
      String? discount,
      String? addiotionalDetails,
      bool isAddToMyItems,
      double total});
}

/// @nodoc
class __$$addItemImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addItemImpl>
    implements _$$addItemImplCopyWith<$Res> {
  __$$addItemImplCopyWithImpl(
      _$addItemImpl _value, $Res Function(_$addItemImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? unitCost = null,
    Object? unit = freezed,
    Object? taxRate = freezed,
    Object? quantity = null,
    Object? isTaxable = null,
    Object? discountAmount = freezed,
    Object? description = null,
    Object? discount = freezed,
    Object? addiotionalDetails = freezed,
    Object? isAddToMyItems = null,
    Object? total = null,
  }) {
    return _then(_$addItemImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      unitCost: null == unitCost
          ? _value.unitCost
          : unitCost // ignore: cast_nullable_to_non_nullable
              as double,
      unit: freezed == unit
          ? _value.unit
          : unit // ignore: cast_nullable_to_non_nullable
              as String?,
      taxRate: freezed == taxRate
          ? _value.taxRate
          : taxRate // ignore: cast_nullable_to_non_nullable
              as double?,
      quantity: null == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      isTaxable: null == isTaxable
          ? _value.isTaxable
          : isTaxable // ignore: cast_nullable_to_non_nullable
              as bool,
      discountAmount: freezed == discountAmount
          ? _value.discountAmount
          : discountAmount // ignore: cast_nullable_to_non_nullable
              as double?,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      discount: freezed == discount
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as String?,
      addiotionalDetails: freezed == addiotionalDetails
          ? _value.addiotionalDetails
          : addiotionalDetails // ignore: cast_nullable_to_non_nullable
              as String?,
      isAddToMyItems: null == isAddToMyItems
          ? _value.isAddToMyItems
          : isAddToMyItems // ignore: cast_nullable_to_non_nullable
              as bool,
      total: null == total
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$addItemImpl implements addItem {
  const _$addItemImpl(
      {required this.invoiceId,
      required this.unitCost,
      required this.unit,
      required this.taxRate,
      required this.quantity,
      required this.isTaxable,
      required this.discountAmount,
      required this.description,
      required this.discount,
      required this.addiotionalDetails,
      required this.isAddToMyItems,
      required this.total});

  @override
  final String invoiceId;
  @override
  final double unitCost;
  @override
  final String? unit;
  @override
  final double? taxRate;
  @override
  final int quantity;
  @override
  final bool isTaxable;
  @override
  final double? discountAmount;
  @override
  final String description;
  @override
  final String? discount;
  @override
  final String? addiotionalDetails;
  @override
  final bool isAddToMyItems;
  @override
  final double total;

  @override
  String toString() {
    return 'InvoicesEvent.addItem(invoiceId: $invoiceId, unitCost: $unitCost, unit: $unit, taxRate: $taxRate, quantity: $quantity, isTaxable: $isTaxable, discountAmount: $discountAmount, description: $description, discount: $discount, addiotionalDetails: $addiotionalDetails, isAddToMyItems: $isAddToMyItems, total: $total)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addItemImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.unitCost, unitCost) ||
                other.unitCost == unitCost) &&
            (identical(other.unit, unit) || other.unit == unit) &&
            (identical(other.taxRate, taxRate) || other.taxRate == taxRate) &&
            (identical(other.quantity, quantity) ||
                other.quantity == quantity) &&
            (identical(other.isTaxable, isTaxable) ||
                other.isTaxable == isTaxable) &&
            (identical(other.discountAmount, discountAmount) ||
                other.discountAmount == discountAmount) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.discount, discount) ||
                other.discount == discount) &&
            (identical(other.addiotionalDetails, addiotionalDetails) ||
                other.addiotionalDetails == addiotionalDetails) &&
            (identical(other.isAddToMyItems, isAddToMyItems) ||
                other.isAddToMyItems == isAddToMyItems) &&
            (identical(other.total, total) || other.total == total));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      invoiceId,
      unitCost,
      unit,
      taxRate,
      quantity,
      isTaxable,
      discountAmount,
      description,
      discount,
      addiotionalDetails,
      isAddToMyItems,
      total);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addItemImplCopyWith<_$addItemImpl> get copyWith =>
      __$$addItemImplCopyWithImpl<_$addItemImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addItem(
        invoiceId,
        unitCost,
        unit,
        taxRate,
        quantity,
        isTaxable,
        discountAmount,
        description,
        discount,
        addiotionalDetails,
        isAddToMyItems,
        total);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addItem?.call(
        invoiceId,
        unitCost,
        unit,
        taxRate,
        quantity,
        isTaxable,
        discountAmount,
        description,
        discount,
        addiotionalDetails,
        isAddToMyItems,
        total);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addItem != null) {
      return addItem(
          invoiceId,
          unitCost,
          unit,
          taxRate,
          quantity,
          isTaxable,
          discountAmount,
          description,
          discount,
          addiotionalDetails,
          isAddToMyItems,
          total);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addItem(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addItem?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addItem != null) {
      return addItem(this);
    }
    return orElse();
  }
}

abstract class addItem implements InvoicesEvent {
  const factory addItem(
      {required final String invoiceId,
      required final double unitCost,
      required final String? unit,
      required final double? taxRate,
      required final int quantity,
      required final bool isTaxable,
      required final double? discountAmount,
      required final String description,
      required final String? discount,
      required final String? addiotionalDetails,
      required final bool isAddToMyItems,
      required final double total}) = _$addItemImpl;

  String get invoiceId;
  double get unitCost;
  String? get unit;
  double? get taxRate;
  int get quantity;
  bool get isTaxable;
  double? get discountAmount;
  String get description;
  String? get discount;
  String? get addiotionalDetails;
  bool get isAddToMyItems;
  double get total;
  @JsonKey(ignore: true)
  _$$addItemImplCopyWith<_$addItemImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addTaxDetailImplCopyWith<$Res> {
  factory _$$addTaxDetailImplCopyWith(
          _$addTaxDetailImpl value, $Res Function(_$addTaxDetailImpl) then) =
      __$$addTaxDetailImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String invoiceId,
      double? rate,
      String label,
      String taxType,
      bool isInclusive});
}

/// @nodoc
class __$$addTaxDetailImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addTaxDetailImpl>
    implements _$$addTaxDetailImplCopyWith<$Res> {
  __$$addTaxDetailImplCopyWithImpl(
      _$addTaxDetailImpl _value, $Res Function(_$addTaxDetailImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? rate = freezed,
    Object? label = null,
    Object? taxType = null,
    Object? isInclusive = null,
  }) {
    return _then(_$addTaxDetailImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      rate: freezed == rate
          ? _value.rate
          : rate // ignore: cast_nullable_to_non_nullable
              as double?,
      label: null == label
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
      taxType: null == taxType
          ? _value.taxType
          : taxType // ignore: cast_nullable_to_non_nullable
              as String,
      isInclusive: null == isInclusive
          ? _value.isInclusive
          : isInclusive // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$addTaxDetailImpl implements addTaxDetail {
  const _$addTaxDetailImpl(
      {required this.invoiceId,
      required this.rate,
      required this.label,
      required this.taxType,
      required this.isInclusive});

  @override
  final String invoiceId;
  @override
  final double? rate;
  @override
  final String label;
  @override
  final String taxType;
  @override
  final bool isInclusive;

  @override
  String toString() {
    return 'InvoicesEvent.addTaxDetail(invoiceId: $invoiceId, rate: $rate, label: $label, taxType: $taxType, isInclusive: $isInclusive)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addTaxDetailImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.rate, rate) || other.rate == rate) &&
            (identical(other.label, label) || other.label == label) &&
            (identical(other.taxType, taxType) || other.taxType == taxType) &&
            (identical(other.isInclusive, isInclusive) ||
                other.isInclusive == isInclusive));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, invoiceId, rate, label, taxType, isInclusive);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addTaxDetailImplCopyWith<_$addTaxDetailImpl> get copyWith =>
      __$$addTaxDetailImplCopyWithImpl<_$addTaxDetailImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addTaxDetail(invoiceId, rate, label, taxType, isInclusive);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addTaxDetail?.call(invoiceId, rate, label, taxType, isInclusive);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addTaxDetail != null) {
      return addTaxDetail(invoiceId, rate, label, taxType, isInclusive);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addTaxDetail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addTaxDetail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addTaxDetail != null) {
      return addTaxDetail(this);
    }
    return orElse();
  }
}

abstract class addTaxDetail implements InvoicesEvent {
  const factory addTaxDetail(
      {required final String invoiceId,
      required final double? rate,
      required final String label,
      required final String taxType,
      required final bool isInclusive}) = _$addTaxDetailImpl;

  String get invoiceId;
  double? get rate;
  String get label;
  String get taxType;
  bool get isInclusive;
  @JsonKey(ignore: true)
  _$$addTaxDetailImplCopyWith<_$addTaxDetailImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addNoteImplCopyWith<$Res> {
  factory _$$addNoteImplCopyWith(
          _$addNoteImpl value, $Res Function(_$addNoteImpl) then) =
      __$$addNoteImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String notes, String invoiceId, bool isDefault});
}

/// @nodoc
class __$$addNoteImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addNoteImpl>
    implements _$$addNoteImplCopyWith<$Res> {
  __$$addNoteImplCopyWithImpl(
      _$addNoteImpl _value, $Res Function(_$addNoteImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notes = null,
    Object? invoiceId = null,
    Object? isDefault = null,
  }) {
    return _then(_$addNoteImpl(
      notes: null == notes
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String,
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      isDefault: null == isDefault
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$addNoteImpl implements addNote {
  const _$addNoteImpl(
      {required this.notes, required this.invoiceId, required this.isDefault});

  @override
  final String notes;
  @override
  final String invoiceId;
  @override
  final bool isDefault;

  @override
  String toString() {
    return 'InvoicesEvent.addNote(notes: $notes, invoiceId: $invoiceId, isDefault: $isDefault)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addNoteImpl &&
            (identical(other.notes, notes) || other.notes == notes) &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.isDefault, isDefault) ||
                other.isDefault == isDefault));
  }

  @override
  int get hashCode => Object.hash(runtimeType, notes, invoiceId, isDefault);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addNoteImplCopyWith<_$addNoteImpl> get copyWith =>
      __$$addNoteImplCopyWithImpl<_$addNoteImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addNote(notes, invoiceId, isDefault);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addNote?.call(notes, invoiceId, isDefault);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addNote != null) {
      return addNote(notes, invoiceId, isDefault);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addNote(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addNote?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addNote != null) {
      return addNote(this);
    }
    return orElse();
  }
}

abstract class addNote implements InvoicesEvent {
  const factory addNote(
      {required final String notes,
      required final String invoiceId,
      required final bool isDefault}) = _$addNoteImpl;

  String get notes;
  String get invoiceId;
  bool get isDefault;
  @JsonKey(ignore: true)
  _$$addNoteImplCopyWith<_$addNoteImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addsSignatureImplCopyWith<$Res> {
  factory _$$addsSignatureImplCopyWith(
          _$addsSignatureImpl value, $Res Function(_$addsSignatureImpl) then) =
      __$$addsSignatureImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceId, File signature});
}

/// @nodoc
class __$$addsSignatureImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addsSignatureImpl>
    implements _$$addsSignatureImplCopyWith<$Res> {
  __$$addsSignatureImplCopyWithImpl(
      _$addsSignatureImpl _value, $Res Function(_$addsSignatureImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? signature = null,
  }) {
    return _then(_$addsSignatureImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      signature: null == signature
          ? _value.signature
          : signature // ignore: cast_nullable_to_non_nullable
              as File,
    ));
  }
}

/// @nodoc

class _$addsSignatureImpl implements addsSignature {
  const _$addsSignatureImpl({required this.invoiceId, required this.signature});

  @override
  final String invoiceId;
  @override
  final File signature;

  @override
  String toString() {
    return 'InvoicesEvent.addsSignature(invoiceId: $invoiceId, signature: $signature)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addsSignatureImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.signature, signature) ||
                other.signature == signature));
  }

  @override
  int get hashCode => Object.hash(runtimeType, invoiceId, signature);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addsSignatureImplCopyWith<_$addsSignatureImpl> get copyWith =>
      __$$addsSignatureImplCopyWithImpl<_$addsSignatureImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addsSignature(invoiceId, signature);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addsSignature?.call(invoiceId, signature);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addsSignature != null) {
      return addsSignature(invoiceId, signature);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addsSignature(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addsSignature?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addsSignature != null) {
      return addsSignature(this);
    }
    return orElse();
  }
}

abstract class addsSignature implements InvoicesEvent {
  const factory addsSignature(
      {required final String invoiceId,
      required final File signature}) = _$addsSignatureImpl;

  String get invoiceId;
  File get signature;
  @JsonKey(ignore: true)
  _$$addsSignatureImplCopyWith<_$addsSignatureImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addDiscountDetailImplCopyWith<$Res> {
  factory _$$addDiscountDetailImplCopyWith(_$addDiscountDetailImpl value,
          $Res Function(_$addDiscountDetailImpl) then) =
      __$$addDiscountDetailImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceId, String discountType, double percentage});
}

/// @nodoc
class __$$addDiscountDetailImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addDiscountDetailImpl>
    implements _$$addDiscountDetailImplCopyWith<$Res> {
  __$$addDiscountDetailImplCopyWithImpl(_$addDiscountDetailImpl _value,
      $Res Function(_$addDiscountDetailImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? discountType = null,
    Object? percentage = null,
  }) {
    return _then(_$addDiscountDetailImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      discountType: null == discountType
          ? _value.discountType
          : discountType // ignore: cast_nullable_to_non_nullable
              as String,
      percentage: null == percentage
          ? _value.percentage
          : percentage // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$addDiscountDetailImpl implements addDiscountDetail {
  const _$addDiscountDetailImpl(
      {required this.invoiceId,
      required this.discountType,
      required this.percentage});

  @override
  final String invoiceId;
  @override
  final String discountType;
  @override
  final double percentage;

  @override
  String toString() {
    return 'InvoicesEvent.addDiscountDetail(invoiceId: $invoiceId, discountType: $discountType, percentage: $percentage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addDiscountDetailImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.discountType, discountType) ||
                other.discountType == discountType) &&
            (identical(other.percentage, percentage) ||
                other.percentage == percentage));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, invoiceId, discountType, percentage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addDiscountDetailImplCopyWith<_$addDiscountDetailImpl> get copyWith =>
      __$$addDiscountDetailImplCopyWithImpl<_$addDiscountDetailImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addDiscountDetail(invoiceId, discountType, percentage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addDiscountDetail?.call(invoiceId, discountType, percentage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addDiscountDetail != null) {
      return addDiscountDetail(invoiceId, discountType, percentage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addDiscountDetail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addDiscountDetail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addDiscountDetail != null) {
      return addDiscountDetail(this);
    }
    return orElse();
  }
}

abstract class addDiscountDetail implements InvoicesEvent {
  const factory addDiscountDetail(
      {required final String invoiceId,
      required final String discountType,
      required final double percentage}) = _$addDiscountDetailImpl;

  String get invoiceId;
  String get discountType;
  double get percentage;
  @JsonKey(ignore: true)
  _$$addDiscountDetailImplCopyWith<_$addDiscountDetailImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$getInvoicesDetailsImplCopyWith<$Res> {
  factory _$$getInvoicesDetailsImplCopyWith(_$getInvoicesDetailsImpl value,
          $Res Function(_$getInvoicesDetailsImpl) then) =
      __$$getInvoicesDetailsImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceId});
}

/// @nodoc
class __$$getInvoicesDetailsImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$getInvoicesDetailsImpl>
    implements _$$getInvoicesDetailsImplCopyWith<$Res> {
  __$$getInvoicesDetailsImplCopyWithImpl(_$getInvoicesDetailsImpl _value,
      $Res Function(_$getInvoicesDetailsImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
  }) {
    return _then(_$getInvoicesDetailsImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$getInvoicesDetailsImpl implements getInvoicesDetails {
  const _$getInvoicesDetailsImpl({required this.invoiceId});

  @override
  final String invoiceId;

  @override
  String toString() {
    return 'InvoicesEvent.getInvoicesDetails(invoiceId: $invoiceId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$getInvoicesDetailsImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, invoiceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$getInvoicesDetailsImplCopyWith<_$getInvoicesDetailsImpl> get copyWith =>
      __$$getInvoicesDetailsImplCopyWithImpl<_$getInvoicesDetailsImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return getInvoicesDetails(invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return getInvoicesDetails?.call(invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (getInvoicesDetails != null) {
      return getInvoicesDetails(invoiceId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return getInvoicesDetails(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return getInvoicesDetails?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (getInvoicesDetails != null) {
      return getInvoicesDetails(this);
    }
    return orElse();
  }
}

abstract class getInvoicesDetails implements InvoicesEvent {
  const factory getInvoicesDetails({required final String invoiceId}) =
      _$getInvoicesDetailsImpl;

  String get invoiceId;
  @JsonKey(ignore: true)
  _$$getInvoicesDetailsImplCopyWith<_$getInvoicesDetailsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$editItemImplCopyWith<$Res> {
  factory _$$editItemImplCopyWith(
          _$editItemImpl value, $Res Function(_$editItemImpl) then) =
      __$$editItemImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String itemId,
      String invoiceId,
      double unitCost,
      String? unit,
      double? taxRate,
      int quantity,
      bool isTaxable,
      double? discountAmount,
      String description,
      String? discount,
      String? addiotionalDetails,
      bool isAddToMyItems,
      double total});
}

/// @nodoc
class __$$editItemImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$editItemImpl>
    implements _$$editItemImplCopyWith<$Res> {
  __$$editItemImplCopyWithImpl(
      _$editItemImpl _value, $Res Function(_$editItemImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemId = null,
    Object? invoiceId = null,
    Object? unitCost = null,
    Object? unit = freezed,
    Object? taxRate = freezed,
    Object? quantity = null,
    Object? isTaxable = null,
    Object? discountAmount = freezed,
    Object? description = null,
    Object? discount = freezed,
    Object? addiotionalDetails = freezed,
    Object? isAddToMyItems = null,
    Object? total = null,
  }) {
    return _then(_$editItemImpl(
      itemId: null == itemId
          ? _value.itemId
          : itemId // ignore: cast_nullable_to_non_nullable
              as String,
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      unitCost: null == unitCost
          ? _value.unitCost
          : unitCost // ignore: cast_nullable_to_non_nullable
              as double,
      unit: freezed == unit
          ? _value.unit
          : unit // ignore: cast_nullable_to_non_nullable
              as String?,
      taxRate: freezed == taxRate
          ? _value.taxRate
          : taxRate // ignore: cast_nullable_to_non_nullable
              as double?,
      quantity: null == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      isTaxable: null == isTaxable
          ? _value.isTaxable
          : isTaxable // ignore: cast_nullable_to_non_nullable
              as bool,
      discountAmount: freezed == discountAmount
          ? _value.discountAmount
          : discountAmount // ignore: cast_nullable_to_non_nullable
              as double?,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      discount: freezed == discount
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as String?,
      addiotionalDetails: freezed == addiotionalDetails
          ? _value.addiotionalDetails
          : addiotionalDetails // ignore: cast_nullable_to_non_nullable
              as String?,
      isAddToMyItems: null == isAddToMyItems
          ? _value.isAddToMyItems
          : isAddToMyItems // ignore: cast_nullable_to_non_nullable
              as bool,
      total: null == total
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$editItemImpl implements editItem {
  const _$editItemImpl(
      {required this.itemId,
      required this.invoiceId,
      required this.unitCost,
      required this.unit,
      required this.taxRate,
      required this.quantity,
      required this.isTaxable,
      required this.discountAmount,
      required this.description,
      required this.discount,
      required this.addiotionalDetails,
      required this.isAddToMyItems,
      required this.total});

  @override
  final String itemId;
  @override
  final String invoiceId;
  @override
  final double unitCost;
  @override
  final String? unit;
  @override
  final double? taxRate;
  @override
  final int quantity;
  @override
  final bool isTaxable;
  @override
  final double? discountAmount;
  @override
  final String description;
  @override
  final String? discount;
  @override
  final String? addiotionalDetails;
  @override
  final bool isAddToMyItems;
  @override
  final double total;

  @override
  String toString() {
    return 'InvoicesEvent.editItem(itemId: $itemId, invoiceId: $invoiceId, unitCost: $unitCost, unit: $unit, taxRate: $taxRate, quantity: $quantity, isTaxable: $isTaxable, discountAmount: $discountAmount, description: $description, discount: $discount, addiotionalDetails: $addiotionalDetails, isAddToMyItems: $isAddToMyItems, total: $total)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$editItemImpl &&
            (identical(other.itemId, itemId) || other.itemId == itemId) &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.unitCost, unitCost) ||
                other.unitCost == unitCost) &&
            (identical(other.unit, unit) || other.unit == unit) &&
            (identical(other.taxRate, taxRate) || other.taxRate == taxRate) &&
            (identical(other.quantity, quantity) ||
                other.quantity == quantity) &&
            (identical(other.isTaxable, isTaxable) ||
                other.isTaxable == isTaxable) &&
            (identical(other.discountAmount, discountAmount) ||
                other.discountAmount == discountAmount) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.discount, discount) ||
                other.discount == discount) &&
            (identical(other.addiotionalDetails, addiotionalDetails) ||
                other.addiotionalDetails == addiotionalDetails) &&
            (identical(other.isAddToMyItems, isAddToMyItems) ||
                other.isAddToMyItems == isAddToMyItems) &&
            (identical(other.total, total) || other.total == total));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      itemId,
      invoiceId,
      unitCost,
      unit,
      taxRate,
      quantity,
      isTaxable,
      discountAmount,
      description,
      discount,
      addiotionalDetails,
      isAddToMyItems,
      total);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$editItemImplCopyWith<_$editItemImpl> get copyWith =>
      __$$editItemImplCopyWithImpl<_$editItemImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return editItem(
        itemId,
        invoiceId,
        unitCost,
        unit,
        taxRate,
        quantity,
        isTaxable,
        discountAmount,
        description,
        discount,
        addiotionalDetails,
        isAddToMyItems,
        total);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return editItem?.call(
        itemId,
        invoiceId,
        unitCost,
        unit,
        taxRate,
        quantity,
        isTaxable,
        discountAmount,
        description,
        discount,
        addiotionalDetails,
        isAddToMyItems,
        total);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (editItem != null) {
      return editItem(
          itemId,
          invoiceId,
          unitCost,
          unit,
          taxRate,
          quantity,
          isTaxable,
          discountAmount,
          description,
          discount,
          addiotionalDetails,
          isAddToMyItems,
          total);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return editItem(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return editItem?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (editItem != null) {
      return editItem(this);
    }
    return orElse();
  }
}

abstract class editItem implements InvoicesEvent {
  const factory editItem(
      {required final String itemId,
      required final String invoiceId,
      required final double unitCost,
      required final String? unit,
      required final double? taxRate,
      required final int quantity,
      required final bool isTaxable,
      required final double? discountAmount,
      required final String description,
      required final String? discount,
      required final String? addiotionalDetails,
      required final bool isAddToMyItems,
      required final double total}) = _$editItemImpl;

  String get itemId;
  String get invoiceId;
  double get unitCost;
  String? get unit;
  double? get taxRate;
  int get quantity;
  bool get isTaxable;
  double? get discountAmount;
  String get description;
  String? get discount;
  String? get addiotionalDetails;
  bool get isAddToMyItems;
  double get total;
  @JsonKey(ignore: true)
  _$$editItemImplCopyWith<_$editItemImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$editPaymentImplCopyWith<$Res> {
  factory _$$editPaymentImplCopyWith(
          _$editPaymentImpl value, $Res Function(_$editPaymentImpl) then) =
      __$$editPaymentImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String invoiceId,
      double amount,
      DateTime date,
      String notes,
      String paymentMethod,
      String paymentId});
}

/// @nodoc
class __$$editPaymentImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$editPaymentImpl>
    implements _$$editPaymentImplCopyWith<$Res> {
  __$$editPaymentImplCopyWithImpl(
      _$editPaymentImpl _value, $Res Function(_$editPaymentImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? amount = null,
    Object? date = null,
    Object? notes = null,
    Object? paymentMethod = null,
    Object? paymentId = null,
  }) {
    return _then(_$editPaymentImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as double,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      notes: null == notes
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String,
      paymentMethod: null == paymentMethod
          ? _value.paymentMethod
          : paymentMethod // ignore: cast_nullable_to_non_nullable
              as String,
      paymentId: null == paymentId
          ? _value.paymentId
          : paymentId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$editPaymentImpl implements editPayment {
  const _$editPaymentImpl(
      {required this.invoiceId,
      required this.amount,
      required this.date,
      required this.notes,
      required this.paymentMethod,
      required this.paymentId});

  @override
  final String invoiceId;
  @override
  final double amount;
  @override
  final DateTime date;
  @override
  final String notes;
  @override
  final String paymentMethod;
  @override
  final String paymentId;

  @override
  String toString() {
    return 'InvoicesEvent.editPayment(invoiceId: $invoiceId, amount: $amount, date: $date, notes: $notes, paymentMethod: $paymentMethod, paymentId: $paymentId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$editPaymentImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.notes, notes) || other.notes == notes) &&
            (identical(other.paymentMethod, paymentMethod) ||
                other.paymentMethod == paymentMethod) &&
            (identical(other.paymentId, paymentId) ||
                other.paymentId == paymentId));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, invoiceId, amount, date, notes, paymentMethod, paymentId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$editPaymentImplCopyWith<_$editPaymentImpl> get copyWith =>
      __$$editPaymentImplCopyWithImpl<_$editPaymentImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return editPayment(
        invoiceId, amount, date, notes, paymentMethod, paymentId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return editPayment?.call(
        invoiceId, amount, date, notes, paymentMethod, paymentId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (editPayment != null) {
      return editPayment(
          invoiceId, amount, date, notes, paymentMethod, paymentId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return editPayment(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return editPayment?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (editPayment != null) {
      return editPayment(this);
    }
    return orElse();
  }
}

abstract class editPayment implements InvoicesEvent {
  const factory editPayment(
      {required final String invoiceId,
      required final double amount,
      required final DateTime date,
      required final String notes,
      required final String paymentMethod,
      required final String paymentId}) = _$editPaymentImpl;

  String get invoiceId;
  double get amount;
  DateTime get date;
  String get notes;
  String get paymentMethod;
  String get paymentId;
  @JsonKey(ignore: true)
  _$$editPaymentImplCopyWith<_$editPaymentImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$editsPhotoImplCopyWith<$Res> {
  factory _$$editsPhotoImplCopyWith(
          _$editsPhotoImpl value, $Res Function(_$editsPhotoImpl) then) =
      __$$editsPhotoImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String invoiceId,
      File photo,
      String? description,
      String? addionalDetails,
      String photoId});
}

/// @nodoc
class __$$editsPhotoImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$editsPhotoImpl>
    implements _$$editsPhotoImplCopyWith<$Res> {
  __$$editsPhotoImplCopyWithImpl(
      _$editsPhotoImpl _value, $Res Function(_$editsPhotoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? photo = null,
    Object? description = freezed,
    Object? addionalDetails = freezed,
    Object? photoId = null,
  }) {
    return _then(_$editsPhotoImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      photo: null == photo
          ? _value.photo
          : photo // ignore: cast_nullable_to_non_nullable
              as File,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      addionalDetails: freezed == addionalDetails
          ? _value.addionalDetails
          : addionalDetails // ignore: cast_nullable_to_non_nullable
              as String?,
      photoId: null == photoId
          ? _value.photoId
          : photoId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$editsPhotoImpl implements editsPhoto {
  const _$editsPhotoImpl(
      {required this.invoiceId,
      required this.photo,
      required this.description,
      required this.addionalDetails,
      required this.photoId});

  @override
  final String invoiceId;
  @override
  final File photo;
  @override
  final String? description;
  @override
  final String? addionalDetails;
  @override
  final String photoId;

  @override
  String toString() {
    return 'InvoicesEvent.editsPhoto(invoiceId: $invoiceId, photo: $photo, description: $description, addionalDetails: $addionalDetails, photoId: $photoId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$editsPhotoImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.photo, photo) || other.photo == photo) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.addionalDetails, addionalDetails) ||
                other.addionalDetails == addionalDetails) &&
            (identical(other.photoId, photoId) || other.photoId == photoId));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, invoiceId, photo, description, addionalDetails, photoId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$editsPhotoImplCopyWith<_$editsPhotoImpl> get copyWith =>
      __$$editsPhotoImplCopyWithImpl<_$editsPhotoImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return editsPhoto(invoiceId, photo, description, addionalDetails, photoId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return editsPhoto?.call(
        invoiceId, photo, description, addionalDetails, photoId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (editsPhoto != null) {
      return editsPhoto(
          invoiceId, photo, description, addionalDetails, photoId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return editsPhoto(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return editsPhoto?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (editsPhoto != null) {
      return editsPhoto(this);
    }
    return orElse();
  }
}

abstract class editsPhoto implements InvoicesEvent {
  const factory editsPhoto(
      {required final String invoiceId,
      required final File photo,
      required final String? description,
      required final String? addionalDetails,
      required final String photoId}) = _$editsPhotoImpl;

  String get invoiceId;
  File get photo;
  String? get description;
  String? get addionalDetails;
  String get photoId;
  @JsonKey(ignore: true)
  _$$editsPhotoImplCopyWith<_$editsPhotoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addReviewsImplCopyWith<$Res> {
  factory _$$addReviewsImplCopyWith(
          _$addReviewsImpl value, $Res Function(_$addReviewsImpl) then) =
      __$$addReviewsImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceId, String reviewLink, bool isToReview});
}

/// @nodoc
class __$$addReviewsImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addReviewsImpl>
    implements _$$addReviewsImplCopyWith<$Res> {
  __$$addReviewsImplCopyWithImpl(
      _$addReviewsImpl _value, $Res Function(_$addReviewsImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? reviewLink = null,
    Object? isToReview = null,
  }) {
    return _then(_$addReviewsImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      reviewLink: null == reviewLink
          ? _value.reviewLink
          : reviewLink // ignore: cast_nullable_to_non_nullable
              as String,
      isToReview: null == isToReview
          ? _value.isToReview
          : isToReview // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$addReviewsImpl implements addReviews {
  const _$addReviewsImpl(
      {required this.invoiceId,
      required this.reviewLink,
      required this.isToReview});

  @override
  final String invoiceId;
  @override
  final String reviewLink;
  @override
  final bool isToReview;

  @override
  String toString() {
    return 'InvoicesEvent.addReviews(invoiceId: $invoiceId, reviewLink: $reviewLink, isToReview: $isToReview)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addReviewsImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.reviewLink, reviewLink) ||
                other.reviewLink == reviewLink) &&
            (identical(other.isToReview, isToReview) ||
                other.isToReview == isToReview));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, invoiceId, reviewLink, isToReview);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addReviewsImplCopyWith<_$addReviewsImpl> get copyWith =>
      __$$addReviewsImplCopyWithImpl<_$addReviewsImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addReviews(invoiceId, reviewLink, isToReview);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addReviews?.call(invoiceId, reviewLink, isToReview);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addReviews != null) {
      return addReviews(invoiceId, reviewLink, isToReview);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addReviews(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addReviews?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addReviews != null) {
      return addReviews(this);
    }
    return orElse();
  }
}

abstract class addReviews implements InvoicesEvent {
  const factory addReviews(
      {required final String invoiceId,
      required final String reviewLink,
      required final bool isToReview}) = _$addReviewsImpl;

  String get invoiceId;
  String get reviewLink;
  bool get isToReview;
  @JsonKey(ignore: true)
  _$$addReviewsImplCopyWith<_$addReviewsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$deleteItemImplCopyWith<$Res> {
  factory _$$deleteItemImplCopyWith(
          _$deleteItemImpl value, $Res Function(_$deleteItemImpl) then) =
      __$$deleteItemImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String itemId, String invoiceId});
}

/// @nodoc
class __$$deleteItemImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$deleteItemImpl>
    implements _$$deleteItemImplCopyWith<$Res> {
  __$$deleteItemImplCopyWithImpl(
      _$deleteItemImpl _value, $Res Function(_$deleteItemImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemId = null,
    Object? invoiceId = null,
  }) {
    return _then(_$deleteItemImpl(
      itemId: null == itemId
          ? _value.itemId
          : itemId // ignore: cast_nullable_to_non_nullable
              as String,
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$deleteItemImpl implements deleteItem {
  const _$deleteItemImpl({required this.itemId, required this.invoiceId});

  @override
  final String itemId;
  @override
  final String invoiceId;

  @override
  String toString() {
    return 'InvoicesEvent.deleteItem(itemId: $itemId, invoiceId: $invoiceId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$deleteItemImpl &&
            (identical(other.itemId, itemId) || other.itemId == itemId) &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, itemId, invoiceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$deleteItemImplCopyWith<_$deleteItemImpl> get copyWith =>
      __$$deleteItemImplCopyWithImpl<_$deleteItemImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return deleteItem(itemId, invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return deleteItem?.call(itemId, invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (deleteItem != null) {
      return deleteItem(itemId, invoiceId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return deleteItem(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return deleteItem?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (deleteItem != null) {
      return deleteItem(this);
    }
    return orElse();
  }
}

abstract class deleteItem implements InvoicesEvent {
  const factory deleteItem(
      {required final String itemId,
      required final String invoiceId}) = _$deleteItemImpl;

  String get itemId;
  String get invoiceId;
  @JsonKey(ignore: true)
  _$$deleteItemImplCopyWith<_$deleteItemImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$deletePaymentImplCopyWith<$Res> {
  factory _$$deletePaymentImplCopyWith(
          _$deletePaymentImpl value, $Res Function(_$deletePaymentImpl) then) =
      __$$deletePaymentImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String paymentId, String invoiceId});
}

/// @nodoc
class __$$deletePaymentImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$deletePaymentImpl>
    implements _$$deletePaymentImplCopyWith<$Res> {
  __$$deletePaymentImplCopyWithImpl(
      _$deletePaymentImpl _value, $Res Function(_$deletePaymentImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? paymentId = null,
    Object? invoiceId = null,
  }) {
    return _then(_$deletePaymentImpl(
      paymentId: null == paymentId
          ? _value.paymentId
          : paymentId // ignore: cast_nullable_to_non_nullable
              as String,
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$deletePaymentImpl implements deletePayment {
  const _$deletePaymentImpl({required this.paymentId, required this.invoiceId});

  @override
  final String paymentId;
  @override
  final String invoiceId;

  @override
  String toString() {
    return 'InvoicesEvent.deletePayment(paymentId: $paymentId, invoiceId: $invoiceId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$deletePaymentImpl &&
            (identical(other.paymentId, paymentId) ||
                other.paymentId == paymentId) &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, paymentId, invoiceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$deletePaymentImplCopyWith<_$deletePaymentImpl> get copyWith =>
      __$$deletePaymentImplCopyWithImpl<_$deletePaymentImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return deletePayment(paymentId, invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return deletePayment?.call(paymentId, invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (deletePayment != null) {
      return deletePayment(paymentId, invoiceId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return deletePayment(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return deletePayment?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (deletePayment != null) {
      return deletePayment(this);
    }
    return orElse();
  }
}

abstract class deletePayment implements InvoicesEvent {
  const factory deletePayment(
      {required final String paymentId,
      required final String invoiceId}) = _$deletePaymentImpl;

  String get paymentId;
  String get invoiceId;
  @JsonKey(ignore: true)
  _$$deletePaymentImplCopyWith<_$deletePaymentImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$deletePhotosImplCopyWith<$Res> {
  factory _$$deletePhotosImplCopyWith(
          _$deletePhotosImpl value, $Res Function(_$deletePhotosImpl) then) =
      __$$deletePhotosImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String photoId, String invoiceId});
}

/// @nodoc
class __$$deletePhotosImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$deletePhotosImpl>
    implements _$$deletePhotosImplCopyWith<$Res> {
  __$$deletePhotosImplCopyWithImpl(
      _$deletePhotosImpl _value, $Res Function(_$deletePhotosImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? photoId = null,
    Object? invoiceId = null,
  }) {
    return _then(_$deletePhotosImpl(
      photoId: null == photoId
          ? _value.photoId
          : photoId // ignore: cast_nullable_to_non_nullable
              as String,
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$deletePhotosImpl implements deletePhotos {
  const _$deletePhotosImpl({required this.photoId, required this.invoiceId});

  @override
  final String photoId;
  @override
  final String invoiceId;

  @override
  String toString() {
    return 'InvoicesEvent.deletePhotos(photoId: $photoId, invoiceId: $invoiceId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$deletePhotosImpl &&
            (identical(other.photoId, photoId) || other.photoId == photoId) &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, photoId, invoiceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$deletePhotosImplCopyWith<_$deletePhotosImpl> get copyWith =>
      __$$deletePhotosImplCopyWithImpl<_$deletePhotosImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return deletePhotos(photoId, invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return deletePhotos?.call(photoId, invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (deletePhotos != null) {
      return deletePhotos(photoId, invoiceId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return deletePhotos(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return deletePhotos?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (deletePhotos != null) {
      return deletePhotos(this);
    }
    return orElse();
  }
}

abstract class deletePhotos implements InvoicesEvent {
  const factory deletePhotos(
      {required final String photoId,
      required final String invoiceId}) = _$deletePhotosImpl;

  String get photoId;
  String get invoiceId;
  @JsonKey(ignore: true)
  _$$deletePhotosImplCopyWith<_$deletePhotosImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$markAsPaidOrUnPaidImplCopyWith<$Res> {
  factory _$$markAsPaidOrUnPaidImplCopyWith(_$markAsPaidOrUnPaidImpl value,
          $Res Function(_$markAsPaidOrUnPaidImpl) then) =
      __$$markAsPaidOrUnPaidImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceId, String? paymentMethod, bool isPaid});
}

/// @nodoc
class __$$markAsPaidOrUnPaidImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$markAsPaidOrUnPaidImpl>
    implements _$$markAsPaidOrUnPaidImplCopyWith<$Res> {
  __$$markAsPaidOrUnPaidImplCopyWithImpl(_$markAsPaidOrUnPaidImpl _value,
      $Res Function(_$markAsPaidOrUnPaidImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? paymentMethod = freezed,
    Object? isPaid = null,
  }) {
    return _then(_$markAsPaidOrUnPaidImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      paymentMethod: freezed == paymentMethod
          ? _value.paymentMethod
          : paymentMethod // ignore: cast_nullable_to_non_nullable
              as String?,
      isPaid: null == isPaid
          ? _value.isPaid
          : isPaid // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$markAsPaidOrUnPaidImpl implements markAsPaidOrUnPaid {
  const _$markAsPaidOrUnPaidImpl(
      {required this.invoiceId,
      required this.paymentMethod,
      required this.isPaid});

  @override
  final String invoiceId;
  @override
  final String? paymentMethod;
  @override
  final bool isPaid;

  @override
  String toString() {
    return 'InvoicesEvent.markAsPaidOrUnPaid(invoiceId: $invoiceId, paymentMethod: $paymentMethod, isPaid: $isPaid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$markAsPaidOrUnPaidImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.paymentMethod, paymentMethod) ||
                other.paymentMethod == paymentMethod) &&
            (identical(other.isPaid, isPaid) || other.isPaid == isPaid));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, invoiceId, paymentMethod, isPaid);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$markAsPaidOrUnPaidImplCopyWith<_$markAsPaidOrUnPaidImpl> get copyWith =>
      __$$markAsPaidOrUnPaidImplCopyWithImpl<_$markAsPaidOrUnPaidImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return markAsPaidOrUnPaid(invoiceId, paymentMethod, isPaid);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return markAsPaidOrUnPaid?.call(invoiceId, paymentMethod, isPaid);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (markAsPaidOrUnPaid != null) {
      return markAsPaidOrUnPaid(invoiceId, paymentMethod, isPaid);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return markAsPaidOrUnPaid(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return markAsPaidOrUnPaid?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (markAsPaidOrUnPaid != null) {
      return markAsPaidOrUnPaid(this);
    }
    return orElse();
  }
}

abstract class markAsPaidOrUnPaid implements InvoicesEvent {
  const factory markAsPaidOrUnPaid(
      {required final String invoiceId,
      required final String? paymentMethod,
      required final bool isPaid}) = _$markAsPaidOrUnPaidImpl;

  String get invoiceId;
  String? get paymentMethod;
  bool get isPaid;
  @JsonKey(ignore: true)
  _$$markAsPaidOrUnPaidImplCopyWith<_$markAsPaidOrUnPaidImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$addBalanceDuesImplCopyWith<$Res> {
  factory _$$addBalanceDuesImplCopyWith(_$addBalanceDuesImpl value,
          $Res Function(_$addBalanceDuesImpl) then) =
      __$$addBalanceDuesImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceId, double balanceDue});
}

/// @nodoc
class __$$addBalanceDuesImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$addBalanceDuesImpl>
    implements _$$addBalanceDuesImplCopyWith<$Res> {
  __$$addBalanceDuesImplCopyWithImpl(
      _$addBalanceDuesImpl _value, $Res Function(_$addBalanceDuesImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? balanceDue = null,
  }) {
    return _then(_$addBalanceDuesImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
      balanceDue: null == balanceDue
          ? _value.balanceDue
          : balanceDue // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$addBalanceDuesImpl implements addBalanceDues {
  const _$addBalanceDuesImpl(
      {required this.invoiceId, required this.balanceDue});

  @override
  final String invoiceId;
  @override
  final double balanceDue;

  @override
  String toString() {
    return 'InvoicesEvent.addBalanceDues(invoiceId: $invoiceId, balanceDue: $balanceDue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addBalanceDuesImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.balanceDue, balanceDue) ||
                other.balanceDue == balanceDue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, invoiceId, balanceDue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addBalanceDuesImplCopyWith<_$addBalanceDuesImpl> get copyWith =>
      __$$addBalanceDuesImplCopyWithImpl<_$addBalanceDuesImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return addBalanceDues(invoiceId, balanceDue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return addBalanceDues?.call(invoiceId, balanceDue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addBalanceDues != null) {
      return addBalanceDues(invoiceId, balanceDue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return addBalanceDues(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return addBalanceDues?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (addBalanceDues != null) {
      return addBalanceDues(this);
    }
    return orElse();
  }
}

abstract class addBalanceDues implements InvoicesEvent {
  const factory addBalanceDues(
      {required final String invoiceId,
      required final double balanceDue}) = _$addBalanceDuesImpl;

  String get invoiceId;
  double get balanceDue;
  @JsonKey(ignore: true)
  _$$addBalanceDuesImplCopyWith<_$addBalanceDuesImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$createEstimatesNumberImplCopyWith<$Res> {
  factory _$$createEstimatesNumberImplCopyWith(
          _$createEstimatesNumberImpl value,
          $Res Function(_$createEstimatesNumberImpl) then) =
      __$$createEstimatesNumberImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String estimateNumber});
}

/// @nodoc
class __$$createEstimatesNumberImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$createEstimatesNumberImpl>
    implements _$$createEstimatesNumberImplCopyWith<$Res> {
  __$$createEstimatesNumberImplCopyWithImpl(_$createEstimatesNumberImpl _value,
      $Res Function(_$createEstimatesNumberImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? estimateNumber = null,
  }) {
    return _then(_$createEstimatesNumberImpl(
      estimateNumber: null == estimateNumber
          ? _value.estimateNumber
          : estimateNumber // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$createEstimatesNumberImpl implements createEstimatesNumber {
  const _$createEstimatesNumberImpl({required this.estimateNumber});

  @override
  final String estimateNumber;

  @override
  String toString() {
    return 'InvoicesEvent.createEstimatesNumber(estimateNumber: $estimateNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$createEstimatesNumberImpl &&
            (identical(other.estimateNumber, estimateNumber) ||
                other.estimateNumber == estimateNumber));
  }

  @override
  int get hashCode => Object.hash(runtimeType, estimateNumber);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$createEstimatesNumberImplCopyWith<_$createEstimatesNumberImpl>
      get copyWith => __$$createEstimatesNumberImplCopyWithImpl<
          _$createEstimatesNumberImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return createEstimatesNumber(estimateNumber);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return createEstimatesNumber?.call(estimateNumber);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (createEstimatesNumber != null) {
      return createEstimatesNumber(estimateNumber);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return createEstimatesNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return createEstimatesNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (createEstimatesNumber != null) {
      return createEstimatesNumber(this);
    }
    return orElse();
  }
}

abstract class createEstimatesNumber implements InvoicesEvent {
  const factory createEstimatesNumber({required final String estimateNumber}) =
      _$createEstimatesNumberImpl;

  String get estimateNumber;
  @JsonKey(ignore: true)
  _$$createEstimatesNumberImplCopyWith<_$createEstimatesNumberImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$convertingEstimateToInvoiceImplCopyWith<$Res> {
  factory _$$convertingEstimateToInvoiceImplCopyWith(
          _$convertingEstimateToInvoiceImpl value,
          $Res Function(_$convertingEstimateToInvoiceImpl) then) =
      __$$convertingEstimateToInvoiceImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceId});
}

/// @nodoc
class __$$convertingEstimateToInvoiceImplCopyWithImpl<$Res>
    extends _$InvoicesEventCopyWithImpl<$Res, _$convertingEstimateToInvoiceImpl>
    implements _$$convertingEstimateToInvoiceImplCopyWith<$Res> {
  __$$convertingEstimateToInvoiceImplCopyWithImpl(
      _$convertingEstimateToInvoiceImpl _value,
      $Res Function(_$convertingEstimateToInvoiceImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
  }) {
    return _then(_$convertingEstimateToInvoiceImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$convertingEstimateToInvoiceImpl implements convertingEstimateToInvoice {
  const _$convertingEstimateToInvoiceImpl({required this.invoiceId});

  @override
  final String invoiceId;

  @override
  String toString() {
    return 'InvoicesEvent.convertingEstimateToInvoice(invoiceId: $invoiceId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$convertingEstimateToInvoiceImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, invoiceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$convertingEstimateToInvoiceImplCopyWith<_$convertingEstimateToInvoiceImpl>
      get copyWith => __$$convertingEstimateToInvoiceImplCopyWithImpl<
          _$convertingEstimateToInvoiceImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String invoiceNumber) createInvoicesNumber,
    required TResult Function(String invoiceNumber, String invoiceId,
            DateTime date, String terms, DateTime? dueDate, String? poNumber)
        addInvoicesNumber,
    required TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)
        addBusinessDetail,
    required TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)
        addPaymentsInfo,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)
        addsPhoto,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)
        addPayment,
    required TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)
        addsClient,
    required TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        addItem,
    required TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)
        addTaxDetail,
    required TResult Function(String notes, String invoiceId, bool isDefault)
        addNote,
    required TResult Function(String invoiceId, File signature) addsSignature,
    required TResult Function(
            String invoiceId, String discountType, double percentage)
        addDiscountDetail,
    required TResult Function(String invoiceId) getInvoicesDetails,
    required TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)
        editItem,
    required TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)
        editPayment,
    required TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)
        editsPhoto,
    required TResult Function(
            String invoiceId, String reviewLink, bool isToReview)
        addReviews,
    required TResult Function(String itemId, String invoiceId) deleteItem,
    required TResult Function(String paymentId, String invoiceId) deletePayment,
    required TResult Function(String photoId, String invoiceId) deletePhotos,
    required TResult Function(
            String invoiceId, String? paymentMethod, bool isPaid)
        markAsPaidOrUnPaid,
    required TResult Function(String invoiceId, double balanceDue)
        addBalanceDues,
    required TResult Function(String estimateNumber) createEstimatesNumber,
    required TResult Function(String invoiceId) convertingEstimateToInvoice,
  }) {
    return convertingEstimateToInvoice(invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String invoiceNumber)? createInvoicesNumber,
    TResult? Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult? Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult? Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult? Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult? Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult? Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult? Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult? Function(String invoiceId, File signature)? addsSignature,
    TResult? Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult? Function(String invoiceId)? getInvoicesDetails,
    TResult? Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult? Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult? Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult? Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult? Function(String itemId, String invoiceId)? deleteItem,
    TResult? Function(String paymentId, String invoiceId)? deletePayment,
    TResult? Function(String photoId, String invoiceId)? deletePhotos,
    TResult? Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult? Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult? Function(String estimateNumber)? createEstimatesNumber,
    TResult? Function(String invoiceId)? convertingEstimateToInvoice,
  }) {
    return convertingEstimateToInvoice?.call(invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String invoiceNumber)? createInvoicesNumber,
    TResult Function(String invoiceNumber, String invoiceId, DateTime date,
            String terms, DateTime? dueDate, String? poNumber)?
        addInvoicesNumber,
    TResult Function(
            String businessName,
            String invoiceId,
            File? businessLogo,
            String? businessOwnerName,
            String? businessNumber,
            String? addressLine1,
            String? addressLine2,
            String? addressLine3,
            String? email,
            String? phone,
            String? mobile,
            String? website)?
        addBusinessDetail,
    TResult Function(
            String? additionalPaymentInstructions,
            String? businessName,
            String? paymentInstructions,
            String? paypalEmail,
            String invoiceId)?
        addPaymentsInfo,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails)?
        addsPhoto,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod)?
        addPayment,
    TResult Function(
            String invoiceId,
            String? phoneNumber,
            String? mobileNumber,
            String? faxNumber,
            String clientName,
            String? clientEmail,
            String? address3,
            String? address2,
            String? address1)?
        addsClient,
    TResult Function(
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        addItem,
    TResult Function(String invoiceId, double? rate, String label,
            String taxType, bool isInclusive)?
        addTaxDetail,
    TResult Function(String notes, String invoiceId, bool isDefault)? addNote,
    TResult Function(String invoiceId, File signature)? addsSignature,
    TResult Function(String invoiceId, String discountType, double percentage)?
        addDiscountDetail,
    TResult Function(String invoiceId)? getInvoicesDetails,
    TResult Function(
            String itemId,
            String invoiceId,
            double unitCost,
            String? unit,
            double? taxRate,
            int quantity,
            bool isTaxable,
            double? discountAmount,
            String description,
            String? discount,
            String? addiotionalDetails,
            bool isAddToMyItems,
            double total)?
        editItem,
    TResult Function(String invoiceId, double amount, DateTime date,
            String notes, String paymentMethod, String paymentId)?
        editPayment,
    TResult Function(String invoiceId, File photo, String? description,
            String? addionalDetails, String photoId)?
        editsPhoto,
    TResult Function(String invoiceId, String reviewLink, bool isToReview)?
        addReviews,
    TResult Function(String itemId, String invoiceId)? deleteItem,
    TResult Function(String paymentId, String invoiceId)? deletePayment,
    TResult Function(String photoId, String invoiceId)? deletePhotos,
    TResult Function(String invoiceId, String? paymentMethod, bool isPaid)?
        markAsPaidOrUnPaid,
    TResult Function(String invoiceId, double balanceDue)? addBalanceDues,
    TResult Function(String estimateNumber)? createEstimatesNumber,
    TResult Function(String invoiceId)? convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (convertingEstimateToInvoice != null) {
      return convertingEstimateToInvoice(invoiceId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(createInvoicesNumber value) createInvoicesNumber,
    required TResult Function(addInvoicesNumber value) addInvoicesNumber,
    required TResult Function(addBusinessDetail value) addBusinessDetail,
    required TResult Function(addPaymentsInfo value) addPaymentsInfo,
    required TResult Function(addsPhoto value) addsPhoto,
    required TResult Function(addPayment value) addPayment,
    required TResult Function(addsClient value) addsClient,
    required TResult Function(addItem value) addItem,
    required TResult Function(addTaxDetail value) addTaxDetail,
    required TResult Function(addNote value) addNote,
    required TResult Function(addsSignature value) addsSignature,
    required TResult Function(addDiscountDetail value) addDiscountDetail,
    required TResult Function(getInvoicesDetails value) getInvoicesDetails,
    required TResult Function(editItem value) editItem,
    required TResult Function(editPayment value) editPayment,
    required TResult Function(editsPhoto value) editsPhoto,
    required TResult Function(addReviews value) addReviews,
    required TResult Function(deleteItem value) deleteItem,
    required TResult Function(deletePayment value) deletePayment,
    required TResult Function(deletePhotos value) deletePhotos,
    required TResult Function(markAsPaidOrUnPaid value) markAsPaidOrUnPaid,
    required TResult Function(addBalanceDues value) addBalanceDues,
    required TResult Function(createEstimatesNumber value)
        createEstimatesNumber,
    required TResult Function(convertingEstimateToInvoice value)
        convertingEstimateToInvoice,
  }) {
    return convertingEstimateToInvoice(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult? Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult? Function(addBusinessDetail value)? addBusinessDetail,
    TResult? Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult? Function(addsPhoto value)? addsPhoto,
    TResult? Function(addPayment value)? addPayment,
    TResult? Function(addsClient value)? addsClient,
    TResult? Function(addItem value)? addItem,
    TResult? Function(addTaxDetail value)? addTaxDetail,
    TResult? Function(addNote value)? addNote,
    TResult? Function(addsSignature value)? addsSignature,
    TResult? Function(addDiscountDetail value)? addDiscountDetail,
    TResult? Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult? Function(editItem value)? editItem,
    TResult? Function(editPayment value)? editPayment,
    TResult? Function(editsPhoto value)? editsPhoto,
    TResult? Function(addReviews value)? addReviews,
    TResult? Function(deleteItem value)? deleteItem,
    TResult? Function(deletePayment value)? deletePayment,
    TResult? Function(deletePhotos value)? deletePhotos,
    TResult? Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult? Function(addBalanceDues value)? addBalanceDues,
    TResult? Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult? Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
  }) {
    return convertingEstimateToInvoice?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(createInvoicesNumber value)? createInvoicesNumber,
    TResult Function(addInvoicesNumber value)? addInvoicesNumber,
    TResult Function(addBusinessDetail value)? addBusinessDetail,
    TResult Function(addPaymentsInfo value)? addPaymentsInfo,
    TResult Function(addsPhoto value)? addsPhoto,
    TResult Function(addPayment value)? addPayment,
    TResult Function(addsClient value)? addsClient,
    TResult Function(addItem value)? addItem,
    TResult Function(addTaxDetail value)? addTaxDetail,
    TResult Function(addNote value)? addNote,
    TResult Function(addsSignature value)? addsSignature,
    TResult Function(addDiscountDetail value)? addDiscountDetail,
    TResult Function(getInvoicesDetails value)? getInvoicesDetails,
    TResult Function(editItem value)? editItem,
    TResult Function(editPayment value)? editPayment,
    TResult Function(editsPhoto value)? editsPhoto,
    TResult Function(addReviews value)? addReviews,
    TResult Function(deleteItem value)? deleteItem,
    TResult Function(deletePayment value)? deletePayment,
    TResult Function(deletePhotos value)? deletePhotos,
    TResult Function(markAsPaidOrUnPaid value)? markAsPaidOrUnPaid,
    TResult Function(addBalanceDues value)? addBalanceDues,
    TResult Function(createEstimatesNumber value)? createEstimatesNumber,
    TResult Function(convertingEstimateToInvoice value)?
        convertingEstimateToInvoice,
    required TResult orElse(),
  }) {
    if (convertingEstimateToInvoice != null) {
      return convertingEstimateToInvoice(this);
    }
    return orElse();
  }
}

abstract class convertingEstimateToInvoice implements InvoicesEvent {
  const factory convertingEstimateToInvoice({required final String invoiceId}) =
      _$convertingEstimateToInvoiceImpl;

  String get invoiceId;
  @JsonKey(ignore: true)
  _$$convertingEstimateToInvoiceImplCopyWith<_$convertingEstimateToInvoiceImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$InvoicesState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invoicesInitial,
    required TResult Function(String invoiceId) displayInvoiceId,
    required TResult Function(InvoiceEditDTO invoice) displayInvoice,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invoicesInitial,
    TResult? Function(String invoiceId)? displayInvoiceId,
    TResult? Function(InvoiceEditDTO invoice)? displayInvoice,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invoicesInitial,
    TResult Function(String invoiceId)? displayInvoiceId,
    TResult Function(InvoiceEditDTO invoice)? displayInvoice,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(invoicesInitial value) invoicesInitial,
    required TResult Function(displayInvoiceId value) displayInvoiceId,
    required TResult Function(displayInvoice value) displayInvoice,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(invoicesInitial value)? invoicesInitial,
    TResult? Function(displayInvoiceId value)? displayInvoiceId,
    TResult? Function(displayInvoice value)? displayInvoice,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(invoicesInitial value)? invoicesInitial,
    TResult Function(displayInvoiceId value)? displayInvoiceId,
    TResult Function(displayInvoice value)? displayInvoice,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvoicesStateCopyWith<$Res> {
  factory $InvoicesStateCopyWith(
          InvoicesState value, $Res Function(InvoicesState) then) =
      _$InvoicesStateCopyWithImpl<$Res, InvoicesState>;
}

/// @nodoc
class _$InvoicesStateCopyWithImpl<$Res, $Val extends InvoicesState>
    implements $InvoicesStateCopyWith<$Res> {
  _$InvoicesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$invoicesInitialImplCopyWith<$Res> {
  factory _$$invoicesInitialImplCopyWith(_$invoicesInitialImpl value,
          $Res Function(_$invoicesInitialImpl) then) =
      __$$invoicesInitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$invoicesInitialImplCopyWithImpl<$Res>
    extends _$InvoicesStateCopyWithImpl<$Res, _$invoicesInitialImpl>
    implements _$$invoicesInitialImplCopyWith<$Res> {
  __$$invoicesInitialImplCopyWithImpl(
      _$invoicesInitialImpl _value, $Res Function(_$invoicesInitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$invoicesInitialImpl implements invoicesInitial {
  const _$invoicesInitialImpl();

  @override
  String toString() {
    return 'InvoicesState.invoicesInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$invoicesInitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invoicesInitial,
    required TResult Function(String invoiceId) displayInvoiceId,
    required TResult Function(InvoiceEditDTO invoice) displayInvoice,
  }) {
    return invoicesInitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invoicesInitial,
    TResult? Function(String invoiceId)? displayInvoiceId,
    TResult? Function(InvoiceEditDTO invoice)? displayInvoice,
  }) {
    return invoicesInitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invoicesInitial,
    TResult Function(String invoiceId)? displayInvoiceId,
    TResult Function(InvoiceEditDTO invoice)? displayInvoice,
    required TResult orElse(),
  }) {
    if (invoicesInitial != null) {
      return invoicesInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(invoicesInitial value) invoicesInitial,
    required TResult Function(displayInvoiceId value) displayInvoiceId,
    required TResult Function(displayInvoice value) displayInvoice,
  }) {
    return invoicesInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(invoicesInitial value)? invoicesInitial,
    TResult? Function(displayInvoiceId value)? displayInvoiceId,
    TResult? Function(displayInvoice value)? displayInvoice,
  }) {
    return invoicesInitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(invoicesInitial value)? invoicesInitial,
    TResult Function(displayInvoiceId value)? displayInvoiceId,
    TResult Function(displayInvoice value)? displayInvoice,
    required TResult orElse(),
  }) {
    if (invoicesInitial != null) {
      return invoicesInitial(this);
    }
    return orElse();
  }
}

abstract class invoicesInitial implements InvoicesState {
  const factory invoicesInitial() = _$invoicesInitialImpl;
}

/// @nodoc
abstract class _$$displayInvoiceIdImplCopyWith<$Res> {
  factory _$$displayInvoiceIdImplCopyWith(_$displayInvoiceIdImpl value,
          $Res Function(_$displayInvoiceIdImpl) then) =
      __$$displayInvoiceIdImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String invoiceId});
}

/// @nodoc
class __$$displayInvoiceIdImplCopyWithImpl<$Res>
    extends _$InvoicesStateCopyWithImpl<$Res, _$displayInvoiceIdImpl>
    implements _$$displayInvoiceIdImplCopyWith<$Res> {
  __$$displayInvoiceIdImplCopyWithImpl(_$displayInvoiceIdImpl _value,
      $Res Function(_$displayInvoiceIdImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
  }) {
    return _then(_$displayInvoiceIdImpl(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$displayInvoiceIdImpl implements displayInvoiceId {
  const _$displayInvoiceIdImpl({required this.invoiceId});

  @override
  final String invoiceId;

  @override
  String toString() {
    return 'InvoicesState.displayInvoiceId(invoiceId: $invoiceId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$displayInvoiceIdImpl &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, invoiceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$displayInvoiceIdImplCopyWith<_$displayInvoiceIdImpl> get copyWith =>
      __$$displayInvoiceIdImplCopyWithImpl<_$displayInvoiceIdImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invoicesInitial,
    required TResult Function(String invoiceId) displayInvoiceId,
    required TResult Function(InvoiceEditDTO invoice) displayInvoice,
  }) {
    return displayInvoiceId(invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invoicesInitial,
    TResult? Function(String invoiceId)? displayInvoiceId,
    TResult? Function(InvoiceEditDTO invoice)? displayInvoice,
  }) {
    return displayInvoiceId?.call(invoiceId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invoicesInitial,
    TResult Function(String invoiceId)? displayInvoiceId,
    TResult Function(InvoiceEditDTO invoice)? displayInvoice,
    required TResult orElse(),
  }) {
    if (displayInvoiceId != null) {
      return displayInvoiceId(invoiceId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(invoicesInitial value) invoicesInitial,
    required TResult Function(displayInvoiceId value) displayInvoiceId,
    required TResult Function(displayInvoice value) displayInvoice,
  }) {
    return displayInvoiceId(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(invoicesInitial value)? invoicesInitial,
    TResult? Function(displayInvoiceId value)? displayInvoiceId,
    TResult? Function(displayInvoice value)? displayInvoice,
  }) {
    return displayInvoiceId?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(invoicesInitial value)? invoicesInitial,
    TResult Function(displayInvoiceId value)? displayInvoiceId,
    TResult Function(displayInvoice value)? displayInvoice,
    required TResult orElse(),
  }) {
    if (displayInvoiceId != null) {
      return displayInvoiceId(this);
    }
    return orElse();
  }
}

abstract class displayInvoiceId implements InvoicesState {
  const factory displayInvoiceId({required final String invoiceId}) =
      _$displayInvoiceIdImpl;

  String get invoiceId;
  @JsonKey(ignore: true)
  _$$displayInvoiceIdImplCopyWith<_$displayInvoiceIdImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$displayInvoiceImplCopyWith<$Res> {
  factory _$$displayInvoiceImplCopyWith(_$displayInvoiceImpl value,
          $Res Function(_$displayInvoiceImpl) then) =
      __$$displayInvoiceImplCopyWithImpl<$Res>;
  @useResult
  $Res call({InvoiceEditDTO invoice});
}

/// @nodoc
class __$$displayInvoiceImplCopyWithImpl<$Res>
    extends _$InvoicesStateCopyWithImpl<$Res, _$displayInvoiceImpl>
    implements _$$displayInvoiceImplCopyWith<$Res> {
  __$$displayInvoiceImplCopyWithImpl(
      _$displayInvoiceImpl _value, $Res Function(_$displayInvoiceImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoice = null,
  }) {
    return _then(_$displayInvoiceImpl(
      invoice: null == invoice
          ? _value.invoice
          : invoice // ignore: cast_nullable_to_non_nullable
              as InvoiceEditDTO,
    ));
  }
}

/// @nodoc

class _$displayInvoiceImpl implements displayInvoice {
  const _$displayInvoiceImpl({required this.invoice});

  @override
  final InvoiceEditDTO invoice;

  @override
  String toString() {
    return 'InvoicesState.displayInvoice(invoice: $invoice)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$displayInvoiceImpl &&
            (identical(other.invoice, invoice) || other.invoice == invoice));
  }

  @override
  int get hashCode => Object.hash(runtimeType, invoice);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$displayInvoiceImplCopyWith<_$displayInvoiceImpl> get copyWith =>
      __$$displayInvoiceImplCopyWithImpl<_$displayInvoiceImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invoicesInitial,
    required TResult Function(String invoiceId) displayInvoiceId,
    required TResult Function(InvoiceEditDTO invoice) displayInvoice,
  }) {
    return displayInvoice(invoice);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invoicesInitial,
    TResult? Function(String invoiceId)? displayInvoiceId,
    TResult? Function(InvoiceEditDTO invoice)? displayInvoice,
  }) {
    return displayInvoice?.call(invoice);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invoicesInitial,
    TResult Function(String invoiceId)? displayInvoiceId,
    TResult Function(InvoiceEditDTO invoice)? displayInvoice,
    required TResult orElse(),
  }) {
    if (displayInvoice != null) {
      return displayInvoice(invoice);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(invoicesInitial value) invoicesInitial,
    required TResult Function(displayInvoiceId value) displayInvoiceId,
    required TResult Function(displayInvoice value) displayInvoice,
  }) {
    return displayInvoice(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(invoicesInitial value)? invoicesInitial,
    TResult? Function(displayInvoiceId value)? displayInvoiceId,
    TResult? Function(displayInvoice value)? displayInvoice,
  }) {
    return displayInvoice?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(invoicesInitial value)? invoicesInitial,
    TResult Function(displayInvoiceId value)? displayInvoiceId,
    TResult Function(displayInvoice value)? displayInvoice,
    required TResult orElse(),
  }) {
    if (displayInvoice != null) {
      return displayInvoice(this);
    }
    return orElse();
  }
}

abstract class displayInvoice implements InvoicesState {
  const factory displayInvoice({required final InvoiceEditDTO invoice}) =
      _$displayInvoiceImpl;

  InvoiceEditDTO get invoice;
  @JsonKey(ignore: true)
  _$$displayInvoiceImplCopyWith<_$displayInvoiceImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
