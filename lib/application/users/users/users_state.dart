part of 'users_bloc.dart';

@freezed
class UsersState with _$UsersState {
  const factory UsersState.usersInitial() = usersInitial;
  // const factory UsersState.isUserPermitted(
  //     {required bool isPermitted,
  //     required bool isLoading,
  //     required bool isNotSatrted}) = isUserPermitted;
  const factory UsersState.updateBusinessResult(
      {required bool isLoading,
      required bool isSuccess,
      required bool isError}) = updateBusinessResult;
  const factory UsersState.addTrialDateState(
      {required bool isLoading,
      required bool isSuccess,
      required UserProfileDTO? user,
      required bool isError}) = addTrialDateState;
  const factory UsersState.viewUser(
      {required UserProfileDTO? user,
      required bool isLoading,
      required bool isError}) = viewUser;
  
}
