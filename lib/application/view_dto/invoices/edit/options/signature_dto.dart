import 'dart:io';

import 'package:linq_pe/application/view_dto/transaction/secondary_transaction_dto.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/signature/signature.dart';

class SignatureDTO {
  final String invoiceId;

  final File? signature;
  final DateTime? signDate;

  SignatureDTO(
      {required this.invoiceId,
      required this.signature,
      required this.signDate});
}

Future<SignatureDTO?> convertSignatureModelToDTO(SignatureModel? sign) async {
  if (sign == null) {
    return null;
  }
  return SignatureDTO(
      invoiceId: sign.invoiceId,
      signature: await uint8ListToFile(sign.signature),
      signDate: sign.signDate);
}
