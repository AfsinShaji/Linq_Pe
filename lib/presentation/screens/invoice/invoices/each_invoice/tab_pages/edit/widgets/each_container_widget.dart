import 'package:flutter/material.dart';
import 'package:linq_pe/utilities/colors.dart';

class EditContainerWidget extends StatelessWidget {
  const EditContainerWidget(
      {super.key, required this.child, this.padding, this.alignment});
  final Widget child;
  final double? padding;
  final AlignmentGeometry? alignment;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      alignment: alignment,
      width: size.width,
      padding: EdgeInsets.all(padding ?? size.width * 0.05),
      margin: EdgeInsets.all(size.width * 0.02),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: LinqPeColors.kWhiteColor),
      child: child,
    );
  }
}