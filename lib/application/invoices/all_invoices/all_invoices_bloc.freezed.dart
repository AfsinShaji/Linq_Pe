// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'all_invoices_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AllInvoicesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllInvoicesList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllInvoicesList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllInvoicesList,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getAllInvoicesList value) getAllInvoicesList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getAllInvoicesList value)? getAllInvoicesList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getAllInvoicesList value)? getAllInvoicesList,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AllInvoicesEventCopyWith<$Res> {
  factory $AllInvoicesEventCopyWith(
          AllInvoicesEvent value, $Res Function(AllInvoicesEvent) then) =
      _$AllInvoicesEventCopyWithImpl<$Res, AllInvoicesEvent>;
}

/// @nodoc
class _$AllInvoicesEventCopyWithImpl<$Res, $Val extends AllInvoicesEvent>
    implements $AllInvoicesEventCopyWith<$Res> {
  _$AllInvoicesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$getAllInvoicesListImplCopyWith<$Res> {
  factory _$$getAllInvoicesListImplCopyWith(_$getAllInvoicesListImpl value,
          $Res Function(_$getAllInvoicesListImpl) then) =
      __$$getAllInvoicesListImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$getAllInvoicesListImplCopyWithImpl<$Res>
    extends _$AllInvoicesEventCopyWithImpl<$Res, _$getAllInvoicesListImpl>
    implements _$$getAllInvoicesListImplCopyWith<$Res> {
  __$$getAllInvoicesListImplCopyWithImpl(_$getAllInvoicesListImpl _value,
      $Res Function(_$getAllInvoicesListImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$getAllInvoicesListImpl implements getAllInvoicesList {
  const _$getAllInvoicesListImpl();

  @override
  String toString() {
    return 'AllInvoicesEvent.getAllInvoicesList()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$getAllInvoicesListImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllInvoicesList,
  }) {
    return getAllInvoicesList();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllInvoicesList,
  }) {
    return getAllInvoicesList?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllInvoicesList,
    required TResult orElse(),
  }) {
    if (getAllInvoicesList != null) {
      return getAllInvoicesList();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getAllInvoicesList value) getAllInvoicesList,
  }) {
    return getAllInvoicesList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getAllInvoicesList value)? getAllInvoicesList,
  }) {
    return getAllInvoicesList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getAllInvoicesList value)? getAllInvoicesList,
    required TResult orElse(),
  }) {
    if (getAllInvoicesList != null) {
      return getAllInvoicesList(this);
    }
    return orElse();
  }
}

abstract class getAllInvoicesList implements AllInvoicesEvent {
  const factory getAllInvoicesList() = _$getAllInvoicesListImpl;
}

/// @nodoc
mixin _$AllInvoicesState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() allInvoiceInitial,
    required TResult Function(List<InvoiceEditDTO> invoiceList)
        displayInvoiceList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? allInvoiceInitial,
    TResult? Function(List<InvoiceEditDTO> invoiceList)? displayInvoiceList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? allInvoiceInitial,
    TResult Function(List<InvoiceEditDTO> invoiceList)? displayInvoiceList,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(allInvoiceInitial value) allInvoiceInitial,
    required TResult Function(displayInvoiceList value) displayInvoiceList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(allInvoiceInitial value)? allInvoiceInitial,
    TResult? Function(displayInvoiceList value)? displayInvoiceList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(allInvoiceInitial value)? allInvoiceInitial,
    TResult Function(displayInvoiceList value)? displayInvoiceList,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AllInvoicesStateCopyWith<$Res> {
  factory $AllInvoicesStateCopyWith(
          AllInvoicesState value, $Res Function(AllInvoicesState) then) =
      _$AllInvoicesStateCopyWithImpl<$Res, AllInvoicesState>;
}

/// @nodoc
class _$AllInvoicesStateCopyWithImpl<$Res, $Val extends AllInvoicesState>
    implements $AllInvoicesStateCopyWith<$Res> {
  _$AllInvoicesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$allInvoiceInitialImplCopyWith<$Res> {
  factory _$$allInvoiceInitialImplCopyWith(_$allInvoiceInitialImpl value,
          $Res Function(_$allInvoiceInitialImpl) then) =
      __$$allInvoiceInitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$allInvoiceInitialImplCopyWithImpl<$Res>
    extends _$AllInvoicesStateCopyWithImpl<$Res, _$allInvoiceInitialImpl>
    implements _$$allInvoiceInitialImplCopyWith<$Res> {
  __$$allInvoiceInitialImplCopyWithImpl(_$allInvoiceInitialImpl _value,
      $Res Function(_$allInvoiceInitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$allInvoiceInitialImpl implements allInvoiceInitial {
  const _$allInvoiceInitialImpl();

  @override
  String toString() {
    return 'AllInvoicesState.allInvoiceInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$allInvoiceInitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() allInvoiceInitial,
    required TResult Function(List<InvoiceEditDTO> invoiceList)
        displayInvoiceList,
  }) {
    return allInvoiceInitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? allInvoiceInitial,
    TResult? Function(List<InvoiceEditDTO> invoiceList)? displayInvoiceList,
  }) {
    return allInvoiceInitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? allInvoiceInitial,
    TResult Function(List<InvoiceEditDTO> invoiceList)? displayInvoiceList,
    required TResult orElse(),
  }) {
    if (allInvoiceInitial != null) {
      return allInvoiceInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(allInvoiceInitial value) allInvoiceInitial,
    required TResult Function(displayInvoiceList value) displayInvoiceList,
  }) {
    return allInvoiceInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(allInvoiceInitial value)? allInvoiceInitial,
    TResult? Function(displayInvoiceList value)? displayInvoiceList,
  }) {
    return allInvoiceInitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(allInvoiceInitial value)? allInvoiceInitial,
    TResult Function(displayInvoiceList value)? displayInvoiceList,
    required TResult orElse(),
  }) {
    if (allInvoiceInitial != null) {
      return allInvoiceInitial(this);
    }
    return orElse();
  }
}

abstract class allInvoiceInitial implements AllInvoicesState {
  const factory allInvoiceInitial() = _$allInvoiceInitialImpl;
}

/// @nodoc
abstract class _$$displayInvoiceListImplCopyWith<$Res> {
  factory _$$displayInvoiceListImplCopyWith(_$displayInvoiceListImpl value,
          $Res Function(_$displayInvoiceListImpl) then) =
      __$$displayInvoiceListImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<InvoiceEditDTO> invoiceList});
}

/// @nodoc
class __$$displayInvoiceListImplCopyWithImpl<$Res>
    extends _$AllInvoicesStateCopyWithImpl<$Res, _$displayInvoiceListImpl>
    implements _$$displayInvoiceListImplCopyWith<$Res> {
  __$$displayInvoiceListImplCopyWithImpl(_$displayInvoiceListImpl _value,
      $Res Function(_$displayInvoiceListImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceList = null,
  }) {
    return _then(_$displayInvoiceListImpl(
      invoiceList: null == invoiceList
          ? _value._invoiceList
          : invoiceList // ignore: cast_nullable_to_non_nullable
              as List<InvoiceEditDTO>,
    ));
  }
}

/// @nodoc

class _$displayInvoiceListImpl implements displayInvoiceList {
  const _$displayInvoiceListImpl(
      {required final List<InvoiceEditDTO> invoiceList})
      : _invoiceList = invoiceList;

  final List<InvoiceEditDTO> _invoiceList;
  @override
  List<InvoiceEditDTO> get invoiceList {
    if (_invoiceList is EqualUnmodifiableListView) return _invoiceList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_invoiceList);
  }

  @override
  String toString() {
    return 'AllInvoicesState.displayInvoiceList(invoiceList: $invoiceList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$displayInvoiceListImpl &&
            const DeepCollectionEquality()
                .equals(other._invoiceList, _invoiceList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_invoiceList));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$displayInvoiceListImplCopyWith<_$displayInvoiceListImpl> get copyWith =>
      __$$displayInvoiceListImplCopyWithImpl<_$displayInvoiceListImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() allInvoiceInitial,
    required TResult Function(List<InvoiceEditDTO> invoiceList)
        displayInvoiceList,
  }) {
    return displayInvoiceList(invoiceList);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? allInvoiceInitial,
    TResult? Function(List<InvoiceEditDTO> invoiceList)? displayInvoiceList,
  }) {
    return displayInvoiceList?.call(invoiceList);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? allInvoiceInitial,
    TResult Function(List<InvoiceEditDTO> invoiceList)? displayInvoiceList,
    required TResult orElse(),
  }) {
    if (displayInvoiceList != null) {
      return displayInvoiceList(invoiceList);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(allInvoiceInitial value) allInvoiceInitial,
    required TResult Function(displayInvoiceList value) displayInvoiceList,
  }) {
    return displayInvoiceList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(allInvoiceInitial value)? allInvoiceInitial,
    TResult? Function(displayInvoiceList value)? displayInvoiceList,
  }) {
    return displayInvoiceList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(allInvoiceInitial value)? allInvoiceInitial,
    TResult Function(displayInvoiceList value)? displayInvoiceList,
    required TResult orElse(),
  }) {
    if (displayInvoiceList != null) {
      return displayInvoiceList(this);
    }
    return orElse();
  }
}

abstract class displayInvoiceList implements AllInvoicesState {
  const factory displayInvoiceList(
          {required final List<InvoiceEditDTO> invoiceList}) =
      _$displayInvoiceListImpl;

  List<InvoiceEditDTO> get invoiceList;
  @JsonKey(ignore: true)
  _$$displayInvoiceListImplCopyWith<_$displayInvoiceListImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
