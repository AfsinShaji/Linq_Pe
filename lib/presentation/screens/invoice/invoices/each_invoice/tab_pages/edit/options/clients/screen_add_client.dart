import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/contact/contact_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/options/items/screen_items.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

import '../business_details/screen_business_details.dart';

class AddClientScreen extends StatelessWidget {
  AddClientScreen({super.key, this.contact});
  final ContactsDTO? contact;
  final TextEditingController clientNameTextController =
      TextEditingController();
  final TextEditingController emailTextController = TextEditingController();
  final TextEditingController mobileNumberTextController =
      TextEditingController();
  final TextEditingController phoneNumberTextController =
      TextEditingController();
  final TextEditingController faxTextController = TextEditingController();
  final TextEditingController address1TextController = TextEditingController();
  final TextEditingController address2TextController = TextEditingController();
  final TextEditingController address3TextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.18,
        title: Text('Add Client',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButtonWidget(
          clientNameTextController: clientNameTextController,
          phoneNumberTextController: phoneNumberTextController,
          mobileNumberTextController: mobileNumberTextController,
          faxTextController: faxTextController,
          emailTextController: emailTextController,
          address3TextController: address3TextController,
          address2TextController: address2TextController,
          address1TextController: address1TextController),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            EditContainerWidget(
                child: Column(
              children: [
                DetailsField(
                  initialValue: contact == null ? '' : contact!.displayName,
                  fontSize: size.width * 0.04,
                  height: size.height * 0.05,
                  hintText: 'Client Name',
                  isNumber: false,
                  size: size,
                  textEditingController: clientNameTextController,
                ),
                DetailsField(
                  initialValue: '',
                  fontSize: size.width * 0.04,
                  height: size.height * 0.05,
                  hintText: 'Email',
                  isNumber: false,
                  size: size,
                  textEditingController: emailTextController,
                ),
                DetailFieldRow(
                  initialValue: contact == null ? '' : contact!.contactNumber,
                  size: size,
                  fieldName: 'Mobile',
                  hintText: 'Mobile Number',
                  isNumberField: true,
                  textController: mobileNumberTextController,
                ),
                DetailFieldRow(
                  initialValue: '',
                  size: size,
                  fieldName: 'Phone',
                  hintText: 'Phone Number',
                  isNumberField: true,
                  textController: phoneNumberTextController,
                ),
                DetailFieldRow(
                  initialValue: '',
                  size: size,
                  fieldName: 'Fax',
                  hintText: 'Fax Number',
                  isNumberField: true,
                  textController: faxTextController,
                ),
              ],
            )),
            EditContainerWidget(
                child: Column(
              children: [
                DetailsField(
                  initialValue: '',
                  fontSize: size.width * 0.04,
                  height: size.height * 0.05,
                  hintText: 'Address 1',
                  isNumber: false,
                  size: size,
                  textEditingController: address1TextController,
                ),
                DetailsField(
                  initialValue: '',
                  fontSize: size.width * 0.04,
                  height: size.height * 0.05,
                  hintText: 'Address 2',
                  isNumber: false,
                  size: size,
                  textEditingController: address2TextController,
                ),
                DetailsField(
                  initialValue: '',
                  fontSize: size.width * 0.04,
                  height: size.height * 0.05,
                  hintText: 'Address 3',
                  isNumber: false,
                  size: size,
                  textEditingController: address3TextController,
                ),
              ],
            ))
          ],
        ),
      )),
    );
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget({
    super.key,
    required this.clientNameTextController,
    required this.phoneNumberTextController,
    required this.mobileNumberTextController,
    required this.faxTextController,
    required this.emailTextController,
    required this.address3TextController,
    required this.address2TextController,
    required this.address1TextController,
  });

  final TextEditingController clientNameTextController;
  final TextEditingController phoneNumberTextController;
  final TextEditingController mobileNumberTextController;
  final TextEditingController faxTextController;
  final TextEditingController emailTextController;
  final TextEditingController address3TextController;
  final TextEditingController address2TextController;
  final TextEditingController address1TextController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(
      onTap: () {
        if (clientNameTextController.text.isNotEmpty &&
            ref.watch(invoiceIdProvider).isNotEmpty) {
          log('k');
          BlocProvider.of<InvoicesBloc>(context).add(InvoicesEvent.addsClient(
              invoiceId: ref.watch(invoiceIdProvider),
              phoneNumber: phoneNumberTextController.text,
              mobileNumber: mobileNumberTextController.text,
              faxNumber: faxTextController.text,
              clientName: clientNameTextController.text,
              clientEmail: emailTextController.text,
              address3: address3TextController.text,
              address2: address2TextController.text,
              address1: address1TextController.text));
          Navigator.pop(context);
          Navigator.pop(context);
        }
      },
    );
  }
}
