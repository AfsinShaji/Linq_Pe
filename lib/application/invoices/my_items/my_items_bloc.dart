import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/options/item_dto.dart';
import 'package:linq_pe/domain/models/invoices/edit/options/item/item.dart';
import 'package:linq_pe/infrastructure/invoices/options/my_items/my_items_implementation.dart';

part 'my_items_event.dart';
part 'my_items_state.dart';
part 'my_items_bloc.freezed.dart';

class MyItemsBloc extends Bloc<MyItemsEvent, MyItemsState> {
  MyItemsBloc() : super(const myItemsInitial()) {
    on<addToMyItem>((event, emit) async {
      await MyItemsImplementation.instance.addToMyItems(
          item: ItemModel(
        itemId: event.item.itemId,
        invoiceId: event.item.invoiceId,
        addiotionalDetails: event.item.addiotionalDetails,
        discount: event.item.discount,
        description: event.item.description,
        discountAmount: event.item.discountAmount,
        isTaxable: event.item.isTaxable,
        quantity: event.item.quantity,
        taxRate: event.item.taxRate,
        unit: event.item.unit,
        unitCost: event.item.unitCost,
        total: event.item.total,
      ));
      add(const MyItemsEvent.getAllMyItem());
    });
    on<getAllMyItem>((event, emit) {
      final myItemList = MyItemsImplementation.instance.getAllMyItems();

      emit(displayMyItems(myItems: convertItemModelToDTO(myItemList)!));
    });
  }
}
