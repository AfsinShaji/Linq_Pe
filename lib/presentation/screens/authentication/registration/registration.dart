import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:linq_pe/application/authentication/authentication_bloc.dart';
import 'package:linq_pe/presentation/screens/authentication/registration/widgets/login_option.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/loading_overlay.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/logo_widget.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/sign_button.dart';
import 'package:linq_pe/presentation/screens/authentication/widgets/sign_text_field.dart';
import 'package:linq_pe/presentation/view_state/registration_riverpod.dart/registration.dart';
import 'package:linq_pe/utilities/colors.dart';

class RegistrationScreen extends StatelessWidget {
  RegistrationScreen({super.key});

  final formKey = GlobalKey<FormState>();

  final TextEditingController fullNameTextController = TextEditingController();

  final TextEditingController passwordTextController = TextEditingController();
  final TextEditingController phoneNumberTextController =
      TextEditingController();

  final TextEditingController passwordConfirmTextController =
      TextEditingController();

  final TextEditingController emailTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
   
   final size = MediaQuery.of(context).size;
 final   width = size.width;

    return Scaffold(
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            LinqPeColors.kPinkColor.withOpacity(0.7),
            LinqPeColors.kPinkColor.withOpacity(0.9)
          ],
          // begin: Alignment.topLeft,
          // end: Alignment.bottomRight,
        )),
        child: SafeArea(
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
                    builder: (context, authState) {
                  if (authState is authResults) {
                    if (authState.isLoading) {
                      Future.delayed(Duration.zero, () {
                        showLoadingOverlay(context);
                      });
                    }
                  }
                  return Column(
                    children: [
                      const LogoWidget(),
                      SignTextField(
                        controller: fullNameTextController,
                        text: "Enter Full name",
                        icon: Icons.person_2_outlined,
                        isTextPasswordType: false,
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                      ),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      SignTextField(
                        controller: emailTextController,
                        text: "Enter Email Id",
                        icon: Icons.person_2_outlined,
                        isTextPasswordType: false,
                        isTextEmailType: true,
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                      ),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      SignTextField(
                        isTextNumberType: true,
                        controller: phoneNumberTextController,
                        text: "Enter Phone Number",
                        icon: Icons.phone,
                        isTextPasswordType: false,
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                      ),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      SignTextField(
                        controller: passwordTextController,
                        text: "Enter password",
                        icon: Icons.lock_outline,
                        isTextPasswordType: true,
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                      ),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      SignTextField(
                        controller: passwordConfirmTextController,
                        text: "Re-enter password",
                        icon: Icons.lock_outline,
                        isTextPasswordType: true,
                        isTextPasswordConfirmType: true,
                        enteredPassword: passwordTextController.text,
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      RegisterButtonWidget(
                        formKey: formKey,
                        width: width,
                        email: emailTextController.text,
                        name: fullNameTextController.text,
                        password: passwordTextController.text,
                        phoneNumber: phoneNumberTextController.text,
                      ),
                      const LoginOption()
                    ],
                  );
                }),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class RegisterButtonWidget extends ConsumerWidget {
  const RegisterButtonWidget({
    super.key,
    required this.formKey,
    required this.width,
    required this.name,
    required this.email,
    required this.phoneNumber,
    required this.password,
  });

  final GlobalKey<FormState> formKey;
  final double width;
  final String name;
  final String email;
  final String phoneNumber;
  final String password;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SignButton(
        text: "Register",
        onTap: () {
          if (formKey.currentState!.validate()) {
            addEmail(email, ref);
            addPhoneNumber(phoneNumber, ref);
            addFullName(name, ref);
            BlocProvider.of<AuthenticationBloc>(context).add(
                AuthenticationEvent.registration(
                    name: name,
                    email: email,
                    phoneNumber: phoneNumber,
                    context: context,
                    businessType: '',
                    purpose: '',
                    password: password));
          }
        },
        width: width);
  }
}
