import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linq_pe/application/invoices/invoices_bloc.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/each_container_widget.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/image_picking_sheet.dart';
import 'package:linq_pe/presentation/screens/invoice/invoices/each_invoice/tab_pages/edit/widgets/save_button.dart';
import 'package:linq_pe/presentation/view_state/invoice/edit/business_details.dart';
import 'package:linq_pe/presentation/view_state/invoice/invoice_edit.dart';
import 'package:linq_pe/utilities/colors.dart';

class BusinessDetailsScreen extends StatelessWidget {
  BusinessDetailsScreen({super.key});
  final TextEditingController businessNameTextController =
      TextEditingController();
  final TextEditingController businessOwnerNameTextController =
      TextEditingController();
  final TextEditingController businessNumberTextController =
      TextEditingController();
  final TextEditingController addressLine1TextController =
      TextEditingController();
  final TextEditingController addressLine2TextController =
      TextEditingController();
  final TextEditingController addressLine3TextController =
      TextEditingController();
  final TextEditingController emailTextController = TextEditingController();
  final TextEditingController phoneNumberTextController =
      TextEditingController();
  final TextEditingController mobileNumberTextController =
      TextEditingController();
  final TextEditingController websiteTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: LinqPeColors.kWhiteColor.withOpacity(0.9),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: LinqPeColors.kWhiteColor,
            )),
        titleSpacing: size.width * 0.1,
        title: Text('Business Details',
            style: GoogleFonts.poppins(
              textStyle: TextStyle(
                letterSpacing: .8,
                fontSize: size.width * 0.06,
                color: LinqPeColors.kWhiteColor,
                fontWeight: FontWeight.w500,
              ),
            )),
        backgroundColor: LinqPeColors.kPinkColor,
        shadowColor: LinqPeColors.kBlackColor,
      ),
      bottomSheet: SaveButtonWidget(
          businessNameTextController: businessNameTextController,
          businessOwnerNameTextController: businessOwnerNameTextController,
          businessNumberTextController: businessNumberTextController,
          addressLine1TextController: addressLine1TextController,
          addressLine2TextController: addressLine2TextController,
          addressLine3TextController: addressLine3TextController,
          emailTextController: emailTextController,
          phoneNumberTextController: phoneNumberTextController,
          mobileNumberTextController: mobileNumberTextController,
          websiteTextController: websiteTextController),
      body: SafeArea(
          child: Container(
        padding: EdgeInsets.all(size.width * 0.01),
        width: size.width,
        // height: size.height * 0.1,
        child: SingleChildScrollView(
          child: BlocBuilder<InvoicesBloc, InvoicesState>(
            builder: (context, invoiceState) {
              InvoiceEditDTO? invoice;

              if (invoiceState is displayInvoice) {
                invoice = invoiceState.invoice;
              }
              if (invoice == null) {
                return const SizedBox();
              }
              if (invoice.businessDetails == null) {}
              return Column(
                children: [
                  EditContainerWidget(
                    padding: 0,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: size.width,
                          decoration: BoxDecoration(
                              color: LinqPeColors.kBlackColor.withOpacity(0.5),
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5))),
                          padding: EdgeInsets.symmetric(
                              vertical: size.height * 0.01,
                              horizontal: size.width * 0.05),
                          child: Text('Business Logo',
                              style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                  letterSpacing: .5,
                                  fontSize: size.width * 0.04,
                                  color: LinqPeColors.kWhiteColor,
                                  fontWeight: FontWeight.w600,
                                ),
                              )),
                        ),
                        BusinessLogoWidget(size: size, invoice: invoice),
                      ],
                    ),
                  ),
                  DetailsFieldsContainer(
                    initialValues: [
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.businessName,
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.businessOwnerName ?? '',
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.businessNumber ?? ''
                    ],
                    size: size,
                    hintTextList: const [
                      'Business Name',
                      'Business Owner Name',
                      'Business Number'
                    ],
                    isNumberKeyboard: const [false, false, false],
                    textEditingController1: businessNameTextController,
                    textEditingController2: businessOwnerNameTextController,
                    textEditingController3: businessNumberTextController,
                  ),
                  DetailsFieldsContainer(
                    initialValues: [
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.addressLine1 ?? '',
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.addressLine2 ?? '',
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.addressLine3 ?? '',
                    ],
                    size: size,
                    hintTextList: const [
                      'Address Line 1',
                      'Address Line 2',
                      'Address Line 3'
                    ],
                    isNumberKeyboard: const [false, false, false],
                    textEditingController1: addressLine1TextController,
                    textEditingController2: addressLine2TextController,
                    textEditingController3: addressLine3TextController,
                  ),
                  DetailsFieldsContainer(
                    initialValues: [
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.email ?? '',
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.phone ?? '',
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.mobile ?? '',
                      invoice.businessDetails == null
                          ? ''
                          : invoice.businessDetails!.website ?? '',
                    ],
                    size: size,
                    hintTextList: const ['Email', 'Phone', 'Mobile', 'Website'],
                    isNumberKeyboard: const [false, true, true, false],
                    textEditingController1: emailTextController,
                    textEditingController2: phoneNumberTextController,
                    textEditingController3: mobileNumberTextController,
                    textEditingController4: websiteTextController,
                  ),
                  SizedBox(
                    height: size.height * 0.15,
                  ),
                ],
              );
            },
          ),
        ),
      )),
    );
  }
}

class BusinessLogoWidget extends ConsumerWidget {
  const BusinessLogoWidget({
    super.key,
    required this.size,
    required this.invoice,
  });

  final Size size;
  final InvoiceEditDTO? invoice;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    log('Business${ref.watch(businessDetailsBusinessLogoProvider)}');
    return InkWell(
      onTap: () {
        openImagesPickingSheet(context, size, true,false, ref,null);
      },
      child: SizedBox(
          height: size.height * 0.2,
          width: size.width,
          child: EditContainerWidget(
              child: ref.watch(businessDetailsBusinessLogoProvider).isEmpty
                  ? const SizedBox(
                      child: Center(
                        child: Icon(Icons.photo),
                      ),
                    )
                  : Image.file(
                      File(ref.watch(businessDetailsBusinessLogoProvider))))),
    );
  }
}

class SaveButtonWidget extends ConsumerWidget {
  const SaveButtonWidget({
    super.key,
    required this.businessNameTextController,
    required this.businessOwnerNameTextController,
    required this.businessNumberTextController,
    required this.addressLine1TextController,
    required this.addressLine2TextController,
    required this.addressLine3TextController,
    required this.emailTextController,
    required this.phoneNumberTextController,
    required this.mobileNumberTextController,
    required this.websiteTextController,
  });

  final TextEditingController businessNameTextController;
  final TextEditingController businessOwnerNameTextController;
  final TextEditingController businessNumberTextController;
  final TextEditingController addressLine1TextController;
  final TextEditingController addressLine2TextController;
  final TextEditingController addressLine3TextController;
  final TextEditingController emailTextController;
  final TextEditingController phoneNumberTextController;
  final TextEditingController mobileNumberTextController;
  final TextEditingController websiteTextController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SaveButton(onTap: () {
      if (businessNameTextController.text.isNotEmpty &&
          ref.watch(invoiceIdProvider).isNotEmpty) {
        BlocProvider.of<InvoicesBloc>(context).add(
            InvoicesEvent.addBusinessDetail(
                businessName: businessNameTextController.text,
                invoiceId: ref.watch(invoiceIdProvider),
                businessLogo:
                    ref.watch(businessDetailsBusinessLogoProvider).isEmpty
                        ? null
                        : File(ref.watch(businessDetailsBusinessLogoProvider)),
                businessOwnerName: businessOwnerNameTextController.text,
                businessNumber: businessNumberTextController.text,
                addressLine1: addressLine1TextController.text,
                addressLine2: addressLine2TextController.text,
                addressLine3: addressLine3TextController.text,
                email: emailTextController.text,
                phone: phoneNumberTextController.text,
                mobile: mobileNumberTextController.text,
                website: websiteTextController.text));
        Navigator.pop(context);
      }
    });
  }
}

class DetailsFieldsContainer extends StatelessWidget {
  const DetailsFieldsContainer({
    super.key,
    required this.size,
    this.textEditingController1,
    required this.isNumberKeyboard,
    required this.hintTextList,
    this.textEditingController2,
    this.textEditingController3,
    this.textEditingController4,
    required this.initialValues,
  });

  final Size size;
  final TextEditingController? textEditingController1;
  final List<bool> isNumberKeyboard;
  final List<String> initialValues;
  final List<String> hintTextList;
  final TextEditingController? textEditingController2;
  final TextEditingController? textEditingController3;
  final TextEditingController? textEditingController4;

  @override
  Widget build(BuildContext context) {
    return EditContainerWidget(
        child: Column(
      children: [
        DetailsField(
          initialValue: initialValues[0],
          hintText: hintTextList[0],
          size: size,
          textEditingController: textEditingController1,
          isNumber: isNumberKeyboard[0],
        ),
        DetailsField(
          initialValue: initialValues[1],
          hintText: hintTextList[1],
          size: size,
          textEditingController: textEditingController2,
          isNumber: isNumberKeyboard[1],
        ),
        DetailsField(
          initialValue: initialValues[2],
          hintText: hintTextList[2],
          size: size,
          textEditingController: textEditingController3,
          isNumber: isNumberKeyboard[2],
        ),
        hintTextList.length == 4 && isNumberKeyboard.length == 4
            ? DetailsField(
                initialValue: initialValues[3],
                hintText: hintTextList[3],
                size: size,
                textEditingController: textEditingController4,
                isNumber: isNumberKeyboard[3],
              )
            : const SizedBox()
      ],
    ));
  }
}

class DetailsField extends StatefulWidget {
  const DetailsField(
      {super.key,
      required this.size,
      required this.textEditingController,
      required this.isNumber,
      required this.hintText,
      this.fontSize,
      this.height,
      required this.initialValue});

  final Size size;
  final TextEditingController? textEditingController;
  final bool isNumber;
  final String hintText;
  final double? height;
  final double? fontSize;
  final String initialValue;

  @override
  State<DetailsField> createState() => _DetailsFieldState();
}

class _DetailsFieldState extends State<DetailsField> {
  @override
  void initState() {
    if (widget.initialValue.isNotEmpty) {
      widget.textEditingController!.text = widget.initialValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.size.width,
      height: widget.height ?? widget.size.height * 0.04,
      child: TextField(
        // textAlign: TextAlign.right, // Align text to the right
        // textDirection: TextDirection.ltr,

        keyboardType:
            widget.isNumber ? TextInputType.phone : TextInputType.emailAddress,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: widget.hintText,
            hintStyle: GoogleFonts.roboto(
              textStyle: TextStyle(
                letterSpacing: .5,
                fontSize: widget.fontSize ?? widget.size.width * 0.035,
                color: LinqPeColors.kBlackColor.withOpacity(0.45),
                fontWeight: FontWeight.w600,
              ),
            )),
        controller: widget.textEditingController,
        style: GoogleFonts.roboto(
          textStyle: TextStyle(
            letterSpacing: .5,
            fontSize: widget.fontSize ?? widget.size.width * 0.035,
            color: LinqPeColors.kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
