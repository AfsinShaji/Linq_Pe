import 'package:linq_pe/domain/models/invoices/edit/options/tax/tax.dart';

class TaxDTO {
  final String invoiceId;

  final String label;

  final double? rate;

  final String taxType;
final bool isInclusive;
  TaxDTO({
    required this.invoiceId,
    required this.taxType,
    required this.label,
    required this.rate,required this.isInclusive
  });
}

TaxDTO? convertTaxModelToDTO(TaxModel? tax) {
   if (tax == null) {
    return null;
  }
  return TaxDTO(
    isInclusive: tax.isInclusive,
      invoiceId: tax.invoiceId,
      taxType: tax.taxType,
      label: tax.label,
      rate: tax.rate);
}
