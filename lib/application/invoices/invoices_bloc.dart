import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linq_pe/application/view_dto/invoices/edit/edit_dto/invoice_edit_dto.dart';
import 'package:linq_pe/infrastructure/invoices/invoices_implementation.dart';

part 'invoices_event.dart';
part 'invoices_state.dart';
part 'invoices_bloc.freezed.dart';

class InvoicesBloc extends Bloc<InvoicesEvent, InvoicesState> {
  InvoicesBloc() : super(const invoicesInitial()) {
    on<createInvoicesNumber>((event, emit) async {
      final invoiceId = await InvoicesImplementation.instance
          .createInvoiceNumber(invoiceNumber: event.invoiceNumber);
      // emit(displayInvoiceId(invoiceId: invoiceId));
      add(getInvoicesDetails(invoiceId: invoiceId));
      // add(const getAllInvoicesList());
    });
    on<createEstimatesNumber>((event, emit) async {
      final invoiceId = await InvoicesImplementation.instance
          .createEstimateNumber(estimateNumber: event.estimateNumber);
      // emit(displayInvoiceId(invoiceId: invoiceId));
      add(getInvoicesDetails(invoiceId: invoiceId));
      // add(const getAllInvoicesList());
    });
    on<getInvoicesDetails>((event, emit) async {
      final invoiceModel = InvoicesImplementation.instance
          .getInvoiceDetails(invoiceId: event.invoiceId);
      final invoiceDTO = await convertInvoiceEditModelToDTO(invoiceModel);
      emit(displayInvoice(invoice: invoiceDTO));
    });
    // on<getAllInvoicesList>((event, emit) async {
    //   final invoiceModelList =
    //       InvoicesImplementation.instance.getAllInvoiceList();
    //   List<InvoiceEditDTO> invoiceList = [];
    //   for (var invoiceModel in invoiceModelList) {
    //     invoiceList.add(convertInvoiceEditModelToDTO(invoiceModel));
    //   }

    //   emit(displayInvoiceList(invoiceList: invoiceList));
    // });
    on<addInvoicesNumber>((event, emit) async {
      await InvoicesImplementation.instance.addInvoiceNumber(
          invoiceNumber: event.invoiceNumber,
          invoiceId: event.invoiceId,
          date: event.date,
          terms: event.terms,
          dueDate: event.dueDate,
          poNumber: event.poNumber);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addBusinessDetail>((event, emit) async {
      await InvoicesImplementation.instance.addBusinessDetails(
          businessName: event.businessName,
          businessLogo: event.businessLogo,
          businessOwnerName: event.businessOwnerName,
          businessNumber: event.businessNumber,
          addressLine1: event.addressLine1,
          addressLine2: event.addressLine2,
          addressLine3: event.addressLine3,
          invoiceId: event.invoiceId,
          email: event.email,
          phone: event.phone,
          mobile: event.mobile,
          website: event.website);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addDiscountDetail>((event, emit) async {
      await InvoicesImplementation.instance.addDiscountDetails(
          invoiceId: event.invoiceId,
          discountType: event.discountType,
          percentage: event.percentage);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addItem>((event, emit) async {
      await InvoicesImplementation.instance.addItems(
          total: event.total,
          invoiceId: event.invoiceId,
          unitCost: event.unitCost,
          unit: event.unit,
          taxRate: event.taxRate,
          quantity: event.quantity,
          isTaxable: event.isTaxable,
          discountAmount: event.discountAmount,
          description: event.description,
          discount: event.discount,
          addiotionalDetails: event.addiotionalDetails,
          isAddToMyItems: event.isAddToMyItems);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addNote>((event, emit) async {
      await InvoicesImplementation.instance.addNotes(
          notes: event.notes,
          invoiceId: event.invoiceId,
          isDefault: event.isDefault);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addPayment>((event, emit) async {
      await InvoicesImplementation.instance.addPayments(
          invoiceId: event.invoiceId,
          amount: event.amount,
          date: event.date,
          notes: event.notes,
          paymentMethod: event.paymentMethod);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addPaymentsInfo>((event, emit) async {
      await InvoicesImplementation.instance.addPaymentInfo(
          additionalPaymentInstructions: event.additionalPaymentInstructions,
          businessName: event.businessName,
          paymentInstructions: event.paymentInstructions,
          paypalEmail: event.paypalEmail,
          invoiceId: event.invoiceId);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addTaxDetail>((event, emit) async {
      await InvoicesImplementation.instance.addTaxDetails(
          isInclusive: event.isInclusive,
          invoiceId: event.invoiceId,
          rate: event.rate,
          label: event.label,
          taxType: event.taxType);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addsClient>((event, emit) async {
      await InvoicesImplementation.instance.addClient(
          invoiceId: event.invoiceId,
          phoneNumber: event.phoneNumber,
          mobileNumber: event.mobileNumber,
          faxNumber: event.faxNumber,
          clientName: event.clientName,
          clientEmail: event.clientEmail,
          address3: event.address3,
          address2: event.address2,
          address1: event.address1);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addsPhoto>((event, emit) async {
      await InvoicesImplementation.instance.addPhoto(
          invoiceId: event.invoiceId,
          photo: event.photo,
          description: event.description,
          addionalDetails: event.addionalDetails);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addsSignature>((event, emit) async {
      await InvoicesImplementation.instance
          .addSignature(invoiceId: event.invoiceId, signature: event.signature);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<editItem>((event, emit) async {
      await InvoicesImplementation.instance.editItems(
          itemId: event.itemId,
          total: event.total,
          invoiceId: event.invoiceId,
          unitCost: event.unitCost,
          unit: event.unit,
          taxRate: event.taxRate,
          quantity: event.quantity,
          isTaxable: event.isTaxable,
          discountAmount: event.discountAmount,
          description: event.description,
          discount: event.discount,
          addiotionalDetails: event.addiotionalDetails,
          isAddToMyItems: event.isAddToMyItems);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<editPayment>((event, emit) async {
      await InvoicesImplementation.instance.editPayments(
          paymentId: event.paymentId,
          invoiceId: event.invoiceId,
          amount: event.amount,
          date: event.date,
          notes: event.notes,
          paymentMethod: event.paymentMethod);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<editsPhoto>((event, emit) async {
      await InvoicesImplementation.instance.editPhoto(
          photoId: event.photoId,
          invoiceId: event.invoiceId,
          photo: event.photo,
          description: event.description,
          addionalDetails: event.addionalDetails);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addReviews>((event, emit) async {
      await InvoicesImplementation.instance.addReview(
          invoiceId: event.invoiceId,
          isToReview: event.isToReview,
          reviewLink: event.reviewLink);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });

    on<deletePhotos>((event, emit) async {
      await InvoicesImplementation.instance.deletePhoto(
        photoId: event.photoId,
        invoiceId: event.invoiceId,
      );
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<deleteItem>((event, emit) async {
      await InvoicesImplementation.instance.deleteItems(
        itemId: event.itemId,
        invoiceId: event.invoiceId,
      );
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<deletePayment>((event, emit) async {
      await InvoicesImplementation.instance.deletePayments(
        paymentId: event.paymentId,
        invoiceId: event.invoiceId,
      );
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<markAsPaidOrUnPaid>((event, emit) async {
      await InvoicesImplementation.instance.marKPaidOrUnPaid(
          invoiceId: event.invoiceId,
          isPaid: event.isPaid,
          paymentMethod: event.paymentMethod);
      add(getInvoicesDetails(invoiceId: event.invoiceId));
    });
    on<addBalanceDues>((event, emit) async {
      await InvoicesImplementation.instance.addBalanceDue(
          invoiceId: event.invoiceId, balanceDue: event.balanceDue);
    });

      on<convertingEstimateToInvoice>((event, emit) async {
      await InvoicesImplementation.instance.convertEstimateToInvoice(
          invoiceId: event.invoiceId);
    });
  }
}
